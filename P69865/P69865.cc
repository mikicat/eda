#include <iostream>
#include <vector>
using namespace std;
typedef vector<int> VI;
typedef vector<VI> Board;
int TRAP = -1;
int BISHOP = -2;
int KNIGHT = -3;

void dfs_rec(Board& B, int i, int j, int& coins, int type, vector< vector<bool> >& visited) {
  if (B[i][j] == TRAP) return;
  if (not visited[i][j]) {
    visited[i][j] = true;
    if (B[i][j] > 0) {
      coins += B[i][j];
      B[i][j] = 0;
    }
    if (type == BISHOP) {
      if (B[i][j] == BISHOP) B[i][j] = 0;
      if (i+1 < B.size() and j+1 < B[0].size()) dfs_rec(B, i+1, j+1, coins, type, visited);
      if (i+1 < B.size() and j-1 >= 0) dfs_rec(B, i+1, j-1, coins, type, visited);
      if (i-1 >= 0 and j+1 < B[0].size()) dfs_rec(B, i-1, j+1, coins, type, visited);
      if (i-1 >= 0 and j-1 >= 0) dfs_rec(B, i-1, j-1, coins, type, visited);
    }
    else if (type == KNIGHT) {
      if (B[i][j] == KNIGHT) B[i][j] = 0;
      for (int k = -2; k <= 2; k += 2) {
        if (k != 0) {
          if (i+k >= 0 and i+k < B.size()) {
            if (j-1 >= 0) dfs_rec(B, i+k, j-1, coins, type, visited);
            if (j+1 < B[0].size()) dfs_rec(B, i+k, j+1, coins, type, visited);
          }
          if (j+k >= 0 and j+k < B[0].size()) {
            if (i-1 >= 0) dfs_rec(B, i-1, j+k, coins, type, visited);
            if (i+1 < B.size()) dfs_rec(B, i+1, j+k, coins, type, visited);
          }
        }
      }
    }
  }
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    int total_sum = 0;
    Board B(n, VI(m));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        char c;
        cin >> c;
        if (c == '.') B[i][j] = 0;
        else if (c == 'T') B[i][j] = TRAP;
        else if (c == 'B') B[i][j] = BISHOP;
        else if (c == 'K') B[i][j] = KNIGHT;
        else {
          B[i][j] = c-'0';
          total_sum += c-'0';
        }
      }
    }

    int coins = 0;
    for (int i = 0; i < n and coins < total_sum; ++i) {
      for (int j = 0; j < m and coins < total_sum; ++j) {
        if (B[i][j] == BISHOP or B[i][j] == KNIGHT) {
          vector< vector<bool> > visited(n, vector<bool>(m, false));
          dfs_rec(B, i, j, coins, B[i][j], visited);
        }
      }
    }
    cout << coins << endl;
  }
}
