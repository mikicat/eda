#include <iostream>
#include <vector>
using namespace std;
vector<char> vowels;
vector<char> consonants;
vector<bool> vowels_used;
vector<bool> consonants_used;

void combinations(vector<char>& paraula, int i) {
  if (i == paraula.size()) {
    for (int k = 0; k < paraula.size(); ++k) {
      cout << paraula[k];
    }
    cout << endl;
  }
}

int main() {
  int n;
  cin >> n;

  vowels = vector<char>(n);
  consonants = vector<char>(n);
  vowels_used = vector<bool>(n);
  consonants_used = vector<bool>(n);

  for (int i = 0; i < n; ++i) cin >> consonants[i];
  for (int i = 0; i < n; ++i) cin >> vowels[i];
  vector<char> paraula(2*n);
  combinations(paraula, 0);
}
