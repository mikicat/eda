#include <iostream>
#include <string>
#include <map>

using namespace std;

int main() {
  string name;
  map<string, int> results;
  while (cin >> name) {
    string op;
    cin >> op;
    if (op == "enters") {
      if (results.find(name) != results.end()) cout << name << " is already in the casino" << endl;
      else results[name] = 0;
    }
    else if (op == "wins") {
      int qty;
      cin >> qty;
      if (results.find(name) == results.end()) cout << name << " is not in the casino" << endl;
      else results[name] += qty;
    }
    else {
      if (results.find(name) == results.end()) cout << name << " is not in the casino" << endl;
      else {
        cout << name << " has won " << results[name] << endl;
        results.erase(results.find(name));
      }
    }
  }
  cout << "----------" << endl;
  for (map<string, int>::iterator it = results.begin(); it != results.end(); ++it) {
    cout << it->first << " is winning " << it->second << endl;
  }
}
