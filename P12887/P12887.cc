#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef pair<int, int> P;
typedef vector< vector<P> > Graph;

int mst(const Graph& G) {
  vector<bool> visitats(G.size(), false);
  priority_queue<P, vector<P>, greater<P> > q;
  int tree_weight = 0;
  visitats[0] = true;
  for (P adj : G[0]) q.push(adj);
  int size = 1;

  while (size < G.size()) {
    P u = q.top();
    q.pop();
    if (not visitats[u.second]) {
      visitats[u.second] = true;
      ++size;
      tree_weight += u.first;
      for (P adj : G[u.second]) q.push(adj);
    }
  }
  return tree_weight;
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    Graph G(n);
    for (int i = 0; i < m; ++i) {
      int u, v, w;
      cin >> u >> v >> w;
      --u, --v;
      G[u].push_back(make_pair(w, v)); // PRIMER EL PES (Criteri despres per la priority queue)
      G[v].push_back(make_pair(w, u)); // S'han de fer els 2 pq és un graf unidireccional
    }
    cout << mst(G) << endl;

  }
}
