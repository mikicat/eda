#include <iostream>
using namespace std;

int exp(int n, int k, int m) {
  if (k == 0) return 1;
  int x = exp(n, k/2, m);
  if (k%2 == 0) return (x*x)%m;
  else return ((x*n) % m * x) % m;
}

int main() {
  int n, k, m;
  while (cin >> n >> k >> m) {
    cout << exp(n, k, m) << endl;
  }
}
