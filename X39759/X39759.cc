// INACABAT - NO FUNCIONA
#include <iostream>
#include <vector>
#include <limits>
#include <queue>
using namespace std;
typedef pair<int, int> PI;
typedef vector< vector<int> > matrix;

const int INF = numeric_limits<int>::max();

class KnightGame {
  int n, m; // Mides taulell
  int W; // Points won each time knight gets to a goal
  int L; // Points lost for each move
  vector<PI> dirs = {{1,2},{1,-2},{-1,2},{-1,-2},{2,1},{-2,1},{2,-1},{-2,-1}};
  int best_sol;
  int partial_sol;
  vector<PI> goals; // List of goals

  bool inside(const PI& x) {
    return x.first >= 0 and x.first < n and x.second >= 0 and x.second < m;
  }

  int bfs(const PI& ini, const PI& fi) {
    matrix dist(n, vector<int>(m, INF));
    queue<PI> q;
    dist[ini.first][ini.second] = 0;
    q.push(ini);

    while (not q.empty()) {
      PI x = q.front();
      q.pop();
      if (x == fi) dist[x.first][x.second];
      for (auto& d: dirs) {
        PI y = x;
        y.first += d.first;
        y.second += d.second;
        if (inside(y) and dist[y.first][y.second] == INF) {
          dist[y.first][y.second] = dist[x.first][x.second]+1;
          q.push(y);
        }
      }
    }
    return -1;
  }

public:
  KnightGame(int n, int m, int W, int L, vector<PI> goals) {
    this->n = n;
    this->m = m;
    this->W = W;
    this->L = L;
    this->goals = goals;
    best_sol = 0;
    partial_sol = 0;
    bool stop = false;
    for (int i = 0; i < goals.size()-1 and not stop; ++i) {
      int dist = bfs(goals[i], goals[i+1]);
      if (dist == -1) stop = true;
      else {
        partial_sol += W;
        partial_sol -= L*dist;
        if (partial_sol > best_sol) best_sol = partial_sol;
      }
    }
    cout << best_sol << endl;
  }

};

int main() {
  int n, m;
  while (cin >> n >> m) {
    int W, L, k;
    cin >> W >> L >> k;
    vector<PI> goals(k+1);
    goals[0] = {0, 0};
    for (int i = 1; i <= k; ++i) {
      int x, y;
      cin >> x >> y;
      goals[i] = {x, y};
    }
    KnightGame game(n, m, W, L, goals);
  }
}
