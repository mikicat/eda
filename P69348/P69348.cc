#include <iostream>
#include <vector>
using namespace std;

void permutations(vector<int>& v, int i, vector<bool>& usat) {
  if (i == v.size()) {
    cout << '(';
    for (int k = 0; k < v.size(); ++k) {
      if (k > 0) cout << ',';
      cout << v[k];
    }
    cout << ')' << endl;
  }
  else {
    for (int j = 0; j < v.size(); ++j) {
      if (not usat[j]) {
        v[i] = j+1;
        usat[j] = true;
        permutations(v, i+1, usat);
        usat[j] = false;
      }
    }
  }
}

int main() {
  int n;
  cin >> n;
  vector<int> v(n);
  vector<bool> usat(n, false);
  permutations(v, 0, usat);
}
