#include <iostream>
#include <vector>
using namespace std;
typedef vector< vector<char> > Graph;
typedef vector< vector<bool> > RefG;

void DFS(const Graph& G, int x, int y, RefG& visitats, int& count)
{
  if (not visitats[x][y]) {
    visitats[x][y] = true;
    if (G[x][y] == 't') ++count;
    if (G[x][y] != 'X') {
      if (y != G[0].size()-1) DFS(G, x, y+1, visitats, count);
      if (x != G.size()-1) DFS(G, x+1, y, visitats, count);
      if (y != 0) DFS(G, x, y-1, visitats, count);
      if (x != 0) DFS(G, x-1, y, visitats, count);
    }
  }
}

int main() {
  int n, m;
  cin >> n >> m;
  Graph G(n, vector<char>(m));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> G[i][j];
    }
  }
  int x, y;
  cin >> x >> y;
  RefG visitats(n, vector<bool>(m, false));
  int count = 0;
  DFS(G, x-1, y-1, visitats, count);

  cout << count << endl;

}
