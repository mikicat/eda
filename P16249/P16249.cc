#include <iostream>
#include <queue>
#include <vector>
#include <map>
using namespace std;

void topological_sort(map<string, int>& num, map<int, string>& str, vector < vector<int> >& graf, vector<int>& apuntat) {
  priority_queue<string, vector<string>, greater<string> > Q;
  for (map<string, int>::const_iterator it = num.begin(); it != num.end(); ++it) {
    if (apuntat[it->second] == 0) Q.push(it->first);
  }
  queue<string> aux;
  while (!Q.empty()) {
    string u = Q.top();
    Q.pop();
    aux.push(u);
    for (int v : graf[num[u]]) {
      if (--apuntat[v] == 0) Q.push(str[v]);
    }
  }
  if (aux.size() != graf.size()) cout << "NO VALID ORDERING" << endl;
  else {
    while (not aux.empty()) {
      cout << aux.front();
      aux.pop();
    }
    cout << endl;
  }
}

int main() {
  int n;
  while (cin >> n) {
    map <string, int> num;
    map <int, string> str;
    for (int i = 0; i < n; ++i) {
      string entry;
      cin >> entry;
      num.insert(make_pair(entry, i));
      str.insert(make_pair(i, entry));
    }
    int m;
    cin >> m;
    vector<int> apuntat(n, 0);
    vector <vector<int> > graf(n);
    for (int i = 0; i < m; ++i) {
      string u, v;
      cin >> u >> v;
      graf[num[u]].push_back(num[v]);
      apuntat[num[v]]++;
    }
    topological_sort(num, str, graf, apuntat);

  }
}
