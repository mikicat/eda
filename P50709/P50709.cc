#include <iostream>
#include <queue>
using namespace std;

int main() {
  char c;
  priority_queue<int> q;
  while (cin >> c) {
    if (c == 'S') {
      int x;
      cin >> x;
      q.push(x);
    }
    else if (c == 'A') {
      if (q.empty()) cout << "error!" << endl;
      else cout << q.top() << endl;
    }
    else if (c == 'R') {
      if (q.empty()) cout << "error!" << endl;
      else q.pop();
    }
    else if (c == 'I') {
      if (q.empty()) cout << "error!" << endl;
      else {
        int great = q.top();
        q.pop();
        int x;
        cin >> x;
        great += x;
        q.push(great);
      }
    }
    else if (c == 'D') {
      if (q.empty()) cout << "error!" << endl;
      else {
        int great = q.top();
        q.pop();
        int x;
        cin >> x;
        great -= x;
        q.push(great);
      }
    }
  }
}
