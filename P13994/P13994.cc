#include <iostream>
#include <vector>
#include <queue>
#include <limits>
#include <stack>
using namespace std;
typedef vector< vector< pair<int, int> > > Graph;

const int INF = numeric_limits<int>::max();

void dijkstra(const Graph& G, int x, int y) {
  vector<bool> visitats(G.size(), false);
  vector<int> dist(G.size(), INF);
  vector<int> parent(G.size(), -1);
  priority_queue<pair<int, int>, vector< pair<int, int> >, greater< pair<int, int> > > Q;
  dist[x] = 0;
  Q.push({dist[x], x}); // IMPORTANT QUE SIGUI AQUEST ORDRE (dist, vertex)

  while (not Q.empty() and Q.top().second != y) {
    int u = Q.top().second;
    Q.pop();
    if (not visitats[u]) {
      visitats[u] = true;
      for (pair<int, int> aux : G[u]) {
        int v = aux.first;
        int w = aux.second;
        if (dist[v] > dist[u] + w) {
          dist[v] = dist[u] + w;
          parent[v] = u;
          Q.push({dist[v], v});
        }
      }
    }
  }
  if (Q.empty()) cout << "no path from " << x << " to " << y << endl;
  else {
    stack<int> path;
    while (parent[y] != -1) {
      path.push(y);
      y = parent[y];
    }
    path.push(y);
    cout << path.top();
    path.pop();
    while (not path.empty()) {
      cout << ' ' << path.top();
      path.pop();
    }
    cout << endl;
  }
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    Graph G(n);
    for (int i = 0; i < m; ++i) {
      int x, y, c;
      cin >> x >> y >> c;
      G[x].push_back(make_pair(y,c));
    }
    int x, y;
    cin >> x >> y;
    dijkstra(G, x, y);
  }
}
