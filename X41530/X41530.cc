#include <iostream>
#include <vector>
using namespace std;

void find_cycles(const vector< vector<int> >& adj, vector<bool>& visitats, int i, int pare, bool& b) {
  if (!visitats[i]) {
    visitats[i] = true;
    for (int aresta: adj[i]) {
      if (aresta != pare) find_cycles(adj, visitats, aresta, i, b);
      if (b) return;
    }
  } else b = true;
}

void DFS(const vector <vector <int> >& adj)
{
  vector<bool> visitats(adj.size(), false);
  int arbres = 0;
  for (int i = 0; i < adj.size(); ++i) {
    if (!visitats[i]) {
      bool b = false;
      find_cycles(adj, visitats, i, i, b);
      if (b) {
        cout << "no" << endl;
        return;
      } else ++arbres;
    }
  }
  cout << arbres << endl;
}

int main()
{
  int n, m;
  while (cin >> n >> m) {
    int x, y;
    vector <vector <int> > adj(n);
    for (int i = 0; i < m; ++i) {
      cin >> x >> y;
      adj[x].push_back(y);
      adj[y].push_back(x);
    }
    DFS(adj);
  }
}
