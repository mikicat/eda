#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector< vector<char> > Graph;

int bfs(const Graph& G, int x, int y) {
  vector< vector<bool> > visitats(G.size(), vector<bool>(G[0].size(), false));
  queue<pair<pair<int, int>, int>> Q;
  Q.push(make_pair(make_pair(x, y), 0));
  while (not Q.empty()) {
    pair<pair<int,int>, int> u = Q.front();
    Q.pop();
    int i = u.first.first;
    int j = u.first.second;
    int d = u.second;
    if (G[i][j] == 't') return d;
    else if (not visitats[i][j]) {
      visitats[i][j] = true;
      if (i > 0 and G[i][j] != 'X') Q.push(make_pair(make_pair(i-1,j), d+1));
      if (j > 0 and G[i][j] != 'X') Q.push(make_pair(make_pair(i,j-1), d+1));
      if (i < G.size()-1 and G[i][j] != 'X') Q.push(make_pair(make_pair(i+1,j), d+1));
      if (j < G[i].size()-1 and G[i][j] != 'X') Q.push(make_pair(make_pair(i,j+1), d+1));
    }
  }
  return -1;
}

int main() {
  int n, m;
  cin >> n >> m;
  Graph G(n, vector<char>(m));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) cin >> G[i][j];
  }
  int x, y;
  cin >> x >> y;
  --x, --y;
  int d = bfs(G, x, y);
  if (d < 0) cout << "no treasure can be reached";
  else cout << "minimum distance: " << d;
  cout << endl;
}
