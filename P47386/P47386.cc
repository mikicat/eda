#include <iostream>
#include <map>
#include <set>
#include <string>
using namespace std;

int main() {
  map<string, string> couples;
  set<string> singles;
  string op;
  while (cin >> op) {
    if (op == "affair") {
      string p1, p2;
      cin >> p1 >> p2;
      if (couples.find(p1) != couples.end()) {
        singles.insert(couples[p1]);
        couples.erase(couples[p1]);
      }
      else if (singles.find(p1) != singles.end()) singles.erase(p1);

      if (couples.find(p2) != couples.end()) {
        singles.insert(couples[p2]);
        couples.erase(couples[p2]);
      }
      else if (singles.find(p2) != singles.end()) singles.erase(p2);

      couples[p1] = p2;
      couples[p2] = p1;

    }
    else if (op == "info") {
      cout << "COUPLES:" << endl;
      set<string> visited;
      for (map<string, string>::iterator it = couples.begin(); it != couples.end(); ++it) {
        if (visited.insert(it->first).second) {
          visited.insert(it->second);
          cout << it->first << ' ' << it->second << endl;
        }
      }
      cout << "ALONE:" << endl;
      for (set<string>::iterator it = singles.begin(); it != singles.end(); ++it) {
        cout << *it << endl;
      }
      cout << "----------" << endl;
    }
  }
}
