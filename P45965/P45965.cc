#include <iostream>
#include <vector>
using namespace std;

void combinations (vector<bool>& v, int n, int o, int z) {
  if (z+o == 0) {
    for (int i = 0; i < v.size(); ++i) {
      if (i > 0) cout << ' ';
      cout << v[i];
    }
    cout << endl;
  }
  else {
    if (z > 0) {
      v[n] = false;
      combinations(v, n+1, o, z-1);
    }
    if (o > 0) {
      v[n] = true;
      combinations(v, n+1, o-1, z);
    }
  }
}

int main() {
  int n, o;
  cin >> n >> o;
  vector<bool> v(n);
  combinations(v, 0, o, n-o);
}
