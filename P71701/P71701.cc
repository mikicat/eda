#include <iostream>
#include <vector>
using namespace std;
typedef vector<bool> VB;
typedef vector<VB> Board;

class NKings {
  int n;
  int k;
  Board S; // Current setup

  inline bool check(int i, int j) {
    for (int m = -1; m < 2; ++m) {
      if (i+m >= 0 and i+m < n) {
        for (int l = -1; l < 2; ++l) {
          if (j+l >= 0 and j+l < n) {
            if (S[i+m][j+l]) return false;
          }
        }
      }
    }
    return true;
  }

  void solve(int c, int x, int y) {
    if (c == k) write();
    else {
      bool first = true;
      for (int i = x; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
          if (first) {
            first = false;
            j = y;
          }
          if (check(i, j)) {
            S[i][j] = true;
            solve(c+1, i, j);
            S[i][j] = false;
          }
        }
      }
    }
  }

  void write() {
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if (S[i][j]) cout << 'K';
        else cout << '.';
      }
      cout << endl;
    }
    cout << "----------" << endl;
  }

public:
  NKings(int n, int k) {
    this->n = n;
    this->k = k;
    S = Board(n, VB(n, false));
    solve(0, 0, 0);
  }

};

int main() {
  int n, k;
  cin >> n >> k;
  NKings r(n, k);
}
