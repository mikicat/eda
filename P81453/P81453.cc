#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector<int> VI;
typedef vector<VI> Graph;

void write(VI path) {
    VI reconstructed_path;
    int n = path.size();
    int v = n-1;
    while (v != 0) {
        reconstructed_path.push_back(v);
        v = path[v];
    }
    reconstructed_path.push_back(0);
    for (int i = reconstructed_path.size()-1; i >= 0; --i) {
        if (i != reconstructed_path.size()-1) cout << ' ';
        cout << reconstructed_path[i];
    }
    cout << endl;
}

void bfs(const Graph& G) {
    VI parent(G.size(), -1);
    vector<bool> visited(G.size(), false);
    queue<int> q;
    q.push(0);
    while (not q.empty()) {
        int u = q.front();
        q.pop();
        if (u == G.size()-1) {
            write(parent);
            return;
        }
        if (not visited[u]) {
            visited[u] = true;
            for (int v : G[u]) {
                if (not visited[v]) {
                    parent[v] = u;
                    q.push(v);
                }
            }
        }
    }
}

int main() {
    int n, m;
    while (cin >> n >> m) {
        Graph G(n);
        for (int i = 0; i < m; ++i) {
            int x, y;
            cin >> x >> y;
            G[x].push_back(y);
        }
        bfs(G);
    }
}
