#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector<int> VI;
typedef vector<VI> Graph;

bool is_cyclic (const Graph& G, VI& indegree) {
  int n = G.size();
  queue<int> q;
  for (int u = 0; u < n; ++u) {
    if (indegree[u] == 0) q.push(u);
  }
  while (not q.empty()) {
    int u = q.front();
    q.pop();
    --n;
    for (int v: G[u]) {
      --indegree[v];
      if (indegree[v] == 0) q.push(v);
    }
  }
  return n > 0;
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    Graph G(n);
    VI indegree(n, 0);
    for (int i = 0; i < m; ++i) {
      int x, y;
      cin >> x >> y;
      G[x].push_back(y);
      ++indegree[y];
    }
    bool cyclic = is_cyclic(G, indegree);

    if (cyclic) cout << "yes";
    else cout << "no";
    cout << endl;
  }
}
