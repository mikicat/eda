#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector<char> VC;
typedef vector<bool> VB;
typedef vector<VC> CBoard;
typedef vector<VB> BBoard;

bool check(const CBoard& b, int x, int y) {
  if (x < 0 or x >= b.size() or y < 0 or y >= b[0].size()) return false;
  if (b[x][y] == 'X') return false;
  for (int k = -1; k <= 1; ++k) {
    if (x+k >= 0 and x+k < b.size()) {
      for (int l = -1; l <= 1; ++l) {
        if (y+l >= 0 and y+l < b[0].size()) {
          if (b[x+k][y+l] == 'F') return false;
        }
      }
    }
  }
  return true;
}

bool bfs(const CBoard& b, int x, int y) {
  BBoard visited(b.size(), VB(b[0].size(), false));
  queue<pair<int, int> > q;
  q.push({x, y});
  while (not q.empty()) {
    pair<int, int> p = q.front();
    q.pop();
    if (b[p.first][p.second] == 'B') return true;
    if (not visited[p.first][p.second]) {
      int i = p.first;
      int j = p.second;
      visited[i][j] = true;
      for (int k = -1; k <= 1; k+=2) {
        if (check(b, i+k, j)) q.push({i+k, j});
        if (check(b, i, j+k)) q.push({i, j+k});
      }
    }
  }
  return false;
}

int main() {
  int r, c;
  while (cin >> r >> c) {
    int x, y;
    CBoard b(r, VC(c));
    for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
        cin >> b[i][j];
        if (b[i][j] == 'P') x = i, y = j;
      }
    }
    if (check(b, x, y) and bfs(b, x, y)) cout << "yes";
    else cout << "no";
    cout << endl;
  }
}
