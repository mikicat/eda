#include <iostream>
#include <vector>
using namespace std;
vector<bool> row, col, diag1, diag2;

int n_queens(vector< vector<bool> >& reines, int i) {
  if (i == reines.size()) return 1;
  int sols = 0;
  for (int j = 0; j < reines.size(); ++j) {
    if (reines[i][j] and row[i] and col[j] and diag1[i+j] and diag2[i-j+reines.size()]) {
      reines[i][j] = false;
      row[i] = false;
      col[j] = false;
      diag1[i+j] = false;
      diag2[i-j+reines.size()] = false;
      sols += n_queens(reines, i+1);
      reines[i][j] = true;
      row[i] = true;
      col[j] = true;
      diag1[i+j] = true;
      diag2[i-j+reines.size()] = true;
    }
  }
  return sols;
}

int main() {
  int n;
  cin >> n;
  vector< vector<bool> > reines(n, vector<bool>(n, true));
  row = vector<bool>(n, true);
  col = vector<bool>(n, true);
  diag1 = vector<bool>(2*n-1, true);
  diag2 = vector<bool>(2*n-1, true);
  cout << n_queens(reines, 0) << endl;
}
