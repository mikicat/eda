#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector< vector<int> > Board;

int bishop(Board& g, int x, int y, vector< vector<bool> >& visitats) {
  queue< pair<int, int> > q;
  q.push({x, y});
  int cells = 0;
  int bishops = 0;
  while (not q.empty()) {
    pair<int, int> pos = q.front();
    int i = pos.first;
    int j = pos.second;
    q.pop();
    if (not visitats[i][j] and g[i][j] >= 0) {
      visitats[i][j] = true;
      ++cells;
      bishops += g[i][j];

      if (i+1 < g.size() and j+1 < g[0].size()) q.push({i+1, j+1});
      if (i-1 >= 0 and j-1 >= 0) q.push({i-1, j-1});
      if (i-1 >= 0 and j+1 < g[0].size()) q.push({i-1, j+1});
      if (i+1 < g.size() and j-1 >= 0) q.push({i+1, j-1});
    }
  }
  if (bishops % cells == 0) return bishops/cells;
  else return -1;
}

int main() {
  int t;
  cin >> t;
  for (int i = 0; i < t; ++i) {
    int n, m;
    cin >> n >> m;

    Board g(n, vector<int>(m));
    for (int j = 0; j < n; ++j) {
      for (int k = 0; k < m; ++k) {
        string el;
        cin >> el;
        if (el == "X") g[j][k] = -1;
        else g[j][k] = stoi(el);
      }
    }

    vector< vector<bool> > visitats(n, vector<bool>(m, false));
    int mean = -2;
    for (int x = 0; x < n and mean != -1; ++x) {
      for (int y = 0; y < m and mean != -1; ++y) {
        if (g[x][y] >= 0 and not visitats[x][y]) {
          int r = bishop(g, x, y, visitats);
          if (mean >= 0 and mean != r) mean = -1;
          else mean = r;
        }
      }
    }

    cout << "Case " << i+1 << ": ";
    (mean != -1) ? cout << "yes" : cout << "no";
    cout << endl;
  }
}
