#include <queue>
#include <set>
#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Baboon


struct PLAYER_NAME : public Player {

  /**
   * Factory: returns a new instance of this class.
   * Do not modify this function.
   */

  static Player* factory () {
    return new PLAYER_NAME;
  }

  /**
   * Types and attributes for your player can be defined here.
   */
  const vector<Dir> Movs = {Down, Right, Up, Left}; // Vector de direccions
  struct Node_dist {
    Dir first;
    Pos second;
    int d;
  };

  // Opposite direction
  inline Dir opposite(Pos ini, Dir d) {
    vector<Dir> Opps = {Up, Left, Down, Right};
    if (pos_ok(ini+Opps[int(d)]) and (cell(ini+Opps[int(d)]).type == Street) and (cell(ini+Opps[int(d)]).id < 0) and ((cell(ini+Opps[int(d)]).b_owner < 0) or (cell(ini+Opps[int(d)]).b_owner == me()))) return Opps[int(d)];
    else {
      for (int i = 0; i < 4; ++i) {
        if (i != int(d) and i != (int(d)+2)%4) {
          if(pos_ok(ini+Opps[i]) and (cell(ini+Opps[i]).type == Street) and (cell(ini+Opps[i]).id < 0) and ((cell(ini+Opps[i]).b_owner < 0) or (cell(ini+Opps[i]).b_owner == me()))) return Opps[i];
        }
      }
      return Up; // Mai hauria d'arribar aqui
    }
  }

  // Find citizens
  Dir attack(int id) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();

      Cell actual = cell(u.second);
      if (actual.weapon != NoWeapon) {
        if (thisCitizen.weapon == Hammer) return u.first;
        if (thisCitizen.weapon == Gun and actual.weapon == Bazooka) return u.first;
      }
      if (actual.id >= 0 and citizen(actual.id).player != me()) {
        WeaponType myWeapon = thisCitizen.weapon;
        WeaponType targetWeapon = citizen(actual.id).weapon;
        if (targetWeapon == NoWeapon) return u.first;
        if (myWeapon == Hammer and targetWeapon == Hammer) return u.first;
        else if (myWeapon == Gun and (targetWeapon == Hammer or targetWeapon == Gun)) return u.first;
        else if (myWeapon == Bazooka) return u.first;
        else return avoid(id);
      }
      if (actual.bonus == Food and thisCitizen.life < warrior_ini_life()) return u.first;
      if (actual.bonus == Money) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if (((_new.b_owner < 0) or (_new.b_owner == me() and _new.id < 0))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return random_pos(dir_ini);
  }

  // Find weapons
  Dir heal(int id, bool lowhp, Dir dont) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (((not lowhp) or (lowhp and x != dont)) and pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();

      Cell actual = cell(u.second);
      if (actual.weapon != NoWeapon) {
        if (thisCitizen.weapon == Hammer) return u.first;
        if (thisCitizen.weapon == Gun and actual.weapon == Bazooka) return u.first;
      }
      if (actual.bonus == Food and thisCitizen.life < warrior_ini_life()) return u.first;
      if (actual.bonus == Money) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    if (not lowhp) return random_pos(dir_ini);
    else return opposite(dir_ini, dont);
  }

  // Find money
  Dir bfs_money(int id) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();
      Cell actual = cell(u.second);
      if (actual.bonus == Money) return u.first;
      if (actual.bonus == Food and thisCitizen.life < builder_ini_life()) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return random_pos(dir_ini);
  }

  // Find money versió nit
  Dir rat(int id, Dir d) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (x != d and pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();
      if (not check_stuck(u.second)) {
        Cell actual = cell(u.second);
        if (actual.bonus == Money) return u.first;
        if (actual.bonus == Food and thisCitizen.life < builder_ini_life()) return u.first;
        for (Dir x:Movs) {
          if (visitats.insert(u.second+x).second) {
            if (pos_ok(u.second+x) and not is_citizen_nearby(u.second+x)) {
              Cell _new = cell(u.second+x);
              if (_new.type == Street) {
                if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                  pendents.push({u.first, u.second+x});
                }
              }
            }
          }
        }
      }
    }
    return opposite(dir_ini, d);
  }


  // Find a barricade of your own player
  Dir bfs_barricade(int id, Dir d) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (x != d and pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();
      Cell actual = cell(u.second);
      if (actual.b_owner == me() and actual.id < 0) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return opposite(dir_ini, d);
  }

  // Find hostile citizens
  pair<Dir,int> bfs_nearest_enemy_citizen(Pos dir_ini) {
    queue<Node_dist> pendents;
    set<Pos> visitats;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if (cell(dir_ini+x).b_owner != me()) {
          Node_dist info;
          info.first = x, info.second = dir_ini+x, info.d = 1;
          pendents.push(info);
        }
      }
    }
    while (not pendents.empty()) {
      Node_dist u = pendents.front();
      pendents.pop();
      Cell actual = cell(u.second);
      if (actual.id > 0 and citizen(actual.id).player != me()) {
        return {u.first, u.d};
      }
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if (_new.b_owner < 0) {
                Node_dist _new_n;
                _new_n.first = u.first, _new_n.second = u.second+x, _new_n.d = u.d+1;
                pendents.push(_new_n);
              }
            }
          }
        }
      }
    }
    return {random_pos(dir_ini), -1};
  }

  inline bool is_citizen_nearby(Pos p) {
    pair<Dir, int> near_enemy = bfs_nearest_enemy_citizen(p);
    if (near_enemy.second > 0 and near_enemy.second < 3) return true;
    return false;
  }

  // If there are other citizens nearby, move in the opposite direction
  inline Dir avoid(int id) {
    Citizen thisCitizen = citizen(id);
    pair<Dir, int> near_enemy = bfs_nearest_enemy_citizen(citizen(id).pos);
    if (near_enemy.second > 0 and near_enemy.second < 4) {
      if (thisCitizen.type == Builder) return rat(id, near_enemy.first);
      else return heal(id, true, near_enemy.first);
    }
    if (thisCitizen.type == Builder) return bfs_money(id);
    else return heal(id, false, Up);
  }

  // Checks if citizen is inside a barricade
  inline bool in_barricade(int id) {
    if (cell(citizen(id).pos).b_owner == me()) return true;
    return false;
  }

  inline bool will_be_blocked(Pos p) {
    int blocked = 0;
    for (Dir d:Movs) {
      if (not pos_ok(p+d) or (cell(p+d).type == Building)) {
        Dir op = Movs[(int(d)+2)%4];
        if (not pos_ok(p+op) or (cell(p+op).type == Building)) return true;
      };
    }
    return false;
  }

  // IMPLEMENTAR: VIGILAR QUE NO ES QUEDI ATRAPAT ENTRE ATZUCAC I ENEMIC A L'ANAR A BUSCAR MONEDES/ARMES
  bool check_stuck(Pos dir_ini) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) {
        if ((cell(dir_ini+x).b_owner < 0) or (cell(dir_ini+x).b_owner == me() and cell(dir_ini+x).id < 0)) pendents.push({x, dir_ini+x});
      }
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();
      int obs = 0;
      for (Dir d:Movs) {
        if (not pos_ok((u.second)+d) or (cell((u.second)+d).type == Building) or (cell((u.second)+d).b_owner > 0 and cell((u.second)+d).b_owner != me())) ++obs;
      }
      if (obs > 2) return true;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                obs = 0;
                for (Dir d:Movs) {
                  if (not pos_ok((u.second+x)+d) or (cell((u.second)+d).type == Building) or (cell((u.second+x)+d).b_owner > 0 and cell((u.second+x)+d).b_owner != me())) ++obs;
                }
                if (obs < 2) return false;
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return true;
  }

  // Localitza la barricada del voltant (si n'hi ha)
  inline pair<Dir, bool> locate_barricade(Pos p) {
    for (Dir x:Movs) {
      if (pos_ok(p+x) and cell(p+x).b_owner == me()) return {x, true};
    }
    return {Up, false}; // Si no hi ha barricada disponible per entrar-hi
  }

  // Return a valid random direction
  inline Dir random_pos(Pos ini) {
    vector<int> perms = random_permutation(4);
    for (int p:perms) {
      Dir _new_d = Movs[p];
      if (pos_ok(ini+_new_d) and (cell(ini+_new_d).type == Street) and (cell(ini+_new_d).id < 0) and ((cell(ini+_new_d).b_owner < 0) or (cell(ini+_new_d).b_owner == me()))) return _new_d;
    }
    return Up; // Mai hauria d'arribar aqui
  }

  /**
   * Play method, invoked once per each round.
   */
  virtual void play () {
    vector<int> b = builders(me());
    for (int i:b) {
      if (is_day()) {
        if (barricades(me()).size() < max_num_barricades()) {
          Dir r = random_pos(citizen(i).pos);
          if (not in_barricade(i) and not locate_barricade(citizen(i).pos).second and not locate_barricade(citizen(i).pos+r).second) {
            if (will_be_blocked(citizen(i).pos+r)) build(i, r);
            else move(i, bfs_money(i));
          }
          else if (round()%(num_rounds_per_day()/2) == num_rounds_per_day()/2 - 2) build(i, r);
          else move(i, bfs_money(i));
        }
        else move(i, bfs_money(i));
      }
      else {
        if (in_barricade(i)) {
          if (not is_citizen_nearby(citizen(i).pos)) move(i, bfs_money(i));
        }
        else move(i, avoid(i));
      }
    }
    vector<int> w = warriors(me());
    for (int i:w) {
      if (is_day()) move (i, heal(i, false, Up));
      else {
        if (citizen(i).life >= warrior_ini_life()/4) move(i, attack(i));
        else move(i, avoid(i));
      }
    }
  }
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
