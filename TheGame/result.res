Game

Seed 24066

ThePurge 1.0

NUM_PLAYERS	4
NUM_DAYS	5
NUM_ROUNDS_PER_DAY	50
BOARD_ROWS	15
BOARD_COLS	30
NUM_INI_BUILDERS	4
NUM_INI_WARRIORS	2
NUM_INI_MONEY	10
NUM_INI_FOOD	5
NUM_INI_GUNS	4
NUM_INI_BAZOOKAS	2
BUILDER_INI_LIFE	60
WARRIOR_INI_LIFE	100
MONEY_POINTS	5
KILL_BUILDER_POINTS	100
KILL_WARRIOR_POINTS	250
FOOD_INCR_LIFE	20
LIFE_LOST_IN_ATTACK	20
BUILDER_STRENGTH_ATTACK	1
HAMMER_STRENGTH_ATTACK	10
GUN_STRENGTH_ATTACK	100
BAZOOKA_STRENGTH_ATTACK	1000
BUILDER_STRENGTH_DEMOLISH	10
HAMMER_STRENGTH_DEMOLISH	10
GUN_STRENGTH_DEMOLISH	10
BAZOOKA_STRENGTH_DEMOLISH	30
NUM_ROUNDS_REGEN_BUILDER	50
NUM_ROUNDS_REGEN_WARRIOR	50
NUM_ROUNDS_REGEN_FOOD	10
NUM_ROUNDS_REGEN_MONEY	5
NUM_ROUNDS_REGEN_WEAPON	40
BARRICADE_RESISTANCE_STEP	40
BARRICADE_MAX_RESISTANCE	320
MAX_NUM_BARRICADES	3
names          R5D8 DennisRodman Mao JonyBeltranx


   000000000011111111112222222222
   012345678901234567890123456789
00 ......W..M...............WZ...
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.C...CM.B..FB..........G....
03 .BBBBBBBBBB...BC......C.......
04 ..C........M..B...............
05 .C......BBB..WB....M....F...C.
06 .....C..B.B...B...............
07 ........B.B...BWBBBBBB..C.W...
08 .......BB.B...BMB......M.F....
09 .....B..C.B.....BBBBBBBB......
10 .BBB.BBB..B..C.F.......B.Z....
11 .B.B...BW.....C........B....C.
12 .B...C.B......FC....M..B.GM...
13 .BBBBBBB......C..........W..W.
14 ......M......M......G.....G...

citizens
24
type	id	player	row	column	weapon	life
b	0	0	11	28	n	60
b	1	0	2	3	n	60
b	2	0	10	13	n	60
b	3	0	12	15	n	60
w	4	0	13	25	h	100
w	5	0	0	6	h	100
b	6	1	9	8	n	60
b	7	1	4	2	n	60
b	8	1	5	1	n	60
b	9	1	3	22	n	60
w	10	1	7	26	h	100
w	11	1	5	13	h	100
b	12	2	3	15	n	60
b	13	2	7	24	n	60
b	14	2	11	14	n	60
b	15	2	12	5	n	60
w	16	2	7	15	h	100
w	17	2	11	8	h	100
b	18	3	2	7	n	60
b	19	3	13	14	n	60
b	20	3	6	5	n	60
b	21	3	5	28	n	60
w	22	3	0	25	h	100
w	23	3	13	28	h	100

barricades
0
player	row	column	resistance

round 0
day 1

score	0	0	0	0

status	0	0	0	0

commands
22
18	b	l	
6	m	d	
7	m	r	
19	m	d	
12	m	d	
8	m	u	
1	m	r	
20	m	u	
9	m	l	
4	m	u	
10	m	l	
3	m	l	
21	m	d	
13	m	d	
15	m	u	
22	m	r	
11	m	d	
2	m	r	
5	m	r	
16	m	d	
17	m	d	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .......W.M................W...
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B..C.bCM.B..FB..........G....
03 .BBBBBBBBBB...B......C........
04 .C.C.......M..BC..............
05 .....C..BBB...B....M....F.....
06 ........B.B..WB.............C.
07 ........B.B...B.BBBBBB...W....
08 .......BB.B...BWB......MCF....
09 .....B....B.....BBBBBBBB......
10 .BBB.BBBC.B...CF.......B.Z....
11 .B.B.C.B......C........B......
12 .B.....BW.....C.....M..B.WM.C.
13 .BBBBBBB....................W.
14 ......M......MC.....G.....G...

citizens
24
type	id	player	row	column	weapon	life
b	0	0	12	28	n	60
b	1	0	2	4	n	60
b	2	0	10	14	n	60
b	3	0	12	14	n	60
w	4	0	12	25	g	100
w	5	0	0	7	h	100
b	6	1	10	8	n	60
b	7	1	4	3	n	60
b	8	1	4	1	n	60
b	9	1	3	21	n	60
w	10	1	7	25	h	100
w	11	1	6	13	h	100
b	12	2	4	15	n	60
b	13	2	8	24	n	60
b	14	2	11	14	n	60
b	15	2	11	5	n	60
w	16	2	8	15	h	100
w	17	2	12	8	h	100
b	18	3	2	7	n	60
b	19	3	14	14	n	60
b	20	3	5	5	n	60
b	21	3	6	28	n	60
w	22	3	0	26	b	100
w	23	3	13	28	h	100

barricades
1
player	row	column	resistance
3	2	6	40

round 1
day 1

score	0	0	5	0

status	0	0	0	0

commands
24
12	m	d	
6	m	d	
1	b	r	
13	m	l	
18	m	r	
7	m	r	
8	m	d	
3	m	r	
9	m	l	
10	m	d	
5	m	r	
11	m	d	
19	m	l	
20	m	u	
14	m	d	
4	m	r	
21	m	l	
15	m	l	
22	m	d	
23	m	d	
2	m	r	
16	m	u	
0	m	l	
17	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........WM....................
01 .B....BBBBB...BBBBBBBBBBB.W...
02 .B..Cbb.C.B..FB..........G....
03 .BBBBBBBBBB...B.....C.........
04 ....CC.....M..B...............
05 .C......BBB...BC...M....F.....
06 ........B.B...B............C..
07 ........B.B..WBWBBBBBB........
08 .......BB.B...B.B......C.W....
09 .....B....B.....BBBBBBBB......
10 .BBB.BBB..B....C.......B.Z....
11 .B.BC..BC..............B......
12 .B.....B......CC....M..B..WC..
13 .BBBBBBBW.....................
14 ......M......C......G.....G.W.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	12	27	n	60
b	1	0	2	4	n	60
b	2	0	10	15	n	60
b	3	0	12	15	n	60
w	4	0	12	26	g	100
w	5	0	0	8	h	100
b	6	1	11	8	n	60
b	7	1	4	4	n	60
b	8	1	5	1	n	60
b	9	1	3	20	n	60
w	10	1	8	25	h	100
w	11	1	7	13	h	100
b	12	2	5	15	n	60
b	13	2	8	23	n	60
b	14	2	12	14	n	60
b	15	2	11	4	n	60
w	16	2	7	15	h	100
w	17	2	13	8	h	100
b	18	3	2	8	n	60
b	19	3	14	13	n	60
b	20	3	4	5	n	60
b	21	3	6	27	n	60
w	22	3	1	26	b	100
w	23	3	14	28	h	100

barricades
2
player	row	column	resistance
0	2	5	40
3	2	6	40

round 2
day 1

score	5	0	10	10

status	0	0	0	0

commands
22
6	m	d	
12	m	r	
2	b	u	
7	m	d	
19	m	l	
8	m	d	
20	m	r	
21	m	u	
9	m	l	
13	m	u	
10	m	d	
11	m	d	
15	b	u	
3	m	u	
22	m	d	
16	m	u	
17	m	d	
23	m	l	
1	m	u	
5	m	r	
4	m	d	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .........W....................
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B..FB..........GW...
03 .BBBBBBBBBB...B....C..........
04 ......C....M..B...............
05 ....C...BBB...B.C..M....F..C..
06 .C......B.B...BW..............
07 ........B.B...B.BBBBBB.C......
08 .......BB.B..WB.B.............
09 .....B....B....bBBBBBBBB.W....
10 .BBBbBBB..B....C.......B.Z....
11 .B.BC..B.......C.......B......
12 .B.....BC.....C.....M..B......
13 .BBBBBBB..................WC..
14 ......M.W...C.......G.....GW..

citizens
24
type	id	player	row	column	weapon	life
b	0	0	13	27	n	60
b	1	0	1	4	n	60
b	2	0	10	15	n	60
b	3	0	11	15	n	60
w	4	0	13	26	g	100
w	5	0	0	9	h	100
b	6	1	12	8	n	60
b	7	1	5	4	n	60
b	8	1	6	1	n	60
b	9	1	3	19	n	60
w	10	1	9	25	h	100
w	11	1	8	13	h	100
b	12	2	5	16	n	60
b	13	2	7	23	n	60
b	14	2	12	14	n	60
b	15	2	11	4	n	60
w	16	2	6	15	h	100
w	17	2	14	8	h	100
b	18	3	2	8	n	60
b	19	3	14	12	n	60
b	20	3	4	6	n	60
b	21	3	5	27	n	60
w	22	3	2	26	b	100
w	23	3	14	27	h	100

barricades
4
player	row	column	resistance
0	2	5	40
3	2	6	40
0	9	15	40
2	10	4	40

round 3
day 1

score	10	0	10	10

status	0	0	0	0

commands
22
19	m	u	
12	m	r	
6	m	u	
20	m	r	
21	m	u	
7	m	r	
13	m	u	
1	m	u	
14	m	r	
4	m	d	
5	m	r	
8	m	d	
15	m	u	
9	m	d	
10	m	d	
3	m	r	
2	m	l	
16	m	r	
0	m	u	
11	m	d	
22	m	d	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C.....W...................
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B..FB..........G....
03 .BBBBBBBBBB...B...........W...
04 .......C...M..B....C.......C..
05 .....C..BBB...B..C.M....F.....
06 ........B.B...B.W......C......
07 .C......B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 .....B....B..W.bBBBBBBBB......
10 .BBBcBBB..B...C........B.W....
11 .B.B...BC.......C......B......
12 .B.....B.......C....M..B...C..
13 .BBBBBBB....C.................
14 ......MW............G.....WW..

citizens
24
type	id	player	row	column	weapon	life
b	0	0	12	27	n	60
b	1	0	0	4	n	60
b	2	0	10	14	n	60
b	3	0	11	16	n	60
w	4	0	14	26	g	100
w	5	0	0	10	h	100
b	6	1	11	8	n	60
b	7	1	5	5	n	60
b	8	1	7	1	n	60
b	9	1	4	19	n	60
w	10	1	10	25	b	100
w	11	1	9	13	h	100
b	12	2	5	17	n	60
b	13	2	6	23	n	60
b	14	2	12	15	n	60
b	15	2	10	4	n	60
w	16	2	6	16	h	100
w	17	2	14	7	h	100
b	18	3	2	8	n	60
b	19	3	13	12	n	60
b	20	3	4	7	n	60
b	21	3	4	27	n	60
w	22	3	3	26	b	100
w	23	3	14	27	h	100

barricades
4
player	row	column	resistance
0	2	5	40
3	2	6	40
0	9	15	40
2	10	4	40

round 4
day 1

score	10	0	10	10

status	0	0	0	0

commands
23
6	m	u	
7	m	d	
19	m	r	
8	m	d	
20	b	r	
12	m	r	
1	b	r	
13	m	u	
2	m	u	
5	m	r	
21	m	u	
4	m	u	
3	m	r	
22	m	d	
0	m	u	
14	m	r	
23	m	u	
9	m	d	
15	m	u	
10	m	d	
11	m	d	
16	m	r	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....Cb.....W..................
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B..FB..........G....
03 .BBBBBBBBBB...B............C..
04 .......Cb..M..B...........W...
05 ........BBB...B...CC...CF.....
06 .....C..B.B...B..W............
07 ........B.B...B.BBBBBB........
08 .C.....BB.B...B.B.............
09 ....CB....B...CbBBBBBBBB......
10 .BBBbBBBC.B..W.........B......
11 .B.B...B.........C....MB.W.C..
12 .B.....B........C...M..B......
13 .BBBBBBB.....C............WW..
14 ......W.............G.........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	11	27	n	60
b	1	0	0	4	n	60
b	2	0	9	14	n	60
b	3	0	11	17	n	60
w	4	0	13	26	g	100
w	5	0	0	11	h	100
b	6	1	10	8	n	60
b	7	1	6	5	n	60
b	8	1	8	1	n	60
b	9	1	5	19	n	60
w	10	1	11	25	b	100
w	11	1	10	13	h	100
b	12	2	5	18	n	60
b	13	2	5	23	n	60
b	14	2	12	16	n	60
b	15	2	9	4	n	60
w	16	2	6	17	h	100
w	17	2	14	6	h	100
b	18	3	2	8	n	60
b	19	3	13	13	n	60
b	20	3	4	7	n	60
b	21	3	3	27	n	60
w	22	3	4	26	b	100
w	23	3	13	27	h	100

barricades
6
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
0	9	15	40
2	10	4	40

round 5
day 1

score	10	5	15	10

status	0	0	0	0

commands
22
1	m	d	
19	m	r	
12	m	d	
13	m	d	
20	m	r	
14	m	r	
15	m	u	
0	m	r	
21	m	u	
2	m	l	
6	m	d	
7	m	d	
22	m	d	
8	m	r	
17	m	r	
23	m	d	
5	m	d	
9	m	u	
10	m	r	
4	m	l	
11	m	d	
3	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b..............M.........
01 .B..C.BBBBBW..BBBBBBBBBBB.....
02 .B...bb.C.B..FB..........G.C..
03 .BBBBBBBBBB...B.M.............
04 ........c..M..B....C..........
05 ........BBB.M.B.........F.W...
06 ........B.B..MB..WC....C......
07 .....C..B.B...B.BBBBBB........
08 ..C.C..BB.B...B.B.............
09 .....B....B..C.bBBBBBBBB......
10 .BBBbBBB..B............B......
11 .B.B...BC....W....C...MB..W.C.
12 .B.....B.........C..M..B......
13 .BBBBBBB......C..........W....
14 .......W............G......W..

citizens
24
type	id	player	row	column	weapon	life
b	0	0	11	28	n	60
b	1	0	1	4	n	60
b	2	0	9	13	n	60
b	3	0	11	18	n	60
w	4	0	13	25	g	100
w	5	0	1	11	h	100
b	6	1	11	8	n	60
b	7	1	7	5	n	60
b	8	1	8	2	n	60
b	9	1	4	19	n	60
w	10	1	11	26	b	100
w	11	1	11	13	h	100
b	12	2	6	18	n	60
b	13	2	6	23	n	60
b	14	2	12	17	n	60
b	15	2	8	4	n	60
w	16	2	6	17	h	100
w	17	2	14	7	h	100
b	18	3	2	8	n	60
b	19	3	13	14	n	60
b	20	3	4	8	n	60
b	21	3	2	27	n	60
w	22	3	5	26	b	100
w	23	3	14	27	h	100

barricades
6
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
0	9	15	40
2	10	4	40

round 6
day 1

score	10	5	15	10

status	0	0	0	0

commands
22
6	m	r	
12	m	u	
5	m	d	
13	m	u	
14	m	r	
15	m	r	
16	m	u	
17	m	r	
19	m	r	
8	m	u	
9	m	u	
20	m	r	
4	m	l	
21	m	l	
22	m	d	
3	m	r	
23	m	l	
10	m	r	
2	m	u	
11	m	d	
1	m	u	
0	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....Cb..............M.M.......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.BW.FB..........GC...
03 .BBBBBBBBBB...B.M..C..........
04 ........bC.M..B...............
05 ........BBB.M.B..WC....CF.....
06 ........B.B..MB...........W...
07 ..C..C..B.B...B.BBBBBB........
08 .....C.BB.B..CB.B.............
09 .....B....B....bBBBBBBBB......
10 .BBBbBBB..B............B....C.
11 .B.B...B.C.........C..MB...W..
12 .B.....B.....W....C.M..B......
13 .BBBBBBB.......C........W.....
14 ........W...........G.....W...

citizens
24
type	id	player	row	column	weapon	life
b	0	0	10	28	n	60
b	1	0	0	4	n	60
b	2	0	8	13	n	60
b	3	0	11	19	n	60
w	4	0	13	24	g	100
w	5	0	2	11	h	100
b	6	1	11	9	n	60
b	7	1	7	5	n	60
b	8	1	7	2	n	60
b	9	1	3	19	n	60
w	10	1	11	27	b	100
w	11	1	12	13	h	100
b	12	2	5	18	n	60
b	13	2	5	23	n	60
b	14	2	12	18	n	60
b	15	2	8	5	n	60
w	16	2	5	17	h	100
w	17	2	14	8	h	100
b	18	3	2	8	n	60
b	19	3	13	15	n	60
b	20	3	4	9	n	60
b	21	3	2	26	n	60
w	22	3	6	26	b	100
w	23	3	14	26	h	100

barricades
6
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
0	9	15	40
2	10	4	40

round 7
day 1

score	10	5	15	10

status	0	0	0	0

commands
23
5	m	r	
3	m	r	
6	m	r	
19	m	r	
20	m	r	
7	m	r	
21	m	l	
8	m	u	
9	m	l	
2	m	u	
22	m	u	
23	m	l	
10	m	u	
11	m	d	
12	m	u	
13	m	u	
14	m	r	
15	m	r	
4	m	l	
16	m	u	
1	m	r	
17	m	r	
0	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....c..............M.M.......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B.WFB..........C....
03 .BBBBBBBBBB...B.M.C...........
04 ........b.CM..B..WC....C......
05 ........BBB.M.B.........F.W...
06 ..C.....B.B..MB...............
07 ......C.B.B..CB.BBBBBB........
08 ......CBB.B...B.B.............
09 .....B....B....bBBBBBBBB....C.
10 .BBBbBBB..B............B...W..
11 .B.B...B..C.........C.MB......
12 .B.....B...........CM..B......
13 .BBBBBBB.....W..C......W......
14 .........W..........G....W....

citizens
24
type	id	player	row	column	weapon	life
b	0	0	9	28	n	60
b	1	0	0	5	n	60
b	2	0	7	13	n	60
b	3	0	11	20	n	60
w	4	0	13	23	g	100
w	5	0	2	12	h	100
b	6	1	11	10	n	60
b	7	1	7	6	n	60
b	8	1	6	2	n	60
b	9	1	3	18	n	60
w	10	1	10	27	b	100
w	11	1	13	13	h	100
b	12	2	4	18	n	60
b	13	2	4	23	n	60
b	14	2	12	19	n	60
b	15	2	8	6	n	60
w	16	2	4	17	h	100
w	17	2	14	9	h	100
b	18	3	2	8	n	60
b	19	3	13	16	n	60
b	20	3	4	10	n	60
b	21	3	2	25	n	60
w	22	3	5	26	b	100
w	23	3	14	25	h	100

barricades
6
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
0	9	15	40
2	10	4	40

round 8
day 1

score	10	5	15	10

status	0	0	0	0

commands
20
19	m	r	
5	m	r	
6	m	r	
7	m	l	
20	m	r	
21	m	u	
22	m	u	
23	m	l	
3	m	d	
13	m	d	
8	m	l	
2	m	u	
9	m	l	
15	m	d	
4	m	l	
1	m	r	
0	m	u	
17	m	r	
10	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....bC.............M.M.......
01 .B....BBBBB...BBBBBBBBBBBC..M.
02 .B.M.bb.C.B..WB...............
03 .BBBBBBBBBB...B.MC............
04 ........b..C..B..WC.......W...
05 ........BBB.M.B........CF.....
06 .C......B.B..CB...............
07 .....C..B.B...B.BBBBBB........
08 .......BB.B...B.B...........C.
09 .....BC...B....bBBBBBBBB...W..
10 .BBBbBBB..B............B......
11 .B.B...B...C..........MB......
12 .B.....B...........CC..B......
13 .BBBBBBB.........C....W.......
14 ..........W..W......G...W.....

citizens
24
type	id	player	row	column	weapon	life
b	0	0	8	28	n	60
b	1	0	0	6	n	60
b	2	0	6	13	n	60
b	3	0	12	20	n	60
w	4	0	13	22	g	100
w	5	0	2	13	h	100
b	6	1	11	11	n	60
b	7	1	7	5	n	60
b	8	1	6	1	n	60
b	9	1	3	17	n	60
w	10	1	9	27	b	100
w	11	1	14	13	h	100
b	12	2	4	18	n	60
b	13	2	5	23	n	60
b	14	2	12	19	n	60
b	15	2	9	6	n	60
w	16	2	4	17	h	100
w	17	2	14	10	h	100
b	18	3	2	8	n	60
b	19	3	13	17	n	60
b	20	3	4	11	n	60
b	21	3	1	25	n	60
w	22	3	4	26	b	100
w	23	3	14	24	h	100

barricades
6
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
0	9	15	40
2	10	4	40

round 9
day 1

score	20	5	15	15

status	0	0	0	0

commands
20
4	m	u	
19	m	u	
20	m	d	
6	m	u	
21	m	r	
7	m	u	
13	m	r	
8	m	l	
3	m	d	
15	b	r	
22	m	d	
23	m	l	
2	m	u	
5	m	u	
17	m	r	
1	m	l	
0	m	u	
9	m	l	
10	m	u	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....c..............M.M.......
01 .B....BBBBB..WBBBBBBBBBBB.C.M.
02 .B.M.bb.C.B...B...............
03 .BBBBBBBBBB...B.C.............
04 ........b.....B..WC...........
05 ........BBBCMCB.........C.W...
06 C....C..B.B...B...............
07 ........B.B...B.BBBBBB......C.
08 .......BB.B...B.B..........W..
09 .....BCb..B...FbBBBBBBBB......
10 .BBBbBBB..BC...........B......
11 .B.B...B..............MB......
12 .B.....B.........C.C..WB......
13 .BBBBBBB............C.........
14 ...........W..W.....G..W......

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	28	n	60
b	1	0	0	5	n	60
b	2	0	5	13	n	60
b	3	0	13	20	n	60
w	4	0	12	22	g	100
w	5	0	1	13	h	100
b	6	1	10	11	n	60
b	7	1	6	5	n	60
b	8	1	6	0	n	60
b	9	1	3	16	n	60
w	10	1	8	27	b	100
w	11	1	14	14	h	100
b	12	2	4	18	n	60
b	13	2	5	24	n	60
b	14	2	12	19	n	60
b	15	2	9	6	n	60
w	16	2	4	17	h	100
w	17	2	14	11	h	100
b	18	3	2	8	n	60
b	19	3	12	17	n	60
b	20	3	5	11	n	60
b	21	3	1	26	n	60
w	22	3	5	26	b	100
w	23	3	14	23	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 10
day 1

score	20	10	15	15

status	0	0	0	0

commands
21
6	m	u	
4	m	u	
19	m	u	
20	m	r	
7	m	u	
12	m	r	
21	m	r	
8	m	d	
3	m	d	
9	m	u	
10	m	u	
11	m	r	
13	m	r	
14	m	r	
15	m	r	
16	m	r	
17	m	r	
23	m	l	
5	m	u	
1	m	d	
0	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b.......W.....FM.M.......
01 .B...CBBBBB...BBBBBBBBBBB..CM.
02 .B.M.bb.C.B...B.C.............
03 .BBBBBBBBBB...B...............
04 ........b.....B...WC..F.......
05 .....C..BBB.CCB..........CW...
06 ........B.B...B.............C.
07 C.......B.B...B.BBBBBB.....W..
08 .......BB.B...B.B.............
09 .....B.c..BC..FbBBBBBBBB......
10 .BBBbBBB..B............B......
11 .B.B...B.........C....WB......
12 .B.....B............C..B......
13 .BBBBBBB......................
14 ............W..W....C.W.......

citizens
24
type	id	player	row	column	weapon	life
b	0	0	6	28	n	60
b	1	0	1	5	n	60
b	2	0	5	13	n	60
b	3	0	14	20	n	60
w	4	0	11	22	g	100
w	5	0	0	13	h	100
b	6	1	9	11	n	60
b	7	1	5	5	n	60
b	8	1	7	0	n	60
b	9	1	2	16	n	60
w	10	1	7	27	b	100
w	11	1	14	15	h	100
b	12	2	4	19	n	60
b	13	2	5	25	n	60
b	14	2	12	20	n	60
b	15	2	9	7	n	60
w	16	2	4	18	h	100
w	17	2	14	12	h	100
b	18	3	2	8	n	60
b	19	3	11	17	n	60
b	20	3	5	12	n	60
b	21	3	1	27	n	60
w	22	3	5	26	b	100
w	23	3	14	22	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 11
day 1

score	25	10	15	20

status	0	0	0	0

commands
19
6	m	d	
12	m	r	
19	m	d	
5	m	r	
20	m	u	
21	m	r	
14	m	d	
23	m	l	
4	m	d	
15	m	l	
16	m	r	
1	m	d	
7	m	u	
17	m	r	
0	m	u	
8	m	u	
9	m	r	
10	m	u	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b........W....FM.M.......
01 .B....BBBBB...BBBBBBBBBBB...C.
02 .B.M.cb.C.B...B..C............
03 .BBBBBBBBBB...B...............
04 .....C..b...C.B....WC.F.......
05 ........BBB..CB..........CW.C.
06 C.......B.B...B............W..
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 .....BCb..B...FbBBBBBBBB......
10 .BBBbBBB..BC...........B......
11 .B.B...B...............B......
12 .B.....B.........C....WB......
13 .BBBBBBB.......W....C.........
14 .............W......CW........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	5	28	n	60
b	1	0	2	5	n	60
b	2	0	5	13	n	60
b	3	0	14	20	n	60
w	4	0	12	22	g	100
w	5	0	0	14	h	100
b	6	1	10	11	n	60
b	7	1	4	5	n	60
b	8	1	6	0	n	60
b	9	1	2	17	n	60
w	10	1	6	27	b	100
w	11	1	13	15	h	100
b	12	2	4	20	n	60
b	13	2	5	25	n	60
b	14	2	13	20	n	60
b	15	2	9	6	n	60
w	16	2	4	19	h	100
w	17	2	14	13	h	100
b	18	3	2	8	n	60
b	19	3	12	17	n	60
b	20	3	4	12	n	60
b	21	3	1	28	n	60
w	22	3	5	26	b	100
w	23	3	14	21	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 12
day 1

score	25	10	15	25

status	0	0	0	0

commands
21
19	m	r	
6	m	d	
2	m	l	
12	m	r	
13	m	u	
7	m	l	
20	m	l	
8	m	u	
4	m	d	
9	m	l	
14	m	r	
15	m	u	
1	m	l	
21	m	u	
10	m	u	
11	m	u	
22	m	l	
5	m	r	
0	m	u	
16	m	r	
17	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 M....b.........W...FM.M.....C.
01 .B....BBBBBM..BBBBBBBBBBB.....
02 .B.MCbb.C.B...B.C.............
03 .BBBBBBBBBB...B...............
04 ....C...b..C..B.....WCF..C..C.
05 C.......BBB.C.B..........W.W..
06 ........B.B...B...............
07 ........B.B...B.BBBBBB........
08 ......CBB.B...B.B.........M...
09 .....B.b..B...FbBBBBBBBB......
10 .BBBbBBB..B............B......
11 .B.B...B...C...........B......
12 .B.....B.......W..C....B......
13 .BBBBBBB.............CW.......
14 ..............W.....CW........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	4	28	n	60
b	1	0	2	4	n	60
b	2	0	5	12	n	60
b	3	0	14	20	n	60
w	4	0	13	22	g	100
w	5	0	0	15	h	100
b	6	1	11	11	n	60
b	7	1	4	4	n	60
b	8	1	5	0	n	60
b	9	1	2	16	n	60
w	10	1	5	27	b	100
w	11	1	12	15	h	100
b	12	2	4	21	n	60
b	13	2	4	25	n	60
b	14	2	13	21	n	60
b	15	2	8	6	n	60
w	16	2	4	20	h	100
w	17	2	14	14	h	100
b	18	3	2	8	n	60
b	19	3	12	18	n	60
b	20	3	4	11	n	60
b	21	3	0	28	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 13
day 1

score	25	10	15	25

status	0	0	0	0

commands
19
12	m	d	
6	m	u	
7	m	l	
8	m	u	
1	m	l	
5	m	r	
9	m	r	
10	m	u	
11	m	r	
4	m	r	
15	m	u	
19	m	u	
20	m	u	
21	m	l	
16	m	d	
17	m	u	
3	m	u	
2	m	u	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 M....b..........W..FM.M....C..
01 .B....BBBBBM..BBBBBBBBBBB.....
02 .B.C.bb.C.B...B..C............
03 .BBBBBBBBBBC..B...............
04 C..C....b...C.B.......F..C.W..
05 ........BBB...B.....WC...W..C.
06 ........B.B...B...............
07 ......C.B.B...B.BBBBBB........
08 .......BB.B...B.B.........M...
09 M....B.b..B...FbBBBBBBBB......
10 .BBBbBBB..BC...........B......
11 .B.B...B..........C....B......
12 .B.....B........W......B......
13 .BBBBBBB......W.....CC.W......
14 .....................W........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	5	28	n	60
b	1	0	2	3	n	60
b	2	0	4	12	n	60
b	3	0	13	20	n	60
w	4	0	13	23	g	100
w	5	0	0	16	h	100
b	6	1	10	11	n	60
b	7	1	4	3	n	60
b	8	1	4	0	n	60
b	9	1	2	17	n	60
w	10	1	4	27	b	100
w	11	1	12	16	h	100
b	12	2	5	21	n	60
b	13	2	4	25	n	60
b	14	2	13	21	n	60
b	15	2	7	6	n	60
w	16	2	5	20	h	100
w	17	2	13	14	h	100
b	18	3	2	8	n	60
b	19	3	11	18	n	60
b	20	3	3	11	n	60
b	21	3	0	27	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 14
day 1

score	30	10	15	25

status	0	0	0	0

commands
19
6	m	u	
5	m	r	
4	m	r	
19	m	u	
20	m	u	
3	m	d	
21	m	l	
2	m	u	
1	m	u	
12	m	d	
0	m	d	
7	m	l	
8	m	u	
9	m	r	
10	m	l	
11	m	u	
15	m	d	
16	m	d	
17	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 M....b...........W.FM.M...C...
01 .B.C..BBBBBM..BBBBBBBBBBB.....
02 .B...bb.C.BC..B...C...........
03 CBBBBBBBBBB.C.B...............
04 ..C.....b.....B.......F..CW...
05 ........BBB...B..........W....
06 ........B.B...B.....WC......C.
07 ........B.B...B.BBBBBB........
08 ......CBB.B...B.B.........M...
09 M....B.b..BC..FbBBBBBBBB......
10 .BBBbBBB..B.......C....B......
11 .B.B...B........W......B......
12 .B.....B......W........B......
13 MBBBBBBB.............C..W.....
14 ...........M........CW........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	6	28	n	60
b	1	0	1	3	n	60
b	2	0	3	12	n	60
b	3	0	14	20	n	60
w	4	0	13	24	g	100
w	5	0	0	17	h	100
b	6	1	9	11	n	60
b	7	1	4	2	n	60
b	8	1	3	0	n	60
b	9	1	2	18	n	60
w	10	1	4	26	b	100
w	11	1	11	16	h	100
b	12	2	6	21	n	60
b	13	2	4	25	n	60
b	14	2	13	21	n	60
b	15	2	8	6	n	60
w	16	2	6	20	h	100
w	17	2	12	14	h	100
b	18	3	2	8	n	60
b	19	3	10	18	n	60
b	20	3	2	11	n	60
b	21	3	0	26	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 15
day 1

score	30	10	15	25

status	0	0	0	0

commands
19
19	m	d	
12	m	r	
5	m	r	
20	m	u	
21	m	l	
4	m	u	
6	m	d	
7	m	l	
3	m	l	
8	m	u	
14	m	l	
9	m	r	
2	m	u	
1	m	u	
0	m	d	
15	m	l	
11	m	u	
16	m	r	
17	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 M..C.b............WFM.M..C....
01 .B....BBBBBC..BBBBBBBBBBB.....
02 CB...bb.C.B.C.B....C..........
03 .BBBBBBBBBB...B...............
04 .C......b.....B.......F..CW...
05 ........BBB...B..........W....
06 ........B.B...B......WC.......
07 ........B.B...B.BBBBBB......C.
08 .....C.BB.B...B.B.........M...
09 M....B.b..B...FbBBBBBBBB.M....
10 .BBBbBBB..BC....W......B......
11 .B.B...B..........C....B......
12 .B.....B...............BW.....
13 MBBBBBBB......W.....C.........
14 ...........M.......C.W........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	28	n	60
b	1	0	0	3	n	60
b	2	0	2	12	n	60
b	3	0	14	19	n	60
w	4	0	12	24	g	100
w	5	0	0	18	h	100
b	6	1	10	11	n	60
b	7	1	4	1	n	60
b	8	1	2	0	n	60
b	9	1	2	19	n	60
w	10	1	4	26	b	100
w	11	1	10	16	h	100
b	12	2	6	22	n	60
b	13	2	4	25	n	60
b	14	2	13	20	n	60
b	15	2	8	5	n	60
w	16	2	6	21	h	100
w	17	2	13	14	h	100
b	18	3	2	8	n	60
b	19	3	11	18	n	60
b	20	3	1	11	n	60
b	21	3	0	25	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 16
day 1

score	30	10	15	30

status	0	0	0	0

commands
19
6	m	d	
12	m	d	
19	m	d	
7	m	l	
5	m	r	
8	m	u	
9	m	d	
20	m	d	
14	m	r	
11	m	d	
15	m	l	
16	m	r	
21	b	l	
17	m	d	
4	m	u	
3	m	l	
2	m	d	
1	m	l	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 M.C..b.............WM.M.bC....
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.BC..B...............
03 .BBBBBBBBBB.C.B....C..........
04 C.......b.....B.......F..CW...
05 ........BBB...B..........W....
06 ........B.B...B.......W.......
07 ........B.B...B.BBBBBBC.......
08 ....C..BB.B...B.B.........M.C.
09 M....B.b..B...FbBBBBBBBB.M....
10 .BBBbBBB..B............B......
11 .B.B...B...C....W......BW.....
12 .B.....B..........C....B......
13 MBBBBBBB.............C........
14 ...........M..W...C..W........

citizens
24
type	id	player	row	column	weapon	life
b	0	0	8	28	n	60
b	1	0	0	2	n	60
b	2	0	3	12	n	60
b	3	0	14	18	n	60
w	4	0	11	24	g	100
w	5	0	0	19	h	100
b	6	1	11	11	n	60
b	7	1	4	0	n	60
b	8	1	1	0	n	60
b	9	1	3	19	n	60
w	10	1	4	26	b	100
w	11	1	11	16	h	100
b	12	2	7	22	n	60
b	13	2	4	25	n	60
b	14	2	13	21	n	60
b	15	2	8	4	n	60
w	16	2	6	22	h	100
w	17	2	14	14	h	100
b	18	3	2	8	n	60
b	19	3	12	18	n	60
b	20	3	2	11	n	60
b	21	3	0	25	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
8
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 17
day 1

score	30	10	15	30

status	0	0	0	0

commands
18
5	m	r	
19	m	d	
20	m	d	
1	m	l	
0	m	l	
6	m	d	
21	m	l	
12	m	d	
4	m	u	
7	m	d	
8	m	u	
14	m	r	
2	m	d	
9	m	d	
15	m	d	
16	m	d	
11	m	d	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 CC...b..............W.M.c.....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B...B...............
03 .BBBBBBBBBBC..B...............
04 ........b...C.B....C..F..CW...
05 C.......BBB...B..........W....
06 ........B.B...B...............
07 ........B.B..MB.BBBBBBW.......
08 .......BB.B...B.B.....C...MC..
09 M...CB.b..B...FbBBBBBBBB.M....
10 .BBBbBBB..B............BW.....
11 .B.B...B...............B......
12 .B.....B...C....W......B......
13 MBBBBBBB..........C...C.......
14 ...........M.W....C..W......F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	8	27	n	60
b	1	0	0	1	n	60
b	2	0	4	12	n	60
b	3	0	14	18	n	60
w	4	0	10	24	g	100
w	5	0	0	20	h	100
b	6	1	12	11	n	60
b	7	1	5	0	n	60
b	8	1	0	0	n	60
b	9	1	4	19	n	60
w	10	1	4	26	b	100
w	11	1	12	16	h	100
b	12	2	8	22	n	60
b	13	2	4	25	n	60
b	14	2	13	22	n	60
b	15	2	9	4	n	60
w	16	2	7	22	h	100
w	17	2	14	13	h	100
b	18	3	2	8	n	60
b	19	3	13	18	n	60
b	20	3	3	11	n	60
b	21	3	0	24	n	60
w	22	3	5	25	b	100
w	23	3	14	21	h	100

barricades
8
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	9	7	40
0	9	15	40
2	10	4	40

round 18
day 1

score	35	15	15	30

status	0	0	0	0

commands
19
5	m	r	
19	m	l	
4	m	u	
6	m	d	
0	m	l	
3	m	l	
12	b	l	
20	m	d	
2	m	r	
1	m	r	
21	m	l	
7	m	d	
14	m	r	
15	m	l	
8	m	d	
23	m	u	
9	m	d	
11	m	d	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C..b...............WMCb.....
01 CB....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b..C.CB.......F..CW...
05 ........BBB...B....C.....W....
06 C.......B.B...B...............
07 ........B.B..MB.BBBBBBW.......
08 .......BB.B...B.B....bC...C...
09 M..C.B.b..B...FbBBBBBBBBWM....
10 .BBBbBBB..B............B......
11 .B.B...B...............B......
12 .B.....B...............B......
13 MBBBBBBB...C....WC...W.C......
14 ...........MW....C..........F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	8	26	n	60
b	1	0	0	2	n	60
b	2	0	4	13	n	60
b	3	0	14	17	n	60
w	4	0	9	24	g	100
w	5	0	0	21	h	100
b	6	1	13	11	n	60
b	7	1	6	0	n	60
b	8	1	1	0	n	60
b	9	1	5	19	n	60
w	10	1	4	26	b	100
w	11	1	13	16	h	100
b	12	2	8	22	n	60
b	13	2	4	25	n	60
b	14	2	13	23	n	60
b	15	2	9	3	n	60
w	16	2	7	22	h	100
w	17	2	14	12	h	100
b	18	3	2	8	n	60
b	19	3	13	17	n	60
b	20	3	4	11	n	60
b	21	3	0	23	n	60
w	22	3	5	25	b	100
w	23	3	13	21	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 19
day 1

score	40	15	15	30

status	0	0	0	0

commands
18
19	m	u	
20	m	d	
12	m	r	
6	m	d	
21	m	l	
7	m	d	
8	m	u	
4	m	r	
23	m	r	
14	m	r	
9	m	d	
3	m	l	
2	m	d	
1	m	r	
11	m	r	
0	m	u	
15	m	l	
16	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 C..C.b...............WC.b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B.......F..CW...
05 ........BBBC.CB..........W....
06 ........B.B...B....C..........
07 C.......B.B..MB.BBBBBB....C...
08 .......BB.B...B.B....bWC......
09 M.C..B.b..B...FbBBBBBBBB.W....
10 .BBBbBBB..B............B......
11 .B.B...B...............B......
12 MB.....B.........C.....B......
13 MBBBBBBB.........W....W.C.....
14 ...........CW...C...........F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	26	n	60
b	1	0	0	3	n	60
b	2	0	5	13	n	60
b	3	0	14	16	n	60
w	4	0	9	25	g	100
w	5	0	0	21	h	100
b	6	1	14	11	n	60
b	7	1	7	0	n	60
b	8	1	0	0	n	60
b	9	1	6	19	n	60
w	10	1	4	26	b	100
w	11	1	13	17	h	100
b	12	2	8	23	n	60
b	13	2	4	25	n	60
b	14	2	13	24	n	60
b	15	2	9	2	n	60
w	16	2	8	22	h	100
w	17	2	14	12	h	100
b	18	3	2	8	n	60
b	19	3	12	17	n	60
b	20	3	5	11	n	60
b	21	3	0	22	n	60
w	22	3	5	25	b	100
w	23	3	13	22	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 20
day 1

score	45	20	15	35

status	0	0	0	0

commands
20
19	m	u	
4	m	l	
6	m	u	
12	m	r	
14	m	d	
7	m	d	
15	m	l	
20	m	d	
21	m	r	
8	m	d	
16	m	r	
0	m	l	
17	m	r	
9	m	r	
23	m	r	
2	m	d	
5	m	l	
11	m	u	
3	m	u	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....Cb..............W..Cb.....
01 CB....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B.......F..CW...
05 ........BBB...B..........W....
06 ........B.BC.CB.....C.........
07 ........B.B..MB.BBBBBB...C....
08 C......BB.B...B.B....b.WC.....
09 MC...B.b..B...FbBBBBBBBBW.....
10 .BBBbBBB..B............B......
11 .B.B...B.........C.....B......
12 MB.....B.........W.....B......
13 MBBBBBBB...C....C......W......
14 .............W..........C...F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	25	n	60
b	1	0	0	4	n	60
b	2	0	6	13	n	60
b	3	0	13	16	n	60
w	4	0	9	24	g	100
w	5	0	0	20	h	100
b	6	1	13	11	n	60
b	7	1	8	0	n	60
b	8	1	1	0	n	60
b	9	1	6	20	n	60
w	10	1	4	26	b	100
w	11	1	12	17	h	100
b	12	2	8	24	n	60
b	13	2	4	25	n	60
b	14	2	14	24	n	60
b	15	2	9	1	n	60
w	16	2	8	23	h	100
w	17	2	14	13	h	100
b	18	3	2	8	n	60
b	19	3	11	17	n	60
b	20	3	6	11	n	60
b	21	3	0	23	n	60
w	22	3	5	25	b	100
w	23	3	13	23	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 21
day 1

score	45	20	15	35

status	0	0	0	0

commands
18
19	m	u	
6	m	u	
12	m	u	
13	m	l	
20	m	d	
4	m	u	
22	m	u	
7	m	d	
23	m	d	
2	m	d	
5	m	l	
8	m	u	
9	m	u	
16	m	l	
3	m	l	
17	m	u	
11	m	u	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....c.............W...Cb.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B.......F.CWW...
05 ........BBB...B.....C.........
06 ........B.B...B...............
07 ........B.BC.CB.BBBBBB..CC....
08 .......BB.B...B.B....bW.W.....
09 CC...B.b..B...FbBBBBBBBB......
10 .BBBbBBB..B......C.....B....M.
11 .B.B...B.........W.....B......
12 MB.....B...C...........B.....M
13 MBBBBBBB.....W.C..............
14 .......................WC...F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	25	n	60
b	1	0	0	5	n	60
b	2	0	7	13	n	60
b	3	0	13	15	n	60
w	4	0	8	24	g	100
w	5	0	0	19	h	100
b	6	1	12	11	n	60
b	7	1	9	0	n	60
b	8	1	0	0	n	60
b	9	1	5	20	n	60
w	10	1	4	26	b	100
w	11	1	11	17	h	100
b	12	2	7	24	n	60
b	13	2	4	24	n	60
b	14	2	14	24	n	60
b	15	2	9	1	n	60
w	16	2	8	22	h	100
w	17	2	13	13	h	100
b	18	3	2	8	n	60
b	19	3	10	17	n	60
b	20	3	7	11	n	60
b	21	3	0	23	n	60
w	22	3	4	25	b	100
w	23	3	14	23	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 22
day 1

score	50	25	15	35

status	0	0	0	0

commands
20
6	m	l	
19	m	r	
7	m	d	
4	m	r	
12	m	d	
20	m	d	
8	m	d	
13	m	d	
9	m	d	
14	m	r	
21	m	r	
15	m	l	
16	m	r	
17	m	d	
22	m	l	
3	m	d	
2	m	d	
0	m	r	
11	m	u	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....c.............W....c.....
01 CB....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B.......F.W.W...
05 ........BBB...B.........C.....
06 ........B.B...B.....C.........
07 ........B.B...B.BBBBBB....C...
08 .......BB.BC.CB.B....b.WCW....
09 C....B.b..B...FbBBBBBBBB......
10 CBBBbBBB..B......WC....B....M.
11 .B.B...B.....M.........B......
12 MB.....B..C............B.....M
13 MBBBBBBB......................
14 .............W.C........WC..F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	26	n	60
b	1	0	0	5	n	60
b	2	0	8	13	n	60
b	3	0	14	15	n	60
w	4	0	8	25	g	100
w	5	0	0	19	h	100
b	6	1	12	10	n	60
b	7	1	10	0	n	60
b	8	1	1	0	n	60
b	9	1	6	20	n	60
w	10	1	4	26	b	100
w	11	1	10	17	h	100
b	12	2	8	24	n	60
b	13	2	5	24	n	60
b	14	2	14	25	n	60
b	15	2	9	0	n	60
w	16	2	8	23	h	100
w	17	2	14	13	h	100
b	18	3	2	8	n	60
b	19	3	10	18	n	60
b	20	3	8	11	n	60
b	21	3	0	24	n	60
w	22	3	4	24	b	100
w	23	3	14	24	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 23
day 1

score	50	25	15	35

status	0	0	0	0

commands
22
12	m	d	
5	m	l	
6	m	u	
4	m	l	
13	m	d	
14	m	r	
19	m	d	
7	m	d	
8	m	u	
9	m	r	
10	m	l	
11	m	r	
15	m	d	
20	m	d	
17	m	u	
21	m	r	
2	m	d	
22	m	d	
3	m	u	
23	m	r	
1	m	r	
0	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....bC....M......W.....bC....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B...M...F..W....
05 ........BBB...B.........W.....
06 ........B.B...B......C..C.....
07 ........B.B...B.BBBBBB.....C..
08 .......BB.B...B.B....b.WW.....
09 .....B.b..BC.CFbBBBBBBBBC.....
10 CBBBbBBB..B.......W....B....M.
11 CB.B...B..C..M....C....B......
12 MB.....B...............B.....M
13 MBBBBBBB.....W.C..............
14 ..........M..............WC.F.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	27	n	60
b	1	0	0	6	n	60
b	2	0	9	13	n	60
b	3	0	13	15	n	60
w	4	0	8	24	g	100
w	5	0	0	18	h	100
b	6	1	11	10	n	60
b	7	1	11	0	n	60
b	8	1	0	0	n	60
b	9	1	6	21	n	60
w	10	1	4	25	b	100
w	11	1	10	18	h	100
b	12	2	9	24	n	60
b	13	2	6	24	n	60
b	14	2	14	26	n	60
b	15	2	10	0	n	60
w	16	2	8	23	h	100
w	17	2	13	13	h	100
b	18	3	2	8	n	60
b	19	3	11	18	n	60
b	20	3	9	11	n	60
b	21	3	0	25	n	60
w	22	3	5	24	b	100
w	23	3	14	25	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 24
day 1

score	50	25	15	35

status	0	0	0	0

commands
20
19	m	l	
20	m	d	
21	m	d	
6	m	d	
7	m	d	
12	m	d	
13	m	d	
8	m	d	
9	m	l	
14	m	r	
15	m	d	
5	m	l	
4	m	d	
2	m	r	
17	m	u	
3	m	u	
1	m	r	
0	m	r	
10	m	l	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b.C...M.....W......b.....
01 CB....BBBBB...BBBBBBBBBBBC...F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B...............
04 ........b.....B...M...F.W.....
05 ........BBB...B.........W.....
06 ........B.B...B.....C.........
07 ........B.B...B.BBBBBB..C...C.
08 .......BB.B...B.B....b.W......
09 .....B.b..B...CbBBBBBBBBW.....
10 .BBBbBBB..BC...........BC...M.
11 CB.B...B.....M...CW....B......
12 CB.....B..C..W.C.......B.....M
13 MBBBBBBB......................
14 ..........M..............W.CF.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	7	28	n	60
b	1	0	0	7	n	60
b	2	0	9	14	n	60
b	3	0	12	15	n	60
w	4	0	9	24	g	100
w	5	0	0	17	h	100
b	6	1	12	10	n	60
b	7	1	12	0	n	60
b	8	1	1	0	n	60
b	9	1	6	20	n	60
w	10	1	4	24	b	100
w	11	1	11	18	h	100
b	12	2	10	24	n	60
b	13	2	7	24	n	60
b	14	2	14	27	n	60
b	15	2	11	0	n	60
w	16	2	8	23	h	100
w	17	2	12	13	h	100
b	18	3	2	8	n	60
b	19	3	11	17	n	60
b	20	3	10	11	n	60
b	21	3	1	25	n	60
w	22	3	5	24	b	100
w	23	3	14	25	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 25
day 0

score	50	30	15	35

status	0	0	0	0

commands
23
6	m	d	
7	m	d	
3	m	u	
19	m	u	
20	m	d	
21	m	d	
5	m	r	
8	m	u	
22	m	d	
4	m	d	
23	m	r	
2	m	d	
1	m	r	
12	m	r	
9	m	u	
10	m	d	
11	m	l	
13	m	r	
0	m	d	
14	m	r	
15	m	u	
16	m	u	
17	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C...Mb..C..M......W.....b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B..........C....
03 .BBBBBBBBBB...B...............
04 ........b.....B...M...F.......
05 ........BBB...B.....C...W.....
06 ........B.B...B.........W.....
07 F.......B.B...B.BBBBBB.W.C....
08 .......BBMB...B.B....b......C.
09 .....B.b..B....bBBBBBBBBW.....
10 CBBBbBBB..B...C..C.....B.C..M.
11 .B.B...B...C.W.C.W.....B......
12 .B.....B...............B.....M
13 CBBBBBBB..C...................
14 ..........M...............W.C.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	8	28	n	60
b	1	0	0	8	n	60
b	2	0	10	14	n	60
b	3	0	11	15	n	60
w	4	0	9	24	g	100
w	5	0	0	18	h	100
b	6	1	13	10	n	60
b	7	1	13	0	n	60
b	8	1	0	0	n	60
b	9	1	5	20	n	60
w	10	1	5	24	b	100
w	11	1	11	17	h	100
b	12	2	10	25	n	40
b	13	2	7	25	n	60
b	14	2	14	28	n	60
b	15	2	10	0	n	60
w	16	2	7	23	h	100
w	17	2	11	13	h	100
b	18	3	2	8	n	60
b	19	3	10	17	n	60
b	20	3	11	11	n	60
b	21	3	2	25	n	60
w	22	3	6	24	b	100
w	23	3	14	26	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 26
day 0

score	50	35	20	35

status	0	0	0	0

commands
23
3	m	u	
12	m	r	
2	m	u	
13	m	d	
6	m	d	
19	m	l	
7	m	d	
14	m	r	
8	m	r	
15	m	u	
9	m	u	
16	m	d	
20	m	u	
10	m	d	
21	m	d	
22	m	d	
17	m	r	
23	m	r	
5	m	r	
11	m	u	
4	m	r	
0	m	d	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C..Mb...C.M.......W....b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B..........C....
04 ........b.....B...M.C.F.......
05 ........BBB...B.........W.....
06 ........B.B...B...............
07 F.......B.B...B.BBBBBB..W.....
08 .......BBMB...B.B....b.W.C....
09 C....B.b..B...CbBBBBBBBB.W..C.
10 .BBBbBBB..BC...CCW.....B..C.M.
11 .B.B...B......W........B......
12 .B.....B...............B.....M
13 .BBBBBBB......................
14 C.........C................W.C

citizens
24
type	id	player	row	column	weapon	life
b	0	0	9	28	n	60
b	1	0	0	9	n	60
b	2	0	9	14	n	60
b	3	0	10	15	n	60
w	4	0	9	25	g	100
w	5	0	0	19	h	100
b	6	1	14	10	n	60
b	7	1	14	0	n	60
b	8	1	0	1	n	60
b	9	1	4	20	n	60
w	10	1	5	24	b	100
w	11	1	10	17	h	100
b	12	2	10	26	n	40
b	13	2	8	25	n	60
b	14	2	14	29	n	60
b	15	2	9	0	n	60
w	16	2	8	23	h	100
w	17	2	11	14	h	100
b	18	3	2	8	n	60
b	19	3	10	16	n	60
b	20	3	10	11	n	60
b	21	3	3	25	n	60
w	22	3	7	24	b	80
w	23	3	14	27	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 27
day 0

score	50	40	20	35

status	0	0	0	0

commands
22
6	m	u	
12	m	r	
19	m	d	
7	m	r	
13	m	r	
14	m	u	
15	m	r	
16	m	u	
8	m	r	
17	m	r	
9	m	l	
20	m	d	
10	m	u	
3	m	u	
5	m	r	
11	m	l	
21	m	l	
4	m	u	
22	m	d	
23	m	r	
1	m	r	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C.Mb....CM........W...b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB...B.........C.....
04 ........b.....B...MC..F.W.....
05 ........BBB...B...............
06 ........B.B...B...............
07 F.......B.B...B.BBBBBB.W......
08 .......BBMB...B.B....b..WWC...
09 .C...B.b..B...CcBBBBBBBB......
10 .BBBbBBB..B.....W......B...CC.
11 .B.B...B...C...WC......B......
12 .B.....B...............B.....M
13 .BBBBBBB..C..................C
14 .C..........................W.

citizens
24
type	id	player	row	column	weapon	life
b	0	0	10	28	n	60
b	1	0	0	10	n	60
b	2	0	9	14	n	60
b	3	0	9	15	n	60
w	4	0	8	25	g	100
w	5	0	0	20	h	100
b	6	1	13	10	n	60
b	7	1	14	1	n	60
b	8	1	0	2	n	60
b	9	1	4	19	n	60
w	10	1	4	24	b	100
w	11	1	10	16	h	100
b	12	2	10	27	n	40
b	13	2	8	26	n	60
b	14	2	13	29	n	60
b	15	2	9	1	n	60
w	16	2	7	23	h	100
w	17	2	11	15	h	100
b	18	3	2	8	n	60
b	19	3	11	16	n	60
b	20	3	11	11	n	60
b	21	3	3	24	n	60
w	22	3	8	24	b	80
w	23	3	14	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 28
day 0

score	55	40	20	35

status	0	0	0	0

commands
23
6	m	u	
19	m	d	
20	m	l	
21	m	u	
7	m	r	
8	m	r	
22	m	r	
12	m	d	
23	m	u	
4	m	u	
3	m	u	
9	m	l	
13	m	d	
14	m	u	
10	m	u	
15	m	r	
16	m	u	
11	m	d	
5	m	r	
1	m	r	
2	m	r	
0	m	r	
17	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...CMb.....C.........W..b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B.........C.....
03 .BBBBBBBBBB...B.........W.....
04 ........b.....B...C...F.......
05 ........BBB...B...............
06 ........B.B...B........W......
07 F.......B.B...B.BBBBBB...W....
08 .......BBMB...BCB....b..W.....
09 ..C..B.b..B....cBBBBBBBB..C...
10 .BBBbBBB..B............B.....C
11 .B.B.M.B..C....WW......B...C..
12 .B.....B..C.....C......B.....C
13 .BBBBBBB....................W.
14 ..C...........................

citizens
24
type	id	player	row	column	weapon	life
b	0	0	10	29	n	60
b	1	0	0	11	n	60
b	2	0	9	15	n	60
b	3	0	8	15	n	60
w	4	0	7	25	g	80
w	5	0	0	21	h	100
b	6	1	12	10	n	60
b	7	1	14	2	n	60
b	8	1	0	3	n	60
b	9	1	4	18	n	60
w	10	1	3	24	b	100
w	11	1	11	16	h	80
b	12	2	11	27	n	40
b	13	2	9	26	n	60
b	14	2	12	29	n	60
b	15	2	9	2	n	60
w	16	2	6	23	h	100
w	17	2	11	15	h	100
b	18	3	2	8	n	60
b	19	3	12	16	n	60
b	20	3	11	10	n	60
b	21	3	2	24	n	60
w	22	3	8	24	b	80
w	23	3	13	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 29
day 0

score	60	45	25	35

status	0	0	0	0

commands
21
12	m	r	
13	m	d	
14	m	u	
19	m	d	
4	m	u	
15	m	r	
16	m	r	
17	m	r	
6	m	l	
20	m	l	
21	m	l	
7	m	r	
5	m	r	
8	m	r	
9	m	u	
1	m	l	
0	m	d	
10	m	u	
11	m	d	
22	m	u	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 M...Cb....C...........W.b.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B........CW.....
03 .BBBBBBBBBB...B...C...........
04 ........b...M.B.......F.......
05 ........BBB...B...............
06 ........B.B...B.........WW....
07 F.......B.B...B.BBBBBB..W.....
08 .......BBMB...BCB....b........
09 ...C.B.b..B....cBBBBBBBB......
10 .BBBbBBB..B............B..C..C
11 .B.B.M.B.C.....W.......B....CC
12 .B.....B.C......W......B....W.
13 .BBBBBBB........C.............
14 ...C..........................

citizens
24
type	id	player	row	column	weapon	life
b	0	0	10	29	n	60
b	1	0	0	10	n	60
b	2	0	9	15	n	60
b	3	0	8	15	n	60
w	4	0	6	25	g	80
w	5	0	0	22	h	100
b	6	1	12	9	n	60
b	7	1	14	3	n	60
b	8	1	0	4	n	60
b	9	1	3	18	n	60
w	10	1	2	24	b	100
w	11	1	12	16	h	60
b	12	2	11	28	n	40
b	13	2	10	26	n	60
b	14	2	11	29	n	40
b	15	2	9	3	n	60
w	16	2	6	24	h	100
w	17	2	11	15	h	100
b	18	3	2	8	n	60
b	19	3	13	16	n	60
b	20	3	11	9	n	60
b	21	3	2	23	n	60
w	22	3	7	24	b	80
w	23	3	12	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 30
day 0

score	60	50	25	35

status	0	0	0	0

commands
21
4	m	u	
6	m	r	
12	m	u	
13	m	d	
7	m	r	
14	m	u	
19	m	d	
15	m	r	
5	m	r	
8	m	l	
16	m	r	
17	m	d	
9	m	u	
1	m	r	
0	m	d	
10	m	l	
11	m	r	
20	m	u	
21	m	d	
22	m	u	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 M..C.b.....C...........Wb.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...C.....W.....
03 .BBBBBBBBBB...B........C......
04 ........b...M.B.......F.......
05 .M......BBB...B..........W....
06 ........B.B...B.........WW....
07 F.......B.B...B.BBBBBB........
08 .......BBMB...BCB....b........
09 ....CB.b..B....cBBBBBBBB......
10 .BBBbBBB.CB............B....CC
11 .B.B.M.B...............B..C.WC
12 .B.....B..C....W.W.....B......
13 .BBBBBBB......................
14 ....C...........C.............

citizens
24
type	id	player	row	column	weapon	life
b	0	0	10	29	n	40
b	1	0	0	11	n	60
b	2	0	9	15	n	60
b	3	0	8	15	n	60
w	4	0	5	25	g	80
w	5	0	0	23	h	100
b	6	1	12	10	n	60
b	7	1	14	4	n	60
b	8	1	0	3	n	60
b	9	1	2	18	n	60
w	10	1	2	24	b	100
w	11	1	12	17	h	60
b	12	2	10	28	n	40
b	13	2	11	26	n	60
b	14	2	11	29	n	20
b	15	2	9	4	n	60
w	16	2	6	25	h	100
w	17	2	12	15	h	100
b	18	3	2	8	n	60
b	19	3	14	16	n	60
b	20	3	10	9	n	60
b	21	3	3	23	n	40
w	22	3	6	24	b	80
w	23	3	11	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 31
day 0

score	60	50	25	35

status	0	0	0	0

commands
22
6	m	u	
4	m	u	
19	m	u	
7	m	r	
12	m	r	
13	m	l	
20	m	u	
0	m	u	
5	m	r	
8	m	l	
21	m	d	
2	m	d	
22	m	r	
9	m	l	
14	m	u	
10	m	l	
1	m	r	
11	m	d	
15	m	d	
16	m	d	
17	m	r	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 M.C..b......C..........Wb.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B..C.....W......
03 .BBBBBBBBBB...B...............
04 ........b...M.B.......FC.W....
05 .M......BBB...BM..............
06 ........B.B...B.........W.....
07 F.......B.B...B.BBBBBB...W....
08 .......BBMB...BCB....b........
09 .....B.b.CB....bBBBBBBBB.....C
10 .BBBcBBB..B....C.......B.....C
11 .B.B.M.B..C............B.C..W.
12 .B.....B........W......B......
13 .BBBBBBB........CW............
14 .....C........................

citizens
23
type	id	player	row	column	weapon	life
b	0	0	9	29	n	40
b	1	0	0	12	n	60
b	2	0	10	15	n	60
b	3	0	8	15	n	60
w	4	0	4	25	g	80
w	5	0	0	23	h	100
b	6	1	11	10	n	60
b	7	1	14	5	n	60
b	8	1	0	2	n	60
b	9	1	2	17	n	60
w	10	1	2	23	b	100
w	11	1	13	17	h	60
b	13	2	11	25	n	60
b	14	2	10	29	n	20
b	15	2	10	4	n	60
w	16	2	7	25	h	80
w	17	2	12	16	h	100
b	18	3	2	8	n	60
b	19	3	13	16	n	60
b	20	3	9	9	n	60
b	21	3	4	23	n	40
w	22	3	6	24	b	80
w	23	3	11	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	30
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 32
day 0

score	60	50	25	135

status	0	0	0	0

commands
22
6	m	r	
19	m	d	
7	m	l	
13	m	l	
8	m	l	
5	m	r	
14	m	u	
4	m	l	
3	m	u	
2	m	d	
9	m	l	
15	m	d	
1	m	d	
10	m	d	
16	m	l	
0	m	u	
20	m	u	
21	m	d	
11	m	r	
22	m	d	
17	m	d	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 MC...b.................Wb.....
01 .B....BBBBB.C.BBBBBBBBBBB....F
02 .B...bb.C.B...B.C.............
03 .BBBBBBBBBB...B........W......
04 ........b...M.B.......F.W.....
05 .M......BBB...BM.......C......
06 ..M.....B.B...B...M.....W.....
07 F.......B.B...BCBBBBBB..W.....
08 ......MBBCB...B.B....b.......C
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBB..B............B....W.
11 .B.BCM.B...C...C.......BC.....
12 .B.....B...............B......
13 .BBBBBBB........W.W...........
14 ....C...........C.............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	8	29	n	40
b	1	0	1	12	n	60
b	2	0	11	15	n	60
b	3	0	7	15	n	60
w	4	0	4	24	g	80
w	5	0	0	23	h	100
b	6	1	11	11	n	60
b	7	1	14	4	n	60
b	8	1	0	1	n	60
b	9	1	2	16	n	60
w	10	1	3	23	b	100
w	11	1	13	18	h	60
b	13	2	11	24	n	60
b	15	2	11	4	n	60
w	16	2	7	24	h	60
w	17	2	13	16	h	100
b	18	3	2	8	n	60
b	19	3	14	16	n	60
b	20	3	8	9	n	60
b	21	3	5	23	n	40
w	22	3	6	24	b	80
w	23	3	10	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	20
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 33
day 0

score	160	50	25	140

status	0	0	0	0

commands
21
4	m	r	
5	m	r	
3	m	u	
2	m	u	
13	m	u	
15	m	r	
19	m	l	
20	m	d	
1	m	d	
6	m	u	
16	m	l	
21	m	l	
0	m	u	
22	m	d	
23	m	u	
7	m	l	
17	m	d	
8	m	l	
9	m	l	
10	m	d	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....b.............F...Wb.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B.C.BC..M...........
03 .BBBBBBBBBB...B...............
04 ........b...M.B.......FW.W....
05 .M......BBB...BM......C.......
06 ..M.....B.B...BC..M...........
07 F.......B.B...B.BBBBBB.WW....C
08 ......MBB.B...B.B....b........
09 .....B.b.CB....bBBBBBBBB....W.
10 .BBBbBBB..BC...C.......BC.....
11 .B.B.C.B...............B......
12 .B.....B...............B......
13 .BBBBBBB...........W..........
14 ...C...........CW.............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	7	29	n	40
b	1	0	2	12	n	60
b	2	0	10	15	n	60
b	3	0	6	15	n	60
w	4	0	4	25	g	80
w	5	0	0	23	h	100
b	6	1	10	11	n	60
b	7	1	14	3	n	60
b	8	1	0	0	n	60
b	9	1	2	15	n	60
w	10	1	4	23	b	100
w	11	1	13	19	h	60
b	13	2	10	24	n	60
b	15	2	11	5	n	60
w	16	2	7	23	h	60
w	17	2	14	16	h	100
b	18	3	2	8	n	60
b	19	3	14	15	n	60
b	20	3	9	9	n	60
b	21	3	5	22	n	40
w	22	3	7	24	b	80
w	23	3	9	28	h	100

barricades
9
player	row	column	resistance
0	0	5	40
3	0	24	10
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 34
day 0

score	160	55	30	140

status	0	0	0	0

commands
21
19	m	u	
20	m	d	
13	m	d	
4	m	u	
15	m	l	
5	m	r	
16	m	u	
17	m	l	
21	m	l	
3	m	u	
22	m	l	
1	m	d	
23	m	u	
2	m	u	
0	m	u	
6	m	u	
7	m	l	
8	m	d	
9	m	d	
10	m	l	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b.............F...W......
01 CB....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...M...........
03 .BBBBBBBBBB.C.BC.........W....
04 ...F....b...M.B.......W.......
05 .M......BBB...BC.....C........
06 ..M.....B.B...B...M....W.....C
07 F.......B.B...B.BBBBBB.W......
08 ......MBB.B...B.B....b......W.
09 .....B.b..BC...cBBBBBBBB......
10 .BBBbBBB.CB............B......
11 .B.BC..B...............BC.....
12 .B.....B...............B......
13 .BBBBBBB.......C....W.........
14 ..C............W..............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	6	29	n	40
b	1	0	3	12	n	60
b	2	0	9	15	n	60
b	3	0	5	15	n	60
w	4	0	3	25	g	80
w	5	0	0	23	h	100
b	6	1	9	11	n	60
b	7	1	14	2	n	60
b	8	1	1	0	n	60
b	9	1	3	15	n	60
w	10	1	4	22	b	100
w	11	1	13	20	h	60
b	13	2	11	24	n	60
b	15	2	11	4	n	60
w	16	2	6	23	h	60
w	17	2	14	15	h	100
b	18	3	2	8	n	60
b	19	3	13	15	n	60
b	20	3	10	9	n	60
b	21	3	5	21	n	40
w	22	3	7	23	b	80
w	23	3	8	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 35
day 0

score	165	55	30	140

status	0	0	0	0

commands
21
13	m	u	
19	m	u	
15	m	u	
5	m	r	
16	m	l	
17	m	u	
20	m	d	
21	m	l	
22	m	u	
4	m	d	
23	m	u	
6	m	u	
7	m	l	
8	m	d	
9	m	u	
1	m	d	
10	m	l	
3	m	r	
2	m	u	
0	m	l	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....b.............F....W.....
01 .B....BBBBB...BBBBBBBBBBB....F
02 CB...bb.C.B...BC..M...........
03 .BBBBBBBBBB...B...............
04 ...F....b...C.B......W...W....
05 .M......BBB...B.C...C.........
06 ..M.....B.B...B...M...WW....C.
07 F.......B.B...B.BBBBBB......W.
08 ......MBB.BC..BCB....b........
09 .....B.b..B....bBBBBBBBB......
10 .BBBcBBB..B............BC.....
11 .B.B...B.C.............B......
12 .B.....B.......C.......B......
13 .BBBBBBB.......W.....W........
14 .C............................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	6	28	n	40
b	1	0	4	12	n	60
b	2	0	8	15	n	60
b	3	0	5	16	n	60
w	4	0	4	25	g	80
w	5	0	0	24	h	100
b	6	1	8	11	n	60
b	7	1	14	1	n	60
b	8	1	2	0	n	60
b	9	1	2	15	n	60
w	10	1	4	21	b	100
w	11	1	13	21	h	60
b	13	2	10	24	n	60
b	15	2	10	4	n	60
w	16	2	6	22	h	60
w	17	2	13	15	h	100
b	18	3	2	8	n	60
b	19	3	12	15	n	60
b	20	3	11	9	n	60
b	21	3	5	20	n	40
w	22	3	6	23	b	80
w	23	3	7	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 36
day 0

score	170	55	30	140

status	0	0	0	0

commands
21
19	m	u	
13	m	u	
6	m	d	
15	m	u	
7	m	l	
16	m	l	
8	m	d	
9	m	r	
20	m	r	
21	m	l	
0	m	u	
17	m	u	
10	m	l	
22	m	l	
5	m	r	
11	m	r	
4	m	d	
3	m	r	
23	m	u	
2	m	u	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....Mb.............F.....W....
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B.C.M...........
03 CBBBBBBBBBB...B...............
04 ...F....b..C..B.....W.........
05 .M......BBB...B..C.C.....W..C.
06 ..M.....B.B...B...M..WW.....W.
07 F.......B.B...BCBBBBBB........
08 ......MBB.B...B.B....b........
09 ....CB.b..BC...bBBBBBBBBC.....
10 .BBBbBBB..B............B......
11 .B.B...B..C....C.......B......
12 .B.....B.......W.......B......
13 .BBBBBBB..............W.......
14 C.............................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	28	n	40
b	1	0	4	11	n	60
b	2	0	7	15	n	60
b	3	0	5	17	n	60
w	4	0	5	25	g	80
w	5	0	0	25	h	100
b	6	1	9	11	n	60
b	7	1	14	0	n	60
b	8	1	3	0	n	60
b	9	1	2	16	n	60
w	10	1	4	20	b	100
w	11	1	13	22	h	60
b	13	2	9	24	n	60
b	15	2	9	4	n	60
w	16	2	6	21	h	60
w	17	2	12	15	h	100
b	18	3	2	8	n	60
b	19	3	11	15	n	60
b	20	3	11	10	n	60
b	21	3	5	19	n	40
w	22	3	6	22	b	80
w	23	3	6	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 37
day 0

score	170	55	30	140

status	0	0	0	0

commands
21
13	m	u	
15	m	u	
0	m	u	
19	m	u	
20	m	d	
5	m	d	
21	m	d	
22	m	l	
4	m	r	
6	m	d	
3	m	r	
16	m	l	
7	m	u	
17	m	u	
2	m	u	
8	m	d	
23	m	u	
1	m	l	
9	m	r	
10	m	l	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.............F..........
01 .B....BBBBB...BBBBBBBBBBBW...F
02 .B...bb.C.B...B..CM...........
03 .BBBBBBBBBB...B...............
04 C..F....b.C...B....W........C.
05 .M......BBB...B...C.......W.W.
06 ..M.....B.B...BC..MCW.W.......
07 F.......B.B...B.BBBBBB........
08 ....C.MBB.B...B.B....b..C.....
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBB..BC...C.......B......
11 .B.B...B.......W.......B......
12 .B.....B..C............B......
13 CBBBBBBB...............W......
14 ..............................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	4	28	n	40
b	1	0	4	10	n	60
b	2	0	6	15	n	60
b	3	0	5	18	n	60
w	4	0	5	26	g	80
w	5	0	1	25	h	100
b	6	1	10	11	n	60
b	7	1	13	0	n	60
b	8	1	4	0	n	60
b	9	1	2	17	n	60
w	10	1	4	19	b	100
w	11	1	13	23	h	60
b	13	2	8	24	n	60
b	15	2	8	4	n	60
w	16	2	6	20	h	40
w	17	2	11	15	h	100
b	18	3	2	8	n	60
b	19	3	10	15	n	60
b	20	3	12	10	n	60
b	21	3	6	19	n	40
w	22	3	6	22	b	80
w	23	3	5	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 38
day 0

score	170	55	30	140

status	0	0	0	0

commands
21
13	m	u	
3	m	d	
6	m	u	
15	m	r	
7	m	u	
0	m	u	
16	m	u	
5	m	d	
4	m	r	
8	m	d	
19	m	l	
9	m	r	
20	m	d	
17	m	u	
21	m	u	
2	m	r	
22	m	l	
1	m	l	
23	m	u	
10	m	d	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.............F..........
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B...C......W....
03 .BBBBBBBBBB...B.............C.
04 ...F....bC....B....W........W.
05 CM......BBB...B....CW......W..
06 ..M.....B.B...B.C.C..W........
07 F.......B.B...B.BBBBBB..C.....
08 .....CMBB.B...B.B....b........
09 .....B.b..BC...bBBBBBBBB......
10 .BBBbBBB..B...CW.......B......
11 .B.B...B...............B......
12 CB.....B...............B......
13 .BBBBBBB..C.........M...W.....
14 ..............................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	3	28	n	40
b	1	0	4	9	n	60
b	2	0	6	16	n	60
b	3	0	6	18	n	60
w	4	0	5	27	g	80
w	5	0	2	25	h	100
b	6	1	9	11	n	60
b	7	1	12	0	n	60
b	8	1	5	0	n	60
b	9	1	2	18	n	60
w	10	1	4	19	b	100
w	11	1	13	24	h	60
b	13	2	7	24	n	60
b	15	2	8	5	n	60
w	16	2	5	20	h	40
w	17	2	10	15	h	100
b	18	3	2	8	n	60
b	19	3	10	14	n	60
b	20	3	13	10	n	60
b	21	3	5	19	n	20
w	22	3	6	21	b	80
w	23	3	4	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 39
day 0

score	175	60	30	140

status	0	0	0	0

commands
20
0	m	u	
6	m	d	
5	m	d	
4	m	u	
3	m	l	
13	m	d	
15	m	r	
16	m	r	
2	m	l	
7	m	u	
8	m	d	
9	m	l	
19	m	u	
10	m	d	
1	m	l	
11	m	u	
20	m	r	
22	m	u	
17	m	l	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.............F..........
01 .B....BBBBB...BBBBBBBBBBB....F
02 .B...bb.C.B...B..C..........C.
03 .BBBBBBBBBB...B..........W..W.
04 ...F....bC....B....W.......W..
05 .M......BBB...B......W........
06 C.M.....B.B...BC.C...W........
07 F.......B.BG..B.BBBBBB........
08 ......CBB.B...B.B....b..C.....
09 .....B.b..B...CbBBBBBBBB......
10 .BBBbBBB..BC..W..M.....B......
11 CB.B...B...............B......
12 .B.....B.............Z.BW.....
13 .BBBBBBB...C........M.........
14 ..............................

citizens
21
type	id	player	row	column	weapon	life
b	0	0	2	28	n	40
b	1	0	4	9	n	60
b	2	0	6	15	n	60
b	3	0	6	17	n	60
w	4	0	4	27	g	80
w	5	0	3	25	h	100
b	6	1	10	11	n	60
b	7	1	11	0	n	60
b	8	1	6	0	n	60
b	9	1	2	17	n	60
w	10	1	4	19	b	100
w	11	1	12	24	h	60
b	13	2	8	24	n	60
b	15	2	8	6	n	60
w	16	2	5	21	h	20
w	17	2	10	14	h	100
b	18	3	2	8	n	60
b	19	3	9	14	n	60
b	20	3	13	11	n	60
w	22	3	6	21	b	80
w	23	3	3	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	30
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 40
day 0

score	175	160	35	140

status	0	0	0	0

commands
19
6	m	d	
7	m	u	
0	m	u	
5	m	d	
8	m	r	
4	m	u	
19	m	l	
3	m	l	
2	m	d	
13	m	d	
9	m	d	
1	m	l	
10	m	d	
11	m	u	
15	m	u	
20	m	u	
22	m	u	
23	m	u	
17	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.............F..........
01 .B....BBBBB...BBBBBBBBBBB...CF
02 .B...bb.C.B...B.............W.
03 .BBBBBBBBBB...B..C.........W..
04 ...F....bC....B..........W....
05 .M......BBB...B....W..........
06 .CM.....B.B...B.C....W........
07 F.....C.B.BG..BCBBBBBB........
08 .......BB.B...B.B....b........
09 .....B.b..B..CWbBBBBBBBBC.....
10 CBBBbBBB..B......M.....B......
11 .B.B...B...C...........BW.....
12 .B.....B...C.........Z.B......
13 .BBBBBBB............M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	1	28	n	40
b	1	0	4	9	n	60
b	2	0	7	15	n	60
b	3	0	6	16	n	60
w	4	0	3	27	g	80
w	5	0	4	25	h	100
b	6	1	11	11	n	60
b	7	1	10	0	n	60
b	8	1	6	1	n	60
b	9	1	3	17	n	60
w	10	1	5	19	b	100
w	11	1	11	24	h	60
b	13	2	9	24	n	60
b	15	2	7	6	n	60
w	17	2	9	14	h	100
b	18	3	2	8	n	60
b	19	3	9	13	n	60
b	20	3	12	11	n	60
w	22	3	6	21	b	80
w	23	3	2	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	20
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 41
day 0

score	175	160	35	390

status	0	0	0	0

commands
19
13	m	r	
15	m	u	
19	m	u	
17	m	l	
20	m	d	
22	m	l	
6	m	u	
0	m	u	
7	m	u	
23	m	u	
5	m	d	
8	m	r	
9	m	d	
4	m	u	
3	m	l	
10	m	l	
11	m	u	
2	m	d	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.............F........C.
01 .B....BBBBB...BBBBBBBBBBB...WF
02 .B...bb.C.B...B............W..
03 .BBBBBBBBBB...B...............
04 ...F....bC....B..C............
05 .M......BBB...B...W......W....
06 ..C...C.B.B...BC....W.........
07 F.......B.BG..B.BBBBBB........
08 .......BB.B..CBCB....b........
09 C....B.b..B..W.bBBBBBBBB.C....
10 .BBBbBBB..BC.....M.....BW.....
11 .B.B...B...............B......
12 .B.....B.............Z.B......
13 .BBBBBBB...C........M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	0	28	n	40
b	1	0	4	9	n	60
b	2	0	8	15	n	60
b	3	0	6	15	n	60
w	4	0	2	27	g	80
w	5	0	5	25	h	100
b	6	1	10	11	n	60
b	7	1	9	0	n	60
b	8	1	6	2	n	60
b	9	1	4	17	n	60
w	10	1	5	18	b	100
w	11	1	10	24	h	60
b	13	2	9	25	n	60
b	15	2	6	6	n	60
w	17	2	9	13	h	100
b	18	3	2	8	n	60
b	19	3	8	13	n	60
b	20	3	13	11	n	60
w	22	3	6	20	b	80
w	23	3	1	28	h	100

barricades
8
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
3	4	8	10
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 42
day 0

score	175	165	35	390

status	0	0	0	0

commands
19
13	m	r	
0	m	r	
19	m	u	
20	m	u	
22	m	u	
15	m	u	
5	m	d	
17	m	u	
23	m	u	
4	m	u	
6	m	d	
7	m	u	
3	m	d	
2	m	d	
8	m	u	
9	m	r	
10	m	l	
11	m	u	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F........WC
01 .B....BBBBB...BBBBBBBBBBB..W.F
02 .B...bb.C.B..GB...............
03 .BBBBBBBBBB.M.B...............
04 ...F.....C....B...C...........
05 .MC...C.BBB...B..W..W.........
06 ........B.B...B..........W....
07 F.......B.BG.CBCBBBBBB........
08 C......BB.B..WB.B....b........
09 .....B.b..B....cBBBBBBBBW.C...
10 .BBBbBBB..B......M.....B......
11 .B.B...B...C.........Z.B......
12 .B.....B...C.........Z.B......
13 .BBBBBBB............M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	0	29	n	40
b	1	0	4	9	n	60
b	2	0	9	15	n	60
b	3	0	7	15	n	60
w	4	0	1	27	g	80
w	5	0	6	25	h	100
b	6	1	11	11	n	60
b	7	1	8	0	n	60
b	8	1	5	2	n	60
b	9	1	4	18	n	60
w	10	1	5	17	b	100
w	11	1	9	24	h	60
b	13	2	9	26	n	60
b	15	2	5	6	n	60
w	17	2	8	13	h	100
b	18	3	2	8	n	60
b	19	3	7	13	n	60
b	20	3	12	11	n	60
w	22	3	5	20	b	80
w	23	3	0	28	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 43
day 0

score	175	165	35	390

status	0	0	0	0

commands
19
0	m	d	
5	m	r	
13	m	d	
6	m	u	
15	m	d	
7	m	u	
17	m	u	
4	m	r	
8	m	u	
9	m	l	
10	m	l	
11	m	u	
19	m	u	
3	m	d	
2	m	d	
20	m	d	
22	m	u	
23	m	r	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F.........W
01 .B....BBBBB...BBBBBBBBBBB...WC
02 .B...bb.C.B..GB...............
03 .BBBBBBBBBB.M.B...............
04 ..CF......C...B..C..W.........
05 .M......BBB...B.W.............
06 ......C.B.B..CB...........W...
07 C.......B.BG..B.BBBBBB........
08 .......BB.B..WBCB....b..W.....
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBB..BC...C.M.....B..C...
11 .BFB...B.............Z.B......
12 .B.M...B.............Z.B......
13 .BBBBBBB...C........M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	1	29	n	60
b	1	0	4	10	n	60
b	2	0	10	15	n	60
b	3	0	8	15	n	60
w	4	0	1	28	g	80
w	5	0	6	26	h	100
b	6	1	10	11	n	60
b	7	1	7	0	n	60
b	8	1	4	2	n	60
b	9	1	4	17	n	60
w	10	1	5	16	b	100
w	11	1	8	24	h	60
b	13	2	10	26	n	60
b	15	2	6	6	n	60
w	17	2	8	13	h	80
b	18	3	2	8	n	60
b	19	3	6	13	n	60
b	20	3	13	11	n	60
w	22	3	4	20	b	80
w	23	3	0	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 44
day 0

score	175	165	35	390

status	0	0	0	0

commands
19
6	m	d	
7	m	u	
19	m	u	
13	m	d	
8	m	l	
9	m	d	
20	m	u	
10	m	l	
0	m	d	
22	m	l	
15	m	u	
11	m	u	
5	m	d	
4	m	u	
2	m	r	
23	m	d	
17	m	u	
3	m	d	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F........W.
01 .B....BBBBB...BBBBBBBBBBB....W
02 .B...bb.C.B..GB..............C
03 .BBBBBBBBBB.M.B...............
04 .C.F.......C..B....W..........
05 .M....C.BBB..CBW.C............
06 C.......B.B...B...............
07 ........B.BG.WB.BBBBBB..W.W...
08 .......BB.B...B.B....b........
09 .....B.b..B....cBBBBBBBB......
10 .BBBbBBB..B.....CM.....B......
11 .BFB...B...C.........Z.B..C...
12 .B.M...B...C.........Z.B......
13 .BBBBBBB............M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	2	29	n	60
b	1	0	4	11	n	60
b	2	0	10	16	n	60
b	3	0	9	15	n	60
w	4	0	0	28	g	80
w	5	0	7	26	h	100
b	6	1	11	11	n	60
b	7	1	6	0	n	60
b	8	1	4	1	n	60
b	9	1	5	17	n	60
w	10	1	5	15	b	100
w	11	1	7	24	h	60
b	13	2	11	26	n	60
b	15	2	5	6	n	60
w	17	2	7	13	h	80
b	18	3	2	8	n	60
b	19	3	5	13	n	60
b	20	3	12	11	n	60
w	22	3	4	19	b	80
w	23	3	1	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 45
day 0

score	175	165	35	390

status	0	0	0	0

commands
19
13	m	d	
15	m	l	
17	m	u	
19	m	u	
20	m	d	
0	m	d	
22	m	d	
6	m	r	
7	m	u	
23	m	d	
5	m	d	
4	m	r	
8	m	l	
9	m	l	
2	m	r	
10	m	u	
1	m	u	
3	m	d	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F.........W
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B...bb.C.B..GB..............W
03 .BBBBBBBBBBCM.B..............C
04 C..F.........CBW..............
05 CM...C..BBB...B.C..W..........
06 ........B.B..WB.........W.....
07 ........B.BG..B.BBBBBB........
08 .......BB.B...B.B....b....W...
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBBM.B....C.C.....B......
11 .BFB...B....C........Z.B......
12 .B.M...B.............Z.B..C...
13 .BBBBBBB...C........M.........
14 ..............................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	3	29	n	60
b	1	0	3	11	n	60
b	2	0	10	17	n	60
b	3	0	10	15	n	60
w	4	0	0	29	g	80
w	5	0	8	26	h	100
b	6	1	11	12	n	60
b	7	1	5	0	n	60
b	8	1	4	0	n	60
b	9	1	5	16	n	60
w	10	1	4	15	b	100
w	11	1	6	24	h	60
b	13	2	12	26	n	60
b	15	2	5	5	n	60
w	17	2	6	13	h	80
b	18	3	2	8	n	60
b	19	3	4	13	n	60
b	20	3	13	11	n	60
w	22	3	5	19	b	80
w	23	3	2	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 46
day 0

score	180	165	35	390

status	0	0	0	0

commands
19
13	m	d	
19	m	u	
6	m	l	
7	m	r	
15	m	l	
8	m	u	
9	m	d	
20	m	u	
0	m	d	
22	m	l	
10	m	u	
11	m	u	
23	m	d	
5	m	d	
4	m	d	
1	m	r	
17	m	u	
3	m	d	
2	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F..........
01 .B....BBBBB...BBBBBBBBBBB....W
02 .B...bb.C.B..GB...............
03 CBBBBBBBBBB.CCBW.............W
04 ...F..........B..............C
05 .C..C...BBB..WB...W.....W.....
06 ........B.B...B.C.............
07 ........B.BG..B.BBBBBB........
08 .......BB.B...B.B....b........
09 .....B.b..B....bBBBBBBBB..W...
10 .BBBbBBBM.B.......C....B......
11 .BFB...B...C...C.....Z.B......
12 .B.M...B...C.........Z.B......
13 .BBBBBBB............M.....C...
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	4	29	n	60
b	1	0	3	12	n	60
b	2	0	10	18	n	60
b	3	0	11	15	n	60
w	4	0	1	29	g	80
w	5	0	9	26	h	100
b	6	1	11	11	n	60
b	7	1	5	1	n	60
b	8	1	3	0	n	60
b	9	1	6	16	n	60
w	10	1	3	15	b	100
w	11	1	5	24	h	60
b	13	2	13	26	n	60
b	15	2	5	4	n	60
w	17	2	5	13	h	80
b	18	3	2	8	n	60
b	19	3	3	13	n	60
b	20	3	12	11	n	60
w	22	3	5	18	b	80
w	23	3	3	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 47
day 0

score	185	170	35	390

status	0	0	0	0

commands
19
19	m	u	
0	m	d	
13	m	l	
5	m	d	
15	m	d	
4	m	d	
20	m	d	
6	m	l	
22	m	d	
7	m	u	
1	m	u	
8	m	u	
3	m	l	
17	m	u	
2	m	r	
23	m	d	
9	m	l	
10	m	d	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F..........
01 .B....BBBBB...BBBBBBBBBBB.....
02 CB...bb.C.B.CCB..............W
03 .BBBBBBBBBB...B...............
04 .C.F.........WBW........W....W
05 ........BBB...B..............C
06 ....C...B.B...BC..W...........
07 ........B.BG..B.BBBBBB........
08 .......BB.B...B.B....b........
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBBM.B........C...B..W...
11 .BFB...B..C...C......Z.B......
12 .B.M...B.............Z.B......
13 .BBBBBBB...C........M....C....
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	29	n	60
b	1	0	2	12	n	60
b	2	0	10	19	n	60
b	3	0	11	14	n	60
w	4	0	2	29	g	80
w	5	0	10	26	h	100
b	6	1	11	10	n	60
b	7	1	4	1	n	60
b	8	1	2	0	n	60
b	9	1	6	15	n	60
w	10	1	4	15	b	100
w	11	1	4	24	h	60
b	13	2	13	25	n	60
b	15	2	6	4	n	60
w	17	2	4	13	h	80
b	18	3	2	8	n	60
b	19	3	2	13	n	60
b	20	3	13	11	n	60
w	22	3	6	18	b	80
w	23	3	4	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 48
day 0

score	185	170	35	390

status	0	0	0	0

commands
19
6	m	l	
7	m	r	
8	m	u	
13	m	l	
19	m	u	
20	m	u	
0	m	d	
9	m	d	
15	m	d	
22	m	l	
5	m	d	
4	m	d	
17	m	u	
10	m	d	
3	m	l	
11	m	u	
2	m	r	
23	m	d	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..MMMb.......M.....F..........
01 CB....BBBBB.CCBBBBBBBBBBB.....
02 .B...bb.C.B...B...............
03 .BBBBBBBBBB..WB.........W....W
04 ..CF..........B...............
05 ........BBB...BW.............W
06 ........B.B...B..W...........C
07 ....C...B.BG..BCBBBBBB........
08 .......BB.B...B.B....b........
09 .....B.b..B....bBBBBBBBB......
10 .BBBbBBBM.B.........C..B......
11 .BFB...B.C...C.......Z.B..W...
12 .B.M...B...C.........Z.B......
13 .BBBBBBB............M...C.....
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	6	29	n	60
b	1	0	1	12	n	60
b	2	0	10	20	n	60
b	3	0	11	13	n	60
w	4	0	3	29	g	80
w	5	0	11	26	h	100
b	6	1	11	9	n	60
b	7	1	4	2	n	60
b	8	1	1	0	n	60
b	9	1	7	15	n	60
w	10	1	5	15	b	100
w	11	1	3	24	h	60
b	13	2	13	24	n	60
b	15	2	7	4	n	60
w	17	2	3	13	h	80
b	18	3	2	8	n	60
b	19	3	1	13	n	60
b	20	3	12	11	n	60
w	22	3	6	17	b	80
w	23	3	5	29	h	100

barricades
7
player	row	column	resistance
0	0	5	40
0	2	5	40
3	2	6	40
2	8	21	40
2	9	7	40
0	9	15	40
2	10	4	40

round 49
day 0

score	185	170	35	390

status	0	0	0	0

commands
19
0	m	d	
13	m	l	
6	m	u	
15	m	d	
19	m	u	
20	m	u	
17	m	u	
5	m	d	
4	m	d	
22	m	l	
2	m	r	
23	m	d	
1	m	u	
7	m	r	
3	m	l	
8	m	u	
9	m	d	
10	m	d	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.MMM.......CC.....F..........
01 .B....BBBBB...BBBBBBBBBBB....M
02 .B......C.B..WB.....G...W.....
03 .BBBBBBBBBB...B...............
04 ...C..........B..............W
05 ........BBB...B...............
06 ........B.B...BWW............W
07 ........B.BG..B.BBBBBB.......C
08 ....C..BB.B...BCB.............
09 .....B....B.....BBBBBBBB......
10 .BBB.BBBMCB..........C.B......
11 .BFB...B...CC........Z.B......
12 .B.M...B.............Z.B..W...
13 .BBBBBBB............M..C......
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	29	n	60
b	1	0	0	12	n	60
b	2	0	10	21	n	60
b	3	0	11	12	n	60
w	4	0	4	29	g	80
w	5	0	12	26	h	100
b	6	1	10	9	n	60
b	7	1	4	3	n	60
b	8	1	0	0	n	60
b	9	1	8	15	n	60
w	10	1	6	15	b	100
w	11	1	2	24	h	60
b	13	2	13	23	n	60
b	15	2	8	4	n	60
w	17	2	2	13	h	80
b	18	3	2	8	n	60
b	19	3	0	13	n	60
b	20	3	11	11	n	60
w	22	3	6	16	b	80
w	23	3	6	29	h	100

barricades
0
player	row	column	resistance

round 50
day 1

score	185	170	35	395

status	0	0	0	0

commands
19
5	m	d	
13	m	l	
18	b	l	
1	b	l	
3	m	u	
15	m	d	
17	m	d	
2	m	d	
19	b	r	
6	m	l	
4	m	u	
7	m	d	
20	m	u	
8	m	r	
9	m	d	
22	m	u	
0	m	d	
10	m	r	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .CMMM......bCCb....F..........
01 .B....BBBBB...BBBBBBBBBBB....M
02 .B.....bC.B...B.....G....W....
03 .BBBBBBBBBB..WB..............W
04 ..............B...............
05 ...C....BBB...B.W.............
06 ........B.B...B.W............W
07 ........B.BG..B.BBBBBB........
08 .......BB.B...B.B.....M.M....C
09 ....CB....B....CBBBBBBBB......
10 .BBB.BBBC.BCC..........B......
11 .BFB...B.............C.B......
12 .B.M...B.............Z.B......
13 .BBBBBBB............M.C...W...
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	29	n	60
b	1	0	0	12	n	60
b	2	0	11	21	n	60
b	3	0	10	12	n	60
w	4	0	3	29	g	80
w	5	0	13	26	h	100
b	6	1	10	8	n	60
b	7	1	5	3	n	60
b	8	1	0	1	n	60
b	9	1	9	15	n	60
w	10	1	6	16	b	100
w	11	1	2	25	h	60
b	13	2	13	22	n	60
b	15	2	9	4	n	60
w	17	2	3	13	h	80
b	18	3	2	8	n	60
b	19	3	0	13	n	60
b	20	3	10	11	n	60
w	22	3	5	16	b	80
w	23	3	6	29	h	100

barricades
3
player	row	column	resistance
0	0	11	40
3	0	14	40
3	2	7	40

round 51
day 1

score	185	175	35	395

status	0	0	0	0

commands
20
1	m	d	
13	m	l	
18	m	l	
4	m	u	
19	m	r	
15	b	d	
6	m	d	
17	m	d	
20	m	u	
7	m	d	
22	m	l	
8	m	r	
9	m	d	
2	m	d	
5	m	d	
3	m	u	
0	m	l	
10	m	r	
11	m	u	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..CMM......b..c....F..........
01 .B....BBBBB.C.BBBBBBBBBBBW...M
02 .B.....c..B...B.....G........W
03 .BBBBBBBBBB...B...............
04 .............WB...............
05 ........BBB...BW..............
06 ...C....B.B...B..W............
07 ........B.BG..B.BBBBBB.......W
08 .......BB.B...B.B.....M.M...C.
09 ....CB....BCC...BBBBBBBB......
10 .BBBbBBB..B....C.......B......
11 .BFB...BC..............B......
12 .B.M...B.............C.B......
13 .BBBBBBB............MC........
14 .....G....................W...

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	28	n	60
b	1	0	1	12	n	60
b	2	0	12	21	n	60
b	3	0	9	12	n	60
w	4	0	2	29	g	80
w	5	0	14	26	h	100
b	6	1	11	8	n	60
b	7	1	6	3	n	60
b	8	1	0	2	n	60
b	9	1	10	15	n	60
w	10	1	6	17	b	100
w	11	1	1	25	h	60
b	13	2	13	21	n	60
b	15	2	9	4	n	60
w	17	2	4	13	h	80
b	18	3	2	7	n	60
b	19	3	0	14	n	60
b	20	3	9	11	n	60
w	22	3	5	15	b	80
w	23	3	7	29	h	100

barricades
4
player	row	column	resistance
0	0	11	40
3	0	14	40
3	2	7	40
2	10	4	40

round 52
day 1

score	185	180	35	395

status	0	0	0	0

commands
20
6	m	r	
18	m	l	
19	m	r	
7	m	u	
13	m	l	
15	m	d	
3	m	r	
1	m	u	
17	m	d	
20	m	u	
4	m	u	
8	m	r	
2	m	l	
9	m	d	
22	m	d	
10	m	l	
23	m	d	
11	m	u	
5	m	u	
0	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...CM......bC.bC...F.....W....
01 .B....BBBBB...BBBBBBBBBBB....W
02 .B....Cb..B...B.....G.........
03 .BBBBBBBBBB...B.........F.....
04 ..............B...............
05 ...C....BBB..WB...............
06 ........B.B...BWW.............
07 ........B.BG..B.BBBBBB........
08 .F.....BB.BC..B.B.....M.M..C.W
09 .....B....B..C..BBBBBBBB......
10 .BBBcBBB..B............B......
11 .BFB...B.C.....C.......B......
12 .B.M...B............C..B......
13 .BBBBBBB............C.....W...
14 .....G........................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	27	n	60
b	1	0	0	12	n	60
b	2	0	12	20	n	60
b	3	0	9	13	n	60
w	4	0	1	29	g	80
w	5	0	13	26	h	100
b	6	1	11	9	n	60
b	7	1	5	3	n	60
b	8	1	0	3	n	60
b	9	1	11	15	n	60
w	10	1	6	16	b	100
w	11	1	0	25	h	60
b	13	2	13	20	n	60
b	15	2	10	4	n	60
w	17	2	5	13	h	80
b	18	3	2	6	n	60
b	19	3	0	15	n	60
b	20	3	8	11	n	60
w	22	3	6	15	b	80
w	23	3	8	29	h	100

barricades
4
player	row	column	resistance
0	0	11	40
3	0	14	40
3	2	7	40
2	10	4	40

round 53
day 1

score	190	185	40	395

status	0	0	0	0

commands
18
6	m	d	
1	b	r	
13	m	r	
7	m	u	
15	m	d	
18	m	l	
8	m	l	
19	m	l	
3	m	r	
17	m	d	
9	m	u	
5	m	u	
20	m	u	
11	m	d	
2	m	u	
0	m	l	
22	m	d	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C.M......bCbc....F..........
01 .B....BBBBB...BBBBBBBBBBBW...W
02 .B...C.b..B...B.....G.........
03 .BBBBBBBBBB...B.........F.....
04 ...C..........B...............
05 ........BBB...B...............
06 ........B.B..WB.W.............
07 ........B.BC..BWBBBBBB........
08 .F.....BB.B...B.B.....M.M.C.W.
09 .....B....B...C.BBBBBBBB......
10 .BBBbBBB..B....C.......B......
11 .BFBC..B............C..B......
12 .B.M...B.C.............B..W...
13 .BBBBBBB.............C........
14 .....G........M...............

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	26	n	60
b	1	0	0	12	n	60
b	2	0	11	20	n	60
b	3	0	9	14	n	60
w	4	0	1	29	g	80
w	5	0	12	26	h	100
b	6	1	12	9	n	60
b	7	1	4	3	n	60
b	8	1	0	2	n	60
b	9	1	10	15	n	60
w	10	1	6	16	b	100
w	11	1	1	25	h	60
b	13	2	13	21	n	60
b	15	2	11	4	n	60
w	17	2	6	13	h	80
b	18	3	2	5	n	60
b	19	3	0	14	n	60
b	20	3	7	11	n	60
w	22	3	7	15	b	80
w	23	3	8	28	h	100

barricades
5
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
2	10	4	40

round 54
day 1

score	190	185	40	395

status	0	0	0	0

commands
20
18	m	u	
13	m	d	
15	m	d	
3	b	r	
19	m	r	
6	m	d	
1	m	d	
0	m	l	
7	m	d	
8	m	r	
20	m	d	
17	m	d	
22	m	d	
9	m	d	
10	m	l	
23	m	l	
5	m	u	
4	m	d	
2	m	d	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...CM......b.bbC...F..........
01 MB...CBBBBB.C.BBBBBBBBBBB.....
02 .B.....b..B...B.....G....W...W
03 .BBBBBBBBBB...B.........F.....
04 ..............B...............
05 ...C....BBB...B...............
06 ........B.B...BW..............
07 ........B.B..WB.BBBBBB........
08 .F.....BB.BC..BWB.....M.MC.W..
09 .....B....B...CbBBBBBBBB......
10 .BBBbBBB..B............B......
11 .BFB...B.......C.......B..W...
12 .B.MC..B............C..B......
13 .BBBBBBB.C....................
14 .....G........M......C........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	25	n	60
b	1	0	1	12	n	60
b	2	0	12	20	n	60
b	3	0	9	14	n	60
w	4	0	2	29	g	80
w	5	0	11	26	h	100
b	6	1	13	9	n	60
b	7	1	5	3	n	60
b	8	1	0	3	n	60
b	9	1	11	15	n	60
w	10	1	6	15	b	100
w	11	1	2	25	h	60
b	13	2	14	21	n	60
b	15	2	12	4	n	60
w	17	2	7	13	h	80
b	18	3	1	5	n	60
b	19	3	0	15	n	60
b	20	3	8	11	n	60
w	22	3	8	15	b	80
w	23	3	8	27	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 55
day 1

score	190	185	40	395

status	0	0	0	0

commands
18
6	m	d	
13	m	l	
18	m	u	
19	m	r	
4	m	u	
7	m	u	
0	m	l	
8	m	r	
20	m	d	
9	m	d	
17	m	d	
10	m	r	
23	m	l	
11	m	l	
5	m	u	
3	m	d	
2	m	u	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....CC.....bCbb.C..F..........
01 MB....BBBBB...BBBBBBBBBBB....W
02 .B.....b..B...B.....G...W.....
03 .BBBBBBBBBB...B.........F.....
04 ...C..........B...............
05 ........BBB...B...............
06 ........B.B...B.W.............
07 ........B.B...B.BBBBBB........
08 .F.....BB.B..WBWB.....M.C.W...
09 .....B....BC...bBBBBBBBB......
10 .BBBbBBB..B...C........B..W...
11 .BFB...B............C..B.....M
12 .B.MC..B.......C.......B......
13 .BBBBBBB......................
14 .....G...C....M.....C.........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	24	n	60
b	1	0	0	12	n	60
b	2	0	11	20	n	60
b	3	0	10	14	n	60
w	4	0	1	29	g	80
w	5	0	10	26	h	100
b	6	1	14	9	n	60
b	7	1	4	3	n	60
b	8	1	0	4	n	60
b	9	1	12	15	n	60
w	10	1	6	16	b	100
w	11	1	2	24	h	60
b	13	2	14	20	n	60
b	15	2	12	4	n	60
w	17	2	8	13	h	80
b	18	3	0	5	n	60
b	19	3	0	16	n	60
b	20	3	9	11	n	60
w	22	3	8	15	b	80
w	23	3	8	26	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 56
day 1

score	195	190	40	395

status	0	0	0	0

commands
19
6	m	r	
18	m	d	
7	m	l	
8	m	l	
13	m	l	
9	m	d	
2	m	r	
19	m	r	
10	m	r	
4	m	u	
11	m	d	
20	m	d	
17	m	d	
0	m	l	
22	m	u	
5	m	r	
3	m	d	
23	m	l	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C.......b.bb..C.F.M.......W
01 MB...CBBBBB.C.BBBBBBBBBBB.....
02 .B.....b..B...B.....G.........
03 .BBBBBBBBBB...B.........W.....
04 ..C...........B...............
05 ........BBB...B...............
06 ........B.B...B..W............
07 ........B.B...BWBBBBBB........
08 .F.....BB.B...B.B.....MC.W....
09 .....B....B..W.bBBBBBBBB......
10 .BBBbBBB..BC...........B...W..
11 .BFB...B......C......C.B.....M
12 MB.MC..B...............B......
13 .BBBBBBB.......C.........M....
14 .....G....C...M....C..........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	23	n	60
b	1	0	1	12	n	60
b	2	0	11	21	n	60
b	3	0	11	14	n	60
w	4	0	0	29	g	80
w	5	0	10	27	h	100
b	6	1	14	10	n	60
b	7	1	4	2	n	60
b	8	1	0	3	n	60
b	9	1	13	15	n	60
w	10	1	6	17	b	100
w	11	1	3	24	h	80
b	13	2	14	19	n	60
b	15	2	12	4	n	60
w	17	2	9	13	h	80
b	18	3	1	5	n	60
b	19	3	0	17	n	60
b	20	3	10	11	n	60
w	22	3	7	15	b	80
w	23	3	8	25	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 57
day 1

score	195	190	40	395

status	0	0	0	0

commands
20
6	m	r	
7	m	l	
0	m	l	
8	m	l	
5	m	r	
4	m	l	
13	m	l	
18	m	l	
9	m	r	
3	m	d	
15	m	l	
2	m	r	
1	m	u	
19	m	r	
10	m	r	
17	m	d	
11	m	u	
20	m	d	
22	m	u	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C........bCbb...CF.M......W.
01 MB..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G...W.....
03 .BBBBBBBBBB...B...............
04 .C............B...............
05 ........BBB...B...............
06 ........B.B...BW..W...........
07 ........B.B...B.BBBBBB........
08 .F.....BB.B...B.B.....C.W.....
09 .....B....B....bBBBBBBBB......
10 .BBBbBBB..B..W.........B....W.
11 .BFB...B...C..........CB.....M
12 MB.C...B......C........B......
13 .BBBBBBB........C........M....
14 .....G.....C..M...C...........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	22	n	60
b	1	0	0	12	n	60
b	2	0	11	22	n	60
b	3	0	12	14	n	60
w	4	0	0	28	g	80
w	5	0	10	28	h	100
b	6	1	14	11	n	60
b	7	1	4	1	n	60
b	8	1	0	2	n	60
b	9	1	13	16	n	60
w	10	1	6	18	b	100
w	11	1	2	24	h	80
b	13	2	14	18	n	60
b	15	2	12	3	n	60
w	17	2	10	13	h	80
b	18	3	1	4	n	60
b	19	3	0	18	n	60
b	20	3	11	11	n	60
w	22	3	6	15	b	80
w	23	3	8	24	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 58
day 1

score	200	190	45	395

status	0	0	0	0

commands
19
13	m	l	
6	m	r	
15	m	r	
7	m	l	
19	m	r	
8	m	l	
5	m	r	
9	m	r	
10	m	r	
3	m	d	
17	m	d	
11	m	r	
20	m	d	
22	m	u	
23	m	l	
4	m	l	
2	m	d	
1	m	d	
0	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C.........b.bb....C.M.....W..
01 MB..C.BBBBB.C.BBBBBBBBBBB.....
02 .B.....b..B...B.....G....W....
03 .BBBBBBBBBB...B...............
04 C.............B...............
05 ........BBB...BW..............
06 ........B.B...B....W..........
07 ........B.B...B.BBBBBBC.......
08 .F.....BB.B.F.B.B......W......
09 .....B....B....bBBBBBBBB......
10 .BBBbBBB..B............B.....W
11 .BFB...B.....W.........B.....M
12 MB..C..B...C..........CB......
13 .BBBBBBB......C..C.......M....
14 .....G......C.M..C............

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	22	n	60
b	1	0	1	12	n	60
b	2	0	12	22	n	60
b	3	0	13	14	n	60
w	4	0	0	27	g	80
w	5	0	10	29	h	100
b	6	1	14	12	n	60
b	7	1	4	0	n	60
b	8	1	0	1	n	60
b	9	1	13	17	n	60
w	10	1	6	19	b	100
w	11	1	2	25	h	80
b	13	2	14	17	n	60
b	15	2	12	4	n	60
w	17	2	11	13	h	80
b	18	3	1	4	n	60
b	19	3	0	19	n	60
b	20	3	12	11	n	60
w	22	3	5	15	b	80
w	23	3	8	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 59
day 1

score	200	190	45	395

status	0	0	0	0

commands
18
6	m	r	
0	m	r	
5	m	d	
3	m	d	
19	m	r	
7	m	u	
13	m	l	
8	m	l	
15	m	u	
20	m	d	
22	m	r	
9	m	r	
17	m	d	
10	m	r	
11	m	l	
4	m	l	
2	m	d	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 C..........b.bb.MM..CM....W...
01 MB..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B.C.B.....G...W.....
03 CBBBBBBBBBB...B...............
04 ..............B...............
05 ........BBB...B.W.............
06 ........B.B...B.....W.........
07 ........B.B...B.BBBBBB.C......
08 .F.....BB.B.F.B.B......W......
09 .....B....B....bBBBBBBBB......
10 .BBBbBBB..B............B......
11 .BFBC..B...............B.....W
12 MB.....B.....W.........B......
13 .BBBBBBB...C......C...C..M....
14 .....G.......CC.C.............

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	23	n	60
b	1	0	2	12	n	60
b	2	0	13	22	n	60
b	3	0	14	14	n	60
w	4	0	0	26	g	80
w	5	0	11	29	h	100
b	6	1	14	13	n	60
b	7	1	3	0	n	60
b	8	1	0	0	n	60
b	9	1	13	18	n	60
w	10	1	6	20	b	100
w	11	1	2	24	h	80
b	13	2	14	16	n	60
b	15	2	11	4	n	60
w	17	2	12	13	h	80
b	18	3	1	4	n	60
b	19	3	0	20	n	60
b	20	3	13	11	n	60
w	22	3	5	16	b	80
w	23	3	8	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 60
day 1

score	210	190	45	395

status	0	0	0	0

commands
19
19	m	r	
13	m	r	
6	m	l	
0	m	u	
7	m	u	
8	m	d	
20	m	d	
15	m	u	
5	m	u	
9	m	d	
22	m	r	
10	m	r	
17	m	u	
1	m	r	
4	m	l	
3	m	u	
23	m	u	
2	m	r	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb.MM...C...W....
01 CB..C.BBBBB...BBBBBBBBBBB.....
02 CB.....b..B..CB.....G.........
03 .BBBBBBBBBB...B.........W.....
04 ..............B...............
05 ........BBB...B..W............
06 ........B.B...B......W.C......
07 ........B.B...B.BBBBBB.W......
08 .F.....BB.B.F.B.B.............
09 .....B....B....bBBBBBBBB......
10 .BBBcBBB..B............B.....W
11 .BFB...B.....W.........B......
12 MB.....B...............B......
13 .BBBBBBB......C........C.M....
14 .....G.....CC....CC...........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	6	23	n	60
b	1	0	2	13	n	60
b	2	0	13	23	n	60
b	3	0	13	14	n	60
w	4	0	0	25	g	80
w	5	0	10	29	h	100
b	6	1	14	12	n	60
b	7	1	2	0	n	60
b	8	1	1	0	n	60
b	9	1	14	18	n	60
w	10	1	6	21	b	100
w	11	1	3	24	h	80
b	13	2	14	17	n	60
b	15	2	10	4	n	60
w	17	2	11	13	h	80
b	18	3	1	4	n	60
b	19	3	0	21	n	60
b	20	3	14	11	n	60
w	22	3	5	17	b	80
w	23	3	7	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 61
day 1

score	210	195	45	400

status	0	0	0	0

commands
16
19	m	l	
4	m	l	
20	m	l	
22	m	r	
15	m	u	
17	m	u	
6	m	u	
7	m	d	
8	m	u	
5	m	u	
1	m	d	
9	m	r	
10	m	r	
2	m	r	
11	m	d	
3	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 C..........b.bb.MM..C...W.....
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G......M..
03 CBBBBBBBBBB..CB...............
04 ..............B.........W.....
05 ........BBB...B...W...........
06 ........B.B...B.......WC......
07 ........B.B...B.BBBBBB.W......
08 .F.....BB.B.F.B.B.............
09 ....CB....B....bBBBBBBBB.....W
10 .BBBbBBB..B..W.........B......
11 .BFB...B...............B......
12 MB.....B...............B.....M
13 .BBBBBBB....CC..........CM....
14 .....G....C......C.C..........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	6	23	n	60
b	1	0	3	13	n	60
b	2	0	13	24	n	60
b	3	0	13	13	n	60
w	4	0	0	24	g	80
w	5	0	9	29	h	100
b	6	1	13	12	n	60
b	7	1	3	0	n	60
b	8	1	0	0	n	60
b	9	1	14	19	n	60
w	10	1	6	22	b	100
w	11	1	4	24	h	80
b	13	2	14	17	n	60
b	15	2	9	4	n	60
w	17	2	10	13	h	80
b	18	3	1	4	n	60
b	19	3	0	20	n	60
b	20	3	14	10	n	60
w	22	3	5	18	b	80
w	23	3	7	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 62
day 1

score	210	195	45	400

status	0	0	0	0

commands
18
6	m	l	
19	m	l	
20	m	l	
13	m	r	
0	m	u	
15	m	l	
7	m	d	
1	m	u	
22	m	r	
2	m	r	
23	m	u	
5	m	d	
17	m	u	
8	m	d	
4	m	r	
9	m	r	
3	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb.MM.C.....W....
01 CB..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..CB.....G......M..
03 .BBBBBBBBBB...B...............
04 C.............B...............
05 ........BBB...B....W...CW.....
06 ........B.B...B.......WW......
07 ........B.B...B.BBBBBB........
08 .F.....BB.B.F.B.B.............
09 ...C.B....B..W.bBBBBBBBB......
10 .BBBbBBB..B............B.....W
11 .BFB...B...............B......
12 MB.....B.....C.........B.....M
13 .BBBBBBB...C.............C....
14 .....G...C........C.C.........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	23	n	60
b	1	0	2	13	n	60
b	2	0	13	25	n	60
b	3	0	12	13	n	60
w	4	0	0	25	g	80
w	5	0	10	29	h	100
b	6	1	13	11	n	60
b	7	1	4	0	n	60
b	8	1	1	0	n	60
b	9	1	14	20	n	60
w	10	1	6	22	b	100
w	11	1	5	24	h	80
b	13	2	14	18	n	60
b	15	2	9	3	n	60
w	17	2	9	13	h	80
b	18	3	1	4	n	60
b	19	3	0	19	n	60
b	20	3	14	9	n	60
w	22	3	5	19	b	80
w	23	3	6	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 63
day 1

score	215	195	45	400

status	0	0	0	0

commands
17
13	m	r	
2	m	r	
19	m	l	
20	m	l	
22	m	r	
15	m	l	
17	m	u	
6	m	l	
7	m	d	
0	m	u	
8	m	u	
5	m	d	
9	m	u	
10	m	u	
11	m	d	
4	m	r	
3	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 C..........b.bb.MMC.......W...
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..CB.....G......M..
03 .BBBBBBBBBB...B...............
04 ..............B........C......
05 C.......BBB...B.....W.W.......
06 ........B.B...B........WW.....
07 ........B.B...B.BBBBBB........
08 .F.....BB.B.FWB.B.............
09 ..C..B....B....bBBBBBBBB......
10 .BBBbBBB..B............B......
11 .BFB...B...............B.....W
12 MB.....B....C..........B.....M
13 .BBBBBBB..C.........C.....C...
14 ...M.G..C.......M..C..........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	4	23	n	60
b	1	0	2	13	n	60
b	2	0	13	26	n	60
b	3	0	12	12	n	60
w	4	0	0	26	g	80
w	5	0	11	29	h	100
b	6	1	13	10	n	60
b	7	1	5	0	n	60
b	8	1	0	0	n	60
b	9	1	13	20	n	60
w	10	1	5	22	b	100
w	11	1	6	24	h	80
b	13	2	14	19	n	60
b	15	2	9	2	n	60
w	17	2	8	13	h	80
b	18	3	1	4	n	60
b	19	3	0	18	n	60
b	20	3	14	8	n	60
w	22	3	5	20	b	80
w	23	3	6	23	h	100

barricades
6
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40

round 64
day 1

score	215	195	45	400

status	0	0	0	0

commands
19
19	m	l	
13	m	l	
6	m	d	
20	b	l	
22	m	u	
15	m	l	
23	m	l	
17	m	l	
0	m	d	
5	m	d	
7	m	d	
8	m	d	
9	m	r	
4	m	r	
3	m	r	
10	m	u	
11	m	d	
2	m	l	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb.MC.........W..
01 CB..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G......M..
03 .BBBBBBBBBB..CB...............
04 ........M.....B.....W.W.......
05 ........BBB...B........C......
06 C.....M.B.B...B.......W.......
07 ........B.B...B.BBBBBB..W.....
08 .F.....BB.B.W.B.B.............
09 .C...B....B....bBBBBBBBB......
10 .BBBbBBB..B............B......
11 .BFB...B...............B......
12 MB.....B.....C.........B.....W
13 .BBBBBBB.............C...C....
14 ...M.G.bC.C.....M.C...........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	23	n	60
b	1	0	3	13	n	60
b	2	0	13	25	n	60
b	3	0	12	13	n	60
w	4	0	0	27	g	80
w	5	0	12	29	h	100
b	6	1	14	10	n	60
b	7	1	6	0	n	60
b	8	1	1	0	n	60
b	9	1	13	21	n	60
w	10	1	4	22	b	100
w	11	1	7	24	h	80
b	13	2	14	18	n	60
b	15	2	9	1	n	60
w	17	2	8	12	h	100
b	18	3	1	4	n	60
b	19	3	0	17	n	60
b	20	3	14	8	n	60
w	22	3	4	20	b	80
w	23	3	6	22	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40
3	14	7	40

round 65
day 1

score	220	195	45	405

status	0	0	0	0

commands
17
13	m	l	
19	m	l	
20	m	l	
4	m	d	
22	m	d	
6	m	r	
5	m	l	
3	m	r	
7	m	d	
2	m	l	
23	m	u	
15	m	l	
8	m	d	
9	m	r	
1	m	d	
17	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb.C.............
01 .B..C.BBBBB...BBBBBBBBBBB..W..
02 CB.....b..B...B.....G......M..
03 .BBBBBBBBBB...B...............
04 ........M....CB.......W.......
05 ........BBB...B.....W.WC......
06 ......M.B.B...B...............
07 C.......B.B.W.B.BBBBBB........
08 .F.....BB.B...B.B.......W.....
09 C....B....B....bBBBBBBBB......
10 .BBBbBBB..B............B......
11 .BFB...B...............B......
12 MB.....B......C........B....W.
13 .BBBBBBB..............C.C.....
14 ..FM.G.c...C....MC............

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	23	n	60
b	1	0	4	13	n	60
b	2	0	13	24	n	60
b	3	0	12	14	n	60
w	4	0	1	27	g	80
w	5	0	12	28	h	100
b	6	1	14	11	n	60
b	7	1	7	0	n	60
b	8	1	2	0	n	60
b	9	1	13	22	n	60
w	10	1	4	22	b	100
w	11	1	8	24	h	80
b	13	2	14	17	n	60
b	15	2	9	0	n	60
w	17	2	7	12	h	100
b	18	3	1	4	n	60
b	19	3	0	16	n	60
b	20	3	14	7	n	60
w	22	3	5	20	b	80
w	23	3	5	22	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40
3	14	7	40

round 66
day 1

score	220	195	45	410

status	0	0	0	0

commands
18
5	m	r	
0	m	r	
6	m	r	
7	m	u	
4	m	d	
8	m	d	
13	m	l	
9	m	d	
3	m	r	
2	m	d	
1	m	l	
15	m	d	
11	m	d	
17	m	u	
19	m	r	
20	m	l	
22	m	d	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb..C............
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G......W..
03 CBBBBBBBBBB...B...............
04 ........M...C.B.......W.......
05 ........BBB...B......W..C.....
06 C.....M.B.B.W.B.....W.........
07 ........B.B...B.BBBBBB........
08 .F.....BB.B...B.B.............
09 .....BM...B....bBBBBBBBBW.....
10 CBBBbBBB..B............B......
11 .BFB...B...............B......
12 MB.....B.......C.......B.....W
13 .BBBBBBB......................
14 ..FM.GCb....C...C.....C.C.....

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	24	n	60
b	1	0	4	12	n	60
b	2	0	14	24	n	60
b	3	0	12	15	n	60
w	4	0	2	27	g	80
w	5	0	12	29	h	100
b	6	1	14	12	n	60
b	7	1	6	0	n	60
b	8	1	3	0	n	60
b	9	1	14	22	n	60
w	10	1	4	22	b	100
w	11	1	9	24	h	80
b	13	2	14	16	n	60
b	15	2	10	0	n	60
w	17	2	6	12	h	100
b	18	3	1	4	n	60
b	19	3	0	17	n	60
b	20	3	14	6	n	60
w	22	3	6	20	b	80
w	23	3	5	21	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40
3	14	7	40

round 67
day 1

score	225	195	50	410

status	0	0	0	0

commands
19
13	m	u	
19	m	r	
4	m	d	
0	m	l	
6	m	u	
15	m	d	
17	m	u	
20	m	l	
22	m	r	
23	m	u	
7	m	u	
8	m	d	
5	m	l	
3	m	u	
9	m	u	
2	m	u	
10	m	d	
1	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb...C...........
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G.........
03 .BBBBBBBBBB...B............W..
04 C.......M..C..B......W........
05 C.......BBB.W.B..F....WC......
06 ......M.B.B...B......W........
07 ........B.B...B.BBBBBB........
08 .F.....BB.B...B.B.......W.....
09 .....BM...B....bBBBBBBBB......
10 .BBBbBBB..B............B......
11 CBFB...B.......C.......B......
12 MB.....B...............B....W.
13 .BBBBBBB....C...C.....C.C.....
14 ..FM.C.b......................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	23	n	60
b	1	0	4	11	n	60
b	2	0	13	24	n	60
b	3	0	11	15	n	60
w	4	0	3	27	g	80
w	5	0	12	28	h	100
b	6	1	13	12	n	60
b	7	1	5	0	n	60
b	8	1	4	0	n	60
b	9	1	13	22	n	60
w	10	1	5	22	b	100
w	11	1	8	24	h	80
b	13	2	13	16	n	60
b	15	2	11	0	n	60
w	17	2	5	12	h	100
b	18	3	1	4	n	60
b	19	3	0	18	n	60
b	20	3	14	5	n	60
w	22	3	6	21	b	80
w	23	3	4	21	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	4	40
3	14	7	40

round 68
day 1

score	225	195	50	410

status	0	0	0	0

commands
19
4	m	r	
6	m	u	
13	m	u	
7	m	r	
15	b	u	
17	m	d	
19	m	r	
20	m	l	
22	m	r	
8	m	r	
23	m	u	
9	m	u	
5	m	d	
0	m	d	
3	m	l	
10	m	r	
2	m	d	
11	m	u	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....C..........
01 .B..C.BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....G.........
03 .BBBBBBBBBB...B......W......W.
04 .C......M.C...B...............
05 .C......BBB...B..F.....W......
06 ......M.B.B.W.B.......WC......
07 ........B.B...B.BBBBBB..W.....
08 .F...M.BB.B...B.B.............
09 .....BM...B....bBBBBBBBB......
10 bBBBbBBB..B............B......
11 CBFB..MB......C........B......
12 MB.....B....C...C.....CB......
13 .BBBBBBB....................W.
14 ..FMC..b................C.....

citizens
20
type	id	player	row	column	weapon	life
b	0	0	6	23	n	60
b	1	0	4	10	n	60
b	2	0	14	24	n	60
b	3	0	11	14	n	60
w	4	0	3	28	g	80
w	5	0	13	28	h	100
b	6	1	12	12	n	60
b	7	1	5	1	n	60
b	8	1	4	1	n	60
b	9	1	12	22	n	60
w	10	1	5	23	b	100
w	11	1	7	24	h	80
b	13	2	12	16	n	60
b	15	2	11	0	n	60
w	17	2	6	12	h	100
b	18	3	1	4	n	60
b	19	3	0	19	n	60
b	20	3	14	4	n	60
w	22	3	6	22	b	80
w	23	3	3	21	h	100

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
3	14	7	40

round 69
day 1

score	225	195	50	410

status	0	0	0	0

commands
19
18	m	u	
19	m	r	
13	m	l	
15	m	d	
4	m	l	
20	m	l	
17	m	d	
6	m	u	
0	m	d	
23	m	u	
7	m	d	
1	m	l	
8	m	r	
5	m	l	
9	m	u	
3	m	l	
2	m	u	
10	m	d	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C......b.bb.....C.........
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...B.....GW........
03 .BBBBBBBBBB..MB............W..
04 ..C.....MC....B...............
05 ........BBB...B..F............
06 .C....M.B.B...B.......WWW.....
07 ........B.B.W.B.BBBBBB.C......
08 .F...M.BB.B...B.B.............
09 .....BM...B....bBBBBBBBB......
10 bBBBbBBB..B............B......
11 .BFB..MB....CC........CB......
12 CB.....B.......C.......B......
13 .BBBBBBB................C..W..
14 ..FC...b......................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	23	n	60
b	1	0	4	9	n	60
b	2	0	13	24	n	60
b	3	0	11	13	n	60
w	4	0	3	27	g	80
w	5	0	13	27	h	100
b	6	1	11	12	n	60
b	7	1	6	1	n	60
b	8	1	4	2	n	60
b	9	1	11	22	n	60
w	10	1	6	23	b	100
w	11	1	6	24	h	80
b	13	2	12	15	n	60
b	15	2	12	0	n	60
w	17	2	7	12	h	100
b	18	3	0	4	n	60
b	19	3	0	20	n	60
b	20	3	14	3	n	60
w	22	3	6	22	b	80
w	23	3	2	21	h	100

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
3	14	7	40

round 70
day 1

score	225	195	55	415

status	0	0	0	0

commands
18
18	m	l	
13	m	u	
19	m	r	
15	b	d	
6	m	l	
20	m	r	
22	m	d	
1	m	l	
7	m	d	
5	m	d	
4	m	d	
17	m	r	
8	m	d	
3	m	u	
23	m	l	
2	m	l	
9	m	u	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C.......b.bb......C........
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM....W.........
03 .BBBBBBBBBB..MB...............
04 ........C.....B............W..
05 ..C.....BBB...B..F......W.....
06 ......M.B.B...B........W......
07 .C......B.B..WB.BBBBBBWC......
08 .F...M.BB.B...B.B.............
09 .....BM.M.B....bBBBBBBBB......
10 bBBBbBBB..B..C........CB......
11 .BFB..MB...C...C.......B......
12 CB.....B...............B......
13 bBBBBBBB...............C......
14 ..F.C..b...................W..

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	23	n	60
b	1	0	4	8	n	60
b	2	0	13	23	n	60
b	3	0	10	13	n	60
w	4	0	4	27	g	80
w	5	0	14	27	h	100
b	6	1	11	11	n	60
b	7	1	7	1	n	60
b	8	1	5	2	n	60
b	9	1	10	22	n	60
w	10	1	6	23	b	100
w	11	1	5	24	h	80
b	13	2	11	15	n	60
b	15	2	12	0	n	60
w	17	2	7	13	h	100
b	18	3	0	3	n	60
b	19	3	0	21	n	60
b	20	3	14	4	n	60
w	22	3	7	22	b	80
w	23	3	2	20	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 71
day 1

score	230	195	55	415

status	0	0	0	0

commands
18
18	m	l	
6	m	l	
5	m	l	
7	m	d	
13	m	d	
15	m	u	
17	m	u	
4	m	u	
19	m	r	
20	m	r	
8	m	d	
9	m	l	
23	m	d	
11	m	l	
3	m	u	
2	m	l	
1	m	l	
0	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C........b.bb.......C.......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM..............
03 .BBBBBBBBBB..MB.....W......W..
04 .......C......B...............
05 ........BBB...B..F.....W......
06 ..C...M.B.B..WB........W......
07 ........B.B...B.BBBBBBW.C.....
08 .C...M.BB.B...B.B.............
09 .....BM.M.B..C.bBBBBBBBB......
10 bBBBbBBB..B..........C.B......
11 CBFB..MB..C............B......
12 .B.....B.......C.......B......
13 bBBBBBBB..............C.......
14 ..F..C.b..................W...

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	24	n	60
b	1	0	4	7	n	60
b	2	0	13	22	n	60
b	3	0	9	13	n	60
w	4	0	3	27	g	80
w	5	0	14	26	h	100
b	6	1	11	10	n	60
b	7	1	8	1	n	60
b	8	1	6	2	n	60
b	9	1	10	21	n	60
w	10	1	6	23	b	100
w	11	1	5	23	h	80
b	13	2	12	15	n	60
b	15	2	11	0	n	60
w	17	2	6	13	h	100
b	18	3	0	2	n	60
b	19	3	0	22	n	60
b	20	3	14	5	n	60
w	22	3	7	22	b	80
w	23	3	3	20	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 72
day 1

score	230	195	55	415

status	0	0	0	0

commands
19
18	m	l	
19	m	r	
13	m	l	
6	m	l	
7	m	r	
5	m	l	
15	m	u	
4	m	u	
3	m	u	
20	m	r	
2	m	l	
22	m	r	
23	m	d	
1	m	d	
0	m	d	
17	m	u	
8	m	r	
9	m	l	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C.........b.bb........C......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM...........W..
03 .BBBBBBBBBB..MB...............
04 ..............B.....W.........
05 .......CBBB..WB..F....W.......
06 ...C..M.B.B...B........W......
07 ........B.B...B.BBBBBB.W......
08 ..C..M.BB.B..CB.B.......C.....
09 .....BM.M.B....bBBBBBBBB......
10 cBBBbBBB..B.........C..B......
11 .BFB..MB.C.............B......
12 .B.....B......C........B......
13 bBBBBBBB.............C........
14 ..F...Cb.................W....

citizens
20
type	id	player	row	column	weapon	life
b	0	0	8	24	n	60
b	1	0	5	7	n	60
b	2	0	13	21	n	60
b	3	0	8	13	n	60
w	4	0	2	27	g	80
w	5	0	14	25	h	100
b	6	1	11	9	n	60
b	7	1	8	2	n	60
b	8	1	6	3	n	60
b	9	1	10	20	n	60
w	10	1	6	23	b	100
w	11	1	5	22	h	80
b	13	2	12	14	n	60
b	15	2	10	0	n	60
w	17	2	5	13	h	100
b	18	3	0	1	n	60
b	19	3	0	23	n	60
b	20	3	14	6	n	60
w	22	3	7	23	b	80
w	23	3	4	20	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 73
day 1

score	230	195	55	415

status	0	0	0	0

commands
19
18	m	l	
13	m	l	
19	m	r	
20	m	r	
6	m	u	
22	m	d	
7	m	r	
5	m	u	
8	m	r	
4	m	u	
15	m	u	
23	m	d	
9	m	l	
17	m	u	
1	m	d	
10	m	d	
3	m	u	
2	m	l	
0	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C..........b.bb.....M...C.....
01 .B....BBBBB...BBBBBBBBBBB..W..
02 .B.....b..B...BM..............
03 .BBBBBBBBBB..MB...............
04 .............WB..F............
05 ........BBB...B..F..W.W.......
06 ....C.MCB.B...B...............
07 ........B.B..CB.BBBBBB.WC.....
08 ...C.M.BB.B...B.B......W......
09 C....BM.M.B....bBBBBBBBB......
10 bBBBbBBB.CB........C...B..M...
11 .BFB..MB...............B......
12 .B.....B.....C.........B......
13 bBBBBBBB............C....W....
14 ..F....c......................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	24	n	60
b	1	0	6	7	n	60
b	2	0	13	20	n	60
b	3	0	7	13	n	60
w	4	0	1	27	g	80
w	5	0	13	25	h	100
b	6	1	10	9	n	60
b	7	1	8	3	n	60
b	8	1	6	4	n	60
b	9	1	10	19	n	60
w	10	1	7	23	b	100
w	11	1	5	22	h	80
b	13	2	12	13	n	60
b	15	2	9	0	n	60
w	17	2	4	13	h	100
b	18	3	0	0	n	60
b	19	3	0	24	n	60
b	20	3	14	7	n	60
w	22	3	8	23	b	80
w	23	3	5	20	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 74
day 1

score	230	195	55	415

status	0	0	0	0

commands
20
13	m	u	
18	m	d	
5	m	l	
15	m	r	
6	m	u	
7	m	r	
4	m	u	
19	m	l	
8	m	r	
1	m	l	
3	m	u	
20	m	r	
2	m	u	
0	m	r	
17	m	u	
9	m	d	
22	m	r	
23	m	r	
10	m	r	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....MM..C...W..
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM..............
03 .BBBBBBBBBB..WB...............
04 ..............B..F............
05 ........BBB...B..F...W.W......
06 .....CC.B.B..CB...............
07 ........B.B...B.BBBBBB..WC....
08 ....CM.BB.B...B.B.......W.....
09 .C...BM.MCB....bBBBBBBBB......
10 bBBBbBBB..B............B..M...
11 .BFB..MB.....C.....C...B......
12 .B.....B............C..B......
13 bBBBBBBB................W.....
14 ..F....bC.....................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	7	25	n	60
b	1	0	6	6	n	60
b	2	0	12	20	n	60
b	3	0	6	13	n	60
w	4	0	0	27	g	80
w	5	0	13	24	h	100
b	6	1	9	9	n	60
b	7	1	8	4	n	60
b	8	1	6	5	n	60
b	9	1	11	19	n	60
w	10	1	7	24	b	100
w	11	1	5	23	h	80
b	13	2	11	13	n	60
b	15	2	9	1	n	60
w	17	2	3	13	h	100
b	18	3	1	0	n	60
b	19	3	0	23	n	60
b	20	3	14	8	n	60
w	22	3	8	24	b	80
w	23	3	5	21	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 75
day 0

score	235	195	60	415

status	0	0	0	0

commands
20
13	m	l	
18	m	d	
19	m	l	
6	m	l	
20	m	u	
22	m	r	
0	m	u	
23	m	r	
5	m	l	
7	m	r	
15	m	r	
17	m	d	
8	m	d	
4	m	l	
3	m	u	
2	m	r	
1	m	d	
9	m	d	
10	m	r	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....MM.C...W...
01 .B....BBBBB...BBBBBBBBBBB.....
02 CB.....b..B...BM..............
03 .BBBBBBBBBB...B...............
04 .............WB..F............
05 ........BBB..CB..F....W.W.....
06 ........B.B...B..........C....
07 .....CC.B.B...B.BBBBBB...W....
08 .....C.BB.B...B.B........W....
09 ..C..BM.C.B....bBBBBBBBB......
10 bBBBbBBB..B............B..M...
11 .BFB..MB....C..........B......
12 .B.....B...........C.C.B......
13 bBBBBBBBC..............W......
14 ..F....b......................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	6	25	n	60
b	1	0	7	6	n	60
b	2	0	12	21	n	60
b	3	0	5	13	n	60
w	4	0	0	26	g	80
w	5	0	13	23	h	100
b	6	1	9	8	n	60
b	7	1	8	5	n	60
b	8	1	7	5	n	60
b	9	1	12	19	n	60
w	10	1	7	25	b	100
w	11	1	5	24	h	80
b	13	2	11	12	n	60
b	15	2	9	2	n	60
w	17	2	4	13	h	100
b	18	3	2	0	n	60
b	19	3	0	22	n	60
b	20	3	13	8	n	60
w	22	3	8	25	b	80
w	23	3	5	22	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 76
day 0

score	235	205	60	415

status	0	0	0	0

commands
20
18	m	d	
13	m	d	
15	m	r	
19	m	l	
20	m	d	
6	m	d	
17	m	d	
3	m	d	
22	m	l	
0	m	u	
23	m	r	
7	m	l	
5	m	l	
4	m	l	
8	m	u	
9	m	d	
10	m	u	
11	m	r	
1	m	d	
2	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....MMC...W....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM..............
03 CBBBBBBBBBB...B...............
04 .............WB..F............
05 ........BBB...B..F.....WWC....
06 .....C..B.B..CB..........W....
07 ........B.B...B.BBBBBB........
08 ....C.CBB.B...B.B.......W.....
09 ...C.BM...B....bBBBBBBBB......
10 bBBBbBBBC.B............B..M...
11 .BFB..MB...............B......
12 .B.....B....C.........CB......
13 bBBBBBBB...........C..W.......
14 ..F....bC.....................

citizens
20
type	id	player	row	column	weapon	life
b	0	0	5	25	n	40
b	1	0	8	6	n	60
b	2	0	12	22	n	60
b	3	0	6	13	n	40
w	4	0	0	25	g	80
w	5	0	13	22	h	100
b	6	1	10	8	n	60
b	7	1	8	4	n	60
b	8	1	6	5	n	60
b	9	1	13	19	n	60
w	10	1	6	25	b	100
w	11	1	5	24	h	80
b	13	2	12	12	n	60
b	15	2	9	3	n	60
w	17	2	4	13	h	100
b	18	3	3	0	n	60
b	19	3	0	21	n	60
b	20	3	14	8	n	60
w	22	3	8	24	b	80
w	23	3	5	23	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 77
day 0

score	235	205	60	415

status	0	0	0	0

commands
20
3	m	d	
6	m	u	
7	m	u	
18	m	d	
19	m	l	
20	m	u	
13	m	u	
8	m	u	
22	m	u	
9	m	d	
0	m	u	
15	m	l	
17	m	d	
10	m	u	
11	m	u	
5	m	l	
4	m	l	
23	m	r	
1	m	d	
2	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....MC...W.....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM..............
03 .BBBBBBBBBB...B...............
04 C.............B..F......WC....
05 .....C..BBB..WB..F......WW....
06 ........B.B...B...............
07 ....C...B.B..CB.BBBBBB..W.....
08 .......BB.B...B.B.............
09 ..C..BC.C.B....bBBBBBBBB......
10 bBBBbBBB..B............B..M...
11 .BFB..MB....C..........B......
12 .B.....B...............B......
13 bBBBBBBBC............WC.......
14 ..F....b...........C..........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	4	25	n	40
b	1	0	9	6	n	60
b	2	0	13	22	n	60
b	3	0	7	13	n	40
w	4	0	0	24	g	80
w	5	0	13	21	h	100
b	6	1	9	8	n	60
b	7	1	7	4	n	60
b	8	1	5	5	n	60
b	9	1	14	19	n	60
w	10	1	5	25	b	100
w	11	1	4	24	h	80
b	13	2	11	12	n	60
b	15	2	9	2	n	60
w	17	2	5	13	h	100
b	18	3	4	0	n	60
b	19	3	0	20	n	60
b	20	3	13	8	n	60
w	22	3	7	24	b	80
w	23	3	5	24	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 78
day 0

score	240	205	60	420

status	0	0	0	0

commands
20
6	m	l	
18	m	r	
7	m	u	
3	m	d	
0	m	u	
13	m	d	
15	m	r	
8	m	u	
19	m	l	
5	m	d	
4	m	l	
9	m	l	
20	m	r	
10	m	u	
2	m	r	
22	m	u	
1	m	u	
23	m	u	
17	m	d	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....C...W......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM..............
03 .BBBBBBBBBB...B..........C....
04 .C...C........B..F.....W.W..MM
05 ........BBB...B..F......W.....
06 ....C...B.B..WB.........W.....
07 ........B.B...B.BBBBBB........
08 ......CBB.B..CB.B.............
09 ...C.B.C..B....bBBBBBBBB......
10 bBBBbBBB..B............B..M...
11 .BFB..MB...............B......
12 .B.....B....C..........B......
13 bBBBBBBB.C.............C......
14 ..F....b..........C..W........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	3	25	n	40
b	1	0	8	6	n	60
b	2	0	13	23	n	60
b	3	0	8	13	n	40
w	4	0	0	23	g	80
w	5	0	14	21	h	100
b	6	1	9	7	n	60
b	7	1	6	4	n	60
b	8	1	4	5	n	60
b	9	1	14	18	n	60
w	10	1	4	25	b	100
w	11	1	4	23	h	60
b	13	2	12	12	n	60
b	15	2	9	3	n	60
w	17	2	6	13	h	100
b	18	3	4	1	n	60
b	19	3	0	19	n	60
b	20	3	13	9	n	60
w	22	3	6	24	b	80
w	23	3	5	24	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 79
day 0

score	240	205	60	425

status	0	0	0	0

commands
19
6	m	r	
13	m	d	
7	m	u	
15	m	r	
3	m	d	
17	m	d	
0	m	u	
8	m	r	
18	m	d	
20	m	r	
5	m	l	
22	m	l	
23	m	u	
9	m	u	
4	m	l	
2	m	r	
10	m	u	
11	m	u	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....C..W.......
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B.....b..B...BM.........C....
03 .BBBBBBBBBB...B........W.W....
04 ......C.......B..F..M...W...MM
05 .C..C...BBB...B..F............
06 ........B.B...B........W......
07 ........B.B..WB.BBBBBB........
08 .....C.BB.B...B.B.............
09 ....CB..C.B..C.bBBBBBBBB......
10 bBBBbBBB..B...........MB..M...
11 .BFB..MB...............B......
12 .B.....B...............B......
13 bBBBBBBB..C.C.....C.....C.....
14 ..F....b............W.........

citizens
20
type	id	player	row	column	weapon	life
b	0	0	2	25	n	40
b	1	0	8	5	n	60
b	2	0	13	24	n	60
b	3	0	9	13	n	40
w	4	0	0	22	g	80
w	5	0	14	20	h	100
b	6	1	9	8	n	60
b	7	1	5	4	n	60
b	8	1	4	6	n	60
b	9	1	13	18	n	60
w	10	1	3	25	b	100
w	11	1	3	23	h	60
b	13	2	13	12	n	60
b	15	2	9	4	n	60
w	17	2	7	13	h	100
b	18	3	5	1	n	60
b	19	3	0	19	n	60
b	20	3	13	10	n	60
w	22	3	6	23	b	80
w	23	3	4	24	g	100

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 80
day 0

score	240	205	60	425

status	0	0	0	0

commands
19
3	m	r	
18	m	u	
20	m	u	
13	m	r	
0	m	u	
15	m	d	
17	m	d	
22	m	u	
6	m	d	
23	m	u	
7	m	r	
8	m	r	
5	m	u	
4	m	l	
2	m	u	
1	m	l	
9	m	l	
10	m	u	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb....C.W........
01 .B....BBBBB...BBBBBBBBBBBC....
02 .B.....b..B...BM.........W....
03 .BBBBBBBBBB...B.......W.W.....
04 .C.....C......B..F..M.......MM
05 .....C..BBB...B..F.....W......
06 ........B.B...B...............
07 ........B.B...B.BBBBBB........
08 ....C..BB.B..WB.B.......C.....
09 .....B....B...CbBBBBBBBB......
10 bBBBcBBBC.B...........MB..MF..
11 .BFB..MB...............B......
12 .B.....B..C............BC.....
13 bBBBBBBB.....C...C..W.........
14 ..F....b......................

citizens
21
type	id	player	row	column	weapon	life
b	0	0	1	25	n	40
b	1	0	8	4	n	60
b	2	0	12	24	n	60
b	3	0	9	14	n	40
w	4	0	0	21	g	80
w	5	0	13	20	h	100
b	6	1	10	8	n	60
b	7	1	5	5	n	60
b	8	1	4	7	n	60
b	9	1	13	17	n	60
w	10	1	2	25	b	100
w	11	1	3	22	h	60
b	13	2	13	13	n	60
b	15	2	10	4	n	60
w	17	2	8	13	h	100
b	18	3	4	1	n	60
b	19	3	0	19	n	60
b	20	3	12	10	n	60
w	22	3	5	23	b	80
w	23	3	3	24	g	100
b	25	2	8	24	n	60

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 81
day 0

score	240	205	60	425

status	0	0	0	0

commands
20
13	m	r	
25	m	d	
3	m	r	
6	m	d	
18	m	d	
0	m	u	
5	m	l	
17	m	d	
7	m	d	
19	m	l	
4	m	l	
2	m	u	
20	m	u	
22	m	u	
23	m	l	
8	m	r	
1	m	d	
9	m	u	
10	m	u	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb...C.W....C....
01 .B.M..BBBBB...BBBBBBBBBBBW....
02 .B.....b..B...BM..............
03 .BBBBBBBBBB...B......W.W......
04 ........C.....B..FC.M..W....MM
05 .C......BBB...BM.F............
06 .....C..B.B...B...............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 ....CB....B..W.cBBBBBBBBC.....
10 bBBBcBBB..B...........MB..MF..
11 .BFB..MBC.C............BC.....
12 .B.....B.........C.....B......
13 bBBBBBBB......C....W..........
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	0	25	n	40
b	1	0	9	4	n	60
b	2	0	11	24	n	60
b	3	0	9	15	n	40
w	4	0	0	20	g	80
w	5	0	13	19	h	100
b	6	1	11	8	n	60
b	7	1	6	5	n	60
b	8	1	4	8	n	60
b	9	1	12	17	n	60
w	10	1	1	25	b	100
w	11	1	3	21	h	60
b	13	2	13	14	n	60
b	15	2	10	4	n	60
w	17	2	9	13	h	100
b	18	3	5	1	n	60
b	19	3	0	18	n	60
b	20	3	11	10	n	60
w	22	3	4	23	b	80
w	23	3	3	23	g	100
b	25	2	9	24	n	60
b	26	2	4	18	n	60

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	40
2	13	0	40
3	14	7	40

round 82
day 0

score	240	205	60	425

status	0	0	0	0

commands
21
18	m	u	
3	m	u	
19	m	l	
6	m	d	
7	m	u	
20	m	d	
22	m	l	
23	m	l	
13	m	r	
8	m	r	
9	m	l	
25	m	r	
0	m	r	
10	m	u	
5	m	u	
4	m	l	
26	m	r	
11	m	l	
17	m	r	
2	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb..C.W.....WC...
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..MBM..............
03 .BBBBBBBBBB...B.....W.W.......
04 .C.......C....B..F.CM.W.....MM
05 .....C..BBB...BM.F............
06 ........B.B...B...............
07 ........B.B...B.BBBBBB........
08 .......BB.B...BCB.............
09 ....CB....B...WbBBBBBBBB.C....
10 bBBBcBBB..B...........MBC.MF..
11 .BFB..MB...............B......
12 .B.....BC.C.....C..W...B......
13 bBBBBBBB.......C..............
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	0	26	n	40
b	1	0	9	4	n	60
b	2	0	10	24	n	60
b	3	0	8	15	n	40
w	4	0	0	19	g	80
w	5	0	12	19	h	100
b	6	1	12	8	n	60
b	7	1	5	5	n	60
b	8	1	4	9	n	60
b	9	1	12	16	n	60
w	10	1	0	25	b	100
w	11	1	3	20	h	60
b	13	2	13	15	n	60
b	15	2	10	4	n	60
w	17	2	9	14	h	100
b	18	3	4	1	n	60
b	19	3	0	17	n	60
b	20	3	12	10	n	60
w	22	3	4	22	b	80
w	23	3	3	22	g	100
b	25	2	9	25	n	60
b	26	2	4	19	n	60

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	30
2	13	0	40
3	14	7	40

round 83
day 0

score	240	205	60	425

status	0	0	0	0

commands
21
18	m	l	
3	m	u	
6	m	u	
19	m	l	
20	m	u	
13	m	l	
7	m	l	
0	m	r	
22	m	l	
23	m	l	
5	m	l	
25	m	r	
26	m	d	
4	m	l	
8	m	r	
17	m	d	
2	m	r	
1	m	d	
9	m	d	
10	m	r	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bb.C.W.......WC..
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..MBM..............
03 .BBBBBBBBBB...B....W.W........
04 C.........C...B..F..MW......MM
05 ....C...BBB...BM.F.C..........
06 ........B.B...B...............
07 ........B.B...BCBBBBBB........
08 .......BB.B...B.B.............
09 ....CB....B....bBBBBBBBB..C...
10 bBBBcBBB..B...W.......MB.CMF..
11 .BFB..MBC.C............B......
12 .B.....B..........W....B......
13 bBBBBBBB......C.C.............
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	0	27	n	40
b	1	0	9	4	n	60
b	2	0	10	25	n	60
b	3	0	7	15	n	40
w	4	0	0	18	g	80
w	5	0	12	18	h	100
b	6	1	11	8	n	60
b	7	1	5	4	n	60
b	8	1	4	10	n	60
b	9	1	13	16	n	60
w	10	1	0	26	b	100
w	11	1	3	19	h	60
b	13	2	13	14	n	60
b	15	2	10	4	n	60
w	17	2	10	14	h	100
b	18	3	4	0	n	60
b	19	3	0	16	n	60
b	20	3	11	10	n	60
w	22	3	4	21	b	80
w	23	3	3	21	g	100
b	25	2	9	26	n	60
b	26	2	5	19	n	60

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	20
2	13	0	40
3	14	7	40

round 84
day 0

score	240	205	60	425

status	0	0	0	0

commands
21
0	m	r	
13	m	u	
6	m	d	
7	m	d	
18	m	u	
19	m	l	
20	m	d	
8	m	r	
22	m	d	
5	m	d	
23	m	l	
4	m	l	
9	m	l	
3	m	u	
2	m	r	
1	m	d	
25	m	r	
26	m	l	
17	m	r	
10	m	r	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bbC.W.........WC.
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..MBM..............
03 CBBBBBBBBBB...B...W.W.........
04 ...........C..B..F..M.......MM
05 ........BBB...BM.FC..W........
06 ....C...B.B...BC..............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 ....CB....B....bBBBBBBBB...C..
10 bBBBcBBB..B....W......MB..CF..
11 .BFB..MB...............B......
12 .B.....BC.C...C........B......
13 bBBBBBBB.......C..W...........
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	0	28	n	40
b	1	0	9	4	n	60
b	2	0	10	26	n	60
b	3	0	6	15	n	40
w	4	0	0	17	g	80
w	5	0	13	18	h	100
b	6	1	12	8	n	60
b	7	1	6	4	n	60
b	8	1	4	11	n	60
b	9	1	13	15	n	60
w	10	1	0	27	b	100
w	11	1	3	18	h	60
b	13	2	12	14	n	60
b	15	2	10	4	n	60
w	17	2	10	15	h	100
b	18	3	3	0	n	60
b	19	3	0	15	n	60
b	20	3	12	10	n	60
w	22	3	5	21	b	80
w	23	3	3	20	g	100
b	25	2	9	27	n	60
b	26	2	5	18	n	60

barricades
9
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	10	4	10
2	13	0	40
3	14	7	40

round 85
day 0

score	245	205	60	425

status	0	0	0	0

commands
21
6	m	u	
7	m	d	
0	m	r	
8	m	d	
5	m	l	
4	m	l	
13	m	u	
9	m	u	
3	m	u	
18	m	u	
19	m	l	
20	m	u	
25	m	r	
26	m	d	
10	m	r	
2	m	r	
11	m	l	
1	m	d	
22	m	l	
17	m	d	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...........b.bc.W...........WC
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 CB.....b..B..MBM..............
03 .BBBBBBBBBB...B..W.W..........
04 ..............B..F..M.......MM
05 ........BBBC..BC.F..W.........
06 ........B.B...B...C...........
07 ....C...B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 ....CB....B....bBBBBBBBB....C.
10 bBBBCBBB..B...........MB...C..
11 .BFB..MBC.C...CW.......B......
12 .B.....B.......C.......B......
13 bBBBBBBB.........W............
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	0	29	n	40
b	1	0	9	4	n	60
b	2	0	10	27	n	60
b	3	0	5	15	n	40
w	4	0	0	16	g	80
w	5	0	13	17	h	100
b	6	1	11	8	n	60
b	7	1	7	4	n	60
b	8	1	5	11	n	60
b	9	1	12	15	n	60
w	10	1	0	28	b	100
w	11	1	3	17	h	60
b	13	2	11	14	n	60
b	15	2	10	4	n	60
w	17	2	11	15	h	100
b	18	3	2	0	n	60
b	19	3	0	14	n	60
b	20	3	11	10	n	60
w	22	3	5	20	b	80
w	23	3	3	19	g	100
b	25	2	9	28	n	60
b	26	2	6	18	n	60

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 86
day 0

score	250	205	60	425

status	0	0	0	0

commands
22
0	m	d	
13	m	u	
18	m	u	
15	m	d	
5	m	u	
4	m	l	
19	m	r	
20	m	d	
22	m	d	
6	m	d	
7	m	d	
3	m	r	
2	m	u	
23	m	l	
1	m	d	
8	m	d	
9	m	l	
10	m	r	
25	m	u	
26	m	l	
17	m	d	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....G.....b.bcW.............W
01 CB.M..BBBBB...BBBBBBBBBBB....C
02 .B.....b..B..MBM..............
03 .BBBBBBBBBB...B.W.W...........
04 ..............B..F..M.......MM
05 ........BBB...B.CF............
06 ........B.BC..B..C..W.........
07 ........B.B...B.BBBBBB........
08 ....C..BB.B...B.B...........C.
09 .....B....B....bBBBBBBBB...C..
10 bBBBCBBB..B...C.......MB......
11 .BFBC.MB...............B......
12 .B.....BC.C...CW.W.....B......
13 bBBBBBBB......................
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	1	29	n	40
b	1	0	10	4	n	60
b	2	0	9	27	n	60
b	3	0	5	16	n	40
w	4	0	0	15	g	80
w	5	0	12	17	h	100
b	6	1	12	8	n	60
b	7	1	8	4	n	60
b	8	1	6	11	n	60
b	9	1	12	14	n	60
w	10	1	0	29	b	100
w	11	1	3	16	h	60
b	13	2	10	14	n	60
b	15	2	11	4	n	60
w	17	2	12	15	h	100
b	18	3	1	0	n	60
b	19	3	0	14	n	40
b	20	3	12	10	n	60
w	22	3	6	20	b	80
w	23	3	3	18	g	100
b	25	2	8	28	n	60
b	26	2	6	17	n	60

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 87
day 0

score	250	205	60	425

status	0	0	0	0

commands
21
3	m	r	
0	m	d	
6	m	u	
5	m	l	
4	m	l	
7	m	r	
13	m	u	
8	m	d	
18	m	u	
9	m	d	
15	m	r	
10	m	d	
20	m	u	
25	m	u	
22	m	l	
2	m	u	
23	m	l	
26	m	l	
11	m	l	
1	m	d	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....G.....b.bcW..............
01 .B.M..BBBBB...BBBBBBBBBBB....W
02 .B.....b..B..MBM.............C
03 .BBBBBBBBBB...BW.W............
04 ..............B..F..M.......MM
05 ........BBB...B..C............
06 ........B.B...B.C..W..........
07 ........B.BC..B.BBBBBB......C.
08 .....C.BB.B...B.B..........C..
09 .....B....B...CbBBBBBBBB......
10 bBBB.BBB..B...........MB......
11 .BFBCCMBC.C............B......
12 .B.....B......W.W......B......
13 bBBBBBBB......C...............
14 ..F....b......................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	2	29	n	40
b	1	0	11	4	n	60
b	2	0	8	27	n	60
b	3	0	5	17	n	60
w	4	0	0	15	g	80
w	5	0	12	16	h	100
b	6	1	11	8	n	60
b	7	1	8	5	n	60
b	8	1	7	11	n	60
b	9	1	13	14	n	60
w	10	1	1	29	b	100
w	11	1	3	15	h	60
b	13	2	9	14	n	60
b	15	2	11	5	n	60
w	17	2	12	14	h	100
b	18	3	0	0	n	60
b	19	3	0	14	n	40
b	20	3	11	10	n	60
w	22	3	6	19	b	80
w	23	3	3	17	g	100
b	25	2	7	28	n	60
b	26	2	6	16	n	60

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	30
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 88
day 0

score	250	205	60	425

status	0	0	0	0

commands
21
18	m	r	
3	m	r	
6	m	d	
20	m	d	
0	m	d	
22	m	u	
5	m	d	
23	m	d	
4	m	l	
13	m	l	
15	m	r	
1	m	r	
7	m	r	
8	m	d	
2	m	u	
9	m	l	
10	m	d	
11	m	d	
25	m	u	
26	m	l	
17	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C...G.....b.bcW..............
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..MBM.............W
03 .BBBBBBBBBB...B..............C
04 .....M........BW.W..M.......MM
05 ........BBB...B...CW..........
06 ........B.B...BC............C.
07 ........B.B...B.BBBBBB.....C..
08 ......CBB.BC..B.B.............
09 .....B....B..C.bBBBBBBBB......
10 bBBB.BBB..B...........MB......
11 .BFB.CCB...............B......
12 .B.....BC.C............B......
13 bBBBBBBB.....CW.W..........C..
14 ..F....b......................

citizens
23
type	id	player	row	column	weapon	life
b	0	0	3	29	n	40
b	1	0	11	5	n	60
b	2	0	7	27	n	60
b	3	0	5	18	n	60
w	4	0	0	15	g	80
w	5	0	13	16	h	100
b	6	1	12	8	n	60
b	7	1	8	6	n	60
b	8	1	8	11	n	60
b	9	1	13	13	n	60
w	10	1	2	29	b	100
w	11	1	4	15	h	60
b	13	2	9	13	n	60
b	15	2	11	6	n	60
w	17	2	13	14	h	100
b	18	3	0	1	n	60
b	19	3	0	14	n	40
b	20	3	12	10	n	60
w	22	3	5	19	b	80
w	23	3	4	17	g	100
b	25	2	6	28	n	60
b	26	2	6	15	n	60
b	27	3	13	27	n	60

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	20
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 89
day 0

score	250	205	65	425

status	0	0	0	0

commands
22
3	m	u	
13	m	d	
15	m	d	
18	m	r	
6	m	u	
25	m	u	
26	m	d	
7	m	u	
8	m	d	
0	m	d	
17	m	l	
9	m	d	
20	m	u	
10	m	d	
5	m	l	
11	m	d	
27	m	l	
4	m	l	
22	m	l	
23	m	d	
2	m	u	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C..GZ....b.bcW..............
01 .B.M..BBBBB...BBBBBBBBBBB.....
02 .B.....b..B..MBM..............
03 .BBBBBBBBBB...B..............W
04 .....M........B...C.M.W.....MC
05 ........BBB...BW.WW.........C.
06 ........B.B...B............C..
07 ......C.B.B...BCBBBBBB........
08 .......BB.B...B.B.............
09 .....B....BC...bBBBBBBBB......
10 bBBB.BBB..B..C.......MMB......
11 .BFBC..BC.C............B......
12 .B....CB...............B......
13 bBBBBBBB......WW..........C...
14 ..F....b.....C................

citizens
24
type	id	player	row	column	weapon	life
b	0	0	4	29	n	40
b	1	0	11	4	n	60
b	2	0	6	27	n	60
b	3	0	4	18	n	60
w	4	0	0	15	g	80
w	5	0	13	15	h	100
b	6	1	11	8	n	60
b	7	1	7	6	n	60
b	8	1	9	11	n	60
b	9	1	14	13	n	40
w	10	1	3	29	b	100
w	11	1	5	15	h	60
b	13	2	10	13	n	60
b	15	2	12	6	n	60
w	17	2	13	14	h	100
b	18	3	0	2	n	60
b	19	3	0	14	n	40
b	20	3	11	10	n	60
w	22	3	5	18	b	80
w	23	3	5	17	g	100
b	25	2	5	28	n	60
b	26	2	7	15	n	60
b	27	3	13	26	n	60
w	28	2	4	22	h	100

barricades
8
player	row	column	resistance
0	0	11	40
0	0	13	40
3	0	14	10
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 90
day 0

score	255	205	65	425

status	0	0	0	0

commands
23
13	m	r	
6	m	d	
7	m	u	
15	m	l	
25	m	u	
18	m	r	
20	m	d	
3	m	u	
8	m	r	
27	m	l	
0	m	d	
5	m	d	
9	m	l	
26	m	d	
10	m	d	
22	m	u	
11	m	d	
4	m	l	
23	m	u	
17	m	r	
28	m	l	
2	m	u	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C.GZ....b.bCW..............
01 .B.M..BBBBB...BBBBBBBBBBB....Z
02 .B.....b..B..MBM..............
03 .BBBBBBBBBB...B...C...........
04 .....M........B..WW.MW......CW
05 ........BBB...B............C.C
06 ......C.B.B...BW..............
07 ........B.B...B.BBBBBB........
08 .......BB.B...BCB.............
09 .....B....B.C..bBBBBBBBB......
10 bBBBCBBB..B...C......MMB......
11 .BFB...B...............B......
12 .B...C.BC.C............B......
13 bBBBBBBB.......W.........C....
14 ..F....b....C..W..............

citizens
24
type	id	player	row	column	weapon	life
b	0	0	5	29	n	40
b	1	0	10	4	n	60
b	2	0	5	27	n	60
b	3	0	3	18	n	60
w	4	0	0	15	g	80
w	5	0	14	15	h	100
b	6	1	12	8	n	60
b	7	1	6	6	n	60
b	8	1	9	12	n	60
b	9	1	14	12	n	40
w	10	1	4	29	b	100
w	11	1	6	15	h	60
b	13	2	10	14	n	60
b	15	2	12	5	n	60
w	17	2	13	15	h	100
b	18	3	0	3	n	60
b	19	3	0	14	n	40
b	20	3	12	10	n	60
w	22	3	4	18	b	80
w	23	3	4	17	g	100
b	25	2	4	28	n	60
b	26	2	8	15	n	60
b	27	3	13	25	n	60
w	28	2	4	21	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 91
day 0

score	255	205	70	425

status	0	0	0	0

commands
23
13	m	r	
3	m	u	
6	m	u	
0	m	d	
7	m	u	
15	m	l	
25	m	l	
5	m	l	
26	m	u	
17	m	d	
4	m	l	
18	m	r	
8	m	d	
2	m	u	
28	m	l	
20	m	u	
9	m	u	
27	m	l	
22	m	u	
10	m	d	
23	m	u	
1	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....CGZ....b.bCW..............
01 .B.M..BBBBB...BBBBBBBBBBB....Z
02 .B.....b..B..MBM..C...........
03 .BBBBBBBBBB...B..WW...........
04 .....M........B.....W......C..
05 ......C.BBB...B............C.W
06 ........B.B...BW.............C
07 ........B.B...BCBBBBBB........
08 .......BB.B...B.B.............
09 ....CB....B....bBBBBBBBB......
10 bBBB.BBB..B.C..C.....MMB......
11 .BFB...BC.C............B......
12 .B..C..B...............B......
13 bBBBBBBB....C...........C.....
14 ..F....b......WW..............

citizens
24
type	id	player	row	column	weapon	life
b	0	0	6	29	n	40
b	1	0	9	4	n	60
b	2	0	5	27	n	60
b	3	0	2	18	n	60
w	4	0	0	15	g	80
w	5	0	14	14	h	100
b	6	1	11	8	n	60
b	7	1	5	6	n	60
b	8	1	10	12	n	60
b	9	1	13	12	n	40
w	10	1	5	29	b	100
w	11	1	6	15	h	60
b	13	2	10	15	n	60
b	15	2	12	4	n	60
w	17	2	14	15	h	100
b	18	3	0	4	n	60
b	19	3	0	14	n	20
b	20	3	11	10	n	60
w	22	3	3	18	b	80
w	23	3	3	17	g	100
b	25	2	4	27	n	40
b	26	2	7	15	n	40
b	27	3	13	24	n	60
w	28	2	4	20	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 92
day 0

score	255	205	75	425

status	0	0	0	0

commands
23
13	m	r	
18	m	r	
20	m	d	
6	m	d	
27	m	l	
7	m	u	
22	m	u	
15	m	l	
8	m	d	
25	m	u	
26	m	d	
3	m	r	
23	m	u	
9	m	l	
2	m	u	
10	m	d	
0	m	d	
17	m	l	
11	m	u	
28	m	l	
5	m	u	
4	m	l	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..G..CZ....b.b.W..............
01 .B.M..BBBBB...BBBBBBBBBBB....Z
02 .B.....b..B..MBM.W.C..........
03 .BBBBBBBBBB...B...W........C..
04 .....MC.......B....W.......C..
05 ........BBB...BW.............W
06 ........B.B...B...............
07 ........B.B...B.BBBBBB.......C
08 .M..C..BB.B...BCB.............
09 .....B....B....bBBBBBBBB......
10 bBBB.BBB..B.....C....MMB......
11 .BFB...B....C..........B......
12 .B.C...BC.C............B......
13 bBBBBBBB...C..W........C......
14 ..F....b.......W..............

citizens
23
type	id	player	row	column	weapon	life
b	0	0	7	29	n	20
b	1	0	8	4	n	60
b	2	0	4	27	n	60
b	3	0	2	19	n	40
w	4	0	0	15	g	80
w	5	0	13	14	h	100
b	6	1	12	8	n	60
b	7	1	4	6	n	60
b	8	1	11	12	n	60
b	9	1	13	11	n	40
w	10	1	5	29	b	100
w	11	1	5	15	h	60
b	13	2	10	16	n	60
b	15	2	12	3	n	60
w	17	2	14	15	h	80
b	18	3	0	5	n	60
b	20	3	12	10	n	60
w	22	3	3	18	b	80
w	23	3	2	17	g	100
b	25	2	3	27	n	40
b	26	2	8	15	n	40
b	27	3	13	23	n	60
w	28	2	4	19	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 93
day 0

score	355	205	75	425

status	0	0	0	0

commands
22
6	m	u	
18	m	r	
20	m	u	
7	m	l	
13	m	r	
15	m	r	
25	m	u	
27	m	l	
22	m	u	
26	m	u	
17	m	u	
3	m	r	
8	m	l	
0	m	d	
28	m	u	
5	m	l	
9	m	u	
10	m	d	
4	m	l	
2	m	u	
1	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..G...C....b.bW...............
01 .B.M..BBBBB...BBBBBBBBBBB....Z
02 .B.....b..B..MBM.WW.C......C..
03 .BBBBBBBBBB...B....W.......C..
04 .....C........BW..............
05 ........BBB...B...............
06 ........B.B...B..............W
07 ........B.B...BCBBBBBB........
08 MM.C...BB.B...B.B............C
09 .....B....B....bBBBBBBBB......
10 bBBB.BBB..B......C...MMB......
11 .BFB...BC.CC...........B......
12 .B..C..B...C...........B......
13 bBBBBBBB.....W.W......C.......
14 ..F....b......................

citizens
23
type	id	player	row	column	weapon	life
b	0	0	8	29	n	20
b	1	0	8	3	n	60
b	2	0	3	27	n	60
b	3	0	2	20	n	40
w	4	0	0	14	g	80
w	5	0	13	13	h	100
b	6	1	11	8	n	60
b	7	1	4	5	n	60
b	8	1	11	11	n	60
b	9	1	12	11	n	40
w	10	1	6	29	b	100
w	11	1	4	15	h	60
b	13	2	10	17	n	60
b	15	2	12	4	n	60
w	17	2	13	15	h	80
b	18	3	0	6	n	60
b	20	3	11	10	n	60
w	22	3	2	18	b	80
w	23	3	2	17	g	100
b	25	2	2	27	n	40
b	26	2	7	15	n	40
b	27	3	13	22	n	60
w	28	2	3	19	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 94
day 0

score	355	210	75	425

status	0	0	0	0

commands
23
6	m	u	
18	m	l	
3	m	r	
0	m	d	
5	m	u	
13	m	r	
15	m	u	
7	m	l	
25	m	u	
8	m	u	
4	m	l	
1	m	l	
9	m	l	
2	m	u	
26	m	d	
10	m	d	
17	m	l	
28	m	r	
20	m	d	
11	m	d	
27	m	u	
22	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..G..C.....b.w................
01 .B.M..BBBBB...BBBBBBBBBBB..C.Z
02 .B.....b..B..MBM.....C.....C..
03 .BBBBBBBBBB...B..WW.W.........
04 ....C.........B...............
05 M.......BBB...BW..............
06 ........B.B...B...............
07 ........B.B...B.BBBBBB.......W
08 MMC....BB.B...BCB.............
09 .....B....B....bBBBBBBBB.....C
10 bBBB.BBBC.BC......C..MMB......
11 .BFBC..B..C............B......
12 .B.....B..C..W........CB......
13 bBBBBBBB......W...............
14 ..F....b...................F..

citizens
23
type	id	player	row	column	weapon	life
b	0	0	9	29	n	20
b	1	0	8	2	n	60
b	2	0	2	27	n	60
b	3	0	2	21	n	40
w	4	0	0	13	g	80
w	5	0	12	13	h	100
b	6	1	10	8	n	60
b	7	1	4	4	n	60
b	8	1	10	11	n	60
b	9	1	12	10	n	40
w	10	1	7	29	b	100
w	11	1	5	15	h	60
b	13	2	10	18	n	60
b	15	2	11	4	n	60
w	17	2	13	14	h	80
b	18	3	0	5	n	60
b	20	3	11	10	n	40
w	22	3	3	18	b	80
w	23	3	3	17	g	100
b	25	2	1	27	n	40
b	26	2	8	15	n	40
b	27	3	12	22	n	60
w	28	2	3	20	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 95
day 0

score	355	210	75	425

status	0	0	0	0

commands
23
18	m	l	
6	m	u	
13	m	r	
15	m	u	
7	m	l	
20	m	l	
25	m	u	
26	m	u	
8	m	u	
9	m	d	
3	m	r	
10	m	d	
11	m	d	
27	m	u	
22	m	r	
0	m	d	
5	m	l	
17	m	u	
28	m	r	
4	m	d	
23	m	d	
1	m	l	
2	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..G.C......b.b....M........C..
01 .B.M..BBBBB..WBBBBBBBBBBB....Z
02 .B.....b..B..MBM......C...C...
03 .BBBBBBBBBB...B....W.W........
04 ...C..........B..W............
05 M.......BBB...B...............
06 ........B.B...BW..............
07 ........B.B...BCBBBBBB........
08 MC.....BB.B...B.B............W
09 .....B..C.BC...bBBBBBBBB......
10 bBBBCBBB..B........C.MMB.....C
11 .BFB...B.C............CB......
12 .B.....B....W.W........B......
13 bBBBBBBB..C...................
14 ..F....b...................F..

citizens
23
type	id	player	row	column	weapon	life
b	0	0	10	29	n	20
b	1	0	8	1	n	60
b	2	0	2	26	n	60
b	3	0	2	22	n	40
w	4	0	1	13	g	80
w	5	0	12	12	h	100
b	6	1	9	8	n	60
b	7	1	4	3	n	60
b	8	1	9	11	n	60
b	9	1	13	10	n	40
w	10	1	8	29	b	100
w	11	1	6	15	h	60
b	13	2	10	19	n	60
b	15	2	10	4	n	60
w	17	2	12	14	h	80
b	18	3	0	4	n	60
b	20	3	11	9	n	40
w	22	3	3	19	b	80
w	23	3	4	17	g	100
b	25	2	0	27	n	40
b	26	2	7	15	n	40
b	27	3	11	22	n	60
w	28	2	3	21	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 96
day 0

score	360	210	75	425

status	0	0	0	0

commands
23
13	m	r	
15	m	u	
25	m	l	
3	m	r	
6	m	l	
18	m	l	
7	m	l	
20	m	r	
27	m	u	
22	m	r	
8	m	u	
0	m	d	
23	m	d	
5	m	d	
4	m	d	
9	m	l	
26	m	d	
10	m	d	
1	m	l	
11	m	r	
2	m	u	
17	m	l	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..GC.......b.b....M.......C...
01 .B.M..BBBBB...BBBBBBBBBBB.C..Z
02 .B.....b..B..WBM.......C......
03 .BBBBBBBBBB...B.....W.W.......
04 ..C...........B...............
05 M.......BBB...B..W............
06 ........B.B...B.W.............
07 ........B.B...B.BBBBBB........
08 C......BB.BC..BCB.............
09 ....CB.C..B....bBBBBBBBB.....W
10 bBBB.BBB..B.........CMCB......
11 .BFB...B..C............B.....C
12 .B.....B.....W.........B......
13 bBBBBBBB.C..W.................
14 ..F....b........F..........F..

citizens
23
type	id	player	row	column	weapon	life
b	0	0	11	29	n	20
b	1	0	8	0	n	60
b	2	0	1	26	n	60
b	3	0	2	23	n	40
w	4	0	2	13	g	80
w	5	0	13	12	h	100
b	6	1	9	7	n	60
b	7	1	4	2	n	60
b	8	1	8	11	n	60
b	9	1	13	9	n	40
w	10	1	9	29	b	100
w	11	1	6	16	h	60
b	13	2	10	20	n	60
b	15	2	9	4	n	60
w	17	2	12	13	h	80
b	18	3	0	3	n	60
b	20	3	11	10	n	40
w	22	3	3	20	b	80
w	23	3	5	17	g	100
b	25	2	0	26	n	40
b	26	2	8	15	n	40
b	27	3	10	22	n	60
w	28	2	3	22	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 97
day 0

score	370	210	75	430

status	0	0	0	0

commands
23
6	m	r	
3	m	r	
0	m	d	
18	m	l	
7	m	l	
8	m	u	
5	m	l	
4	m	d	
13	m	l	
20	m	r	
9	m	d	
15	m	u	
2	m	u	
1	m	u	
10	m	d	
25	m	l	
27	m	d	
22	m	r	
11	m	l	
23	m	d	
26	m	u	
17	m	d	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C.....M..b.b....M......C....
01 .B.M..BBBBB...BBBBBBBBBBB.C..Z
02 .B.....b..B...BM........C.....
03 .BBBBBBBBBB..WB...F..W.W......
04 .C............B...............
05 M.......BBB...B...............
06 ........B.B...BW.W............
07 C.......B.BC..BCBBBBBB........
08 ....C..BB.B...B.B.............
09 .....B..C.B....bBBBBBBBB......
10 bBBB.BBB..B........C.M.B.....W
11 .BFB...B...C..........CB......
12 .B.....B...............B.....C
13 bBBBBBBB...W.W................
14 ..F....b.C......F..........F..

citizens
23
type	id	player	row	column	weapon	life
b	0	0	12	29	n	20
b	1	0	7	0	n	60
b	2	0	1	26	n	60
b	3	0	2	24	n	40
w	4	0	3	13	g	80
w	5	0	13	11	h	100
b	6	1	9	8	n	60
b	7	1	4	1	n	60
b	8	1	7	11	n	60
b	9	1	14	9	n	40
w	10	1	10	29	b	100
w	11	1	6	15	h	60
b	13	2	10	19	n	60
b	15	2	8	4	n	60
w	17	2	13	13	h	80
b	18	3	0	2	n	60
b	20	3	11	11	n	40
w	22	3	3	21	b	80
w	23	3	6	17	g	100
b	25	2	0	25	n	20
b	26	2	7	15	n	40
b	27	3	11	22	n	60
w	28	2	3	23	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 98
day 0

score	370	210	75	430

status	0	0	0	0

commands
23
13	m	r	
3	m	r	
18	m	d	
6	m	d	
15	m	u	
7	m	l	
8	m	d	
25	m	l	
9	m	l	
0	m	d	
10	m	d	
11	m	d	
5	m	u	
20	m	u	
26	m	d	
4	m	d	
1	m	u	
2	m	u	
27	m	u	
22	m	r	
17	m	l	
28	m	r	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........M..b.b....M.....C.C...
01 .BCM..BBBBB...BBBBBBBBBBB....Z
02 .B.....b..B...BM.........C....
03 .BBBBBBBBBB...B...F...W.W.....
04 C............WB...............
05 M.......BBB...B...............
06 C.......B.B...BWW.............
07 ....C...B.B...B.BBBBBB........
08 .......BB.BC..BCB.............
09 .....B....B....bBBBBBBBB......
10 bBBB.BBBC.BC........CMCB......
11 .BFB...B...............B.....W
12 .B.....B...W...........B......
13 bBBBBBBB....W................C
14 ..F....bC.......F..........F..

citizens
23
type	id	player	row	column	weapon	life
b	0	0	13	29	n	20
b	1	0	6	0	n	60
b	2	0	0	26	n	60
b	3	0	2	25	n	40
w	4	0	4	13	g	80
w	5	0	12	11	h	100
b	6	1	10	8	n	60
b	7	1	4	0	n	60
b	8	1	8	11	n	60
b	9	1	14	8	n	40
w	10	1	11	29	b	100
w	11	1	6	15	h	60
b	13	2	10	20	n	60
b	15	2	7	4	n	60
w	17	2	13	12	h	80
b	18	3	1	2	n	60
b	20	3	10	11	n	40
w	22	3	3	22	b	80
w	23	3	6	16	g	100
b	25	2	0	24	n	20
b	26	2	8	15	n	20
b	27	3	10	22	n	60
w	28	2	3	24	h	100

barricades
7
player	row	column	resistance
0	0	11	40
0	0	13	40
3	2	7	40
0	9	15	40
2	10	0	40
2	13	0	40
3	14	7	40

round 99
day 0

score	370	210	75	430

status	0	0	0	0

commands
23
18	m	r	
3	m	u	
13	m	d	
0	m	d	
15	m	u	
20	m	r	
25	m	l	
6	m	d	
27	m	d	
7	m	d	
5	m	u	
22	m	r	
26	m	u	
8	m	d	
23	m	l	
4	m	d	
1	m	u	
9	m	r	
17	m	u	
28	m	r	
10	m	d	
11	m	d	
2	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........M.........M....C.C....
01 .B.C..BBBBB...BBBBBBBBBBBC...Z
02 .B........B...BM..............
03 .BBBBBBBBBB...B...F....W.W....
04 ..............B...............
05 C.......BBB..WB.....M.........
06 C...C...B.B...BWW.............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.............
09 .....B....BC....BBBBBBBB......
10 .BBB.BBB..B.C........M.B......
11 .BFB...BC..W........C.CB......
12 .B.....B....W..........B.....W
13 .BBBBBBB......................
14 ..F......C......F..........F.C

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	29	n	20
b	1	0	6	0	n	60
b	2	0	0	25	n	60
b	3	0	1	25	n	40
w	4	0	5	13	g	80
w	5	0	11	11	h	100
b	6	1	11	8	n	60
b	7	1	5	0	n	40
b	8	1	9	11	n	60
b	9	1	14	9	n	40
w	10	1	12	29	b	100
w	11	1	6	15	h	40
b	13	2	11	20	n	60
b	15	2	6	4	n	60
w	17	2	12	12	h	80
b	18	3	1	3	n	60
b	20	3	10	12	n	40
w	22	3	3	23	b	80
w	23	3	6	16	g	100
b	25	2	0	23	n	20
b	27	3	11	22	n	60
w	28	2	3	25	h	100

barricades
0
player	row	column	resistance

round 100
day 1

score	370	315	75	435

status	0	0	0	0

commands
20
13	m	r	
1	m	d	
0	m	l	
15	m	r	
18	m	u	
20	m	d	
25	b	l	
6	m	d	
17	m	d	
7	m	u	
27	m	u	
22	m	r	
8	m	u	
4	m	u	
9	m	l	
28	m	r	
10	m	d	
11	m	u	
3	m	d	
2	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C....M.........M...bC..C...
01 .B....BBBBB...BBBBBBBBBBB....Z
02 .B........B...BM.........C....
03 .BBBBBBBBBB...B...F.....W.W...
04 C.........M..WB...............
05 ........BBB...BW....M.........
06 .....C..B.B...B.W.....M.......
07 C.......B.B...B.BBBBBB........
08 .......BB.BC..B.B.............
09 .....B....B.....BBBBBBBB......
10 .BBB.BBB..B..........MCB......
11 .BFB...B...WC........C.B.M....
12 .B.....BC..............B......
13 .BBBBBBB....W................W
14 ..F.....C.......F..........FC.

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	28	n	20
b	1	0	7	0	n	60
b	2	0	0	26	n	60
b	3	0	2	25	n	40
w	4	0	4	13	g	80
w	5	0	11	11	h	100
b	6	1	12	8	n	60
b	7	1	4	0	n	40
b	8	1	8	11	n	60
b	9	1	14	8	n	40
w	10	1	13	29	b	100
w	11	1	5	15	h	40
b	13	2	11	21	n	60
b	15	2	6	5	n	60
w	17	2	13	12	h	80
b	18	3	0	3	n	60
b	20	3	11	12	n	40
w	22	3	3	24	b	80
w	23	3	6	16	g	100
b	25	2	0	23	n	20
b	27	3	10	22	n	60
w	28	2	3	26	h	100

barricades
1
player	row	column	resistance
2	0	22	40

round 101
day 1

score	370	315	75	435

status	0	0	0	0

commands
21
13	m	u	
2	b	l	
15	m	r	
3	m	u	
25	m	l	
18	m	r	
17	m	d	
6	m	u	
28	m	r	
1	m	d	
7	m	d	
0	m	l	
8	m	u	
5	m	u	
9	m	l	
20	m	d	
10	m	l	
4	m	l	
11	m	u	
22	m	u	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C...M.........M...c..bC...
01 .B....BBBBB...BBBBBBBBBBBC...Z
02 .B........B...BM........W.....
03 .BBBBBBBBBB...B...F........W..
04 ..........M.W.BW..............
05 C.......BBB...B.W...M.........
06 ......C.B.B...B.......M.......
07 ........B.BC..B.BBBBBB........
08 C......BB.B...B.B.............
09 .....B....B.....BBBBBBBB......
10 .BBB.BBB..BW.........CCB......
11 .BFB...BC..............B.M....
12 .B.....B....C..........B......
13 .BBBBBBB....................W.
14 ..F....C....W...F..........C..

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	27	n	40
b	1	0	8	0	n	60
b	2	0	0	26	n	60
b	3	0	1	25	n	40
w	4	0	4	12	g	80
w	5	0	10	11	h	100
b	6	1	11	8	n	60
b	7	1	5	0	n	40
b	8	1	7	11	n	60
b	9	1	14	7	n	40
w	10	1	13	28	b	100
w	11	1	4	15	h	40
b	13	2	10	21	n	60
b	15	2	6	6	n	60
w	17	2	14	12	h	80
b	18	3	0	4	n	60
b	20	3	12	12	n	40
w	22	3	2	24	b	80
w	23	3	5	16	g	100
b	25	2	0	22	n	20
b	27	3	10	22	n	60
w	28	2	3	27	h	100

barricades
2
player	row	column	resistance
2	0	22	40
0	0	25	40

round 102
day 1

score	370	315	80	435

status	0	0	0	0

commands
20
13	m	d	
18	m	r	
6	m	u	
1	b	d	
15	m	r	
20	m	d	
25	m	l	
27	m	d	
4	m	l	
7	m	d	
17	m	r	
5	m	u	
8	m	u	
3	m	d	
28	m	r	
9	m	l	
2	m	d	
23	m	u	
10	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....C..M.........M..Cb..b....
01 .B....BBBBB...BBBBBBBBBBB.C..Z
02 .B........B...BM........WC....
03 .BBBBBBBBBB...BW..F.........W.
04 ..........MW..B.W.............
05 ........BBB...B.....M.........
06 C......CB.BC..B.......M.......
07 ........B.B...B.BBBBBB........
08 C......BB.B...B.B.............
09 b....B....BW....BBBBBBBB......
10 .BBB.BBBC.B............B......
11 .BFB...B.............CCB.M....
12 .B.....B...............B......
13 .BBBBBBB....C..............W..
14 ..F...C......W..F..........C..

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	27	n	40
b	1	0	8	0	n	60
b	2	0	1	26	n	60
b	3	0	2	25	n	40
w	4	0	4	11	g	80
w	5	0	9	11	h	100
b	6	1	10	8	n	60
b	7	1	6	0	n	40
b	8	1	6	11	n	60
b	9	1	14	6	n	40
w	10	1	13	27	b	100
w	11	1	3	15	h	40
b	13	2	11	21	n	60
b	15	2	6	7	n	60
w	17	2	14	13	h	80
b	18	3	0	5	n	60
b	20	3	13	12	n	40
w	22	3	2	24	b	80
w	23	3	4	16	g	100
b	25	2	0	21	n	20
b	27	3	11	22	n	60
w	28	2	3	28	h	100

barricades
3
player	row	column	resistance
2	0	22	40
0	0	25	40
0	9	0	40

round 103
day 1

score	370	315	80	435

status	0	0	0	0

commands
19
18	b	r	
13	m	d	
6	m	u	
7	m	r	
20	m	r	
27	m	d	
15	m	u	
25	m	l	
8	m	u	
1	m	r	
4	m	l	
5	m	u	
9	m	l	
3	m	d	
11	m	r	
2	m	d	
0	m	l	
17	m	r	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Cb.M.........M.C.b..b....
01 .B....BBBBB...BBBBBBBBBBB....Z
02 .B........B...BM........W.C...
03 .BBBBBBBBBB...B.W.F......C...W
04 ..........W...B.W..M..........
05 .......CBBBC..B.....M.........
06 .C......B.B...B.......M.......
07 ........B.B...B.BBBBBB........
08 .C.....BB.BW..B.B.............
09 b....B..C.B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...............B.M....
12 .B.....B..M..........CCB......
13 .BBBBBBB.....C.............W..
14 ..F..C........W.F.........C...

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	26	n	40
b	1	0	8	1	n	60
b	2	0	2	26	n	60
b	3	0	3	25	n	40
w	4	0	4	10	g	80
w	5	0	8	11	h	100
b	6	1	9	8	n	60
b	7	1	6	1	n	40
b	8	1	5	11	n	60
b	9	1	14	5	n	40
w	10	1	13	27	b	100
w	11	1	3	16	h	40
b	13	2	12	21	n	60
b	15	2	5	7	n	60
w	17	2	14	14	h	80
b	18	3	0	5	n	60
b	20	3	13	13	n	40
w	22	3	2	24	b	80
w	23	3	4	16	g	100
b	25	2	0	20	n	20
b	27	3	12	22	n	60
w	28	2	3	29	h	100

barricades
4
player	row	column	resistance
3	0	6	40
2	0	22	40
0	0	25	40
0	9	0	40

round 104
day 1

score	375	315	80	435

status	0	0	0	0

commands
20
6	m	d	
1	m	r	
7	m	d	
13	m	d	
15	m	l	
18	m	r	
20	m	r	
25	b	l	
5	m	d	
8	m	u	
27	m	d	
9	m	l	
10	m	l	
11	m	r	
17	m	r	
28	m	u	
3	m	d	
22	m	d	
23	m	u	
2	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......c.M.........MbC.b..b....
01 .B....BBBBB...BBBBBBBBBBB....Z
02 .B........B...BM.............W
03 .BBBBBBBBBB...B.WWF.....W.C...
04 ..........WC..B....M.....C....
05 ......C.BBB...B.....M.........
06 ........B.B...B.......M.......
07 .C......B.B...B.BBBBBB........
08 ..C....BB.B...B.B.............
09 b....B....BW....BBBBBBBB......
10 .BBB.BBBC.B............B......
11 .BFB...B...............B.M....
12 .B.....B..M............B......
13 .BBBBBBB......C......CC...W...
14 ..F.C..........WF.........C...

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	26	n	40
b	1	0	8	2	n	60
b	2	0	3	26	n	60
b	3	0	4	25	n	40
w	4	0	4	10	g	80
w	5	0	9	11	h	100
b	6	1	10	8	n	60
b	7	1	7	1	n	40
b	8	1	4	11	n	60
b	9	1	14	4	n	40
w	10	1	13	26	b	100
w	11	1	3	17	h	40
b	13	2	13	21	n	60
b	15	2	5	6	n	60
w	17	2	14	15	h	80
b	18	3	0	6	n	60
b	20	3	13	14	n	40
w	22	3	3	24	b	80
w	23	3	3	16	g	100
b	25	2	0	20	n	20
b	27	3	13	22	n	60
w	28	2	2	29	h	100

barricades
5
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40

round 105
day 1

score	375	315	80	435

status	0	0	0	0

commands
20
1	m	r	
18	m	r	
6	m	d	
20	m	r	
5	m	d	
4	m	l	
3	m	d	
2	m	d	
0	m	l	
7	m	d	
27	m	r	
15	m	d	
25	m	l	
8	m	u	
17	m	r	
22	m	d	
9	m	l	
10	m	d	
11	m	r	
28	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......bCM.........Mc..b..b....
01 .B....BBBBB...BBBBBBBBBBB....W
02 .B........B...BM..............
03 .BBBBBBBBBBC..B.W.W...........
04 .........W....B....M....W.C...
05 ........BBB...B.....M....C....
06 ......C.B.B...B....M..M.......
07 ........B.B...B.BBBBBB........
08 .C.C...BB.B...B.B.............
09 b....B....B.....BBBBBBBB......
10 .BBB.BBB..BW...........B......
11 .BFB...BC..............B.M....
12 .B.....B..M............B......
13 .BBBBBBB.......C.....C.C......
14 ..FC............W........CW...

citizens
22
type	id	player	row	column	weapon	life
b	0	0	14	25	n	40
b	1	0	8	3	n	60
b	2	0	4	26	n	60
b	3	0	5	25	n	40
w	4	0	4	9	g	80
w	5	0	10	11	h	100
b	6	1	11	8	n	60
b	7	1	8	1	n	40
b	8	1	3	11	n	60
b	9	1	14	3	n	40
w	10	1	14	26	b	100
w	11	1	3	18	h	60
b	13	2	13	21	n	60
b	15	2	6	6	n	60
w	17	2	14	16	h	100
b	18	3	0	7	n	60
b	20	3	13	15	n	40
w	22	3	4	24	b	80
w	23	3	3	16	g	100
b	25	2	0	19	n	20
b	27	3	13	23	n	60
w	28	2	1	29	b	100

barricades
5
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40

round 106
day 1

score	375	315	80	435

status	0	0	0	0

commands
22
13	m	d	
1	m	r	
6	m	d	
15	m	d	
5	m	d	
18	m	r	
4	m	r	
20	m	u	
27	m	r	
25	m	l	
22	m	d	
23	m	r	
17	m	u	
7	m	d	
28	m	d	
8	m	u	
3	m	d	
2	m	u	
9	m	l	
0	m	u	
10	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.C.........Cb..b..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........BC..BM..W..........W
03 .BBBBBBBBBB...B..W........C...
04 ..........W...B....M..........
05 ........BBB...B.....M...W.....
06 ........B.B...B.G..M..M..C....
07 ......C.B.B...B.BBBBBB........
08 ....C..BB.B...B.B.............
09 bC...B....B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...W...........B.M....
12 .B.....BC.M....C.......B......
13 .BBBBBBB........W.......CC....
14 ..C..................C...W....

citizens
22
type	id	player	row	column	weapon	life
b	0	0	13	25	n	40
b	1	0	8	4	n	60
b	2	0	3	26	n	60
b	3	0	6	25	n	40
w	4	0	4	10	g	80
w	5	0	11	11	h	100
b	6	1	12	8	n	60
b	7	1	9	1	n	40
b	8	1	2	11	n	60
b	9	1	14	2	n	60
w	10	1	14	25	b	100
w	11	1	2	18	h	60
b	13	2	14	21	n	60
b	15	2	7	6	n	60
w	17	2	13	16	h	100
b	18	3	0	8	n	60
b	20	3	12	15	n	40
w	22	3	5	24	b	80
w	23	3	3	17	g	100
b	25	2	0	18	n	20
b	27	3	13	24	n	60
w	28	2	2	29	b	100

barricades
5
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40

round 107
day 1

score	375	315	85	440

status	0	0	0	0

commands
21
13	m	l	
6	m	r	
1	b	d	
15	m	d	
7	m	r	
3	m	d	
8	m	d	
25	m	l	
17	m	l	
5	m	d	
0	m	u	
4	m	r	
9	m	r	
28	m	d	
10	m	u	
18	m	r	
20	m	l	
11	m	l	
27	m	u	
2	m	d	
22	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b..C.......C.b..b..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...BM.W............
03 .BBBBBBBBBBC..B..W...........W
04 ...........W..B....M......C...
05 ........BBB...B.....M.........
06 ........B.B...B.G..M..M.W.....
07 ........B.B...B.BBBBBB...C....
08 ....C.CBB.B...B.B..M..........
09 b.C.bB....B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...............B.M....
12 .B.....B.CMW..C........BCC....
13 .BBBBBBB.......W.........W....
14 ...C................C.........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	12	25	n	40
b	1	0	8	4	n	60
b	2	0	4	26	n	60
b	3	0	7	25	n	40
w	4	0	4	11	g	80
w	5	0	12	11	h	100
b	6	1	12	9	n	60
b	7	1	9	2	n	40
b	8	1	3	11	n	60
b	9	1	14	3	n	60
w	10	1	13	25	b	100
w	11	1	2	17	h	60
b	13	2	14	20	n	60
b	15	2	8	6	n	60
w	17	2	13	15	h	100
b	18	3	0	9	n	60
b	20	3	12	14	n	40
w	22	3	6	24	b	80
w	23	3	3	17	g	100
b	25	2	0	17	n	20
b	27	3	12	24	n	60
w	28	2	3	29	b	100

barricades
6
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40

round 108
day 1

score	375	315	85	440

status	0	0	0	0

commands
18
13	m	r	
18	m	r	
1	m	l	
27	m	u	
15	m	d	
22	m	d	
25	m	l	
17	m	l	
5	m	l	
0	m	u	
28	m	d	
7	m	r	
8	m	r	
9	m	r	
10	m	u	
11	m	l	
4	m	r	
2	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b...C.....C..b..b..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...BMW.............
03 .BBBBBBBBBB.C.B..W............
04 ............W.B....M.........W
05 ........BBB...B.....M.....C...
06 ........B.B...B.G..M..M.......
07 ........B.B...B.BBBBBB..WC....
08 ...C...BB.B...B.B..M..........
09 b..CbBC...B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...............BCC....
12 .B.....B.CW...C........B.W....
13 .BBBBBBB......W...............
14 ....C................C........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	11	25	n	40
b	1	0	8	3	n	60
b	2	0	5	26	n	60
b	3	0	7	25	n	40
w	4	0	4	12	g	80
w	5	0	12	10	h	100
b	6	1	12	9	n	60
b	7	1	9	3	n	40
b	8	1	3	12	n	60
b	9	1	14	4	n	60
w	10	1	12	25	b	100
w	11	1	2	16	h	60
b	13	2	14	21	n	60
b	15	2	9	6	n	60
w	17	2	13	14	h	100
b	18	3	0	10	n	60
b	20	3	12	14	n	40
w	22	3	7	24	b	80
w	23	3	3	17	g	100
b	25	2	0	16	n	20
b	27	3	11	24	n	60
w	28	2	4	29	b	100

barricades
6
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40

round 109
day 1

score	385	315	85	440

status	0	0	0	0

commands
19
18	m	r	
13	m	r	
5	m	u	
27	m	u	
4	m	r	
15	b	r	
3	m	u	
17	m	r	
2	m	l	
28	m	d	
23	m	u	
1	m	u	
0	m	u	
6	m	u	
7	m	l	
8	m	r	
9	m	r	
10	m	u	
11	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b....C....C..b..b..b....
01 .B.G..BBBBB...BBBBBBBBBBB.....
02 .B........B...BW.W............
03 .BBBBBBBBBB..CB...............
04 .............WB....M..........
05 ........BBB...B.....M....C...W
06 ........B.B...B.G..M..M..C....
07 ...C....B.B...B.BBBBBB..W.....
08 .......BB.B...B.B..M..........
09 b.C.bBCb..B.....BBBBBBBB......
10 .BBB.BBB..B............BCC....
11 .BFB...B.CW............B.W....
12 .B.....B......C........B......
13 .BBBBBBB.......W..............
14 .....C................C.......

citizens
22
type	id	player	row	column	weapon	life
b	0	0	10	25	n	40
b	1	0	7	3	n	60
b	2	0	5	25	n	60
b	3	0	6	25	n	40
w	4	0	4	13	g	80
w	5	0	11	10	h	100
b	6	1	11	9	n	60
b	7	1	9	2	n	40
b	8	1	3	13	n	60
b	9	1	14	5	n	60
w	10	1	11	25	b	100
w	11	1	2	15	h	60
b	13	2	14	22	n	60
b	15	2	9	6	n	60
w	17	2	13	15	h	100
b	18	3	0	11	n	60
b	20	3	12	14	n	40
w	22	3	7	24	b	80
w	23	3	2	17	g	100
b	25	2	0	16	n	20
b	27	3	10	24	n	60
w	28	2	5	29	b	100

barricades
7
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40
2	9	7	40

round 110
day 1

score	385	320	85	440

status	0	0	0	0

commands
20
18	m	l	
6	m	d	
13	m	r	
27	m	u	
1	m	r	
7	m	u	
15	m	r	
25	m	l	
5	m	r	
4	m	d	
17	m	u	
3	m	l	
2	m	l	
28	m	u	
0	m	u	
8	m	l	
23	m	l	
9	m	r	
10	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b...C....C...b..b..b....
01 .B.G..BBBBB...BBBBBBBBBBB...F.
02 .B.M......B...B.W.............
03 .BBBBBBBBBB.C.BW..............
04 ..............B....M.........W
05 ........BBB..WB.....M...C.....
06 ........B.B...B.G..M..M.C.....
07 ....C...B.B...B.BBBBBB..W.....
08 ..C....BB.B...B.B..M..........
09 b...bB.c..B.....BBBBBBBBCC....
10 .BBB.BBB..B............B.W....
11 .BFB...B...W...........B......
12 .B.....B.C....CW.......B......
13 .BBBBBBB......................
14 ......C............M...C......

citizens
22
type	id	player	row	column	weapon	life
b	0	0	9	25	n	40
b	1	0	7	4	n	60
b	2	0	5	24	n	60
b	3	0	6	24	n	40
w	4	0	5	13	g	80
w	5	0	11	11	h	100
b	6	1	12	9	n	60
b	7	1	8	2	n	40
b	8	1	3	12	n	60
b	9	1	14	6	n	60
w	10	1	10	25	b	100
w	11	1	3	15	h	60
b	13	2	14	23	n	60
b	15	2	9	7	n	60
w	17	2	12	15	h	100
b	18	3	0	10	n	60
b	20	3	12	14	n	40
w	22	3	7	24	b	80
w	23	3	2	16	g	100
b	25	2	0	15	n	20
b	27	3	9	24	n	60
w	28	2	4	29	b	100

barricades
7
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40
2	9	7	40

round 111
day 1

score	385	320	85	440

status	0	0	0	0

commands
20
18	m	l	
20	m	u	
6	m	d	
7	m	u	
27	m	u	
5	m	u	
8	m	d	
3	m	l	
4	m	d	
23	m	d	
9	m	r	
13	m	l	
15	m	r	
2	m	l	
11	m	u	
1	m	u	
25	m	l	
0	m	u	
17	m	d	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b..C....C....b..b..b....
01 .B.G..BBBBB...BBBBBBBBBBB...F.
02 .B.M......B...BW..............
03 .BBBBBBBBBB...B.W.............
04 ............C.B....M........W.
05 ........BBB...B.....M..C......
06 ....C...B.B..WB.G..M..MC......
07 ..C.....B.B...B.BBBBBB..W.....
08 .......BB.B...B.B..M....CC....
09 b...bB.bC.B.....BBBBBBBB......
10 .BBB.BBB..BW...........B.W....
11 .BFB...B......C........B......
12 .B.....B...............B......
13 .BBBBBBB.C.....W..............
14 .......C...........M..C.......

citizens
22
type	id	player	row	column	weapon	life
b	0	0	8	25	n	40
b	1	0	6	4	n	60
b	2	0	5	23	n	60
b	3	0	6	23	n	40
w	4	0	6	13	g	80
w	5	0	10	11	h	100
b	6	1	13	9	n	60
b	7	1	7	2	n	40
b	8	1	4	12	n	60
b	9	1	14	7	n	60
w	10	1	10	25	b	100
w	11	1	2	15	h	60
b	13	2	14	22	n	60
b	15	2	9	8	n	60
w	17	2	13	15	h	100
b	18	3	0	9	n	60
b	20	3	11	14	n	40
w	22	3	7	24	b	80
w	23	3	3	16	g	100
b	25	2	0	14	n	20
b	27	3	8	24	n	60
w	28	2	4	28	b	100

barricades
7
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40
2	9	7	40

round 112
day 1

score	385	320	85	440

status	0	0	0	0

commands
22
13	m	l	
6	m	d	
18	m	l	
15	m	d	
7	m	u	
20	m	u	
8	m	d	
5	m	u	
9	m	r	
27	m	l	
3	m	l	
25	m	l	
4	m	d	
22	m	u	
10	m	u	
11	m	d	
2	m	l	
1	m	u	
23	m	u	
17	m	d	
0	m	u	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.C....C.....b.Mb..b....
01 .B.G..BBBBB...BBBBBBBBBBB..MF.
02 .B.M......B...B.W.............
03 .BBBBBBBBBB...BW..............
04 ..............B....M.......W..
05 ....C...BBB.C.B.....M.C.......
06 ..C.....B.B...B.G..M..C.W.....
07 ........B.B..WB.BBBBBB...C....
08 .......BB.B...B.B..M...C......
09 b...bB.b..BW....BBBBBBBB.W....
10 .BBB.BBBC.B...C........B......
11 .BFB...B...............B......
12 .B.....B...............B......
13 .BBBBBBB......................
14 ........CC.....W...M.C........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	7	25	n	40
b	1	0	5	4	n	60
b	2	0	5	22	n	60
b	3	0	6	22	n	40
w	4	0	7	13	g	80
w	5	0	9	11	h	100
b	6	1	14	9	n	60
b	7	1	6	2	n	40
b	8	1	5	12	n	60
b	9	1	14	8	n	60
w	10	1	9	25	b	100
w	11	1	3	15	h	60
b	13	2	14	21	n	60
b	15	2	10	8	n	60
w	17	2	14	15	h	100
b	18	3	0	8	n	60
b	20	3	10	14	n	40
w	22	3	6	24	b	80
w	23	3	2	16	g	100
b	25	2	0	13	n	20
b	27	3	8	23	n	60
w	28	2	4	27	b	100

barricades
7
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40
2	9	7	40

round 113
day 1

score	390	320	85	440

status	0	0	0	0

commands
21
6	m	u	
18	m	l	
20	m	u	
7	m	u	
5	m	r	
2	m	l	
13	m	l	
4	m	d	
8	m	d	
9	m	u	
15	m	d	
25	m	r	
17	m	r	
27	m	l	
22	m	d	
3	m	l	
28	m	u	
23	m	d	
10	m	u	
1	m	u	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M..bC......C....b.Mb..b....
01 .B.G..BBBBB...BBBBBBBBBBB..MF.
02 .B.M......B...BW..............
03 .BBBBBBBBBB...B.W..........W..
04 ....C.........B....M..........
05 ..C.....BBB...B.....MC........
06 ........B.B.C.B.G..M.C........
07 ........B.B...B.BBBBBB..WC....
08 .......BB.B..WB.B..M..C..W....
09 b...bB.b..B.W.C.BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...BC..............B......
12 .B.....B...............B......
13 .BBBBBBBCC....................
14 ................W..MC.........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	7	25	n	40
b	1	0	4	4	n	60
b	2	0	5	21	n	60
b	3	0	6	21	n	40
w	4	0	8	13	g	80
w	5	0	9	12	h	100
b	6	1	13	9	n	60
b	7	1	5	2	n	40
b	8	1	6	12	n	60
b	9	1	13	8	n	60
w	10	1	8	25	b	100
w	11	1	2	15	h	60
b	13	2	14	20	n	60
b	15	2	11	8	n	60
w	17	2	14	16	h	100
b	18	3	0	7	n	60
b	20	3	9	14	n	40
w	22	3	7	24	b	80
w	23	3	3	16	g	100
b	25	2	0	14	n	20
b	27	3	8	22	n	60
w	28	2	3	27	b	100

barricades
7
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
0	9	0	40
0	9	4	40
2	9	7	40

round 114
day 1

score	390	320	85	440

status	0	0	0	0

commands
19
5	m	r	
13	m	l	
6	m	r	
18	m	l	
3	m	l	
15	m	d	
20	m	r	
7	m	u	
2	m	l	
25	m	r	
17	m	r	
27	b	l	
8	m	l	
1	m	l	
0	m	u	
23	m	u	
10	m	u	
28	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M..c........C...b.MbF.b....
01 .B.G..BBBBB...BBBBBBBBBBB..MF.
02 .B.M......B...B.W..........W..
03 .BBBBBBBBBB...BW..............
04 ..CC..........B....M..........
05 ........BBB...B.....C.........
06 ........B.BC..B.G..MC....C....
07 ........B.B...B.BBBBBB..WW....
08 .......BB.B..WB.B..M.bC.......
09 b.F.bB.b..B..W.CBBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...............B......
12 .B.....BC..............B......
13 .BBBBBBBC.C...................
14 .................W.C..........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	6	25	n	40
b	1	0	4	3	n	60
b	2	0	5	20	n	60
b	3	0	6	20	n	40
w	4	0	8	13	g	80
w	5	0	9	13	h	100
b	6	1	13	10	n	60
b	7	1	4	2	n	40
b	8	1	6	11	n	60
b	9	1	13	8	n	60
w	10	1	7	25	b	100
w	11	1	3	15	h	60
b	13	2	14	19	n	60
b	15	2	12	8	n	60
w	17	2	14	17	h	100
b	18	3	0	6	n	60
b	20	3	9	15	n	40
w	22	3	7	24	b	80
w	23	3	2	16	g	100
b	25	2	0	15	n	20
b	27	3	8	22	n	60
w	28	2	2	27	b	100

barricades
8
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 115
day 1

score	395	320	90	440

status	0	0	0	0

commands
21
5	m	l	
13	m	r	
15	m	u	
3	m	l	
25	m	r	
18	m	l	
17	m	r	
28	m	u	
6	m	r	
7	m	d	
20	b	u	
2	m	u	
4	m	u	
1	m	d	
8	m	u	
9	m	d	
0	m	u	
27	m	l	
10	m	u	
11	m	r	
22	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M.Cb.........C..b.MbF.b....
01 .B.G..BBBBB...BBBBBBBBBBB..WF.
02 .B.M......B...B.W.............
03 .BBBBBBBBBB...B.W.............
04 ..............B....MC.........
05 ..CC....BBBC..B..........C....
06 ........B.B...B.G..C....WW....
07 ........B.B..WB.BBBBBB........
08 .......BB.B...BbB..M.c......F.
09 b.F.bB.b..B.W..CBBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...BC..............B......
12 .B.....B...............B......
13 .BBBBBBB...C..................
14 ........C.........W.C.........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	25	n	40
b	1	0	5	3	n	60
b	2	0	4	20	n	60
b	3	0	6	19	n	40
w	4	0	7	13	g	80
w	5	0	9	12	h	100
b	6	1	13	11	n	60
b	7	1	5	2	n	40
b	8	1	5	11	n	60
b	9	1	14	8	n	60
w	10	1	6	25	b	100
w	11	1	3	16	h	60
b	13	2	14	20	n	60
b	15	2	11	8	n	60
w	17	2	14	18	h	100
b	18	3	0	5	n	60
b	20	3	9	15	n	40
w	22	3	6	24	b	80
w	23	3	2	16	g	100
b	25	2	0	16	n	20
b	27	3	8	21	n	60
w	28	2	1	27	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 116
day 1

score	400	320	95	440

status	0	0	0	0

commands
21
6	m	u	
7	m	d	
13	m	r	
3	m	l	
18	m	d	
20	m	u	
0	m	r	
8	m	u	
27	m	l	
5	m	u	
22	m	u	
9	m	r	
2	m	l	
15	m	u	
4	m	u	
1	m	u	
10	m	u	
11	m	r	
25	m	r	
17	m	r	
28	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M..b..........C.b.MbF.b....
01 .B.G.CBBBBB...BBBBBBBBBBB...F.
02 .B.M......B...B.W..........W..
03 .BBBBBBBBBB...B..W............
04 ...C.......C..B....C..........
05 ........BBB...B.........WWC..M
06 ..C.....B.B..WB.G.C...........
07 ........B.B...B.BBBBBB........
08 .......BB.B.W.BcB..MCb......F.
09 b.F.bB.b..B.....BBBBBBBB......
10 .BBB.BBBC.B............B......
11 .BFB...B...............B......
12 .B.....B...C...........B......
13 .BBBBBBB......................
14 .........C.........W.C........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	26	n	40
b	1	0	4	3	n	60
b	2	0	4	19	n	60
b	3	0	6	18	n	40
w	4	0	6	13	g	80
w	5	0	8	12	h	100
b	6	1	12	11	n	60
b	7	1	6	2	n	40
b	8	1	4	11	n	60
b	9	1	14	9	n	60
w	10	1	5	25	b	100
w	11	1	3	17	h	60
b	13	2	14	21	n	60
b	15	2	10	8	n	60
w	17	2	14	19	h	100
b	18	3	1	5	n	60
b	20	3	8	15	n	40
w	22	3	5	24	b	80
w	23	3	2	16	g	100
b	25	2	0	17	n	20
b	27	3	8	20	n	60
w	28	2	2	27	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 117
day 1

score	405	320	95	440

status	0	0	0	0

commands
22
13	m	r	
2	m	d	
15	m	u	
6	m	d	
18	m	l	
7	m	d	
25	m	r	
4	m	d	
20	m	u	
3	m	l	
17	m	r	
27	m	l	
5	m	d	
28	m	d	
8	m	l	
1	m	l	
0	m	r	
9	m	u	
22	m	u	
10	m	r	
23	m	d	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M..b...........Cb.MbF.b....
01 .B.GC.BBBBB...BBBBBBBBBBB...F.
02 .B.M......B...B..W............
03 .BBBBBBBBBB...B.W..........W..
04 ..C.......C...B.........W.....
05 ........BBB...B....C......WC.M
06 ........B.B...B.GC............
07 ..C.....B.B..WBCBBBBBB........
08 .......BB.B...BbB..C.b......F.
09 b.F.bB.bC.B.W...BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B...............B......
12 .B.....B...............B......
13 .BBBBBBB.C.C..................
14 ....................W.C.......

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	27	n	40
b	1	0	4	2	n	60
b	2	0	5	19	n	60
b	3	0	6	17	n	40
w	4	0	7	13	g	80
w	5	0	9	12	h	100
b	6	1	13	11	n	60
b	7	1	7	2	n	40
b	8	1	4	10	n	60
b	9	1	13	9	n	60
w	10	1	5	26	b	100
w	11	1	2	17	h	60
b	13	2	14	22	n	60
b	15	2	9	8	n	60
w	17	2	14	20	h	100
b	18	3	1	4	n	60
b	20	3	7	15	n	40
w	22	3	4	24	b	80
w	23	3	3	16	g	100
b	25	2	0	18	n	20
b	27	3	8	19	n	60
w	28	2	3	27	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 118
day 1

score	405	320	95	445

status	0	0	0	0

commands
21
2	m	d	
6	m	r	
18	m	l	
7	m	d	
20	m	u	
8	m	l	
9	m	u	
4	m	l	
13	m	r	
15	m	l	
3	m	l	
0	m	r	
27	m	r	
22	m	r	
11	m	r	
23	m	u	
25	m	r	
17	m	r	
5	m	d	
28	m	d	
1	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...M..b............c.MbF.b....
01 .B.C..BBBBB...BBBBBBBBBBB...F.
02 .B.M......B...B.W.W...........
03 .BBBBBBBBBB...B...............
04 .C.......C....B..........W.W..
05 ....M...BBB...B...........W.CM
06 ........B.B...BCC..C..........
07 ........B.B.W.B.BBBBBB........
08 ..C....BB.B...BbB...Cb......F.
09 b.F.bB.c..B.....BBBBBBBB......
10 .BBB.BBB..B.W..........B......
11 .BFB...B...............B......
12 .BM....B.C.............B......
13 .BBBBBBB....C.................
14 .....................W.C......

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	28	n	40
b	1	0	4	1	n	60
b	2	0	6	19	n	60
b	3	0	6	16	n	40
w	4	0	7	12	g	80
w	5	0	10	12	h	100
b	6	1	13	12	n	60
b	7	1	8	2	n	40
b	8	1	4	9	n	60
b	9	1	12	9	n	60
w	10	1	5	26	b	100
w	11	1	2	18	h	60
b	13	2	14	23	n	60
b	15	2	9	7	n	60
w	17	2	14	21	h	100
b	18	3	1	3	n	60
b	20	3	6	15	n	40
w	22	3	4	25	b	80
w	23	3	2	16	g	100
b	25	2	0	19	n	20
b	27	3	8	20	n	60
w	28	2	4	27	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 119
day 1

score	405	320	95	445

status	0	0	0	0

commands
22
6	m	r	
13	m	r	
15	m	l	
18	m	u	
20	m	u	
3	m	u	
0	m	r	
25	m	r	
7	m	d	
27	m	r	
5	m	d	
17	m	r	
4	m	d	
28	m	r	
8	m	l	
9	m	u	
22	m	u	
2	m	r	
10	m	r	
11	m	r	
23	m	r	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C..b............bCMbF.b....
01 .B....BBBBB...BBBBBBBBBBB...F.
02 .B.M......B...B..W.W..........
03 .BBBBBBBBBB...B..........W....
04 ..C.....C.....B.............W.
05 ....M...BBB...BCC..........W.C
06 ........B.B...B.....C.........
07 ........BMB...B.BBBBBB........
08 .......BB.B.W.BbB....c......F.
09 b.C.bBCb..B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B.C..W..........B......
12 .BM....B...............B......
13 .BBBBBBB.....C................
14 .........M............W.C.....

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	29	n	40
b	1	0	4	2	n	60
b	2	0	6	20	n	60
b	3	0	5	16	n	40
w	4	0	8	12	g	80
w	5	0	11	12	h	100
b	6	1	13	13	n	60
b	7	1	9	2	n	60
b	8	1	4	8	n	60
b	9	1	11	9	n	60
w	10	1	5	27	b	100
w	11	1	2	19	h	60
b	13	2	14	24	n	60
b	15	2	9	6	n	60
w	17	2	14	22	h	100
b	18	3	0	3	n	60
b	20	3	5	15	n	40
w	22	3	3	25	b	80
w	23	3	2	17	g	100
b	25	2	0	20	n	20
b	27	3	8	21	n	60
w	28	2	4	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 120
day 1

score	410	320	95	450

status	0	0	0	0

commands
20
6	m	l	
18	m	d	
7	m	u	
8	m	l	
9	m	u	
20	m	u	
5	m	d	
10	m	r	
11	m	r	
13	m	u	
15	m	r	
25	m	r	
27	m	r	
4	m	d	
22	m	r	
2	m	r	
1	m	r	
23	m	r	
0	m	d	
17	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b............b.CbF.b....
01 .B.C..BBBBB..MBBBBBBBBBBB...F.
02 .B.M......B...B...W.W.........
03 .BBBBBBBBBB...B...........W...
04 ...C...C......BC............W.
05 ....M...BBB...B.C...........W.
06 ........B.B...B......C.......C
07 ........BMB...B.BBBBBB........
08 ..C....BB.B...BbB....bC.....F.
09 b...bB.c..B.W...BBBBBBBB......
10 .BBB.BBB.CB............B......
11 .BFB...B...............B......
12 .BM....B....W..........B......
13 .BBBBBBB....C...........C.....
14 .........M...........W........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	6	29	n	40
b	1	0	4	3	n	60
b	2	0	6	21	n	60
b	3	0	5	16	n	40
w	4	0	9	12	g	80
w	5	0	12	12	h	100
b	6	1	13	12	n	60
b	7	1	8	2	n	60
b	8	1	4	7	n	60
b	9	1	10	9	n	60
w	10	1	5	28	b	100
w	11	1	2	20	h	60
b	13	2	13	24	n	60
b	15	2	9	7	n	60
w	17	2	14	21	h	100
b	18	3	1	3	n	60
b	20	3	4	15	n	40
w	22	3	3	26	b	80
w	23	3	2	18	g	100
b	25	2	0	21	n	20
b	27	3	8	22	n	60
w	28	2	4	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 121
day 1

score	410	320	100	450

status	0	0	0	0

commands
22
18	m	d	
6	m	l	
20	m	u	
27	m	u	
7	m	u	
13	m	l	
8	m	l	
9	m	u	
10	m	d	
15	m	r	
1	m	r	
11	m	r	
5	m	l	
25	m	r	
17	m	l	
28	m	d	
22	m	d	
4	m	u	
3	m	r	
23	m	r	
2	m	r	
0	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b............b..cF.b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B.C......B...B....W.W........
03 .BBBBBBBBBB...BC..............
04 ....C.C.......B...........W...
05 ....M...BBB...B..C..........W.
06 ........B.B...B.......C.....W.
07 ..C.....BMB...B.BBBBBBC......C
08 .......BB.B.W.BbB....b......F.
09 b...bB.bCCB.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B........M......B......
12 .BM....B...W...........B......
13 .BBBBBBB...C...........C......
14 .........M..........W.........

citizens
22
type	id	player	row	column	weapon	life
b	0	0	7	29	n	40
b	1	0	4	4	n	60
b	2	0	6	22	n	60
b	3	0	5	17	n	40
w	4	0	8	12	g	80
w	5	0	12	11	h	100
b	6	1	13	11	n	60
b	7	1	7	2	n	60
b	8	1	4	6	n	60
b	9	1	9	9	n	60
w	10	1	6	28	b	100
w	11	1	2	21	h	60
b	13	2	13	23	n	60
b	15	2	9	8	n	60
w	17	2	14	20	h	100
b	18	3	2	3	n	60
b	20	3	3	15	n	40
w	22	3	4	26	b	80
w	23	3	2	19	g	100
b	25	2	0	22	n	20
b	27	3	7	22	n	60
w	28	2	5	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 122
day 1

score	410	320	100	455

status	0	0	0	0

commands
22
6	m	l	
7	m	u	
8	m	l	
18	m	u	
9	m	u	
1	m	d	
0	m	d	
13	m	l	
20	m	r	
5	m	l	
4	m	r	
10	m	d	
27	m	r	
11	m	r	
3	m	r	
2	m	r	
15	m	r	
22	m	d	
25	m	r	
23	m	r	
17	m	u	
28	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b............b..bC.b....
01 .B.C..BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B.....W.W.......
03 .BBBBBBBBBB...B.C.............
04 .....C........B...............
05 ....C...BBB...B...C.......W...
06 ..C.....B.B...B........C....W.
07 ........BMB...B.BBBBBB.C....W.
08 .......BBCB..WBbB....b......FC
09 b...bB.b.CB.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B........M......B......
12 .BM....B..W............B......
13 .BBBBBBB..C.........W.C.......
14 .........M....................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	8	29	n	40
b	1	0	5	4	n	60
b	2	0	6	23	n	60
b	3	0	5	18	n	40
w	4	0	8	13	g	80
w	5	0	12	10	h	100
b	6	1	13	10	n	60
b	7	1	6	2	n	60
b	8	1	4	5	n	60
b	9	1	8	9	n	60
w	10	1	7	28	b	100
w	11	1	2	22	h	60
b	13	2	13	22	n	60
b	15	2	9	9	n	60
w	17	2	13	20	h	100
b	18	3	1	3	n	60
b	20	3	3	16	n	40
w	22	3	5	26	b	80
w	23	3	2	20	g	100
b	25	2	0	23	n	40
b	27	3	7	23	n	60
w	28	2	6	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 123
day 1

score	415	320	100	455

status	0	0	0	0

commands
18
13	m	u	
18	m	u	
4	m	d	
6	m	r	
0	m	l	
7	m	r	
3	m	r	
8	m	r	
9	m	u	
20	m	r	
25	m	l	
17	m	u	
27	m	l	
22	m	d	
2	m	r	
11	m	r	
23	m	r	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C..b............b..c..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B......W.W......
03 .BBBBBBBBBB...B..C............
04 ......C.......B...............
05 ........BBB...B....C..........
06 ...CC...B.B...B.........C.W.W.
07 ........BCB...B.BBBBBBC.....W.
08 .......BB.B...BbB....b......C.
09 bM..bB.b.CB..W..BBBBBBBB......
10 .BBB.BBB..B............B......
11 .BFB...B........M......B......
12 .BM....B..W.........W.CB......
13 MBBBBBBB...C..................
14 .........M....................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	8	28	n	60
b	1	0	6	4	n	60
b	2	0	6	24	n	60
b	3	0	5	19	n	40
w	4	0	9	13	g	80
w	5	0	12	10	h	100
b	6	1	13	11	n	60
b	7	1	6	3	n	60
b	8	1	4	6	n	60
b	9	1	7	9	n	60
w	10	1	7	28	b	100
w	11	1	2	23	h	60
b	13	2	12	22	n	60
b	15	2	9	9	n	60
w	17	2	12	20	h	100
b	18	3	0	3	n	60
b	20	3	3	17	n	40
w	22	3	6	26	b	80
w	23	3	2	21	g	100
b	25	2	0	22	n	40
b	27	3	7	22	n	60
w	28	2	6	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 124
day 1

score	415	325	100	455

status	0	0	0	0

commands
21
18	m	r	
5	m	r	
6	m	r	
20	m	r	
27	m	u	
7	m	l	
8	m	r	
13	m	u	
22	m	l	
15	m	d	
23	m	r	
4	m	d	
3	m	r	
9	m	u	
2	m	d	
25	m	l	
1	m	d	
0	m	d	
17	m	u	
10	m	d	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C.b............b.Cb..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B.......W.W.....
03 .BBBBBBBBBB...B...C...........
04 .......C......B...............
05 ........BBB...B.....C.........
06 ..C.....BCB...B.......C..W..W.
07 ....C...B.B...B.BBBBBB..C.....
08 .......BB.B...BbB....b......W.
09 bM..bB.b..B.....BBBBBBBB....C.
10 .BBB.BBB.CB..W.........B......
11 .BFB...B........M...W.CB......
12 .BM....B...W...........B......
13 MBBBBBBB....C.............M...
14 .........M....................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	9	28	n	60
b	1	0	7	4	n	60
b	2	0	7	24	n	60
b	3	0	5	20	n	40
w	4	0	10	13	g	80
w	5	0	12	11	h	100
b	6	1	13	12	n	60
b	7	1	6	2	n	60
b	8	1	4	7	n	60
b	9	1	6	9	n	60
w	10	1	8	28	b	100
w	11	1	2	24	h	60
b	13	2	11	22	n	60
b	15	2	10	9	n	60
w	17	2	11	20	h	100
b	18	3	0	4	n	60
b	20	3	3	18	n	40
w	22	3	6	25	b	80
w	23	3	2	22	g	100
b	25	2	0	21	n	40
b	27	3	6	22	n	60
w	28	2	6	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 125
day 0

score	415	325	100	455

status	0	0	0	0

commands
22
6	m	r	
13	m	d	
7	m	l	
15	m	d	
25	m	l	
2	m	d	
18	m	r	
0	m	r	
20	m	d	
5	m	r	
4	m	d	
27	m	d	
8	m	r	
17	m	l	
9	m	d	
22	m	d	
28	m	d	
10	m	d	
11	m	r	
3	m	d	
23	m	r	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Cb............bC.b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B........W.W....
03 .BBBBBBBBBB...B...............
04 ........C.....B...C...........
05 ........BBB..MB...............
06 .C......B.B...B.....C.........
07 ........BCB...B.BBBBBBC..W..W.
08 ....C..BB.B...BbB....b..C.....
09 bM..bB.b..B.....BBBBBBBB....WC
10 .BBB.BBB..B............B......
11 .BFB...B.C...W..M..W...B......
12 .BM....B....W.........CB......
13 MBBBBBBB.....C............M...
14 .........M....................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	9	29	n	60
b	1	0	8	4	n	60
b	2	0	8	24	n	60
b	3	0	6	20	n	40
w	4	0	11	13	g	80
w	5	0	12	12	h	100
b	6	1	13	13	n	60
b	7	1	6	1	n	60
b	8	1	4	8	n	60
b	9	1	7	9	n	60
w	10	1	9	28	b	100
w	11	1	2	25	h	60
b	13	2	12	22	n	60
b	15	2	11	9	n	60
w	17	2	11	19	h	100
b	18	3	0	5	n	60
b	20	3	4	18	n	40
w	22	3	7	25	b	80
w	23	3	2	23	g	100
b	25	2	0	20	n	40
b	27	3	7	22	n	60
w	28	2	7	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 126
day 0

score	415	325	100	455

status	0	0	0	0

commands
22
13	m	d	
18	m	r	
20	m	d	
2	m	d	
15	m	d	
0	m	u	
6	m	d	
5	m	u	
25	m	l	
27	m	u	
7	m	d	
8	m	r	
17	m	l	
22	m	d	
9	m	d	
28	m	d	
4	m	d	
3	m	l	
1	m	d	
23	m	r	
10	m	r	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......c............c..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B.........W.W...
03 .BBBBBBBBBB...B...............
04 .........C....B...............
05 ........BBB.MMB...C...........
06 ........B.B...B....C..C.......
07 .C......B.B...B.BBBBBB........
08 .......BBCB...BbB....b...W..WC
09 bM..cB.b..B.....BBBBBBBBC....W
10 .BBB.BBB..B............B......
11 .BFB...B....W...M.W....B......
12 .BM....B.C...W.........B......
13 MBBBBBBB..............C...M...
14 .........M...C................

citizens
22
type	id	player	row	column	weapon	life
b	0	0	8	29	n	60
b	1	0	9	4	n	60
b	2	0	9	24	n	60
b	3	0	6	19	n	40
w	4	0	12	13	g	80
w	5	0	11	12	h	100
b	6	1	14	13	n	60
b	7	1	7	1	n	60
b	8	1	4	9	n	60
b	9	1	8	9	n	60
w	10	1	9	29	b	100
w	11	1	2	26	h	60
b	13	2	13	22	n	60
b	15	2	12	9	n	60
w	17	2	11	18	h	100
b	18	3	0	6	n	60
b	20	3	5	18	n	40
w	22	3	8	25	b	80
w	23	3	2	24	g	100
b	25	2	0	19	n	40
b	27	3	6	22	n	60
w	28	2	8	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 127
day 0

score	415	325	100	455

status	0	0	0	0

commands
22
13	m	r	
18	m	r	
15	m	d	
20	m	u	
27	m	d	
6	m	r	
22	m	d	
7	m	d	
25	m	l	
17	m	l	
28	m	r	
2	m	d	
0	m	u	
23	m	r	
5	m	d	
4	m	d	
8	m	r	
9	m	d	
3	m	l	
10	m	u	
1	m	l	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......bC..........Cb..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B..........W.W..
03 .BBBBBBBBBB...B...............
04 ..........C...B...C...........
05 ........BBB.MMB...............
06 ........B.B...B...C...........
07 ........B.B...B.BBBBBBC......C
08 .C.....BB.B...BbB....b......WW
09 bM.CbB.b.CB.....BBBBBBBB.W....
10 .BBB.BBB..B............BC.....
11 .BFB...B........MW..M..B......
12 .BM....B....W..........B......
13 MBBBBBBB.C...W.........C..M...
14 .........M....C...............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	7	29	n	40
b	1	0	9	3	n	60
b	2	0	10	24	n	60
b	3	0	6	18	n	40
w	4	0	13	13	g	80
w	5	0	12	12	h	100
b	6	1	14	14	n	60
b	7	1	8	1	n	60
b	8	1	4	10	n	60
b	9	1	9	9	n	60
w	10	1	8	29	b	100
w	11	1	2	27	h	60
b	13	2	13	23	n	60
b	15	2	13	9	n	60
w	17	2	11	17	h	100
b	18	3	0	7	n	60
b	20	3	4	18	n	40
w	22	3	9	25	b	80
w	23	3	2	25	g	100
b	25	2	0	18	n	40
b	27	3	7	22	n	60
w	28	2	8	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 128
day 0

score	415	325	100	455

status	0	0	0	0

commands
22
6	m	r	
13	m	r	
15	m	d	
2	m	d	
18	m	r	
0	m	u	
25	m	l	
5	m	d	
7	m	d	
20	m	u	
27	m	d	
22	m	d	
23	m	r	
17	m	l	
28	m	r	
4	m	r	
8	m	r	
1	m	l	
3	m	l	
9	m	l	
10	m	u	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.C........C.b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B...........W.W.
03 .BBBBBBBBBB...B...C...........
04 ...........C..B...............
05 ........BBB.MMB...............
06 ........B.B...B..C...........C
07 ........B.B...B.BBBBBB.......W
08 .......BB.B...BbB....bC.....W.
09 bCC.bB.bC.B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B.W....
11 .BFB...B........W...M..BC.....
12 .BM....B...............B......
13 MBBBBBBB....W.W.........C.M...
14 .........C.....C..............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	6	29	n	40
b	1	0	9	2	n	60
b	2	0	11	24	n	60
b	3	0	6	17	n	40
w	4	0	13	14	g	80
w	5	0	13	12	h	100
b	6	1	14	15	n	60
b	7	1	9	1	n	60
b	8	1	4	11	n	60
b	9	1	9	8	n	60
w	10	1	7	29	b	80
w	11	1	2	28	h	60
b	13	2	13	24	n	60
b	15	2	14	9	n	60
w	17	2	11	16	h	100
b	18	3	0	8	n	60
b	20	3	3	18	n	40
w	22	3	10	25	b	80
w	23	3	2	26	g	100
b	25	2	0	17	n	40
b	27	3	8	22	n	60
w	28	2	8	28	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 129
day 0

score	415	330	110	455

status	0	0	0	0

commands
22
18	m	r	
20	m	d	
13	m	r	
2	m	d	
15	m	l	
0	m	u	
5	m	d	
25	m	l	
17	m	d	
28	m	r	
4	m	r	
6	m	r	
7	m	u	
27	m	r	
22	m	d	
8	m	d	
9	m	d	
3	m	l	
1	m	l	
23	m	r	
10	m	u	
11	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b..C......C..b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B............W.W
03 .BBBBBBBBBB...B...............
04 ..............B...C...........
05 ........BBBCMMB..............C
06 ........B.B...B.C............W
07 ........B.B...B.BBBBBB........
08 .C.....BB.B...BbB....b.C.....W
09 bC..bB.b..B.....BBBBBBBB......
10 .BBB.BBBC.B.F..........B......
11 .BFB...B............M..B.W....
12 .BM....B........W......BC.....
13 MBBBBBBB.......W.........CM...
14 ........C...W...C.............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	5	29	n	40
b	1	0	9	1	n	60
b	2	0	12	24	n	60
b	3	0	6	16	n	40
w	4	0	13	15	g	80
w	5	0	14	12	h	100
b	6	1	14	16	n	60
b	7	1	8	1	n	60
b	8	1	5	11	n	60
b	9	1	10	8	n	60
w	10	1	6	29	b	80
w	11	1	2	29	h	60
b	13	2	13	25	n	60
b	15	2	14	8	n	60
w	17	2	12	16	h	100
b	18	3	0	9	n	60
b	20	3	4	18	n	40
w	22	3	11	25	b	80
w	23	3	2	27	g	100
b	25	2	0	16	n	40
b	27	3	8	23	n	60
w	28	2	8	29	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 130
day 0

score	415	330	110	455

status	0	0	0	0

commands
22
18	m	r	
13	m	r	
2	m	d	
20	m	d	
6	m	r	
15	m	l	
0	m	u	
5	m	r	
25	m	l	
4	m	u	
3	m	l	
7	m	u	
1	m	l	
17	m	d	
8	m	r	
28	m	u	
27	m	r	
22	m	d	
23	m	r	
9	m	d	
10	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b...C....C...b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B.............W.
03 .BBBBBBBBBB...B..............W
04 ..............B..............C
05 ........BBB.CMB...C..........W
06 ........B.B...BC..............
07 .C......B.B...B.BBBBBB.......W
08 .......BB.B...BbB....b..C.....
09 c...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 .BFB...BC...........M..B......
12 .BM....B.......W.......B.W....
13 MBBBBBBB........W.......C.C...
14 .......C.....W...C............

citizens
22
type	id	player	row	column	weapon	life
b	0	0	4	29	n	40
b	1	0	9	0	n	60
b	2	0	13	24	n	60
b	3	0	6	15	n	40
w	4	0	12	15	g	80
w	5	0	14	13	h	100
b	6	1	14	17	n	60
b	7	1	7	1	n	60
b	8	1	5	12	n	60
b	9	1	11	8	n	60
w	10	1	5	29	b	80
w	11	1	3	29	h	60
b	13	2	13	26	n	60
b	15	2	14	7	n	60
w	17	2	13	16	h	100
b	18	3	0	10	n	60
b	20	3	5	18	n	40
w	22	3	12	25	b	80
w	23	3	2	28	g	100
b	25	2	0	15	n	40
b	27	3	8	24	n	60
w	28	2	7	29	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 131
day 0

score	415	335	115	455

status	0	0	0	0

commands
21
13	m	d	
6	m	r	
7	m	u	
8	m	d	
15	m	l	
9	m	r	
2	m	d	
10	m	u	
11	m	d	
18	m	r	
20	m	d	
25	m	l	
5	m	r	
17	m	d	
27	m	d	
22	m	d	
4	m	r	
28	m	u	
23	m	d	
3	m	d	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b....C..C....b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...F.
02 .B........B...B...............
03 .BBBBBBBBBB...B.............WW
04 ..............B...............
05 .....G..BBB..MB..............W
06 .C......B.B.C.B...C..........W
07 ........B.B...BCBBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..B.....BBBBBBBBC.....
10 CBBB.BBB..B.F..........B......
11 .BFB...B.C..........M..B......
12 .BM....B........W......B......
13 MBBBBBBB.................W....
14 ......C.......W.W.C.....C.C...

citizens
21
type	id	player	row	column	weapon	life
b	1	0	10	0	n	60
b	2	0	14	24	n	60
b	3	0	7	15	n	40
w	4	0	12	16	g	80
w	5	0	14	14	h	100
b	6	1	14	18	n	60
b	7	1	6	1	n	60
b	8	1	6	12	n	60
b	9	1	11	9	n	60
w	10	1	5	29	b	80
w	11	1	3	29	h	60
b	13	2	14	26	n	60
b	15	2	14	6	n	60
w	17	2	14	16	h	100
b	18	3	0	11	n	60
b	20	3	6	18	n	40
w	22	3	13	25	b	80
w	23	3	3	28	g	100
b	25	2	0	14	n	40
b	27	3	9	24	n	60
w	28	2	6	29	b	100

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 132
day 0

score	415	435	115	455

status	0	0	0	0

commands
21
13	m	l	
2	m	l	
6	m	r	
5	m	r	
18	m	d	
7	m	u	
4	m	d	
15	m	l	
25	m	r	
20	m	r	
27	m	d	
8	m	d	
17	m	r	
22	m	d	
23	m	r	
3	m	d	
1	m	d	
28	m	u	
9	m	r	
10	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.Z......C...b..b..b....
01 .B....BBBBBC.MBBBBBBBBBBB...F.
02 .B........B...B.......M......W
03 .BBBBBBBBBB...B.............W.
04 .......M......B...............
05 .C...G..BBB..MB.............W.
06 ........B.B...B....C.........W
07 ........B.B.C.BCBBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........BC.....
11 CBFB...B..C.........M..B....M.
12 .BM....B...............B......
13 MBBBBBBB........W........W....
14 .....C.....F...W.W.C...C.C....

citizens
21
type	id	player	row	column	weapon	life
b	1	0	11	0	n	60
b	2	0	14	23	n	60
b	3	0	7	15	n	40
w	4	0	13	16	g	80
w	5	0	14	15	h	100
b	6	1	14	19	n	60
b	7	1	5	1	n	60
b	8	1	7	12	n	60
b	9	1	11	10	n	60
w	10	1	5	28	b	80
w	11	1	2	29	h	40
b	13	2	14	25	n	40
b	15	2	14	5	n	60
w	17	2	14	17	h	100
b	18	3	1	11	n	60
b	20	3	6	19	n	40
w	22	3	13	25	b	80
w	23	3	3	28	g	100
b	25	2	0	15	n	40
b	27	3	10	24	n	60
w	28	2	6	29	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	30
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 133
day 0

score	415	435	115	455

status	0	0	0	0

commands
21
13	m	r	
5	m	r	
18	m	u	
15	m	l	
4	m	r	
20	m	u	
6	m	r	
25	m	l	
7	m	u	
27	m	d	
22	m	d	
23	m	u	
17	m	r	
8	m	u	
1	m	d	
28	m	u	
3	m	d	
2	m	u	
9	m	r	
10	m	l	
11	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.Z..C..C....b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...FW
02 .B........B...B.......M.....W.
03 .BBBBBBBBBB...B...............
04 .C.....M......B...............
05 .....G..BBB..MB....C.......W.W
06 ........B.B.C.B...............
07 ........B.B...BCBBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 .BFB...B...C........M..BC...M.
12 CBM....B...............B......
13 MBBBBBBB.........W.....C......
14 ....C......F....W.W.C....WC...

citizens
21
type	id	player	row	column	weapon	life
b	1	0	12	0	n	60
b	2	0	13	23	n	60
b	3	0	7	15	n	40
w	4	0	13	17	g	80
w	5	0	14	16	h	100
b	6	1	14	20	n	60
b	7	1	4	1	n	60
b	8	1	6	12	n	60
b	9	1	11	11	n	60
w	10	1	5	27	b	80
w	11	1	1	29	h	40
b	13	2	14	26	n	40
b	15	2	14	4	n	60
w	17	2	14	18	h	100
b	18	3	0	11	n	60
b	20	3	5	19	n	40
w	22	3	14	25	b	80
w	23	3	2	28	g	100
b	25	2	0	14	n	40
b	27	3	11	24	n	60
w	28	2	5	29	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	20
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 134
day 0

score	415	435	115	455

status	0	0	0	0

commands
21
18	m	l	
5	m	r	
6	m	r	
7	m	r	
20	m	u	
13	m	r	
15	m	l	
8	m	u	
27	m	r	
25	m	r	
4	m	r	
17	m	r	
9	m	u	
1	m	d	
10	m	l	
11	m	u	
3	m	d	
2	m	l	
22	m	r	
23	m	u	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.Z.C....C...b..b..b...W
01 .B....BBBBB..MBBBBBBBBBBB...W.
02 .B........B...B.......M.......
03 .BBBBBBBBBB...B...............
04 ..C....M......BM...C..M.......
05 .....G..BBB.CMB...........W.W.
06 ........B.B...B...............
07 ........B.B...BCBBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..BCF..........B......
11 .BFB...B............M..B.C..M.
12 .BM....B...............B......
13 CBBBBBBB..........W...C.......
14 ...C.......F.....W.W.C....WC..

citizens
21
type	id	player	row	column	weapon	life
b	1	0	13	0	n	60
b	2	0	13	22	n	60
b	3	0	7	15	n	40
w	4	0	13	18	g	80
w	5	0	14	17	h	100
b	6	1	14	21	n	60
b	7	1	4	2	n	60
b	8	1	5	12	n	60
b	9	1	10	11	n	60
w	10	1	5	26	b	80
w	11	1	0	29	h	40
b	13	2	14	27	n	40
b	15	2	14	3	n	60
w	17	2	14	19	h	100
b	18	3	0	10	n	60
b	20	3	4	19	n	40
w	22	3	14	26	b	80
w	23	3	1	28	g	100
b	25	2	0	15	n	40
b	27	3	11	25	n	60
w	28	2	5	28	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 135
day 0

score	420	435	115	455

status	0	0	0	0

commands
21
18	m	l	
13	m	r	
6	m	r	
20	m	r	
27	m	r	
7	m	r	
5	m	r	
15	m	r	
22	m	r	
8	m	u	
4	m	r	
3	m	u	
23	m	u	
9	m	u	
10	m	u	
2	m	u	
11	m	d	
1	m	u	
25	m	l	
17	m	r	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.ZC....C....b..b..b..W.
01 .B....BBBBB..MBBBBBBBBBBB....W
02 .B........B...B.......M.......
03 .BBBBBBBBBB...B...............
04 ...C...M....C.BM....C.M...W...
05 .....G..BBB..MB............W..
06 ........B.B...BC..............
07 ........B.B...B.BBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..BC....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 .BFB...B............M..B..C.M.
12 CBM....B..............CB......
13 .BBBBBBB...........W..........
14 ....C......F......W.W.C....WC.

citizens
21
type	id	player	row	column	weapon	life
b	1	0	12	0	n	60
b	2	0	12	22	n	60
b	3	0	6	15	n	40
w	4	0	13	19	g	80
w	5	0	14	18	h	100
b	6	1	14	22	n	60
b	7	1	4	3	n	60
b	8	1	4	12	n	60
b	9	1	9	11	n	60
w	10	1	4	26	b	80
w	11	1	1	29	h	40
b	13	2	14	28	n	40
b	15	2	14	4	n	60
w	17	2	14	20	h	100
b	18	3	0	9	n	60
b	20	3	4	20	n	40
w	22	3	14	27	b	80
w	23	3	0	28	g	100
b	25	2	0	14	n	40
b	27	3	11	26	n	60
w	28	2	5	27	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 136
day 0

score	420	435	115	455

status	0	0	0	0

commands
21
5	m	r	
18	m	l	
13	m	r	
20	m	r	
6	m	r	
4	m	r	
3	m	u	
7	m	r	
2	m	u	
8	m	u	
27	m	r	
22	m	r	
1	m	u	
23	m	d	
15	m	r	
9	m	u	
10	m	l	
11	m	d	
25	m	l	
17	m	r	
28	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.C....C.....b..b..b....
01 .B....BBBBB..MBBBBBBBBBBB...W.
02 .B........B...B.......M......W
03 .BBBBBBBBBB.C.B...............
04 ....C..M......BM.....CM..W.W..
05 .....G..BBB..MBC..............
06 ........B.B...B...............
07 ........B.B...B.BBBBBB........
08 .....F.BB.BC..BbB....b........
09 b...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 CBFB...B............M.CB...CM.
12 .BM....B....G..........B......
13 .BBBBBBB............W.........
14 .....C.....F.......W.W.C....WC

citizens
21
type	id	player	row	column	weapon	life
b	1	0	11	0	n	60
b	2	0	11	22	n	60
b	3	0	5	15	n	40
w	4	0	13	20	g	80
w	5	0	14	19	h	100
b	6	1	14	23	n	60
b	7	1	4	4	n	60
b	8	1	3	12	n	60
b	9	1	8	11	n	60
w	10	1	4	25	b	80
w	11	1	2	29	h	40
b	13	2	14	29	n	40
b	15	2	14	5	n	60
w	17	2	14	21	h	100
b	18	3	0	8	n	60
b	20	3	4	21	n	40
w	22	3	14	28	b	80
w	23	3	1	28	g	100
b	25	2	0	13	n	40
b	27	3	11	27	n	60
w	28	2	4	27	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 137
day 0

score	420	435	115	455

status	0	0	0	0

commands
21
13	m	u	
18	m	r	
15	m	r	
5	m	r	
4	m	u	
25	m	d	
20	m	r	
27	m	r	
17	m	r	
6	m	r	
28	m	l	
7	m	r	
8	m	u	
22	m	r	
9	m	u	
23	m	d	
10	m	l	
3	m	u	
2	m	d	
11	m	d	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b..C.........b..b..b....
01 .B....BBBBB..CBBBBBBBBBBB.....
02 .B........B.C.B.......M.....W.
03 .BBBBBBBBBB...B..............W
04 .....C.M......BC......C.W.W...
05 .....G..BBB..MB...............
06 ........B.B...B...............
07 ........B.BC..B.BBBBBB........
08 .....F.BB.B...BbB....b........
09 b...bB.b..B.....BBBBBBBB......
10 CBBB.BBB..B.F..........B......
11 .BFB...B............M..B....C.
12 .BM....B....G.......W.CB......
13 .BBBBBBB.....................C
14 ......C....F........W.W.C....W

citizens
21
type	id	player	row	column	weapon	life
b	1	0	10	0	n	60
b	2	0	12	22	n	60
b	3	0	4	15	n	40
w	4	0	12	20	g	80
w	5	0	14	20	h	100
b	6	1	14	24	n	60
b	7	1	4	5	n	60
b	8	1	2	12	n	60
b	9	1	7	11	n	60
w	10	1	4	24	b	80
w	11	1	3	29	h	40
b	13	2	13	29	n	40
b	15	2	14	6	n	60
w	17	2	14	22	h	100
b	18	3	0	9	n	60
b	20	3	4	22	n	40
w	22	3	14	29	b	80
w	23	3	2	28	g	100
b	25	2	1	13	n	40
b	27	3	11	28	n	60
w	28	2	4	26	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 138
day 0

score	425	435	120	465

status	0	0	0	0

commands
21
18	m	r	
20	m	u	
6	m	r	
27	m	d	
7	m	r	
8	m	d	
13	m	u	
15	m	r	
22	m	u	
23	m	d	
9	m	u	
2	m	u	
10	m	l	
11	m	d	
5	m	r	
25	m	u	
17	m	r	
28	m	l	
4	m	u	
3	m	u	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b...C..C.....b..b..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B.......M.......
03 .BBBBBBBBBB.C.BC......C.....W.
04 ......CM......B........W.W...W
05 .....G..BBB..MB...............
06 ........B.BC..B...............
07 ........B.B...B.BBBBBB........
08 .....F.BB.B...BbB....b........
09 c...bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 .BFB...B............W.CB......
12 .BM....B....G...M......B....CC
13 .BBBBBBB.....................W
14 .......C...F.........W.W.C....

citizens
21
type	id	player	row	column	weapon	life
b	1	0	9	0	n	60
b	2	0	11	22	n	60
b	3	0	3	15	n	40
w	4	0	11	20	g	80
w	5	0	14	21	h	100
b	6	1	14	25	n	60
b	7	1	4	6	n	60
b	8	1	3	12	n	60
b	9	1	6	11	n	60
w	10	1	4	23	b	80
w	11	1	4	29	h	40
b	13	2	12	29	n	40
b	15	2	14	7	n	60
w	17	2	14	23	h	100
b	18	3	0	10	n	60
b	20	3	3	22	n	40
w	22	3	13	29	b	80
w	23	3	3	28	g	100
b	25	2	0	13	n	40
b	27	3	12	28	n	60
w	28	2	4	25	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 139
day 0

score	430	435	120	465

status	0	0	0	0

commands
21
6	m	r	
13	m	u	
5	m	r	
18	m	r	
7	m	r	
4	m	r	
15	m	r	
25	m	r	
17	m	r	
3	m	u	
20	m	u	
27	m	u	
22	m	u	
2	m	d	
1	m	r	
23	m	d	
8	m	d	
28	m	l	
9	m	u	
10	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b....C..C....b..b..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...BC......C.......
03 .BBBBBBBBBB...B........W......
04 .......C....C.B.........W...W.
05 .....G..BBBC.MB..............W
06 ........B.B...B...............
07 ........B.B...B.BBBBBB........
08 .....F.BB.B...BbB....b........
09 bC..bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B......
11 .BFB...B.............W.B....CC
12 .BM....B....G...M.....CB.....W
13 .BBBBBBB......................
14 ........C..F..........W.W.C...

citizens
21
type	id	player	row	column	weapon	life
b	1	0	9	1	n	60
b	2	0	12	22	n	60
b	3	0	2	15	n	40
w	4	0	11	21	g	80
w	5	0	14	22	h	100
b	6	1	14	26	n	60
b	7	1	4	7	n	60
b	8	1	4	12	n	60
b	9	1	5	11	n	60
w	10	1	3	23	b	80
w	11	1	5	29	h	40
b	13	2	11	29	n	40
b	15	2	14	8	n	60
w	17	2	14	24	h	100
b	18	3	0	11	n	60
b	20	3	2	22	n	40
w	22	3	12	29	b	80
w	23	3	4	28	g	100
b	25	2	0	14	n	40
b	27	3	11	28	n	60
w	28	2	4	24	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 140
day 0

score	430	440	120	470

status	0	0	0	0

commands
20
13	m	u	
5	m	r	
15	m	r	
25	m	r	
18	m	d	
6	m	r	
17	m	r	
20	m	l	
28	m	u	
4	m	r	
3	m	d	
2	m	l	
27	m	u	
22	m	u	
23	m	d	
1	m	r	
7	m	l	
8	m	d	
10	m	u	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b........C...b..b..b....
01 .B....BBBBBC..BBBBBBBBBBB.....
02 .B........B...B......C.W......
03 .BBBBBBBBBB...BC........W.....
04 ......C.......B...............
05 .....G..BBBCCMB.............W.
06 ........B.B...B..............W
07 ........B.B...B.BBBBBB........
08 .....F.BB.B...BbB....b........
09 b.C.bB.b..B.....BBBBBBBB......
10 .BBB.BBB..B.F..........B....CC
11 .BFB...B..............WB.....W
12 .BM....B....G...M....C.B......
13 .BBBBBBB......................
14 .........C.F...........W.W.C..

citizens
21
type	id	player	row	column	weapon	life
b	1	0	9	2	n	60
b	2	0	12	21	n	60
b	3	0	3	15	n	40
w	4	0	11	22	g	80
w	5	0	14	23	h	100
b	6	1	14	27	n	60
b	7	1	4	6	n	60
b	8	1	5	12	n	60
b	9	1	5	11	n	60
w	10	1	2	23	b	80
w	11	1	6	29	h	40
b	13	2	10	29	n	40
b	15	2	14	9	n	60
w	17	2	14	25	h	100
b	18	3	1	11	n	60
b	20	3	2	21	n	40
w	22	3	11	29	b	80
w	23	3	5	28	g	100
b	25	2	0	15	n	40
b	27	3	10	28	n	60
w	28	2	3	24	b	80

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 141
day 0

score	430	440	120	470

status	0	0	0	0

commands
21
6	m	r	
5	m	r	
13	m	u	
18	m	d	
4	m	d	
15	m	r	
25	m	l	
3	m	d	
20	m	d	
2	m	l	
17	m	r	
27	m	u	
7	m	l	
22	m	u	
1	m	r	
28	m	u	
8	m	d	
9	m	u	
23	m	d	
10	m	l	
11	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b.......C....b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........BC..B.......W.W.....
03 .BBBBBBBBBB...B......C........
04 .....C.....C..BC..............
05 .....G..BBB..MB...............
06 ........B.B.C.B.............W.
07 ........B.B...B.BBBBBB.C.....W
08 .....F.BB.B...BbB....b........
09 b..CbB.b..B.....BBBBBBBB....CC
10 .BBB.BBB..B.FM.........B.....W
11 .BFB..MB...............B......
12 .BM....B....G...M...C.WB......
13 .BBBBBBB......................
14 ..........CF............W.W.C.

citizens
22
type	id	player	row	column	weapon	life
b	1	0	9	3	n	60
b	2	0	12	20	n	60
b	3	0	4	15	n	40
w	4	0	12	22	g	80
w	5	0	14	24	h	100
b	6	1	14	28	n	60
b	7	1	4	5	n	60
b	8	1	6	12	n	60
b	9	1	4	11	n	60
w	10	1	2	22	b	80
w	11	1	7	29	h	40
b	13	2	9	29	n	40
b	15	2	14	10	n	60
w	17	2	14	26	h	100
b	18	3	2	11	n	60
b	20	3	3	21	n	40
w	22	3	10	29	b	80
w	23	3	6	28	g	100
b	25	2	0	14	n	40
b	27	3	9	28	n	60
w	28	2	2	24	b	80
b	29	3	7	23	n	60

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 142
day 0

score	430	440	120	470

status	0	0	0	0

commands
22
6	m	r	
13	m	u	
7	m	l	
8	m	d	
18	m	u	
5	m	r	
15	m	r	
4	m	d	
25	m	l	
3	m	d	
9	m	d	
10	m	l	
20	m	d	
11	m	d	
2	m	l	
17	m	r	
1	m	r	
28	m	l	
27	m	u	
29	m	u	
22	m	u	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......b......C.....b..b..b....
01 MB...MBBBBBC..BBBBBBBBBBB.....
02 .B........B...B......W.W......
03 .BBBBBBBBBB...B...............
04 ....C.........B......C........
05 .....G..BBBC.MBC..............
06 ........B.B...B........C......
07 ........B.B.C.B.BBBBBB......WW
08 .....F.BB.B...BbB....b......CC
09 b...cB.b..B.....BBBBBBBB.....W
10 .BBB.BBB..B.FM........MB......
11 .BFB..MB...............B......
12 .BM....B....G...M..C...B......
13 .BBBBBBB..............W.......
14 ...........C.............W.W.C

citizens
22
type	id	player	row	column	weapon	life
b	1	0	9	4	n	60
b	2	0	12	19	n	60
b	3	0	5	15	n	40
w	4	0	13	22	g	80
w	5	0	14	25	h	100
b	6	1	14	29	n	60
b	7	1	4	4	n	60
b	8	1	7	12	n	60
b	9	1	5	11	n	60
w	10	1	2	21	b	80
w	11	1	7	29	h	40
b	13	2	8	29	n	20
b	15	2	14	11	n	60
w	17	2	14	27	h	100
b	18	3	1	11	n	60
b	20	3	4	21	n	40
w	22	3	9	29	b	80
w	23	3	7	28	g	100
b	25	2	0	13	n	40
b	27	3	8	28	n	60
w	28	2	2	23	b	80
b	29	3	6	23	n	60

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 143
day 0

score	430	440	120	470

status	0	0	0	0

commands
21
18	m	d	
5	m	r	
20	m	d	
4	m	r	
1	m	u	
3	m	d	
27	m	d	
6	m	u	
13	m	u	
15	m	r	
2	m	l	
29	m	l	
22	m	u	
25	m	l	
17	m	r	
7	m	l	
28	m	l	
8	m	d	
23	m	r	
9	m	r	
10	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb.....C......b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........BC..B...M...W..F....
03 .BBBBBBBBBB...B......W........
04 ...C..........B...............
05 .....G..BBB.CMB......C........
06 ........B.B...BC......C.......
07 ........B.B...B.BBBBBB......W.
08 ....CF.BB.B.C.BbB....b........
09 b...bB.b..B.....BBBBBBBB....CW
10 .BBB.BBB..B.FM........MB......
11 .BFB..MB...............B......
12 .BM....B....G...M.C....B......
13 .BBBBBBB...............W.....C
14 ............C.............W.W.

citizens
20
type	id	player	row	column	weapon	life
b	1	0	8	4	n	60
b	2	0	12	18	n	60
b	3	0	6	15	n	40
w	4	0	13	23	g	80
w	5	0	14	26	h	100
b	6	1	13	29	n	60
b	7	1	4	3	n	60
b	8	1	8	12	n	60
b	9	1	5	12	n	60
w	10	1	3	21	b	80
b	15	2	14	12	n	60
w	17	2	14	28	h	100
b	18	3	2	11	n	60
b	20	3	5	21	n	40
w	22	3	9	29	b	80
w	23	3	7	28	g	100
b	25	2	0	12	n	40
b	27	3	9	28	n	60
w	28	2	2	22	b	80
b	29	3	6	22	n	60

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 144
day 0

score	430	440	120	820

status	0	0	0	0

commands
20
15	m	r	
18	m	d	
5	m	u	
20	m	d	
25	m	l	
17	m	r	
28	m	d	
27	m	d	
4	m	r	
29	m	u	
6	m	u	
22	m	d	
2	m	l	
7	m	l	
23	m	d	
8	m	d	
9	m	r	
10	m	d	
1	m	r	
3	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb....C.......b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 .BBBBBBBBBBC..B.......W.......
04 ..C.....Z.....B......W........
05 .....G..BBB..CB.......C.......
06 ........B.B...B......C........
07 ........B.B...BCBBBBBB........
08 .....C.BB.B...BbB....b......W.
09 b...bB.b..B.C...BBBBBBBB......
10 .BBB.BBB..B.FM........MB....CW
11 .BFB..MB...............B......
12 .BM....B....G...MC.....B.....C
13 .BBBBBBB................W.W...
14 .............C...............W

citizens
20
type	id	player	row	column	weapon	life
b	1	0	8	5	n	60
b	2	0	12	17	n	60
b	3	0	7	15	n	40
w	4	0	13	24	g	80
w	5	0	13	26	h	100
b	6	1	12	29	n	60
b	7	1	4	2	n	60
b	8	1	9	12	n	60
b	9	1	5	13	n	60
w	10	1	4	21	b	80
b	15	2	14	13	n	60
w	17	2	14	29	h	100
b	18	3	3	11	n	60
b	20	3	6	21	n	40
w	22	3	10	29	b	80
w	23	3	8	28	g	100
b	25	2	0	11	n	40
b	27	3	10	28	n	60
w	28	2	3	22	b	80
b	29	3	5	22	n	60

barricades
9
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	15	10
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 145
day 0

score	430	445	120	820

status	0	0	0	0

commands
20
18	m	d	
6	m	d	
15	m	u	
25	m	l	
7	m	l	
5	m	u	
20	m	l	
4	m	r	
8	m	d	
2	m	l	
9	m	d	
3	m	d	
17	m	u	
1	m	l	
28	m	d	
10	m	d	
27	m	d	
29	m	d	
22	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb...C........b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 .BBBBBBBBBB...B...............
04 .C......Z..C..B.......W.......
05 .....G..BBB...B......W........
06 ........B.B..CB.....C.C.......
07 ........B.B...BCBBBBBB........
08 ....C..BB.B...B.B....b........
09 b...bB.b..B.....BBBBBBBB....W.
10 .BBB.BBB..B.CM........MB......
11 .BFB..MB...............B....CW
12 .BM....B....G...C......B..W...
13 .BBBBBBB.....C...........W...C
14 .............................W

citizens
20
type	id	player	row	column	weapon	life
b	1	0	8	4	n	60
b	2	0	12	16	n	60
b	3	0	7	15	n	40
w	4	0	13	25	g	80
w	5	0	12	26	h	100
b	6	1	13	29	n	40
b	7	1	4	1	n	60
b	8	1	10	12	n	60
b	9	1	6	13	n	60
w	10	1	5	21	b	80
b	15	2	13	13	n	60
w	17	2	14	29	h	100
b	18	3	4	11	n	60
b	20	3	6	20	n	40
w	22	3	11	29	b	80
w	23	3	9	28	g	100
b	25	2	0	10	n	40
b	27	3	11	28	n	60
w	28	2	4	22	b	80
b	29	3	6	22	n	60

barricades
8
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 146
day 0

score	435	445	120	820

status	0	0	0	0

commands
20
15	m	u	
25	m	r	
17	m	u	
18	m	l	
28	m	d	
20	m	l	
6	m	u	
5	m	u	
7	m	l	
27	m	l	
29	m	d	
4	m	r	
8	m	r	
9	m	d	
22	m	d	
10	m	l	
3	m	u	
2	m	u	
1	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb....C.......b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 .BBBBBBBBBB...B...............
04 C.......Z.C...B...............
05 .....G..BBB...B.....W.W.......
06 ........B.B...BC...C..........
07 ........B.B..CB.BBBBBBC.......
08 .......BB.B...B.B....b........
09 b...cB.b..B.....BBBBBBBB......
10 .BBB.BBB..B..C........MB....W.
11 .BFB..MB........C......B..WC.W
12 .BM....B....GC.........B......
13 .BBBBBBB..................W...
14 .............................W

citizens
19
type	id	player	row	column	weapon	life
b	1	0	9	4	n	60
b	2	0	11	16	n	60
b	3	0	6	15	n	40
w	4	0	13	26	g	80
w	5	0	11	26	h	100
b	7	1	4	0	n	60
b	8	1	10	13	n	60
b	9	1	7	13	n	60
w	10	1	5	20	b	80
b	15	2	12	13	n	60
w	17	2	14	29	h	100
b	18	3	4	10	n	60
b	20	3	6	19	n	40
w	22	3	11	29	b	80
w	23	3	10	28	g	100
b	25	2	0	11	n	40
b	27	3	11	27	n	60
w	28	2	5	22	b	80
b	29	3	7	22	n	60

barricades
8
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 147
day 0

score	435	450	120	920

status	0	0	0	0

commands
18
15	m	d	
25	m	d	
18	m	l	
17	m	l	
20	m	l	
27	m	u	
7	m	u	
8	m	d	
29	m	d	
22	m	d	
9	m	u	
5	m	r	
4	m	r	
28	m	d	
10	m	l	
3	m	u	
2	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb............b..b..b....
01 MB...MBBBBBC..BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 CBBBBBBBBBB...B...............
04 ........ZC....B...............
05 .....G..BBB...BC...W..........
06 ........B.B..CB...C...W.......
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B....bC.......
09 b...bB.b..B.....BBBBBBBB......
10 .BBBCBBB..B.....C.....MB...CW.
11 .BFB..MB.....C.........B...W..
12 .BM....B....G..........B.....W
13 .BBBBBBB.....C.............W..
14 ............................W.

citizens
19
type	id	player	row	column	weapon	life
b	1	0	10	4	n	60
b	2	0	10	16	n	60
b	3	0	5	15	n	40
w	4	0	13	27	g	80
w	5	0	11	27	h	100
b	7	1	3	0	n	60
b	8	1	11	13	n	60
b	9	1	6	13	n	60
w	10	1	5	19	b	80
b	15	2	13	13	n	60
w	17	2	14	28	h	100
b	18	3	4	9	n	60
b	20	3	6	18	n	40
w	22	3	12	29	b	80
w	23	3	10	28	g	100
b	25	2	1	11	n	40
b	27	3	10	27	n	60
w	28	2	6	22	b	80
b	29	3	8	22	n	60

barricades
8
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 148
day 0

score	435	450	120	920

status	0	0	0	0

commands
19
18	m	l	
5	m	d	
7	m	u	
20	m	l	
4	m	r	
8	m	u	
27	m	u	
15	m	r	
9	m	u	
29	m	l	
25	m	d	
3	m	u	
2	m	r	
17	m	l	
22	m	u	
10	m	l	
28	m	d	
1	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....Mb............b..b..b....
01 MB...MBBBBB...BBBBBBBBBBB.....
02 CB........BC..B...M......F....
03 .BBBBBBBBBB...B...............
04 ........C.....BC..............
05 .....G..BBB..CB...W...........
06 ........B.B...B..C............
07 ..C.....B.B...B.BBBBBBW.......
08 .......BB.B...B.B....c........
09 b...bB.b..B.....BBBBBBBB...C..
10 .BBB.BBB..B..C...C....MB......
11 .BFBC.MB...............B....WW
12 .BM....B....G..........B...W..
13 .BBBBBBB......C.............W.
14 ...M.......................W..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	4	n	60
b	2	0	10	17	n	60
b	3	0	4	15	n	40
w	4	0	13	28	g	80
w	5	0	12	27	h	100
b	7	1	2	0	n	60
b	8	1	10	13	n	60
b	9	1	5	13	n	60
w	10	1	5	18	b	80
b	15	2	13	14	n	60
w	17	2	14	27	h	100
b	18	3	4	8	n	60
b	20	3	6	17	n	40
w	22	3	11	29	b	80
w	23	3	11	28	g	100
b	25	2	2	11	n	40
b	27	3	9	27	n	60
w	28	2	7	22	b	80
b	29	3	8	21	n	60
b	30	2	7	2	n	60

barricades
8
player	row	column	resistance
3	0	6	40
2	0	19	40
2	0	22	40
0	0	25	40
3	8	21	40
0	9	0	40
0	9	4	40
2	9	7	40

round 149
day 0

score	435	450	120	920

status	0	0	0	0

commands
20
18	m	l	
5	m	d	
15	m	r	
25	m	d	
4	m	d	
30	m	d	
17	m	u	
28	m	d	
20	m	l	
27	m	d	
7	m	u	
1	m	r	
8	m	u	
3	m	u	
29	m	l	
9	m	l	
22	m	d	
23	m	d	
2	m	r	
10	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M........................
01 CB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 .BBBBBBBBBBC..BC..............
04 .......C......B...............
05 .....G..BBB.C.B..W............
06 ........B.B...B.C.............
07 ........B.B...B.BBBBBB........
08 ..C....BB.B...B.B...C.W.......
09 .....B...MB..C..BBBBBBBB......
10 .BBB.BBB..B.......C...MB...C..
11 .BFB.CMB...............B......
12 .BM....B....G..........B....WW
13 .BBBBBBB.......C...........W..
14 ...M.......................WW.

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	5	n	60
b	2	0	10	18	n	60
b	3	0	3	15	n	40
w	4	0	14	28	g	80
w	5	0	13	27	h	80
b	7	1	1	0	n	60
b	8	1	9	13	n	60
b	9	1	5	12	n	60
w	10	1	5	17	b	80
b	15	2	13	15	n	60
w	17	2	14	27	h	100
b	18	3	4	7	n	60
b	20	3	6	16	n	40
w	22	3	12	29	b	80
w	23	3	12	28	g	100
b	25	2	3	11	n	40
b	27	3	10	27	n	60
w	28	2	8	22	b	80
b	29	3	8	20	n	60
b	30	2	8	2	n	60

barricades
0
player	row	column	resistance

round 150
day 1

score	435	455	120	920

status	0	0	0	0

commands
18
15	m	r	
18	b	r	
20	m	u	
7	m	u	
25	m	u	
27	m	d	
30	m	d	
29	b	l	
28	m	r	
3	m	d	
8	m	l	
9	m	d	
10	m	u	
22	m	d	
2	m	l	
1	m	r	
23	m	d	
5	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....M........................
01 .B...MBBBBB...BBBBBBBBBBB.....
02 .B........BC..B...M......F....
03 .BBBBBBBBBB...B...............
04 .......Cb.....BC.W............
05 .....G..BBB...B.C.............
06 ........B.B.C.B...............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..bC..W......
09 ..C..B...MB.C...BBBBBBBB......
10 .BBB.BBB..B......C....MB......
11 .BFB..CB...............B...C..
12 .BM....B....G..........B......
13 .BBBBBBB........C.........W.WW
14 ...M..M....................WW.

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	6	n	60
b	2	0	10	17	n	60
b	3	0	4	15	n	40
w	4	0	14	28	g	80
w	5	0	13	26	h	80
b	7	1	0	0	n	60
b	8	1	9	12	n	60
b	9	1	6	12	n	60
w	10	1	4	17	b	80
b	15	2	13	16	n	60
w	17	2	14	27	h	100
b	18	3	4	7	n	60
b	20	3	5	16	n	40
w	22	3	13	29	b	80
w	23	3	13	28	g	100
b	25	2	2	11	n	40
b	27	3	11	27	n	60
w	28	2	8	23	b	80
b	29	3	8	20	n	60
b	30	2	9	2	n	60

barricades
2
player	row	column	resistance
3	4	8	40
3	8	19	40

round 151
day 1

score	440	455	120	920

status	0	0	0	0

commands
19
15	m	r	
4	m	r	
3	m	d	
18	m	d	
2	m	l	
1	m	l	
7	m	r	
20	m	u	
8	m	l	
25	m	u	
9	m	l	
30	m	r	
17	m	l	
10	m	u	
5	m	l	
27	m	d	
28	m	r	
29	m	r	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C...M........................
01 .B...MBBBBBC..BBBBBBBBBBB.....
02 .B........B...B...M......F....
03 .BBBBBBBBBB...B..W............
04 ..F.....b.....B.C.............
05 .....G.CBBB...BC..............
06 ........B.BC..B...............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.C..W.....
09 ...C.B...MBC....BBBBBBBB......
10 .BBB.BBB..B.....C.....MB......
11 .BFB.C.B...............B......
12 .BM....B....G..........B...C..
13 .BBBBBBB.........C.......W.W.W
14 ...M..M...................W..W

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	5	n	60
b	2	0	10	16	n	60
b	3	0	5	15	n	40
w	4	0	14	29	g	80
w	5	0	13	25	h	80
b	7	1	0	1	n	60
b	8	1	9	11	n	60
b	9	1	6	11	n	60
w	10	1	3	17	b	80
b	15	2	13	17	n	60
w	17	2	14	26	h	100
b	18	3	5	7	n	60
b	20	3	4	16	n	40
w	22	3	13	29	b	80
w	23	3	13	27	g	100
b	25	2	1	11	n	40
b	27	3	12	27	n	60
w	28	2	8	24	b	80
b	29	3	8	21	n	60
b	30	2	9	3	n	60

barricades
2
player	row	column	resistance
3	4	8	40
3	8	19	40

round 152
day 1

score	440	455	120	920

status	0	0	0	0

commands
20
15	m	r	
3	b	d	
7	m	r	
18	m	l	
1	b	l	
8	m	d	
25	m	u	
20	m	u	
30	m	r	
17	m	l	
28	m	r	
9	m	d	
10	m	u	
27	m	l	
29	m	r	
2	m	l	
5	m	l	
4	m	l	
22	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C..M.....C..................
01 .B...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B..WM......F....
03 .BBBBBBBBBB...B.C.............
04 ..F.....b.....B...............
05 .....GC.BBB...BC..............
06 ........B.B...Bb..............
07 ........B.BC..B.BBBBBB........
08 .......BB.B...B.B..b..C..W....
09 ....CB...MB.....BBBBBBBB......
10 .BBB.BBB..BC...C......MB......
11 .BFBbC.B...............B......
12 .BM....B....G..........B..C...
13 .BBBBBBB..........C.....W.....
14 ...M..M..................W.WWW

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	5	n	60
b	2	0	10	15	n	60
b	3	0	5	15	n	40
w	4	0	14	28	g	80
w	5	0	13	24	h	80
b	7	1	0	2	n	60
b	8	1	10	11	n	60
b	9	1	7	11	n	60
w	10	1	2	17	b	80
b	15	2	13	18	n	60
w	17	2	14	25	h	100
b	18	3	5	6	n	60
b	20	3	3	16	n	40
w	22	3	14	29	b	80
w	23	3	14	27	g	100
b	25	2	0	11	n	40
b	27	3	12	26	n	60
w	28	2	8	25	b	80
b	29	3	8	22	n	60
b	30	2	9	4	n	60

barricades
4
player	row	column	resistance
3	4	8	40
0	6	15	40
3	8	19	40
0	11	4	40

round 153
day 1

score	440	455	120	920

status	0	0	0	0

commands
19
18	m	l	
15	m	r	
20	m	r	
27	m	u	
2	b	u	
3	m	d	
25	m	l	
29	m	u	
1	m	d	
30	m	u	
5	m	l	
23	m	l	
7	m	r	
4	m	u	
17	m	l	
28	m	u	
8	m	d	
9	m	d	
10	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C.M....C...................
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B...W......F....
03 .BBBBBBBBBB...B..C............
04 ..F.....b.....B......F........
05 .....C..BBB...B...............
06 ........B.B...Bc..............
07 ........B.B...B.BBBBBBC..W....
08 ....C..BB.BC..B.B..b..........
09 .....B...MB....bBBBBBBBB......
10 .BBB.BBB..B....C......MB......
11 .BFBb..B...C...........B..C...
12 .BM..C.B....G..........B......
13 .BBBBBBB...........C...W....W.
14 ...M..M.................W.W..W

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	5	n	60
b	2	0	10	15	n	60
b	3	0	6	15	n	40
w	4	0	13	28	g	80
w	5	0	13	23	h	80
b	7	1	0	3	n	60
b	8	1	11	11	n	60
b	9	1	8	11	n	60
w	10	1	2	18	b	80
b	15	2	13	19	n	60
w	17	2	14	24	h	100
b	18	3	5	5	n	60
b	20	3	3	17	n	40
w	22	3	14	29	b	80
w	23	3	14	26	g	100
b	25	2	0	10	n	40
b	27	3	11	26	n	60
w	28	2	7	25	b	80
b	29	3	7	22	n	60
b	30	2	8	4	n	60

barricades
5
player	row	column	resistance
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 154
day 1

score	440	460	120	920

status	0	0	0	0

commands
20
5	m	l	
15	m	r	
4	m	u	
25	m	l	
7	m	r	
18	m	d	
3	m	d	
2	m	d	
1	m	l	
8	m	l	
30	m	r	
17	m	l	
9	m	d	
10	m	d	
28	m	u	
20	m	d	
27	m	u	
29	m	u	
22	m	u	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....CM...C....................
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B..........F....
03 .BBBBBBBBBB...B...W...........
04 ..F.....b.....B..C...F........
05 ........BBB...B...............
06 .....C..B.B...Bb......C..W....
07 ........B.B...BCBBBBBB........
08 .....C.BB.B...B.B..b.........F
09 .....B...MBC...bBBBBBBBB......
10 .BBB.BBB..B...........MB..C...
11 .BFBb..B..C....C.......B......
12 MBM.C..B....G..........B....W.
13 .BBBBBBB............C.W......W
14 ...M..M................W.W....

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	4	n	60
b	2	0	11	15	n	60
b	3	0	7	15	n	40
w	4	0	12	28	g	80
w	5	0	13	22	h	80
b	7	1	0	4	n	60
b	8	1	11	10	n	60
b	9	1	9	11	n	60
w	10	1	3	18	b	80
b	15	2	13	20	n	60
w	17	2	14	23	h	100
b	18	3	6	5	n	60
b	20	3	4	17	n	40
w	22	3	13	29	b	80
w	23	3	14	25	g	100
b	25	2	0	9	n	40
b	27	3	10	26	n	60
w	28	2	6	25	b	80
b	29	3	6	22	n	60
b	30	2	8	5	n	60

barricades
5
player	row	column	resistance
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 155
day 1

score	440	460	120	920

status	0	0	0	0

commands
19
18	m	d	
1	m	l	
20	m	r	
15	m	r	
7	m	r	
25	b	l	
8	m	l	
9	m	d	
27	m	u	
29	m	u	
30	m	r	
17	m	l	
5	m	u	
22	m	u	
4	m	u	
23	m	l	
28	m	u	
3	m	d	
2	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....C..bC....................
01 MB...MBBBBB...BBBBBBBBBBB.....
02 .B........B...B..........F....
03 .BBBBBBBBBB...B...W...........
04 ..F.....b.....B...C..F........
05 ........BBB...B.......C..W....
06 ........B.B...Bb..............
07 .....C..B.B...B.BBBBBB........
08 ......CBB.B...BCB..b.........F
09 .....B...MB....bBBBBBBBB..C...
10 .BBB.BBB..BC..........MB......
11 .BFBb..B.C.............B....W.
12 MBMC...B....G..C......WB.....W
13 .BBBBBBB.............C........
14 ...M..M...............W.W.....

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	3	n	60
b	2	0	12	15	n	60
b	3	0	8	15	n	40
w	4	0	11	28	g	80
w	5	0	12	22	h	80
b	7	1	0	5	n	60
b	8	1	11	9	n	60
b	9	1	10	11	n	60
w	10	1	3	18	b	80
b	15	2	13	21	n	60
w	17	2	14	22	h	100
b	18	3	7	5	n	60
b	20	3	4	18	n	40
w	22	3	12	29	b	80
w	23	3	14	24	g	100
b	25	2	0	9	n	40
b	27	3	9	26	n	60
w	28	2	5	25	b	80
b	29	3	5	22	n	60
b	30	2	8	6	n	60

barricades
6
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 156
day 1

score	440	465	120	920

status	0	0	0	0

commands
20
5	m	u	
18	m	d	
7	m	d	
1	m	l	
20	m	r	
27	m	u	
4	m	u	
3	m	d	
8	m	u	
29	m	u	
15	m	u	
2	m	d	
25	m	r	
30	m	d	
9	m	d	
22	m	u	
23	m	l	
17	m	l	
10	m	r	
28	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.C...................
01 MB...CBBBBB...BBBBBBBBBBB.....
02 .B........B...B..........F....
03 .BBBBBBBBBB...B....W..........
04 ..F.....b.....B....C.FC..W....
05 ........BBB...B...............
06 ........B.B...Bb..............
07 ........B.B...B.BBBBBB........
08 .....C.BB.B...B.B..b......C..F
09 .....BC..MB....cBBBBBBBB......
10 .BBB.BBB.CB...........MB....W.
11 .BFBb..B...C..........WB.....W
12 MBC....B....G........C.B......
13 .BBBBBBB.......C..............
14 ...M..M..............W.W......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	2	n	60
b	2	0	13	15	n	60
b	3	0	9	15	n	40
w	4	0	10	28	g	80
w	5	0	11	22	h	80
b	7	1	1	5	n	60
b	8	1	10	9	n	60
b	9	1	11	11	n	60
w	10	1	3	19	b	80
b	15	2	12	21	n	60
w	17	2	14	21	h	100
b	18	3	8	5	n	60
b	20	3	4	19	n	40
w	22	3	11	29	b	80
w	23	3	14	23	g	100
b	25	2	0	10	n	40
b	27	3	8	26	n	60
w	28	2	4	25	b	80
b	29	3	4	22	n	60
b	30	2	9	6	n	60

barricades
6
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 157
day 1

score	445	470	120	920

status	0	0	0	0

commands
20
18	m	l	
20	m	r	
5	m	u	
1	m	u	
27	m	d	
7	m	u	
4	m	d	
3	m	d	
8	m	u	
15	m	u	
9	m	l	
29	m	d	
2	m	d	
10	m	r	
22	m	u	
25	m	l	
30	m	r	
23	m	l	
17	m	u	
28	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....C..bC..........G........M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B..........F....
03 .BBBBBBBBBB...B.....W....W....
04 ..F.....b.....B.....CF........
05 ........BBB...B.......C.......
06 ........B.B...Bb..............
07 ........B.B...B.BBBBBB........
08 .G..C..BB.B...B.B..b.........F
09 .....B.C.CB....bBBBBBBBB..C...
10 .BBB.BBB..B....C......WB.....W
11 .BCBb..B..C..........C.B....W.
12 MB.....B....G..........B......
13 .BBBBBBB.............W........
14 ...M..M........C......W.......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	2	n	60
b	2	0	14	15	n	60
b	3	0	10	15	n	40
w	4	0	11	28	g	80
w	5	0	10	22	h	80
b	7	1	0	5	n	60
b	8	1	9	9	n	60
b	9	1	11	10	n	60
w	10	1	3	20	b	80
b	15	2	11	21	n	60
w	17	2	13	21	h	100
b	18	3	8	4	n	60
b	20	3	4	20	n	40
w	22	3	10	29	b	80
w	23	3	14	22	g	100
b	25	2	0	9	n	40
b	27	3	9	26	n	60
w	28	2	3	25	b	80
b	29	3	5	22	n	60
b	30	2	9	7	n	60

barricades
6
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 158
day 1

score	450	475	120	920

status	0	0	0	0

commands
19
15	m	d	
25	m	r	
7	m	l	
8	m	d	
30	m	r	
5	m	d	
4	m	u	
28	m	u	
9	m	l	
18	m	l	
3	m	d	
10	m	r	
20	m	r	
27	m	u	
29	m	u	
22	m	d	
2	m	l	
23	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C...b.C.........G........M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B..........W....
03 .BBBBBBBBBB...B......W........
04 ..F.....b.....B......CC.......
05 ........BBB...B...............
06 ........B.B...Bb..............
07 ........B.B...B.BBBBBB........
08 .G.C...BB.B...B.B..b......C..F
09 .....B..C.B....bBBBBBBBB......
10 .BBB.BBB.CB............B....W.
11 .B.Bb..B.C.....C......WB.....W
12 MBC....B....G........C.B......
13 .BBBBBBB.............WW.......
14 ...M..M.......C...............

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	2	n	60
b	2	0	14	14	n	60
b	3	0	11	15	n	40
w	4	0	10	28	g	80
w	5	0	11	22	h	80
b	7	1	0	4	n	60
b	8	1	10	9	n	60
b	9	1	11	9	n	60
w	10	1	3	21	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	8	3	n	60
b	20	3	4	21	n	60
w	22	3	11	29	b	80
w	23	3	13	22	g	100
b	25	2	0	10	n	40
b	27	3	8	26	n	60
w	28	2	2	25	b	100
b	29	3	4	22	n	60
b	30	2	9	8	n	60

barricades
6
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 159
day 1

score	450	475	120	920

status	0	0	0	0

commands
15
7	m	l	
8	m	l	
5	m	l	
18	m	l	
9	m	l	
20	m	d	
4	m	u	
10	m	r	
27	m	u	
3	m	d	
2	m	l	
25	m	l	
1	m	r	
28	m	r	
22	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C....bC..........G........M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B...........W...
03 .BBBBBBBBBB...B.......W.......
04 ..F.....b.....B.......C.......
05 ........BBB...B......C........
06 ........B.B...Bb..............
07 ........B.B...B.BBBBBB....C...
08 .GC....BB.B...B.B..b...M.....F
09 .....B..C.B....bBBBBBBBB....W.
10 .BBB.BBBC.B............B.....W
11 .B.Bb..BC............W.B......
12 MB.C...B....G..C.....C.B......
13 .BBBBBBB.............WW.......
14 ...M..M......C................

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	3	n	60
b	2	0	14	13	n	60
b	3	0	12	15	n	40
w	4	0	9	28	g	80
w	5	0	11	21	h	80
b	7	1	0	3	n	60
b	8	1	10	8	n	60
b	9	1	11	8	n	60
w	10	1	3	22	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	8	2	n	60
b	20	3	5	21	n	60
w	22	3	10	29	b	80
w	23	3	13	22	g	100
b	25	2	0	9	n	40
b	27	3	7	26	n	60
w	28	2	2	26	b	100
b	29	3	4	22	n	60
b	30	2	9	8	n	60

barricades
6
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
0	9	15	40
0	11	4	40

round 160
day 1

score	450	475	120	920

status	0	0	0	0

commands
17
18	m	l	
20	m	d	
7	m	l	
27	m	d	
25	m	l	
5	m	l	
29	m	d	
8	m	r	
4	m	u	
9	m	d	
22	m	u	
30	b	l	
10	m	r	
28	m	r	
3	m	d	
2	m	l	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C.....c...........G.M......M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B............W..
03 .BBBBBBBBBB...B........W......
04 ..F.....b.....B...............
05 ........BBB...B.......C.....M.
06 ........B.B...Bb.....C........
07 ........B.B...B.BBBBBB........
08 .C.....BB.B...B.B..b...M..C.WF
09 .....B.bC.B....bBBBBBBBB.....W
10 .BBB.BBB.CB............B......
11 .B.Bb..B............W..B......
12 MB..C..BC...G........C.B......
13 .BBBBBBB.......C.....WW.......
14 ...M..M.....C.................

citizens
20
type	id	player	row	column	weapon	life
b	1	0	12	4	n	60
b	2	0	14	12	n	60
b	3	0	13	15	n	40
w	4	0	8	28	g	80
w	5	0	11	20	h	80
b	7	1	0	2	n	60
b	8	1	10	9	n	60
b	9	1	12	8	n	60
w	10	1	3	23	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	8	1	n	60
b	20	3	6	21	n	60
w	22	3	9	29	b	80
w	23	3	13	22	g	100
b	25	2	0	8	n	40
b	27	3	8	26	n	60
w	28	2	2	27	b	100
b	29	3	5	22	n	60
b	30	2	9	8	n	60

barricades
7
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
2	9	7	40
0	9	15	40
0	11	4	40

round 161
day 1

score	450	475	120	920

status	0	0	0	0

commands
15
18	m	d	
20	m	r	
4	m	r	
2	m	u	
27	m	l	
7	m	l	
5	m	d	
3	m	l	
25	m	r	
30	m	d	
8	m	d	
1	m	u	
28	m	d	
9	m	d	
10	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C......bC..........GMM......M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B...............
03 .BBBBBBBBBB...B............W..
04 ..F.....b.....B........W......
05 ........BBB...B.......C.....M.
06 ........B.B...Bb...M..C.......
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b...M.C...W
09 .C...B.b..B....bBBBBBBBB.....W
10 .BBB.BBBC.B............B......
11 .B.Bc..B.C.............B......
12 MB.....B....G.......WC.B......
13 .BBBBBBBC...C.C......WW.......
14 ...M..M.......................

citizens
20
type	id	player	row	column	weapon	life
b	1	0	11	4	n	60
b	2	0	13	12	n	60
b	3	0	13	14	n	40
w	4	0	8	29	g	100
w	5	0	12	20	h	80
b	7	1	0	1	n	60
b	8	1	11	9	n	60
b	9	1	13	8	n	60
w	10	1	4	23	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	9	1	n	60
b	20	3	6	22	n	60
w	22	3	9	29	b	80
w	23	3	13	22	g	100
b	25	2	0	9	n	40
b	27	3	8	25	n	60
w	28	2	3	27	b	100
b	29	3	5	22	n	60
b	30	2	10	8	n	60

barricades
7
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
2	9	7	40
0	9	15	40
0	11	4	40

round 162
day 1

score	450	475	120	920

status	0	0	0	0

commands
16
18	m	l	
7	m	l	
2	m	u	
20	m	d	
25	m	r	
27	m	l	
30	m	d	
28	m	d	
8	m	r	
29	m	l	
5	m	d	
4	m	u	
3	m	d	
9	m	d	
10	m	u	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......b.C.........GMM......M
01 MB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B...............
03 .BBBBBBBBBB...B........W......
04 ..F.....b.....B............W..
05 ........BBB...B......C......M.
06 ........B.B...Bb...M..........
07 ........B.B...B.BBBBBBC......W
08 .......BB.B...B.B..b...MC.....
09 C....B.b..B....bBBBBBBBB.....W
10 .BBBCBBB..B............B......
11 .B.Bb..BC.C............B......
12 MB.....B....C........C.B......
13 .BBBBBBB............WWW.......
14 ...M..M.C.....C...............

citizens
20
type	id	player	row	column	weapon	life
b	1	0	10	4	n	60
b	2	0	12	12	n	60
b	3	0	14	14	n	40
w	4	0	7	29	g	100
w	5	0	13	20	h	80
b	7	1	0	0	n	60
b	8	1	11	10	n	60
b	9	1	14	8	n	60
w	10	1	3	23	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	9	0	n	60
b	20	3	7	22	n	60
w	22	3	9	29	b	80
w	23	3	13	22	g	100
b	25	2	0	10	n	40
b	27	3	8	24	n	60
w	28	2	4	27	b	100
b	29	3	5	21	n	60
b	30	2	11	8	n	60

barricades
7
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
2	9	7	40
0	9	15	40
0	11	4	40

round 163
day 1

score	450	475	120	920

status	0	0	0	0

commands
17
5	m	d	
4	m	u	
18	m	d	
7	m	d	
25	m	r	
8	m	r	
9	m	l	
3	m	u	
2	m	u	
10	m	u	
20	m	d	
30	m	u	
27	m	l	
29	m	d	
1	m	u	
22	m	u	
28	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b..C........GMM......M
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B........W......
03 .BBBBBBBBBB...B...............
04 ..F.....b.....B...............
05 ........BBB...B............WM.
06 ........B.B...Bb...M.C.......W
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b..CC.....W
09 ....CB.b..B....bBBBBBBBB......
10 CBBB.BBBC.B............B......
11 .B.Bb..B...CC..........B......
12 MB.....B.............C.B......
13 .BBBBBBB......C......WW.......
14 ...M..MC............W.........

citizens
20
type	id	player	row	column	weapon	life
b	1	0	9	4	n	60
b	2	0	11	12	n	60
b	3	0	13	14	n	40
w	4	0	6	29	g	100
w	5	0	14	20	h	80
b	7	1	1	0	n	60
b	8	1	11	11	n	60
b	9	1	14	7	n	60
w	10	1	2	23	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	10	0	n	60
b	20	3	8	22	n	60
w	22	3	8	29	b	80
w	23	3	13	22	g	100
b	25	2	0	11	n	40
b	27	3	8	23	n	60
w	28	2	5	27	b	100
b	29	3	6	21	n	60
b	30	2	10	8	n	60

barricades
7
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
2	9	7	40
0	9	15	40
0	11	4	40

round 164
day 1

score	450	480	120	925

status	0	0	0	0

commands
16
7	m	u	
25	m	r	
8	m	u	
4	m	u	
9	m	l	
18	m	d	
10	m	l	
20	b	l	
5	m	r	
3	m	u	
27	m	u	
29	m	l	
30	m	d	
2	m	u	
22	m	u	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......b...C.......GMM......M
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B.......W.......
03 .BBBBBBBBBB...B...............
04 ..F.....b.....B...............
05 ........BBB...B.............WW
06 ........B.B...Bb...MC.........
07 ........B.B...B.BBBBBB.C.....W
08 .......BB.B...B.B..b.bC.......
09 ....CB.b..B....bBBBBBBBB......
10 .BBB.BBB..BCC..........B......
11 CB.Bb..BC..............B......
12 MB.....B......C......C.B......
13 .BBBBBBB.............WW.......
14 ...M..C..............W........

citizens
20
type	id	player	row	column	weapon	life
b	1	0	9	4	n	60
b	2	0	10	12	n	60
b	3	0	12	14	n	40
w	4	0	5	29	g	100
w	5	0	14	21	h	80
b	7	1	0	0	n	60
b	8	1	10	11	n	60
b	9	1	14	6	n	60
w	10	1	2	22	b	80
b	15	2	12	21	n	60
w	17	2	13	21	h	100
b	18	3	11	0	n	60
b	20	3	8	22	n	60
w	22	3	7	29	b	80
w	23	3	13	22	g	100
b	25	2	0	12	n	40
b	27	3	7	23	n	60
w	28	2	5	28	b	100
b	29	3	6	20	n	60
b	30	2	11	8	n	60

barricades
8
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 165
day 1

score	450	485	125	925

status	0	0	0	0

commands
18
1	m	l	
15	m	u	
5	m	r	
18	m	d	
25	m	r	
4	m	u	
7	m	d	
30	m	r	
3	m	u	
2	m	u	
20	m	u	
27	m	u	
29	m	l	
22	m	u	
28	m	r	
8	m	u	
9	m	l	
10	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b....C......GMM......M
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......W........
03 .BBBBBBBBBB...B...............
04 ..F.....b.....B..............W
05 ........BBB...B..............W
06 ........B.B...Bb...C...C.....W
07 ........B.B...B.BBBBBBC.......
08 .......BB.B...B.B..b.b........
09 ...C.B.b..BCC..bBBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.Bb..B.C....C......C.B......
12 CB.....B...............B......
13 .BBBBBBB.............WW.......
14 ...M.C................W.......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	9	3	n	60
b	2	0	9	12	n	60
b	3	0	11	14	n	40
w	4	0	4	29	g	100
w	5	0	14	22	h	80
b	7	1	1	0	n	60
b	8	1	9	11	n	60
b	9	1	14	5	n	60
w	10	1	2	21	b	80
b	15	2	11	21	n	60
w	17	2	13	21	h	100
b	18	3	12	0	n	60
b	20	3	7	22	n	60
w	22	3	6	29	b	80
w	23	3	13	22	g	100
b	25	2	0	13	n	40
b	27	3	6	23	n	60
w	28	2	5	29	b	100
b	29	3	6	19	n	60
b	30	2	11	9	n	60

barricades
8
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 166
day 1

score	450	485	125	935

status	0	0	0	0

commands
19
15	m	r	
1	m	u	
18	m	d	
3	m	l	
7	m	u	
25	m	r	
8	m	u	
20	m	u	
30	m	l	
5	m	r	
4	m	u	
27	m	u	
28	m	u	
9	m	l	
2	m	r	
29	m	u	
10	m	l	
22	m	l	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......b.....C.....GMM......M
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B.....W.........
03 .BBBBBBBBBB...B..............W
04 ..F.....b.....B..............W
05 ........BBB...B....C...C......
06 ........B.B...Bb......C.....W.
07 ........B.B...B.BBBBBB........
08 ...C...BB.BC..B.B..b.b........
09 .....B.b..B..C.bBBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.Bb..BC....C........CB..F...
12 .B.....B...............B......
13 CBBBBBBB.............W........
14 ...MC.................WW......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	8	3	n	60
b	2	0	9	13	n	60
b	3	0	11	13	n	40
w	4	0	3	29	g	100
w	5	0	14	23	h	80
b	7	1	0	0	n	60
b	8	1	8	11	n	60
b	9	1	14	4	n	60
w	10	1	2	20	b	80
b	15	2	11	22	n	60
w	17	2	13	21	h	100
b	18	3	13	0	n	60
b	20	3	6	22	n	60
w	22	3	6	28	b	80
w	23	3	14	22	g	100
b	25	2	0	14	n	40
b	27	3	5	23	n	60
w	28	2	4	29	b	100
b	29	3	5	19	n	60
b	30	2	11	8	n	60

barricades
8
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 167
day 1

score	450	485	125	935

status	0	0	0	0

commands
20
7	m	d	
8	m	d	
15	m	d	
1	m	d	
25	m	r	
9	m	l	
3	m	l	
10	m	d	
18	m	d	
30	m	d	
17	m	r	
5	m	u	
4	m	u	
20	m	u	
28	m	u	
27	m	u	
2	m	r	
29	m	u	
22	m	u	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b......C....GMM......M
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B........BF..B..............W
03 .BBBBBBBBBB...B.....W........W
04 ..F.....b.....B....C...C......
05 ........BBB...B.......C.....W.
06 ......F.B.B...Bb..............
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.b........
09 ...C.B.b..BC..CbBBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.Bb..B....C..........B..F...
12 .B.....BC.............CB.....M
13 .BBBBBBB..............WW......
14 C..C...................W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	9	3	n	60
b	2	0	9	14	n	60
b	3	0	11	12	n	40
w	4	0	2	29	g	100
w	5	0	13	23	h	80
b	7	1	1	0	n	60
b	8	1	9	11	n	60
b	9	1	14	3	n	60
w	10	1	3	20	b	80
b	15	2	12	22	n	60
w	17	2	13	22	h	100
b	18	3	14	0	n	60
b	20	3	5	22	n	60
w	22	3	5	28	b	80
w	23	3	14	23	g	100
b	25	2	0	15	n	40
b	27	3	4	23	n	60
w	28	2	3	29	b	100
b	29	3	4	19	n	60
b	30	2	12	8	n	60

barricades
8
player	row	column	resistance
2	0	8	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 168
day 1

score	450	490	125	935

status	0	0	0	0

commands
18
7	m	u	
15	m	u	
8	m	u	
9	m	l	
1	m	u	
25	b	l	
20	m	u	
10	m	d	
4	m	u	
27	m	u	
30	m	r	
29	m	u	
5	m	r	
22	m	u	
17	m	r	
3	m	u	
2	m	r	
28	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......b.....bC....GMM......M
01 .B....BBBBB...BBBBBBBBBBB....W
02 .B........BF..B..............W
03 .BBBBBBBBBB...B....C...C......
04 ..F.....b.....B.....W.C.....W.
05 ....M...BBB...B...............
06 ......F.B.B...Bb..............
07 ........B.B...B.BBBBBB........
08 ...C...BB.BC..B.B..b.b........
09 .....B.b..B....cBBBBBBBB......
10 .BBB.BBB..B.C..........B......
11 .B.Bb..B..............CB..F...
12 .B.....B.C.............B.....M
13 .BBBBBBB...............WW.....
14 C.C.........M..........W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	8	3	n	60
b	2	0	9	15	n	60
b	3	0	10	12	n	40
w	4	0	1	29	g	100
w	5	0	13	24	h	80
b	7	1	0	0	n	60
b	8	1	8	11	n	60
b	9	1	14	2	n	60
w	10	1	4	20	b	80
b	15	2	11	22	n	60
w	17	2	13	23	h	100
b	18	3	14	0	n	60
b	20	3	4	22	n	60
w	22	3	4	28	b	80
w	23	3	14	23	g	100
b	25	2	0	15	n	40
b	27	3	3	23	n	60
w	28	2	2	29	b	100
b	29	3	3	19	n	60
b	30	2	12	9	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 169
day 1

score	450	490	125	935

status	0	0	0	0

commands
18
18	m	u	
4	m	u	
7	m	d	
5	m	r	
8	m	d	
3	m	d	
2	m	u	
15	m	d	
20	m	u	
9	m	l	
10	m	d	
25	m	r	
27	m	u	
30	m	d	
1	m	u	
28	m	u	
29	m	u	
22	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b.C...GMM......W
01 CB....BBBBB...BBBBBBBBBBB....W
02 .B........BF..B....C...C......
03 .BBBBBBBBBB...B.......C.....W.
04 ..F.....b.....B...............
05 ....M...BBB...B.....W.........
06 ......F.B.B...Bb.........M....
07 ...C....B.B...B.BBBBBB........
08 .......BB.B...BCB..b.b........
09 .....B.b..BC...bBBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.Bb..B....C..........B..F...
12 .B.....B..............CB.....M
13 CBBBBBBB.C.............W.W....
14 .C..........M..M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	7	3	n	60
b	2	0	8	15	n	60
b	3	0	11	12	n	40
w	4	0	0	29	g	100
w	5	0	13	25	h	80
b	7	1	1	0	n	60
b	8	1	9	11	n	60
b	9	1	14	1	n	60
w	10	1	5	20	b	80
b	15	2	12	22	n	60
w	17	2	13	23	h	100
b	18	3	13	0	n	60
b	20	3	3	22	n	60
w	22	3	3	28	b	80
w	23	3	14	23	g	100
b	25	2	0	16	n	40
b	27	3	2	23	n	60
w	28	2	1	29	b	100
b	29	3	2	19	n	60
b	30	2	13	9	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 170
day 1

score	455	490	125	935

status	0	0	0	0

commands
18
7	m	d	
5	m	r	
15	m	d	
8	m	d	
25	m	r	
4	m	l	
30	m	d	
9	m	r	
3	m	d	
10	m	d	
18	m	u	
28	m	u	
2	m	d	
1	m	u	
20	m	d	
27	m	d	
29	m	d	
22	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b..C..GMM.....WW
01 .B....BBBBB...BBBBBBBBBBB.....
02 CB........BF..B.............W.
03 .BBBBBBBBBB...B....C...C......
04 ..F.....b.....B.......C.......
05 ....M...BBB...B...............
06 ...C..F.B.B...Bb....W....M....
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.b........
09 .....B.b..B....cBBBBBBBB.F....
10 .BBB.BBB..BC...........B......
11 .B.Bb..B...............B..F...
12 CB.....B....C..........B.....M
13 .BBBBBBB..............CW..W...
14 ..C......C..M..M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	6	3	n	60
b	2	0	9	15	n	60
b	3	0	12	12	n	40
w	4	0	0	28	g	100
w	5	0	13	26	h	80
b	7	1	2	0	n	60
b	8	1	10	11	n	60
b	9	1	14	2	n	60
w	10	1	6	20	b	80
b	15	2	13	22	n	60
w	17	2	13	23	h	100
b	18	3	12	0	n	60
b	20	3	4	22	n	60
w	22	3	2	28	b	80
w	23	3	14	23	g	100
b	25	2	0	17	n	40
b	27	3	3	23	n	60
w	28	2	0	29	b	100
b	29	3	3	19	n	60
b	30	2	14	9	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 171
day 1

score	455	490	125	935

status	0	0	0	0

commands
17
5	m	u	
3	m	d	
1	m	u	
25	m	r	
4	m	l	
30	m	r	
18	m	u	
20	m	d	
2	m	d	
27	m	d	
7	m	d	
29	m	d	
22	m	u	
8	m	d	
9	m	r	
10	m	r	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b...C.GMM....WW.
01 .B....BBBBB...BBBBBBBBBBB...W.
02 .B........BF..B...............
03 CBBBBBBBBBB...B...............
04 ..F.....b.M...B....C...C......
05 ...CM...BBB...B.......C.......
06 ......F.B.B...Bb.....W...M....
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B....C.......B......
11 CB.Bb..B...C...........B..F...
12 .B.....B...............B..W..M
13 .BBBBBBB....C.........CW......
14 ...C......C.M..M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	3	n	60
b	2	0	10	15	n	60
b	3	0	13	12	n	40
w	4	0	0	27	g	100
w	5	0	12	26	h	80
b	7	1	3	0	n	60
b	8	1	11	11	n	60
b	9	1	14	3	n	60
w	10	1	6	21	b	80
b	15	2	13	22	n	60
w	17	2	13	23	h	100
b	18	3	11	0	n	60
b	20	3	5	22	n	60
w	22	3	1	28	b	80
w	23	3	14	23	g	100
b	25	2	0	18	n	40
b	27	3	4	23	n	60
w	28	2	0	28	b	100
b	29	3	4	19	n	60
b	30	2	14	10	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 172
day 1

score	455	490	125	935

status	0	0	0	0

commands
15
5	m	u	
7	m	d	
25	m	r	
30	m	r	
8	m	d	
3	m	d	
9	m	r	
18	m	u	
1	m	r	
20	m	d	
27	m	d	
29	m	u	
4	m	l	
2	m	d	
22	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b....CGMM...W.W.
01 .B....BBBBB...BBBBBBBBBBB..W..
02 .B........BF..B...............
03 .BBBBBBBBBB...B....C..........
04 C.F.....b.M...B...............
05 ....C...BBB...B........C......
06 ......F.B.B...Bb.....WC..M....
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.b........
09 .....B.b..B....bBBBBBBBB.F....
10 CBBB.BBB..B............B......
11 .B.Bb..B.......C.......B..W...
12 .B.....B...C...........B.....M
13 .BBBBBBB..............CW......
14 ....C......CC..M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	4	n	60
b	2	0	11	15	n	60
b	3	0	14	12	n	40
w	4	0	0	26	g	100
w	5	0	11	26	h	100
b	7	1	4	0	n	60
b	8	1	12	11	n	60
b	9	1	14	4	n	60
w	10	1	6	21	b	80
b	15	2	13	22	n	60
w	17	2	13	23	h	100
b	18	3	10	0	n	60
b	20	3	6	22	n	60
w	22	3	1	27	b	80
w	23	3	14	23	g	100
b	25	2	0	19	n	40
b	27	3	5	23	n	60
w	28	2	0	28	b	100
b	29	3	3	19	n	60
b	30	2	14	11	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 173
day 1

score	465	490	125	935

status	0	0	0	0

commands
15
1	m	r	
7	m	u	
8	m	u	
9	m	r	
5	m	d	
25	m	r	
4	m	l	
3	m	u	
30	m	r	
2	m	d	
18	m	u	
28	m	d	
20	m	r	
29	m	d	
22	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b.....CMM..W.W..
01 .B....BBBBB...BBBBBBBBBBB...W.
02 .B........BF..B...............
03 CBBBBBBBBBB...B...............
04 ..F.....b.M...B....C..........
05 .....C..BBB...B........C......
06 ......F.B.B...Bb.....W.C.M....
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B..b.b........
09 C....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B............B......
11 .B.Bb..B...C...........B......
12 .B.....B.......C...M...B..W..M
13 .BBBBBBB....C.........CW......
14 .....C......C..M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	5	n	60
b	2	0	12	15	n	60
b	3	0	13	12	n	40
w	4	0	0	25	g	100
w	5	0	12	26	h	100
b	7	1	3	0	n	60
b	8	1	11	11	n	60
b	9	1	14	5	n	60
w	10	1	6	21	b	80
b	15	2	13	22	n	60
w	17	2	13	23	h	100
b	18	3	9	0	n	60
b	20	3	6	23	n	60
w	22	3	0	27	b	80
w	23	3	14	23	g	100
b	25	2	0	20	n	40
b	27	3	5	23	n	60
w	28	2	1	28	b	100
b	29	3	4	19	n	60
b	30	2	14	12	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 174
day 1

score	465	490	125	935

status	0	0	0	0

commands
18
5	m	d	
18	m	u	
15	m	u	
25	m	r	
20	m	r	
30	m	r	
28	m	d	
4	m	l	
27	m	r	
2	m	d	
29	m	r	
1	m	r	
22	m	l	
3	m	r	
7	m	u	
8	m	u	
9	m	r	
10	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b......CM.W.W...
01 .B....BBBBB...BBBBBBBBBBB.....
02 CB........BF..B.............W.
03 .BBBBBBBBBB...B...............
04 ..F.....b.M...B.....C.........
05 ......C.BBB...B.........C.....
06 ......F.B.B...Bb......W.CM....
07 ........B.B...B.BBBBBB........
08 C......BB.B...B.B..b.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..BC...........B......
11 .B.Bb..B...............B......
12 .B.....B...........M..CB.....M
13 .BBBBBBB.....C.C.......W..W...
14 ......C......C.M.......W...M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	6	n	60
b	2	0	13	15	n	60
b	3	0	13	13	n	40
w	4	0	0	24	g	100
w	5	0	13	26	h	100
b	7	1	2	0	n	60
b	8	1	10	11	n	60
b	9	1	14	6	n	60
w	10	1	6	22	b	80
b	15	2	12	22	n	60
w	17	2	13	23	h	100
b	18	3	8	0	n	60
b	20	3	6	24	n	60
w	22	3	0	26	b	80
w	23	3	14	23	g	100
b	25	2	0	21	n	40
b	27	3	5	24	n	60
w	28	2	2	28	b	100
b	29	3	4	20	n	60
b	30	2	14	13	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 175
day 0

score	465	490	130	935

status	0	0	0	0

commands
20
7	m	u	
8	m	u	
4	m	l	
9	m	r	
10	m	r	
18	m	u	
20	m	d	
5	m	r	
27	m	r	
2	m	d	
15	m	l	
1	m	d	
25	m	l	
3	m	u	
30	m	l	
17	m	r	
28	m	u	
29	m	r	
22	m	l	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b.....C.MW.W....
01 CB....BBBBB...BBBBBBBBBBB...W.
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 ..F.....b.M...B......C........
05 ........BBB...B.Z........C....
06 ......C.B.B...Bb.......W.M....
07 C.......B.B...B.BBBBBB..C.....
08 .......BB.B...B.B..b.b........
09 .....B.b..BC...bBBBBBBBB.F....
10 .BBB.BBB..B............B......
11 .B.Bb..B...............B......
12 .B.....B.....C.....M.C.B.....M
13 .BBBBBBB...............WW..W..
14 .......C....C..C...........M..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	6	6	n	60
b	2	0	14	15	n	60
b	3	0	12	13	n	40
w	4	0	0	23	g	100
w	5	0	13	27	h	100
b	7	1	1	0	n	60
b	8	1	9	11	n	60
b	9	1	14	7	n	60
w	10	1	6	23	b	80
b	15	2	12	21	n	60
w	17	2	13	24	h	100
b	18	3	7	0	n	60
b	20	3	7	24	n	60
w	22	3	0	25	b	80
w	23	3	13	23	g	100
b	25	2	0	20	n	40
b	27	3	5	25	n	60
w	28	2	1	28	b	100
b	29	3	4	21	n	60
b	30	2	14	12	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 176
day 0

score	470	490	130	935

status	0	0	0	0

commands
20
15	m	l	
7	m	u	
18	m	u	
4	m	l	
5	m	d	
25	m	l	
30	m	l	
17	m	d	
3	m	u	
8	m	u	
2	m	u	
9	m	r	
20	m	d	
10	m	d	
1	m	u	
27	m	d	
29	m	d	
28	m	u	
22	m	l	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......b.....b....C..W.W...W.
01 .B....BBBBB.M.BBBBBBBBBBB.....
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 ..F.....b.M...B...............
05 ......C.BBB...B.Z....C........
06 C.......B.B...Bb.........C....
07 ........B.B...B.BBBBBB.W......
08 ...M...BB.BC..B.B..b.b..C.....
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B............B......
11 .B.Bb..B.....C.........B......
12 .B.....B...........MC..B.....M
13 .BBBBBBB.......C........W.....
14 ........C..C............W..W..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	6	n	60
b	2	0	13	15	n	60
b	3	0	11	13	n	40
w	4	0	0	22	g	100
w	5	0	14	27	h	100
b	7	1	0	0	n	60
b	8	1	8	11	n	60
b	9	1	14	8	n	60
w	10	1	7	23	b	80
b	15	2	12	20	n	60
w	17	2	14	24	h	100
b	18	3	6	0	n	60
b	20	3	8	24	n	60
w	22	3	0	24	b	80
w	23	3	13	24	g	100
b	25	2	0	19	n	40
b	27	3	6	25	n	60
w	28	2	0	28	b	100
b	29	3	5	21	n	60
b	30	2	14	11	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 177
day 0

score	480	490	130	940

status	0	0	0	0

commands
20
7	m	d	
8	m	u	
18	m	d	
20	m	d	
15	m	l	
9	m	u	
4	m	l	
27	m	d	
10	m	d	
29	m	l	
5	m	u	
22	m	l	
23	m	d	
25	m	l	
3	m	u	
30	m	u	
2	m	u	
17	m	r	
1	m	u	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b...C..W.W...W..
01 CB....BBBBB.M.BBBBBBBBBBB.....
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 ..F...C.b.M...B...............
05 ........BBB...B.Z...C.........
06 ........B.B...Bb..............
07 C.......B.BC..B.BBBBBB...C....
08 ...M...BB.B...B.B..b.b.W......
09 .....B.b..B....bBBBBBBBBCF....
10 .BBB.BBB..B..C.........B......
11 .B.Bb..B...............B......
12 .B.....B.......C...C...B.....M
13 .BBBBBBBC..C............W..W..
14 .........................W....

citizens
20
type	id	player	row	column	weapon	life
b	1	0	4	6	n	60
b	2	0	12	15	n	60
b	3	0	10	13	n	40
w	4	0	0	21	g	100
w	5	0	13	27	h	100
b	7	1	1	0	n	60
b	8	1	7	11	n	60
b	9	1	13	8	n	60
w	10	1	8	23	b	80
b	15	2	12	19	n	60
w	17	2	14	25	h	80
b	18	3	7	0	n	60
b	20	3	9	24	n	60
w	22	3	0	23	b	80
w	23	3	13	24	g	100
b	25	2	0	18	n	40
b	27	3	7	25	n	60
w	28	2	0	27	b	100
b	29	3	5	20	n	60
b	30	2	13	11	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 178
day 0

score	480	490	135	940

status	0	0	0	0

commands
20
4	m	l	
7	m	d	
15	m	u	
8	m	u	
5	m	u	
3	m	u	
25	m	l	
9	m	u	
30	m	u	
17	m	r	
28	m	l	
10	m	r	
2	m	u	
18	m	d	
1	m	r	
20	m	d	
27	m	d	
29	m	l	
22	m	l	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b..C..W.W...W...
01 .B....BBBBB.M.BBBBBBBBBBB.....
02 CB........BF..B...............
03 .BBBBBBBBBB...B...............
04 ..F....Cb.M...B...............
05 ........BBB...B.Z..C..........
06 ........B.BC..Bb.............M
07 ........B.B...B.BBBBBB........
08 C..M...BB.B...B.B..b.b..WC....
09 .....B.b..B..C.bBBBBBBBB.F....
10 .BBB.BBB..B............BC.....
11 .B.Bb..B.......C...C...B......
12 .B.....BC..C...........B...W.M
13 .BBBBBBB......................
14 ........................W.W...

citizens
20
type	id	player	row	column	weapon	life
b	1	0	4	7	n	60
b	2	0	11	15	n	60
b	3	0	9	13	n	40
w	4	0	0	20	g	100
w	5	0	12	27	h	100
b	7	1	2	0	n	60
b	8	1	6	11	n	60
b	9	1	12	8	n	60
w	10	1	8	24	b	80
b	15	2	11	19	n	60
w	17	2	14	26	h	80
b	18	3	8	0	n	60
b	20	3	10	24	n	60
w	22	3	0	22	b	80
w	23	3	14	24	g	100
b	25	2	0	17	n	40
b	27	3	8	25	n	60
w	28	2	0	26	b	100
b	29	3	5	19	n	60
b	30	2	12	11	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 179
day 0

score	480	490	135	940

status	0	0	0	0

commands
20
18	m	r	
4	m	l	
15	m	d	
7	m	d	
20	m	d	
8	m	u	
27	m	u	
9	m	u	
25	m	l	
29	m	l	
5	m	r	
30	m	r	
17	m	r	
22	m	l	
3	m	u	
28	m	l	
2	m	l	
1	m	r	
10	m	r	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.....b.C..W.W...W....
01 .B....BBBBB.M.BBBBBBBBBBB.....
02 .B........BF..B...............
03 CBBBBBBBBBB...B...............
04 ..F....Cb.M...B...............
05 ........BBBC..B.Z.C...........
06 ........B.B...Bb.............M
07 ........B.B...B.BBBBBB...C....
08 .C.M...BB.B..CB.B..b.b...W....
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B............B......
11 .B.Bb..BC.....C........BC.....
12 .B.....B....C......C...B....WM
13 .BBBBBBB......................
14 ......................M..W.W..

citizens
20
type	id	player	row	column	weapon	life
b	1	0	4	7	n	60
b	2	0	11	14	n	60
b	3	0	8	13	n	40
w	4	0	0	19	g	100
w	5	0	12	28	h	100
b	7	1	3	0	n	60
b	8	1	5	11	n	60
b	9	1	11	8	n	60
w	10	1	8	25	b	80
b	15	2	12	19	n	60
w	17	2	14	27	h	80
b	18	3	8	1	n	60
b	20	3	11	24	n	60
w	22	3	0	21	b	80
w	23	3	14	25	g	100
b	25	2	0	16	n	40
b	27	3	7	25	n	60
w	28	2	0	25	b	100
b	29	3	5	18	n	60
b	30	2	12	12	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	30
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 180
day 0

score	480	490	135	940

status	0	0	0	0

commands
20
7	m	d	
15	m	d	
18	m	r	
25	m	l	
30	m	d	
4	m	l	
5	m	r	
17	m	r	
8	m	u	
20	m	d	
28	m	l	
3	m	u	
27	m	u	
29	m	l	
2	m	r	
9	m	r	
10	m	u	
1	m	r	
22	m	l	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b.....bC..W.W...W...C.
01 .B....BBBBB.M.BBBBBBBBBBB.....
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 C.F....Cb.MC..B...............
05 ........BBB...B.ZC............
06 ........B.B...Bb.........C...M
07 ........B.B..CB.BBBBBB...W....
08 ..CM...BB.B...B.B.Mb.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B.......M....B......
11 .B.Bb..B.C.....C.......B......
12 .B.....B...............BC....W
13 .BBBBBBB....C......C..........
14 ......................M...W.W.

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	7	n	60
b	2	0	11	15	n	60
b	3	0	7	13	n	40
w	4	0	0	18	g	100
w	5	0	12	29	h	100
b	7	1	4	0	n	60
b	8	1	4	11	n	60
b	9	1	11	9	n	60
w	10	1	7	25	b	80
b	15	2	13	19	n	60
w	17	2	14	28	h	80
b	18	3	8	2	n	60
b	20	3	12	24	n	60
w	22	3	0	20	b	80
w	23	3	14	26	g	100
b	25	2	0	15	n	40
b	27	3	6	25	n	60
w	28	2	0	24	b	100
b	29	3	5	17	n	60
b	30	2	13	12	n	60
b	31	0	0	28	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	20
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 181
day 0

score	485	490	135	940

status	0	0	0	0

commands
21
15	m	d	
4	m	l	
25	m	l	
7	m	d	
5	m	l	
30	m	r	
31	m	r	
17	m	u	
18	m	r	
3	m	u	
20	m	d	
2	m	u	
28	m	r	
1	m	r	
8	m	l	
27	m	u	
29	m	l	
9	m	r	
22	m	l	
23	m	r	
10	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b.....c..W.W.....W...C
01 .B....BBBBB.M.BBBBBBBBBBB.....
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 ..F....Cb.C...B.............M.
05 C.......BBB...B.C........C....
06 ........B.B..CBb.........W...M
07 ........B.B...B.BBBBBB........
08 ...C...BB.B...B.B.Mb.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B....C..M....B......
11 .B.Bb..B..C............B......
12 .B.....B...............B....W.
13 .BBBBBBB.....C..........C...W.
14 ....F..............C..M....W..

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	7	n	60
b	2	0	10	15	n	60
b	3	0	6	13	n	40
w	4	0	0	17	g	100
w	5	0	12	28	h	100
b	7	1	5	0	n	60
b	8	1	4	10	n	60
b	9	1	11	10	n	60
w	10	1	6	25	b	80
b	15	2	14	19	n	60
w	17	2	13	28	h	80
b	18	3	8	3	n	60
b	20	3	13	24	n	60
w	22	3	0	19	b	80
w	23	3	14	27	g	100
b	25	2	0	14	n	40
b	27	3	5	25	n	60
w	28	2	0	25	b	100
b	29	3	5	16	n	60
b	30	2	13	13	n	60
b	31	0	0	29	n	60

barricades
9
player	row	column	resistance
2	0	8	40
2	0	14	40
3	4	8	10
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 182
day 0

score	485	495	135	945

status	0	0	0	0

commands
21
15	m	r	
18	m	u	
4	m	l	
20	m	d	
27	m	u	
25	m	l	
29	m	u	
30	m	r	
7	m	u	
17	m	u	
8	m	r	
22	m	l	
9	m	r	
10	m	u	
5	m	d	
28	m	r	
31	m	d	
3	m	u	
23	m	u	
2	m	r	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b....Cb.W.W.......W...
01 .B....BBBBB.M.BBBBBBBBBBB....C
02 .B........BF..B...............
03 .BBBBBBBBBB...B...............
04 C.F....C...C..B.C........C..M.
05 ........BBB..CB..........W....
06 ........B.B...Bb.............M
07 ...C....B.B...B.BBBBBB........
08 .......BB.B...B.B.Mb.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B.....C.M....B......
11 .B.Bb..B...C...........B......
12 .B.....B...............B....W.
13 .BBBBBBB......C............WW.
14 ....F...............C.M.C.....

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	7	n	60
b	2	0	10	16	n	60
b	3	0	5	13	n	40
w	4	0	0	16	g	100
w	5	0	12	28	h	60
b	7	1	4	0	n	60
b	8	1	4	11	n	60
b	9	1	11	11	n	60
w	10	1	5	25	b	80
b	15	2	14	20	n	60
w	17	2	13	28	h	80
b	18	3	7	3	n	60
b	20	3	14	24	n	60
w	22	3	0	18	b	80
w	23	3	13	27	g	100
b	25	2	0	13	n	40
b	27	3	4	25	n	60
w	28	2	0	26	b	100
b	29	3	4	16	n	60
b	30	2	13	14	n	60
b	31	0	1	29	n	60

barricades
8
player	row	column	resistance
2	0	8	40
2	0	14	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 183
day 0

score	485	495	135	945

status	0	0	0	0

commands
21
5	m	u	
4	m	l	
15	m	r	
2	m	r	
7	m	u	
8	m	u	
18	m	d	
25	m	d	
30	m	r	
17	m	u	
28	m	d	
20	m	l	
31	m	d	
27	m	u	
3	m	u	
1	m	r	
9	m	r	
10	m	u	
29	m	u	
22	m	l	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b.....bW.W............
01 .B....BBBBB.MCBBBBBBBBBBB.W...
02 .B........BF..B..............C
03 CBBBBBBBBBBC..B.C........C....
04 ..F.....C....CB..........W..M.
05 ........BBB...B...............
06 ........B.B...Bb.............M
07 ........B.B...B.BBBBBB........
08 ...C...BB.B...B.B.Mb.b........
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B......CM....B......
11 .B.Bb..B....C..........B....W.
12 .B.....B...............B....W.
13 .BBBBBBB.......C............W.
14 ....F................CMC......

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	8	n	60
b	2	0	10	17	n	60
b	3	0	4	13	n	40
w	4	0	0	15	g	100
w	5	0	11	28	h	60
b	7	1	3	0	n	60
b	8	1	3	11	n	60
b	9	1	11	12	n	60
w	10	1	4	25	b	80
b	15	2	14	21	n	60
w	17	2	12	28	h	80
b	18	3	8	3	n	60
b	20	3	14	23	n	60
w	22	3	0	17	b	80
w	23	3	13	28	g	100
b	25	2	1	13	n	40
b	27	3	3	25	n	60
w	28	2	1	26	b	100
b	29	3	3	16	n	60
b	30	2	13	15	n	60
b	31	0	2	29	n	60

barricades
8
player	row	column	resistance
2	0	8	40
2	0	14	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 184
day 0

score	485	495	135	945

status	0	0	0	0

commands
21
18	m	d	
20	m	u	
27	m	u	
15	m	l	
7	m	u	
29	m	u	
5	m	u	
25	m	l	
8	m	u	
22	m	l	
30	m	r	
17	m	u	
9	m	r	
23	m	u	
28	m	d	
4	m	l	
10	m	u	
2	m	r	
31	m	d	
3	m	u	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b.....bWW.............
01 .B....BBBBB.C.BBBBBBBBBBB.....
02 CB........BC..B.C........CW...
03 .BBBBBBBBBB..CB..........W...C
04 ..F......C....B.............M.
05 ........BBB...B...............
06 ........B.B...Bb.............M
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.Mb.b..F.....
09 ...C.B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B.......C...MB....W.
11 .B.Bb..B.....C.........B....W.
12 .B.....B...............B....W.
13 .BBBBBBB........C......C......
14 ....F...............C.M.......

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	9	n	60
b	2	0	10	18	n	60
b	3	0	3	13	n	40
w	4	0	0	15	g	100
w	5	0	10	28	h	60
b	7	1	2	0	n	60
b	8	1	2	11	n	60
b	9	1	11	13	n	60
w	10	1	3	25	b	80
b	15	2	14	20	n	60
w	17	2	11	28	h	80
b	18	3	9	3	n	60
b	20	3	13	23	n	60
w	22	3	0	16	b	80
w	23	3	12	28	g	100
b	25	2	1	12	n	40
b	27	3	2	25	n	60
w	28	2	2	26	b	100
b	29	3	2	16	n	60
b	30	2	13	16	n	60
b	31	0	3	29	n	60

barricades
8
player	row	column	resistance
2	0	8	40
2	0	14	30
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 185
day 0

score	490	495	140	945

status	0	0	0	0

commands
21
15	m	r	
18	m	l	
20	m	d	
5	m	u	
4	m	l	
7	m	u	
8	m	d	
27	m	u	
31	m	d	
29	m	d	
25	m	u	
9	m	d	
10	m	u	
3	m	u	
2	m	r	
30	m	r	
1	m	r	
22	m	l	
23	m	u	
17	m	u	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....M..b...C.bWW.............
01 CB....BBBBB...BBBBBBBBBBBC....
02 .B........B..CB......M...WW...
03 .BBBBBBBBBBC..B.C.............
04 ..F.......C...B.............MC
05 ........BBB...B...............
06 ........B.B...Bb........M....M
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.Mb.b..F.....
09 ..C..B.b..B....bBBBBBBBB.F..W.
10 .BBB.BBB..B........C..MB....W.
11 .B.Bb..B...............B......
12 .B.....B.....C.........B....W.
13 .BBBBBBB.........C............
14 ....F................CMC......

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	10	n	60
b	2	0	10	19	n	60
b	3	0	2	13	n	40
w	4	0	0	15	g	80
w	5	0	9	28	h	60
b	7	1	1	0	n	60
b	8	1	3	11	n	60
b	9	1	12	13	n	60
w	10	1	2	25	b	60
b	15	2	14	21	n	60
w	17	2	10	28	h	60
b	18	3	9	2	n	60
b	20	3	14	23	n	60
w	22	3	0	16	b	80
w	23	3	12	28	g	100
b	25	2	0	12	n	40
b	27	3	1	25	n	60
w	28	2	2	26	b	100
b	29	3	3	16	n	60
b	30	2	13	17	n	60
b	31	0	4	29	n	60

barricades
8
player	row	column	resistance
2	0	8	40
2	0	14	20
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 186
day 0

score	490	495	140	945

status	0	0	0	0

commands
21
18	m	l	
20	m	u	
5	m	d	
7	m	u	
15	m	u	
4	m	l	
25	m	l	
30	m	r	
8	m	d	
31	m	l	
17	m	u	
27	m	u	
28	m	l	
29	m	u	
9	m	r	
3	m	u	
10	m	l	
22	m	l	
23	m	u	
2	m	r	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 C....M..b..C..bWW........C....
01 .B....BBBBB..CBBBBBBBBBBB.....
02 .B........B...B.C....M..W.W...
03 .BBBBBBBBBB...B...............
04 ..F.......CC..B.............C.
05 ........BBB...B...............
06 ........B.B...Bb........M....M
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.Mb.b..F.....
09 .C...B.b..B....bBBBBBBBB.F..W.
10 .BBB.BBB..B.........C.MB....W.
11 .B.Bb..B...............B....W.
12 .B.....B......C........B......
13 .BBBBBBB..........C..C.C......
14 ....F.................M.......

citizens
21
type	id	player	row	column	weapon	life
b	1	0	4	10	n	60
b	2	0	10	20	n	60
b	3	0	1	13	n	40
w	4	0	0	15	g	60
w	5	0	9	28	h	60
b	7	1	0	0	n	60
b	8	1	4	11	n	40
b	9	1	12	14	n	60
w	10	1	2	24	b	40
b	15	2	13	21	n	60
w	17	2	10	28	h	20
b	18	3	9	1	n	60
b	20	3	13	23	n	60
w	22	3	0	16	b	80
w	23	3	11	28	g	100
b	25	2	0	11	n	40
b	27	3	0	25	n	60
w	28	2	2	26	b	100
b	29	3	2	16	n	60
b	30	2	13	18	n	60
b	31	0	4	28	n	60

barricades
8
player	row	column	resistance
2	0	8	40
2	0	14	10
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 187
day 0

score	495	495	140	945

status	0	0	0	0

commands
20
18	m	l	
20	m	d	
7	m	r	
5	m	u	
27	m	d	
29	m	r	
15	m	l	
25	m	l	
8	m	d	
9	m	u	
22	m	l	
23	m	u	
4	m	l	
30	m	d	
10	m	d	
2	m	r	
31	m	r	
28	m	l	
3	m	u	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C...M..b.C..C.WW.............
01 .B....BBBBB...BBBBBBBBBBBC....
02 .B........B...B..C...M...W....
03 .BBBBBBBBBB...B.........W.....
04 ..F........C..B..............C
05 ........BBBC..B...............
06 ........B.B...BbZ.......M....M
07 ........B.B...B.BBBBBB........
08 .......BB.B...B.B.Mb.b..F...W.
09 C....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..B..........CMB......
11 .B.Bb..B......C........B....W.
12 .B.....B...............B......
13 .BBBBBBB............C.........
14 ....F.............C...MC......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	4	11	n	60
b	2	0	10	21	n	60
b	3	0	0	13	n	40
w	4	0	0	15	g	40
w	5	0	8	28	h	60
b	7	1	0	1	n	60
b	8	1	5	11	n	40
b	9	1	11	14	n	60
w	10	1	3	24	b	40
b	15	2	13	20	n	60
b	18	3	9	0	n	60
b	20	3	14	23	n	60
w	22	3	0	16	b	80
w	23	3	11	28	g	100
b	25	2	0	10	n	40
b	27	3	1	25	n	60
w	28	2	2	25	b	100
b	29	3	2	17	n	60
b	30	2	14	18	n	60
b	31	0	4	29	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 188
day 0

score	495	495	140	1195

status	0	0	0	0

commands
20
15	m	d	
18	m	d	
4	m	l	
20	m	l	
25	m	l	
30	m	r	
5	m	u	
28	m	u	
27	m	u	
31	m	d	
2	m	r	
29	m	d	
3	m	l	
22	m	l	
7	m	r	
8	m	d	
23	m	u	
9	m	r	
10	m	d	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C..M..bC..C.WW.........C....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......M...W....
03 .BBBBBBBBBB...B..C............
04 ..F.........C.B.........W.....
05 ........BBB...B..............C
06 ........B.BC..BbZ.......M....M
07 ........B.B...B.BBBBBB......W.
08 .......BB.B...B.B.Mb.b..F.....
09 .....B.b..B....bBBBBBBBB.F....
10 CBBB.BBB..B...........CB....W.
11 .B.Bb..B....M..C.......B......
12 .B.....B...............B......
13 .BBBBBBB.................M....
14 ....F..............CC.C.......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	4	12	n	60
b	2	0	10	22	n	60
b	3	0	0	12	n	40
w	4	0	0	14	g	40
w	5	0	7	28	h	60
b	7	1	0	2	n	60
b	8	1	6	11	n	40
b	9	1	11	15	n	60
w	10	1	4	24	b	40
b	15	2	14	20	n	60
b	18	3	10	0	n	60
b	20	3	14	22	n	60
w	22	3	0	15	b	80
w	23	3	10	28	g	100
b	25	2	0	9	n	40
b	27	3	0	25	n	40
w	28	2	2	25	b	100
b	29	3	3	17	n	60
b	30	2	14	19	n	60
b	31	0	5	29	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 189
day 0

score	500	495	140	1200

status	0	0	0	0

commands
19
7	m	r	
8	m	d	
9	m	l	
18	m	d	
10	m	d	
4	m	l	
20	m	u	
25	m	l	
30	m	l	
28	m	u	
27	m	l	
5	m	u	
31	m	d	
3	m	l	
2	m	d	
29	m	d	
22	m	l	
1	m	d	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C.M..c..C.WW.........C.....
01 .B....BBBBB...BBBBBBBBBBBW....
02 .B........B...B......M........
03 .BBBBBBBBBB...B...............
04 ..F...........B..C............
05 ........BBB.C.B.........W.....
06 ........B.B...BbZ.......M...WC
07 ........B.BC..B.BBBBBB........
08 .......BB.B...B.B.Mb.b..F.....
09 .....B.b..B....bBBBBBBBB.F..W.
10 .BBB.BBB..B............B......
11 CB.Bb..B....M.C.......CB......
12 .B.....B...............B......
13 .BBBBBBB..............C..M....
14 ....F.............C.C.........

citizens
20
type	id	player	row	column	weapon	life
b	1	0	5	12	n	60
b	2	0	11	22	n	60
b	3	0	0	11	n	40
w	4	0	0	13	g	40
w	5	0	6	28	h	60
b	7	1	0	3	n	60
b	8	1	7	11	n	40
b	9	1	11	14	n	60
w	10	1	5	24	b	40
b	15	2	14	20	n	60
b	18	3	11	0	n	60
b	20	3	13	22	n	60
w	22	3	0	14	b	80
w	23	3	9	28	g	100
b	25	2	0	8	n	40
b	27	3	0	24	n	40
w	28	2	1	25	b	100
b	29	3	4	17	n	60
b	30	2	14	18	n	60
b	31	0	6	29	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 190
day 0

score	505	495	140	1200

status	0	0	0	0

commands
20
18	m	u	
4	m	d	
20	m	r	
5	m	u	
15	m	l	
7	m	r	
25	m	l	
31	m	l	
3	m	l	
2	m	d	
8	m	d	
9	m	l	
1	m	d	
30	m	u	
27	m	l	
28	m	u	
29	m	d	
22	m	l	
23	m	u	
10	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....CM.Cb.C..W.........C.W....
01 .B....BBBBB..WBBBBBBBBBBB.....
02 .B........B...B......M........
03 .BBBBBBBBBB...B...............
04 ..F...........B...............
05 ........BBB...B..C..........W.
06 ........B.B.C.BbZ.......W...C.
07 ........B.B...B.BBBBBB........
08 ....M..BB.BC..B.B.Mb.b..F...W.
09 .....B.b..B....bBBBBBBBB.F....
10 CBBB.BBB..B............B......
11 .B.Bb..B....MC.........B......
12 .B.....B..............CB......
13 .BBBBBBB..........C....C.M....
14 ....F..............C..........

citizens
20
type	id	player	row	column	weapon	life
b	1	0	6	12	n	60
b	2	0	12	22	n	60
b	3	0	0	10	n	40
w	4	0	1	13	g	40
w	5	0	5	28	h	60
b	7	1	0	4	n	60
b	8	1	8	11	n	40
b	9	1	11	13	n	60
w	10	1	6	24	b	40
b	15	2	14	19	n	60
b	18	3	10	0	n	60
b	20	3	13	23	n	60
w	22	3	0	13	b	80
w	23	3	8	28	g	100
b	25	2	0	7	n	40
b	27	3	0	23	n	40
w	28	2	0	25	b	100
b	29	3	5	17	n	60
b	30	2	13	18	n	60
b	31	0	6	28	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 191
day 0

score	505	500	140	1200

status	0	0	0	0

commands
19
15	m	r	
4	m	d	
25	m	r	
7	m	r	
30	m	u	
8	m	d	
18	m	u	
20	m	d	
27	m	l	
29	m	d	
5	m	u	
22	m	d	
28	m	l	
23	m	u	
3	m	l	
2	m	d	
9	m	l	
1	m	d	
10	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....C..cC............C.W.....
01 .B....BBBBB..WBBBBBBBBBBB.....
02 .B........B..WB......M........
03 .BBBBBBBBBB...B...............
04 ..F...........B.............W.
05 ........BBB...B...............
06 ........B.B...BbZC..........C.
07 ........B.B.C.B.BBBBBB..W...W.
08 ....M..BB.B...B.B.Mb.b..F.....
09 C....B.b..BC...bBBBBBBBB.F....
10 .BBB.BBB..B............B......
11 .B.Bb..B....C..........B......
12 .B.....B..........C....B......
13 .BBBBBBB..............C..M....
14 ....F...............C..C......

citizens
20
type	id	player	row	column	weapon	life
b	1	0	7	12	n	60
b	2	0	13	22	n	60
b	3	0	0	9	n	40
w	4	0	2	13	g	40
w	5	0	4	28	h	60
b	7	1	0	5	n	60
b	8	1	9	11	n	40
b	9	1	11	12	n	60
w	10	1	7	24	b	40
b	15	2	14	20	n	60
b	18	3	9	0	n	60
b	20	3	14	23	n	60
w	22	3	1	13	b	80
w	23	3	7	28	g	100
b	25	2	0	8	n	40
b	27	3	0	22	n	40
w	28	2	0	24	b	100
b	29	3	6	17	n	60
b	30	2	12	18	n	60
b	31	0	6	28	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 192
day 0

score	505	510	140	1200

status	0	0	0	0

commands
19
15	m	l	
30	m	l	
31	m	u	
4	m	d	
28	m	l	
5	m	u	
18	m	u	
7	m	l	
3	m	r	
20	m	r	
27	m	l	
8	m	d	
9	m	u	
29	m	l	
10	m	d	
22	m	d	
2	m	r	
1	m	u	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C...c.C..........C.W..W...
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B..WB......M........
03 .BBBBBBBBBB..WB.............W.
04 ..F...........B...............
05 ........BBB...B.............C.
06 ....C...B.B.C.BbC....M......W.
07 ........B.B...B.BBBBBB........
08 C...M..BB.B...B.B.Mb.b..W.....
09 .....B.b..B....bBBBBBBBB.F....
10 .BBB.BBB..BCC..........B......
11 GB.Bb..B...............B......
12 .B.M...B.........C.....B......
13 .BBBBBBB...............C.M....
14 ....F..............C....C.....

citizens
22
type	id	player	row	column	weapon	life
b	1	0	6	12	n	60
b	2	0	13	23	n	60
b	3	0	0	10	n	40
w	4	0	3	13	g	40
w	5	0	3	28	h	60
b	7	1	0	4	n	60
b	8	1	10	11	n	40
b	9	1	10	12	n	60
w	10	1	8	24	b	60
b	15	2	14	19	n	60
b	18	3	8	0	n	60
b	20	3	14	24	n	60
w	22	3	2	13	b	80
w	23	3	6	28	g	100
b	25	2	0	8	n	40
b	27	3	0	21	n	40
w	28	2	0	23	b	100
b	29	3	6	16	n	60
b	30	2	12	17	n	60
b	31	0	5	28	n	60
b	32	2	6	4	n	60
w	33	1	0	26	h	100

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 193
day 0

score	505	510	140	1200

status	0	0	0	0

commands
21
18	m	d	
7	m	l	
15	m	u	
31	m	u	
20	m	r	
30	m	u	
32	m	d	
8	m	d	
4	m	d	
28	m	l	
5	m	u	
2	m	r	
9	m	u	
3	m	r	
10	m	d	
1	m	u	
33	m	l	
27	m	l	
29	m	r	
22	m	d	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C....c..C........C.W..W....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......M......W.
03 .BBBBBBBBBB..WB...............
04 ..F..........WB.............C.
05 ........BBB.C.B.............W.
06 .F......B.B...Bb.C...M........
07 ....C...B.B...B.BBBBBB........
08 ....M..BB.B...B.B.Mb.b........
09 C....B.b..B.C..bBBBBBBBBWF....
10 .BBB.BBB..B.........M..B......
11 GB.Bb..B...C.....C.....B......
12 .B.M...B...............B......
13 .BBBBBBB...........C....CM....
14 ....F....................C....

citizens
22
type	id	player	row	column	weapon	life
b	1	0	5	12	n	60
b	2	0	13	24	n	60
b	3	0	0	11	n	40
w	4	0	4	13	g	40
w	5	0	2	28	h	60
b	7	1	0	3	n	60
b	8	1	11	11	n	40
b	9	1	9	12	n	60
w	10	1	9	24	b	60
b	15	2	13	19	n	60
b	18	3	9	0	n	60
b	20	3	14	25	n	60
w	22	3	3	13	b	80
w	23	3	5	28	g	100
b	25	2	0	8	n	40
b	27	3	0	20	n	40
w	28	2	0	22	b	100
b	29	3	6	17	n	60
b	30	2	11	17	n	60
b	31	0	4	28	n	60
b	32	2	7	4	n	60
w	33	1	0	25	h	100

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 194
day 0

score	505	510	140	1200

status	0	0	0	0

commands
22
15	m	r	
18	m	d	
25	m	l	
7	m	l	
20	m	r	
30	m	r	
27	m	l	
29	m	r	
31	m	u	
8	m	l	
4	m	d	
5	m	u	
32	m	d	
9	m	d	
2	m	r	
22	m	d	
10	m	r	
28	m	l	
23	m	u	
33	m	l	
3	m	r	
1	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C....Cb...C......C.W..W.....
01 .B....BBBBB...BBBBBBBBBBB...W.
02 .B........B...B......M........
03 .BBBBBBBBBB...B.............C.
04 ..F....M....CWB.............W.
05 ........BBB..WB...............
06 .F......B.B...Bb..C..M........
07 ........B.B...B.BBBBBB........
08 ....C..BB.B...B.B.Mb.b........
09 .....B.b..B....bBBBBBBBB.W....
10 CBBB.BBB..B.C.......M..B......
11 GB.Bb..B..C.......C....B......
12 .B.M...B...............B......
13 .BBBBBBB............C....C....
14 ....F.....................C...

citizens
22
type	id	player	row	column	weapon	life
b	1	0	4	12	n	60
b	2	0	13	25	n	60
b	3	0	0	12	n	40
w	4	0	5	13	g	40
w	5	0	1	28	h	60
b	7	1	0	2	n	60
b	8	1	11	10	n	40
b	9	1	10	12	n	60
w	10	1	9	25	b	80
b	15	2	13	20	n	60
b	18	3	10	0	n	60
b	20	3	14	26	n	60
w	22	3	4	13	b	80
w	23	3	4	28	g	100
b	25	2	0	7	n	40
b	27	3	0	19	n	40
w	28	2	0	21	b	100
b	29	3	6	18	n	60
b	30	2	11	18	n	60
b	31	0	3	28	n	60
b	32	2	8	4	n	60
w	33	1	0	24	h	100

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 195
day 0

score	510	510	145	1200

status	0	0	0	0

commands
22
18	m	d	
15	m	u	
7	m	l	
8	m	l	
20	m	r	
9	m	r	
25	m	r	
27	m	l	
10	m	d	
31	m	u	
29	m	r	
22	m	d	
4	m	d	
1	m	u	
23	m	u	
30	m	r	
5	m	u	
3	m	d	
33	m	l	
2	m	l	
32	m	r	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .C......c.........C.W..W....W.
01 .B....BBBBB.C.BBBBBBBBBBB.....
02 .B........B...B......M......C.
03 .BBBBBBBBBB.C.B.............W.
04 ..F....M.....WB...............
05 ........BBB...B...............
06 .F......B.B..WBb...C.M........
07 ........B.B...B.BBBBBB.M......
08 .....C.BB.B...B.B.Mb.b......M.
09 .....B.b..B....bBBBBBBBB......
10 .BBB.BBB..B..C......M..B.W....
11 CB.BbC.B.C.........C...B......
12 .B.M...B............C..B......
13 .BBBBBBB................C.....
14 ....F......................C..

citizens
23
type	id	player	row	column	weapon	life
b	1	0	3	12	n	60
b	2	0	13	24	n	60
b	3	0	1	12	n	40
w	4	0	6	13	g	40
w	5	0	0	28	h	60
b	7	1	0	1	n	60
b	8	1	11	9	n	40
b	9	1	10	13	n	60
w	10	1	10	25	b	80
b	15	2	12	20	n	60
b	18	3	11	0	n	60
b	20	3	14	27	n	60
w	22	3	4	13	b	60
w	23	3	3	28	g	100
b	25	2	0	8	n	40
b	27	3	0	18	n	40
w	28	2	0	20	b	100
b	29	3	6	19	n	60
b	30	2	11	19	n	60
b	31	0	2	28	n	60
b	32	2	8	5	n	60
w	33	1	0	23	h	100
b	34	1	11	5	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 196
day 0

score	510	510	145	1200

status	0	0	0	0

commands
21
15	m	u	
25	m	r	
18	m	u	
20	m	u	
7	m	l	
31	m	u	
8	m	l	
27	m	l	
32	m	r	
9	m	r	
34	m	d	
10	m	l	
33	m	l	
29	m	r	
28	m	l	
22	m	u	
23	m	u	
4	m	d	
1	m	u	
5	m	l	
2	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 C.......bC.......C.W..W....W..
01 .B....BBBBB.C.BBBBBBBBBBB...C.
02 .B........B.C.B......M......W.
03 .BBBBBBBBBB..WB...............
04 ..F....M......B...............
05 ........BBB...B...............
06 .F......B.B...Bb....CM........
07 ........B.B..WB.BBBBBB.M......
08 ......CBB.B...B.B.Mb.b......M.
09 .....B.b..B....bBBBBBBBB......
10 CBBB.BBB..B...C.....M..BW.....
11 .B.Bb..BC..........CC..B......
12 .B.M.C.B...............BC.....
13 .BBBBBBB...................C..
14 ....F.........................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	2	12	n	60
b	2	0	12	24	n	60
b	3	0	1	12	n	40
w	4	0	7	13	g	40
w	5	0	0	27	h	60
b	7	1	0	0	n	60
b	8	1	11	8	n	40
b	9	1	10	14	n	60
w	10	1	10	24	b	80
b	15	2	11	20	n	60
b	18	3	10	0	n	60
b	20	3	13	27	n	60
w	22	3	3	13	b	60
w	23	3	2	28	g	100
b	25	2	0	9	n	40
b	27	3	0	17	n	40
w	28	2	0	19	b	100
b	29	3	6	20	n	60
b	30	2	11	19	n	60
b	31	0	1	28	n	60
b	32	2	8	6	n	60
w	33	1	0	22	h	100
b	34	1	12	5	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 197
day 0

score	510	510	145	1200

status	0	0	0	0

commands
21
18	m	u	
15	m	u	
7	m	d	
20	m	u	
8	m	d	
27	m	l	
31	m	u	
25	m	r	
30	m	r	
32	m	u	
28	m	l	
29	m	r	
2	m	r	
22	m	u	
5	m	l	
23	m	u	
4	m	d	
9	m	r	
34	m	l	
10	m	d	
33	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........b.C.....C.W..W....W.C.
01 CB....BBBBB.C.BBBBBBBBBBB...W.
02 .B........B.CWB......M........
03 .BBBBBBBBBB...B...............
04 ..F....M......B...............
05 ........BBB...B...............
06 .F......B.B...Bb.....C........
07 ......C.B.B...B.BBBBBB.M......
08 .......BB.B..WB.B.Mb.b......M.
09 C....B.b..B....bBBBBBBBB......
10 .BBB.BBB..B....C....C..B......
11 .B.Bb..B............C..BW.....
12 .B.MC..BC..............B.C.C..
13 .BBBBBBB......................
14 ....F.........................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	2	12	n	60
b	2	0	12	25	n	60
b	3	0	1	12	n	40
w	4	0	8	13	g	40
w	5	0	0	26	h	60
b	7	1	1	0	n	60
b	8	1	12	8	n	40
b	9	1	10	15	n	60
w	10	1	11	24	b	80
b	15	2	10	20	n	60
b	18	3	9	0	n	60
b	20	3	12	27	n	60
w	22	3	2	13	b	60
w	23	3	1	28	g	100
b	25	2	0	10	n	40
b	27	3	0	16	n	40
w	28	2	0	18	b	100
b	29	3	6	21	n	60
b	30	2	11	20	n	60
b	31	0	0	28	n	60
b	32	2	7	6	n	60
w	33	1	0	21	h	100
b	34	1	12	4	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 198
day 0

score	510	510	150	1205

status	0	0	0	0

commands
22
25	m	l	
18	m	u	
7	m	d	
31	m	r	
20	m	u	
30	m	d	
27	m	l	
3	m	u	
32	m	r	
8	m	d	
2	m	r	
28	m	l	
29	m	r	
1	m	u	
22	m	l	
23	m	u	
9	m	r	
34	m	l	
5	m	l	
4	m	d	
10	m	d	
33	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........bC..C..C.W....W..W..WC
01 .B....BBBBB.C.BBBBBBBBBBB.....
02 CB........B.W.B......M........
03 .BBBBBBBBBB...B...............
04 ..FM...M......B...............
05 ........BBB...B...............
06 .F......B.B...Bb......C.......
07 .......CB.B...B.BBBBBB.M......
08 C..M...BB.B...B.B.Mb.b......M.
09 .....B.b..B..W.bBBBBBBBB......
10 .BBB.BBB..B.....C...C..B......
11 .B.Bb..B...............B...C..
12 .B.C...B............C..BW.C...
13 .BBBBBBBC.....................
14 ....F.........................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	1	12	n	60
b	2	0	12	26	n	60
b	3	0	0	12	n	40
w	4	0	9	13	g	40
w	5	0	0	25	h	60
b	7	1	2	0	n	60
b	8	1	13	8	n	40
b	9	1	10	16	n	60
w	10	1	12	24	b	80
b	15	2	10	20	n	60
b	18	3	8	0	n	60
b	20	3	11	27	n	60
w	22	3	2	12	b	60
w	23	3	0	28	g	100
b	25	2	0	9	n	40
b	27	3	0	15	n	40
w	28	2	0	17	b	100
b	29	3	6	22	n	60
b	30	2	12	20	n	60
b	31	0	0	29	n	60
b	32	2	7	7	n	60
w	33	1	0	22	h	100
b	34	1	12	3	n	60

barricades
7
player	row	column	resistance
2	0	8	40
0	6	15	40
3	8	19	40
3	8	21	40
2	9	7	40
0	9	15	40
0	11	4	40

round 199
day 0

score	510	515	150	1205

status	0	0	0	0

commands
23
7	m	d	
31	m	d	
18	m	r	
3	m	r	
8	m	d	
9	m	l	
2	m	u	
20	m	u	
1	m	u	
34	m	l	
27	m	l	
10	m	r	
5	m	d	
29	m	d	
33	m	r	
22	m	u	
4	m	r	
23	m	r	
15	m	d	
25	m	l	
30	m	d	
32	m	u	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ........C...CCC.W......W.....W
01 .B....BBBBB.W.BBBBBBBBBBBW...C
02 .B........B...B......M........
03 CBBBBBBBBBB...B...............
04 ..FM...M....G.B...............
05 ........BBB...B...............
06 .F.....CB.B...B...............
07 ........B.B...B.BBBBBBCM......
08 .C.M...BB.B...B.B.M.........M.
09 .....B....B...W.BBBBBBBB......
10 .BBB.BBB..B....C.......B...C..
11 .B.B...B............C..B..C...
12 .BC....B...............B.W....
13 .BBBBBBB............C.........
14 ....F...C.....................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	0	12	n	60
b	2	0	11	26	n	60
b	3	0	0	13	n	40
w	4	0	9	14	g	40
w	5	0	1	25	h	60
b	7	1	3	0	n	60
b	8	1	14	8	n	40
b	9	1	10	15	n	60
w	10	1	12	25	b	80
b	15	2	11	20	n	60
b	18	3	8	1	n	60
b	20	3	10	27	n	60
w	22	3	1	12	b	60
w	23	3	0	29	g	100
b	25	2	0	8	n	40
b	27	3	0	14	n	40
w	28	2	0	16	b	100
b	29	3	7	22	n	60
b	30	2	13	20	n	60
b	31	0	1	29	n	60
b	32	2	6	7	n	60
w	33	1	0	23	h	100
b	34	1	12	2	n	60

barricades
0
player	row	column	resistance

round 200
day 1

score	510	515	150	1205

status	0	0	0	0

commands
22
15	m	d	
25	b	l	
30	m	r	
32	m	u	
18	m	r	
7	m	d	
8	m	l	
9	m	u	
28	m	r	
1	b	l	
20	m	u	
31	m	l	
34	m	r	
5	m	d	
10	m	l	
33	m	r	
27	b	r	
29	m	r	
3	m	d	
2	m	u	
22	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .......bC..bC.Cb.W......W.....
01 .B....BBBBB..CBBBBBBBBBBB...CW
02 .B........B.W.B......M...W....
03 .BBBBBBBBBB...B...............
04 C.FM...M....G.B...............
05 .......CBBB...B...............
06 .F......B.B...B...............
07 ........B.B...B.BBBBBB.C......
08 ..CM...BB.B...B.B.M.........M.
09 .....B....B...WCBBBBBBBB...C..
10 .BBB.BBB..B............B..C...
11 .B.B...B...............B......
12 .B.C...B............C..BW.....
13 .BBBBBBB.............C........
14 ....F..C......................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	0	12	n	60
b	2	0	10	26	n	60
b	3	0	1	13	n	40
w	4	0	9	14	g	40
w	5	0	2	25	h	60
b	7	1	4	0	n	60
b	8	1	14	7	n	40
b	9	1	9	15	n	60
w	10	1	12	24	b	80
b	15	2	12	20	n	60
b	18	3	8	2	n	60
b	20	3	9	27	n	60
w	22	3	2	12	b	60
w	23	3	1	29	g	100
b	25	2	0	8	n	40
b	27	3	0	14	n	40
w	28	2	0	17	b	100
b	29	3	7	23	n	60
b	30	2	13	21	n	60
b	31	0	1	28	n	60
b	32	2	5	7	n	60
w	33	1	0	24	h	100
b	34	1	12	3	n	60

barricades
3
player	row	column	resistance
2	0	7	40
0	0	11	40
3	0	15	40

round 201
day 1

score	510	515	150	1210

status	0	0	0	0

commands
22
7	m	r	
8	m	l	
9	m	u	
34	m	r	
18	m	r	
15	m	r	
20	m	u	
3	b	u	
1	b	r	
31	m	u	
29	m	d	
25	m	l	
22	m	d	
5	m	l	
23	m	l	
30	m	r	
32	m	u	
28	m	l	
4	m	l	
2	m	u	
10	m	d	
33	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .......c...bCbCbW..M.....W..C.
01 .B....BBBBB..CBBBBBBBBBBB...W.
02 .B........B...B......M..W.....
03 .BBBBBBBBBB.W.B...............
04 .CFM...C....G.B..............F
05 ........BBB...B...........G...
06 .F......B.B...B...............
07 ........B.B...B.BBBBBB........
08 ...C...BB.B...BCB.M....C...CM.
09 .....B....B..W..BBBBBBBB..C...
10 MBBB.BBB..B............B......
11 .B.B...B...............B......
12 .B..C..B.............C.B......
13 .BBBBBBB..............C.W.....
14 ....F.C.......................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	0	12	n	60
b	2	0	9	26	n	60
b	3	0	1	13	n	40
w	4	0	9	13	g	40
w	5	0	2	24	h	60
b	7	1	4	1	n	60
b	8	1	14	6	n	40
b	9	1	8	15	n	60
w	10	1	13	24	b	80
b	15	2	12	21	n	60
b	18	3	8	3	n	60
b	20	3	8	27	n	60
w	22	3	3	12	b	60
w	23	3	1	28	g	100
b	25	2	0	7	n	40
b	27	3	0	14	n	40
w	28	2	0	16	b	100
b	29	3	8	23	n	60
b	30	2	13	22	n	60
b	31	0	0	28	n	60
b	32	2	4	7	n	60
w	33	1	0	25	h	100
b	34	1	12	4	n	60

barricades
4
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40

round 202
day 1

score	510	515	155	1215

status	0	0	0	0

commands
21
18	m	u	
7	m	r	
20	m	u	
29	m	l	
15	m	d	
22	m	d	
25	m	l	
30	m	d	
32	m	l	
28	m	r	
5	m	r	
3	m	d	
8	m	l	
9	m	u	
34	m	u	
1	m	d	
31	m	l	
4	m	d	
2	m	r	
10	m	u	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ......Cb...b.bCb.W.M.......C..
01 .B....BBBBB.C.BBBBBBBBBBBW..W.
02 .B........B..CB......M...W....
03 .BBBBBBBBBB...B...............
04 ..CM..C.....W.B..............F
05 ........BBB...B...........G...
06 .F......B.B...B...............
07 ...C....B.BM..BCBBBBBB.....C..
08 .......BB.B...B.B.M...C.....M.
09 .....B....B.....BBBBBBBB...C..
10 MBBB.BBB..B..W.........B......
11 .B.BC..B...............B......
12 .B.....B...............BW.....
13 .BBBBBBB.............C........
14 ....FC................C.......

citizens
23
type	id	player	row	column	weapon	life
b	1	0	1	12	n	60
b	2	0	9	27	n	60
b	3	0	2	13	n	40
w	4	0	10	13	g	40
w	5	0	2	25	h	60
b	7	1	4	2	n	60
b	8	1	14	5	n	40
b	9	1	7	15	n	60
w	10	1	12	24	b	80
b	15	2	13	21	n	60
b	18	3	7	3	n	60
b	20	3	7	27	n	60
w	22	3	4	12	b	60
w	23	3	1	28	g	100
b	25	2	0	6	n	40
b	27	3	0	14	n	40
w	28	2	0	17	b	100
b	29	3	8	22	n	60
b	30	2	14	22	n	60
b	31	0	0	27	n	60
b	32	2	4	6	n	60
w	33	1	1	25	h	100
b	34	1	11	4	n	60

barricades
4
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40

round 203
day 1

score	510	515	155	1215

status	0	0	0	0

commands
22
18	m	u	
7	m	r	
20	m	u	
15	m	r	
5	m	r	
29	b	l	
22	m	l	
25	m	l	
8	m	l	
23	m	u	
30	m	r	
32	m	l	
28	m	r	
9	m	d	
31	m	l	
3	m	d	
34	m	u	
1	m	d	
2	m	u	
4	m	u	
10	m	u	
33	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .....C.b...b.bCb..WM......C.W.
01 .B....BBBBB...BBBBBBBBBBB.W...
02 .B........B.C.B......M....W...
03 .BBBBBBBBBB..CB...............
04 ...C.C.....W..B.F............F
05 ........BBB...B...........G...
06 .F.C....B.B...B............C..
07 ........B.BM..B.BBBBBB........
08 .......BB.B...BCB.M..bC....CM.
09 .....B....B..W..BBBBBBBB......
10 MBBBCBBB..B............B......
11 .B.B...B...............BW.....
12 .B.....B...............B......
13 .BBBBBBB..............C.......
14 ....C..................C......

citizens
23
type	id	player	row	column	weapon	life
b	1	0	2	12	n	60
b	2	0	8	27	n	60
b	3	0	3	13	n	40
w	4	0	9	13	g	40
w	5	0	2	26	h	60
b	7	1	4	3	n	60
b	8	1	14	4	n	60
b	9	1	8	15	n	60
w	10	1	11	24	b	80
b	15	2	13	22	n	60
b	18	3	6	3	n	60
b	20	3	6	27	n	60
w	22	3	4	11	b	60
w	23	3	0	28	g	100
b	25	2	0	5	n	40
b	27	3	0	14	n	40
w	28	2	0	18	b	100
b	29	3	8	22	n	60
b	30	2	14	23	n	60
b	31	0	0	26	n	60
b	32	2	4	5	n	60
w	33	1	1	26	h	100
b	34	1	10	4	n	60

barricades
5
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
3	8	21	40

round 204
day 1

score	510	520	155	1215

status	0	0	0	0

commands
21
3	m	l	
18	m	d	
20	m	u	
29	m	l	
22	m	l	
15	m	r	
25	m	l	
30	m	r	
23	m	l	
32	m	d	
7	m	l	
28	m	r	
8	m	l	
5	m	d	
31	b	l	
9	m	d	
34	m	u	
10	m	u	
2	m	r	
4	m	u	
33	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 ....C..b...b.bCb...W.....bCW..
01 .B....BBBBB...BBBBBBBBBBBW....
02 .B........B.C.B......M........
03 .BBBBBBBBBB.C.B...........W...
04 ..C.......W...B.F............F
05 .....C..BBB...B...........GC..
06 .F......B.B...B...............
07 ...C....B.BM..B.BBBBBB........
08 .......BB.B..WB.B.M..c......C.
09 ....CB....B....CBBBBBBBB......
10 MBBB.BBB..B............BW.....
11 .B.B...B...............B......
12 .B.....B...............B......
13 MBBBBBBB...............C......
14 ...C....................C.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	2	12	n	60
b	2	0	8	28	n	60
b	3	0	3	12	n	40
w	4	0	8	13	g	40
w	5	0	3	26	h	60
b	7	1	4	2	n	60
b	8	1	14	3	n	60
b	9	1	9	15	n	60
w	10	1	10	24	b	80
b	15	2	13	23	n	60
b	18	3	7	3	n	60
b	20	3	5	27	n	60
w	22	3	4	10	b	60
w	23	3	0	27	g	100
b	25	2	0	4	n	40
b	27	3	0	14	n	40
w	28	2	0	19	b	100
b	29	3	8	21	n	60
b	30	2	14	24	n	60
b	31	0	0	26	n	60
b	32	2	5	5	n	60
w	33	1	1	25	h	100
b	34	1	9	4	n	60

barricades
6
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 205
day 1

score	515	520	160	1215

status	0	0	0	0

commands
22
2	m	r	
18	m	d	
5	m	d	
15	m	r	
7	m	l	
8	m	l	
31	m	d	
20	m	l	
9	m	l	
25	m	l	
30	m	r	
34	m	l	
4	m	u	
32	m	d	
29	m	l	
28	m	r	
22	m	l	
10	m	u	
3	m	d	
1	m	l	
23	m	l	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ...C...b...b.bCb....W....bW...
01 .B....BBBBB...BBBBBBBBBBB.C...
02 .B........BC..B......M...W....
03 .BBBBBBBBBB...B...............
04 .C.......W..C.B.F.........W..F
05 ........BBB...B...........C...
06 .F...C..B.B...B...............
07 ........B.BM.WB.BBBBBB........
08 ...C...BB.B...B.B.M.Cb.......C
09 ...C.B....B...C.BBBBBBBBW.....
10 MBBB.BBB..B............B......
11 .B.B...B.........M.....B......
12 .B.....B...............B......
13 MBBBBBBB................C.....
14 ..C...M..................C....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	2	11	n	60
b	2	0	8	29	n	60
b	3	0	4	12	n	40
w	4	0	7	13	g	40
w	5	0	4	26	h	60
b	7	1	4	1	n	60
b	8	1	14	2	n	60
b	9	1	9	14	n	60
w	10	1	9	24	b	80
b	15	2	13	24	n	60
b	18	3	8	3	n	60
b	20	3	5	26	n	60
w	22	3	4	9	b	60
w	23	3	0	26	g	100
b	25	2	0	3	n	40
b	27	3	0	14	n	40
w	28	2	0	20	b	100
b	29	3	8	20	n	60
b	30	2	14	25	n	60
b	31	0	1	26	n	60
b	32	2	6	5	n	60
w	33	1	2	25	h	100
b	34	1	9	3	n	60

barricades
6
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 206
day 1

score	515	520	160	1215

status	0	0	0	0

commands
22
18	m	l	
7	m	d	
8	m	l	
15	m	l	
25	m	l	
9	m	l	
30	m	l	
34	m	l	
32	m	d	
20	m	l	
28	m	l	
4	m	l	
10	m	u	
29	m	l	
33	m	d	
31	m	d	
5	m	u	
3	m	d	
22	m	l	
23	m	d	
2	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..C....b...b.bCb...W.....b....
01 .B....BBBBB...BBBBBBBBBBB.W...
02 .B........B...B......M....C...
03 .BBBBBBBBBBC..B..........WW...
04 ........W.....B.F............F
05 .C......BBB.C.B..........C....
06 .F......B.B...B...............
07 .....C..B.BMW.B.BBBBBB.......C
08 ..C....BB.B...B.B.MC.b..W.....
09 ..C..B....B..C..BBBBBBBB......
10 MBBB.BBB..B............B......
11 .B.B...B.........M.....B......
12 .B.....B...............B......
13 MBBBBBBB...............C......
14 .C....M.................C.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	3	11	n	60
b	2	0	7	29	n	60
b	3	0	5	12	n	40
w	4	0	7	12	g	40
w	5	0	3	26	h	60
b	7	1	5	1	n	60
b	8	1	14	1	n	60
b	9	1	9	13	n	60
w	10	1	8	24	b	80
b	15	2	13	23	n	60
b	18	3	8	2	n	60
b	20	3	5	25	n	60
w	22	3	4	8	b	60
w	23	3	1	26	g	100
b	25	2	0	2	n	40
b	27	3	0	14	n	40
w	28	2	0	19	b	100
b	29	3	8	19	n	60
b	30	2	14	24	n	60
b	31	0	2	26	n	60
b	32	2	7	5	n	60
w	33	1	3	25	h	100
b	34	1	9	2	n	60

barricades
6
player	row	column	resistance
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 207
day 1

score	515	520	160	1215

status	0	0	0	0

commands
20
7	m	d	
15	m	l	
8	m	l	
18	m	l	
25	b	l	
4	m	l	
9	m	u	
30	m	l	
34	m	l	
32	m	l	
20	m	u	
10	m	u	
29	m	l	
22	m	l	
28	m	r	
31	m	l	
5	m	d	
3	m	r	
2	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .bC....b...b.bCb....W....b....
01 .B....BBBBB...BBBBBBBBBBB.W...
02 .B........B...B......M...C....
03 .BBBBBBBBBB...B..........W....
04 .......W...C..B.F........CW..F
05 ........BBB..CB...............
06 .C......B.B...B..............C
07 ....C...B.BW..B.BBBBBB..W.....
08 .C.....BB.B..CB.B.C..b........
09 .C...B....B.....BBBBBBBB......
10 MBBB.BBB..B............B......
11 .B.B...B.........M.....B......
12 .B.....B...............B......
13 MBBBBBBB..........M...C.......
14 C.....M................C......

citizens
23
type	id	player	row	column	weapon	life
b	1	0	4	11	n	60
b	2	0	6	29	n	60
b	3	0	5	13	n	40
w	4	0	7	11	g	40
w	5	0	4	26	h	60
b	7	1	6	1	n	60
b	8	1	14	0	n	60
b	9	1	8	13	n	60
w	10	1	7	24	b	80
b	15	2	13	22	n	60
b	18	3	8	1	n	60
b	20	3	4	25	n	60
w	22	3	4	7	b	60
w	23	3	1	26	g	100
b	25	2	0	2	n	40
b	27	3	0	14	n	40
w	28	2	0	20	b	100
b	29	3	8	18	n	60
b	30	2	14	23	n	60
b	31	0	2	25	n	60
b	32	2	7	4	n	60
w	33	1	3	25	h	100
b	34	1	9	1	n	60

barricades
7
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 208
day 1

score	520	520	160	1220

status	0	0	0	0

commands
22
15	m	l	
18	m	l	
7	m	d	
8	m	u	
9	m	d	
25	m	l	
2	m	u	
30	m	l	
32	m	l	
28	m	r	
20	m	l	
31	m	l	
29	m	r	
34	m	l	
22	m	d	
23	m	d	
5	m	d	
4	m	r	
10	m	u	
3	m	d	
33	m	d	
1	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .c.....b...b.bCb.....W...b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......M..C.W...
03 .BBBBBBBBBB...B....M..........
04 ............C.B.F.......CW...F
05 .......WBBB...B...........W..C
06 ........B.B..CB.........W.....
07 .C.C....B.B.W.B.BBBBBB........
08 C......BB.B...B.B..C.b........
09 C....B....B..C..BBBBBBBB......
10 MBBB.BBB..B............B......
11 .B.B...B.........M.....B......
12 .B....MB...............B......
13 CBBBBBBB..........M..C........
14 ......M...............C.......

citizens
23
type	id	player	row	column	weapon	life
b	1	0	4	12	n	60
b	2	0	5	29	n	60
b	3	0	6	13	n	40
w	4	0	7	12	g	40
w	5	0	5	26	h	60
b	7	1	7	1	n	60
b	8	1	13	0	n	60
b	9	1	9	13	n	60
w	10	1	6	24	b	80
b	15	2	13	21	n	60
b	18	3	8	0	n	60
b	20	3	4	24	n	60
w	22	3	5	7	b	60
w	23	3	2	26	g	100
b	25	2	0	1	n	40
b	27	3	0	14	n	40
w	28	2	0	21	b	100
b	29	3	8	19	n	60
b	30	2	14	22	n	60
b	31	0	2	24	n	60
b	32	2	7	3	n	60
w	33	1	4	25	h	100
b	34	1	9	0	n	60

barricades
7
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 209
day 1

score	520	525	160	1220

status	0	0	0	0

commands
21
7	m	d	
2	m	u	
20	m	u	
15	m	l	
25	m	l	
8	m	d	
31	m	l	
5	m	u	
29	m	r	
9	m	d	
34	m	d	
22	m	u	
4	m	d	
23	m	l	
30	m	l	
32	m	d	
3	m	d	
10	m	r	
1	m	d	
28	m	r	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 Cb.....b...b.bCb......W..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......M.C.W....
03 .BBBBBBBBBB...B....M....C.....
04 .......W......B.F.........W..C
05 ........BBB.C.B..........W....
06 ........B.B...B..........W....
07 ........B.B..CB.BBBBBB........
08 CC.C...BB.B.W.B.B...Cb........
09 .....B....B.....BBBBBBBB......
10 CBBB.BBB..B..C.........B......
11 .B.B...B.........M.....B......
12 .B....MB...............B......
13 .BBBBBBB..........M.C.........
14 C.....M..............C........

citizens
23
type	id	player	row	column	weapon	life
b	1	0	5	12	n	60
b	2	0	4	29	n	60
b	3	0	7	13	n	40
w	4	0	8	12	g	40
w	5	0	4	26	h	60
b	7	1	8	1	n	60
b	8	1	14	0	n	60
b	9	1	10	13	n	60
w	10	1	6	25	b	80
b	15	2	13	20	n	60
b	18	3	8	0	n	60
b	20	3	3	24	n	60
w	22	3	4	7	b	60
w	23	3	2	25	g	100
b	25	2	0	0	n	40
b	27	3	0	14	n	40
w	28	2	0	22	b	100
b	29	3	8	20	n	60
b	30	2	14	21	n	60
b	31	0	2	23	n	60
b	32	2	8	3	n	60
w	33	1	5	25	h	100
b	34	1	10	0	n	60

barricades
7
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 210
day 1

score	520	530	160	1220

status	0	0	0	0

commands
19
15	m	l	
31	m	l	
7	m	d	
8	m	r	
9	m	d	
25	m	d	
30	m	l	
32	m	d	
28	m	r	
5	m	l	
34	m	u	
4	m	r	
2	m	u	
20	m	l	
29	m	r	
1	m	d	
10	m	l	
22	m	r	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b....
01 CB....BBBBB...BBBBBBBBBBB.....
02 .B........B...B......MC.W.....
03 .BBBBBBBBBB...B....M...C.....C
04 ........W.....B.F........W....
05 ........BBB...B..........W....
06 ........B.B.C.B.........W.....
07 ........B.B..CB.BBBBBB........
08 C......BB.B..WB.B....c........
09 CC.C.B....B.....BBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.B...B.....C...M.....B......
12 .B....MB...............B......
13 .BBBBBBB..........MC..........
14 .C....M.............C.........

citizens
23
type	id	player	row	column	weapon	life
b	1	0	6	12	n	60
b	2	0	3	29	n	60
b	3	0	7	13	n	40
w	4	0	8	13	g	40
w	5	0	4	25	h	60
b	7	1	9	1	n	60
b	8	1	14	1	n	60
b	9	1	11	13	n	60
w	10	1	6	24	b	80
b	15	2	13	19	n	60
b	18	3	8	0	n	60
b	20	3	3	23	n	60
w	22	3	4	8	b	60
w	23	3	2	24	g	100
b	25	2	1	0	n	40
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	8	21	n	60
b	30	2	14	20	n	60
b	31	0	2	22	n	60
b	32	2	9	3	n	60
w	33	1	5	25	h	100
b	34	1	9	0	n	60

barricades
7
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
3	8	21	40

round 211
day 1

score	520	530	160	1220

status	0	0	0	0

commands
20
31	m	l	
15	m	l	
7	m	u	
5	m	l	
20	m	l	
29	m	r	
8	m	r	
22	m	r	
9	m	r	
25	b	d	
30	m	l	
32	m	r	
34	m	d	
28	m	r	
4	m	d	
3	m	l	
23	m	l	
10	m	u	
33	m	u	
2	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb....
01 CB....BBBBB...BBBBBBBBBBB.....
02 bB.F......B...B......C.W......
03 .BBBBBBBBBB...B....M..C.....C.
04 .........W....B.F.......WW....
05 ........BBB...B.........W.....
06 ........B.B.C.B...............
07 ........B.B.C.B.BBBBBB........
08 CC.....BB.B...B.B....bC.......
09 ....CB....B..W..BBBBBBBB......
10 CBBB.BBB..B............B......
11 .B.B...B......C..M.....B......
12 .B....MB...............B......
13 .BBBBBBB..........C...........
14 ..C...MM...........C....M.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	6	12	n	60
b	2	0	3	28	n	60
b	3	0	7	12	n	40
w	4	0	9	13	g	40
w	5	0	4	24	h	60
b	7	1	8	1	n	60
b	8	1	14	2	n	60
b	9	1	11	14	n	60
w	10	1	5	24	b	80
b	15	2	13	18	n	60
b	18	3	8	0	n	60
b	20	3	3	22	n	60
w	22	3	4	9	b	60
w	23	3	2	23	g	100
b	25	2	1	0	n	40
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	8	22	n	60
b	30	2	14	19	n	60
b	31	0	2	21	n	60
b	32	2	9	4	n	60
w	33	1	4	25	h	100
b	34	1	10	0	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 212
day 1

score	525	530	165	1220

status	0	0	0	0

commands
21
2	m	l	
18	m	u	
15	m	u	
31	m	d	
7	m	r	
8	m	r	
5	m	l	
4	m	r	
9	m	r	
29	m	u	
22	m	r	
23	m	l	
25	m	u	
30	m	r	
3	m	d	
34	m	u	
1	m	l	
10	m	l	
32	m	d	
28	m	l	
33	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 Cb.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.F......B...B.......W.......
03 .BBBBBBBBBB...B....M.CC..W.C..
04 ..........W...B.F......W......
05 ........BBB...B.F......W......
06 ........B.BC..B...............
07 C.......B.B...B.BBBBBBC......M
08 ..C....BB.B.C.B.B....b........
09 C....B....B...W.BBBBBBBB......
10 .BBBCBBB..B............B......
11 .B.B...B.......C.M.....B......
12 .B....MB..........C....B......
13 .BBBBBBB................G.....
14 ...C..MM............C...M.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	6	11	n	60
b	2	0	3	27	n	60
b	3	0	8	12	n	40
w	4	0	9	14	g	40
w	5	0	4	23	h	60
b	7	1	8	2	n	60
b	8	1	14	3	n	60
b	9	1	11	15	n	60
w	10	1	5	23	b	80
b	15	2	12	18	n	60
b	18	3	7	0	n	60
b	20	3	3	22	n	60
w	22	3	4	10	b	60
w	23	3	2	22	g	100
b	25	2	0	0	n	40
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	7	22	n	60
b	30	2	14	20	n	60
b	31	0	3	21	n	60
b	32	2	10	4	n	60
w	33	1	3	25	h	100
b	34	1	9	0	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 213
day 1

score	525	530	165	1220

status	0	0	0	0

commands
22
7	m	r	
15	m	u	
5	m	r	
18	m	r	
25	m	r	
30	m	r	
32	m	d	
28	m	l	
8	m	r	
31	m	l	
20	m	d	
4	m	r	
3	m	d	
9	m	r	
34	m	r	
10	m	l	
33	m	l	
2	m	r	
29	m	u	
22	m	r	
23	m	l	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .c.....b...b.bCb......W..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.F......B...B......W........
03 .BBBBBBBBBB...B....MC...W...C.
04 ...........W..B.F.....C.W.....
05 ........BBB...B.F.....W.......
06 ........B.B...B.......C.......
07 .C......B.BC..B.BBBBBB.......M
08 ...C...BB.B...B.B....b........
09 .C...B....B.C..WBBBBBBBB......
10 .BBB.BBB..B............B......
11 .B.BC..B........CMC....B......
12 .B....MB...............B......
13 .BBBBBBB................G.....
14 .M..C.MM.............C..M.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	7	11	n	60
b	2	0	3	28	n	60
b	3	0	9	12	n	40
w	4	0	9	15	g	40
w	5	0	4	24	h	60
b	7	1	8	3	n	60
b	8	1	14	4	n	60
b	9	1	11	16	n	60
w	10	1	5	22	b	80
b	15	2	11	18	n	60
b	18	3	7	1	n	60
b	20	3	4	22	n	60
w	22	3	4	11	b	60
w	23	3	2	21	g	100
b	25	2	0	1	n	40
b	27	3	0	14	n	40
w	28	2	0	22	b	100
b	29	3	6	22	n	60
b	30	2	14	21	n	60
b	31	0	3	20	n	60
b	32	2	11	4	n	60
w	33	1	3	24	h	100
b	34	1	9	1	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 214
day 1

score	525	530	165	1220

status	0	0	0	0

commands
21
15	m	l	
7	m	l	
8	m	r	
5	m	d	
31	m	l	
18	m	d	
25	m	r	
30	m	r	
34	m	r	
32	m	d	
28	m	r	
10	m	l	
33	m	l	
20	m	l	
4	m	d	
29	m	l	
22	m	d	
3	m	d	
23	m	d	
2	m	r	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .bC....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.F......B...B...............
03 .BBBBBBBBBB...B....C.W.W.....C
04 ..............B.F....C........
05 ........BBBW..B.F....W..W.....
06 ........B.B...B......C........
07 ........B.B...B.BBBBBB.......M
08 .CC....BB.BC..B.B....b........
09 ..C..B....B.....BBBBBBBB......
10 .BBB.BBB..B.C..W.......B......
11 .B.B...B........CC.....B......
12 .B..C.MB...............B......
13 .BBBBBBB................G.....
14 .M...CMM..............C.M.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	8	11	n	60
b	2	0	3	29	n	60
b	3	0	10	12	n	40
w	4	0	10	15	g	40
w	5	0	5	24	h	60
b	7	1	8	2	n	60
b	8	1	14	5	n	60
b	9	1	11	16	n	60
w	10	1	5	21	b	80
b	15	2	11	17	n	60
b	18	3	8	1	n	60
b	20	3	4	21	n	60
w	22	3	5	11	b	60
w	23	3	3	21	g	100
b	25	2	0	2	n	40
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	6	21	n	60
b	30	2	14	22	n	60
b	31	0	3	19	n	60
b	32	2	12	4	n	60
w	33	1	3	23	h	100
b	34	1	9	2	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 215
day 1

score	530	530	170	1220

status	0	0	0	0

commands
21
7	m	r	
15	m	d	
5	m	d	
18	m	d	
8	m	r	
9	m	d	
31	m	d	
4	m	r	
25	m	d	
20	m	r	
30	m	r	
29	m	r	
3	m	d	
22	m	d	
23	m	l	
2	m	d	
10	m	l	
1	m	d	
33	m	d	
32	m	r	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb......W..b....
01 .BC...BBBBB...BBBBBBBBBBB.....
02 bB.F......B...B...............
03 .BBBBBBBBBB...B.....W.........
04 ..............B.F..C..CW.....C
05 ........BBB...B.F...W.........
06 ........B.BW..B.......C.W.....
07 ........B.B...B.BBBBBB.......M
08 ...C...BB.B...B.B....b........
09 .CC..B..M.BC....BBBBBBBB......
10 .BBB.BBB..B.....W......B..M...
11 .B.B...B....C..........B......
12 .B...CMB........CC.....B......
13 .BBBBBBB................G.....
14 .M....CM...............CM.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	11	n	60
b	2	0	4	29	n	60
b	3	0	11	12	n	40
w	4	0	10	16	g	40
w	5	0	6	24	h	60
b	7	1	8	3	n	60
b	8	1	14	6	n	60
b	9	1	12	16	n	60
w	10	1	5	20	b	80
b	15	2	12	17	n	60
b	18	3	9	1	n	60
b	20	3	4	22	n	60
w	22	3	6	11	b	60
w	23	3	3	20	g	100
b	25	2	1	2	n	40
b	27	3	0	14	n	40
w	28	2	0	22	b	100
b	29	3	6	22	n	60
b	30	2	14	23	n	60
b	31	0	4	19	n	60
b	32	2	12	5	n	60
w	33	1	4	23	h	100
b	34	1	9	2	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 216
day 1

score	530	535	170	1220

status	0	0	0	0

commands
21
18	m	l	
5	m	d	
31	m	r	
20	m	d	
29	m	d	
22	m	d	
4	m	r	
15	m	d	
7	m	r	
3	m	l	
8	m	r	
9	m	u	
34	m	r	
10	m	l	
2	m	d	
25	m	d	
33	m	d	
30	m	r	
1	m	d	
32	m	r	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bBCF......B...B...............
03 .BBBBBBBBBB...B.....W.........
04 ..............B.F...C.........
05 ........BBB...B.F..W..CW.....C
06 ........B.B...B...............
07 ........B.BW..B.BBBBBBC.W....M
08 ....C..BB.B...B.B....b........
09 C..C.B..M.B.....BBBBBBBB......
10 .BBB.BBB..BC.....W.....B..M...
11 .B.B...B...C....C......B......
12 .B....CB...............B......
13 .BBBBBBB.........C......G.....
14 .M.....C....F...........C.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	11	n	60
b	2	0	5	29	n	60
b	3	0	11	11	n	40
w	4	0	10	17	g	40
w	5	0	7	24	h	60
b	7	1	8	4	n	60
b	8	1	14	7	n	60
b	9	1	11	16	n	60
w	10	1	5	19	b	80
b	15	2	13	17	n	60
b	18	3	9	0	n	60
b	20	3	5	22	n	60
w	22	3	7	11	b	60
w	23	3	3	20	g	100
b	25	2	2	2	n	40
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	7	22	n	60
b	30	2	14	24	n	60
b	31	0	4	20	n	60
b	32	2	12	6	n	60
w	33	1	5	23	h	100
b	34	1	9	3	n	60

barricades
8
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40

round 217
day 1

score	530	540	180	1220

status	0	0	0	0

commands
21
15	m	r	
7	m	r	
8	m	l	
4	m	r	
9	m	u	
25	m	r	
34	m	u	
18	b	d	
10	m	l	
30	m	r	
32	m	u	
33	m	d	
20	m	d	
28	m	r	
5	m	d	
29	m	d	
2	m	d	
22	m	d	
31	m	r	
3	m	l	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.C......B...B...............
03 .BBBBBBBBBB...B...............
04 ..............B.F...WC........
05 ........BBB...B.F.W...........
06 ........B.B...B.......CW.....C
07 ........B.B...B.BBBBBB.......M
08 ...C.C.BB.BW..B.B....bC.W.....
09 C....B..M.B.....BBBBBBBB......
10 bBBB.BBB..BC....C.W....B..M...
11 .B.B..CB..C............B......
12 .B.....B...............B......
13 .BBBBBBB..........C.....G.....
14 .M....C.....F............C....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	11	n	60
b	2	0	6	29	n	60
b	3	0	11	10	n	40
w	4	0	10	18	g	40
w	5	0	8	24	h	60
b	7	1	8	5	n	60
b	8	1	14	6	n	60
b	9	1	10	16	n	60
w	10	1	5	18	b	80
b	15	2	13	18	n	60
b	18	3	9	0	n	60
b	20	3	6	22	n	60
w	22	3	8	11	b	60
w	23	3	4	20	g	100
b	25	2	2	3	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	8	22	n	60
b	30	2	14	25	n	60
b	31	0	4	21	n	60
b	32	2	11	6	n	60
w	33	1	6	23	h	100
b	34	1	8	3	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 218
day 1

score	530	540	180	1220

status	0	0	0	0

commands
20
7	m	r	
15	m	r	
4	m	r	
8	m	l	
5	m	d	
18	m	d	
20	m	d	
2	m	d	
9	m	l	
31	m	r	
25	m	u	
34	m	u	
10	m	l	
30	m	r	
3	m	l	
32	m	l	
33	m	d	
22	m	d	
23	m	r	
28	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b....
01 .B.C..BBBBB...BBBBBBBBBBB....M
02 bB........B...B...............
03 .BBBBBBBBBB.F.B...............
04 .............MB.F....WC.......
05 ........BBB...B.FW............
06 ........B.B...B...............
07 ...C....B.B...B.BBBBBBCW.....C
08 ......CBB.B...B.B....bC.......
09 .....B..M.BW....BBBBBBBBW.....
10 cBBB.BBB..BC...C...W...B..M...
11 .B.B.C.B.C.............B......
12 .B.....B...............B......
13 .BBBBBBB...........C....G.....
14 .M...C......F.............C...

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	11	n	60
b	2	0	7	29	n	60
b	3	0	11	9	n	40
w	4	0	10	19	g	40
w	5	0	9	24	h	60
b	7	1	8	6	n	60
b	8	1	14	5	n	60
b	9	1	10	15	n	60
w	10	1	5	17	b	80
b	15	2	13	19	n	60
b	18	3	10	0	n	60
b	20	3	7	22	n	60
w	22	3	9	11	b	60
w	23	3	4	21	g	100
b	25	2	1	3	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	8	22	n	60
b	30	2	14	26	n	60
b	31	0	4	22	n	60
b	32	2	11	5	n	60
w	33	1	7	23	h	100
b	34	1	7	3	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 219
day 1

score	535	540	180	1220

status	0	0	0	0

commands
21
4	m	r	
18	m	d	
15	m	r	
25	m	u	
30	m	u	
20	m	u	
32	m	l	
7	m	d	
5	m	d	
28	m	l	
8	m	l	
31	m	r	
29	m	r	
3	m	u	
22	m	r	
2	m	u	
1	m	r	
23	m	r	
9	m	u	
34	m	u	
10	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.C...b...b.bCb......W..b....
01 .B....BBBBB...BBBBBBBBBBB....M
02 bB........B...B...............
03 .BBBBBBBBBB.F.B...............
04 .............MB.F.....WC......
05 ........BBB...B.W.............
06 ...C....B.B...B.......C......C
07 ........B.B...B.BBBBBB.W......
08 .......BB.B...B.B....b.C......
09 .....BC.M.B.W..CBBBBBBBB......
10 bBBB.BBB.CB.C.......W..BW.M...
11 CB.BC..B...............B......
12 .B.....B...............B......
13 .BBBBBBB............C..MG.C...
14 .M..C.......F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	12	n	60
b	2	0	6	29	n	60
b	3	0	10	9	n	40
w	4	0	10	20	g	40
w	5	0	10	24	h	60
b	7	1	9	6	n	60
b	8	1	14	4	n	60
b	9	1	9	15	n	60
w	10	1	5	16	b	100
b	15	2	13	20	n	60
b	18	3	11	0	n	60
b	20	3	6	22	n	60
w	22	3	9	12	b	60
w	23	3	4	22	g	100
b	25	2	0	3	n	60
b	27	3	0	14	n	40
w	28	2	0	22	b	100
b	29	3	8	23	n	60
b	30	2	13	26	n	60
b	31	0	4	23	n	60
b	32	2	11	4	n	60
w	33	1	7	23	h	100
b	34	1	6	3	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 220
day 1

score	535	540	180	1220

status	0	0	0	0

commands
22
31	m	d	
18	m	d	
5	m	r	
15	m	r	
3	m	u	
20	m	r	
7	m	r	
8	m	l	
9	m	l	
25	m	l	
4	m	r	
34	m	u	
2	m	u	
10	m	d	
29	m	r	
22	m	u	
33	m	r	
1	m	r	
30	m	u	
32	m	u	
28	m	l	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .bC....b...b.bCb.....W...b....
01 .B....BBBBB...BBBBBBBBBBB....M
02 bB........B...B.............M.
03 .BBBBBBBBBB.F.B...............
04 ............MMB.F......W......
05 ...C....BBB...B........C.....C
06 ........B.B...B.W......C......
07 ........B.B...B.BBBBBB..W.....
08 .......BB.B.W.B.B....b..C.....
09 .....B.CMCB...C.BBBBBBBB......
10 bBBBCBBB..B..C.......W.B.WM...
11 .B.B...B..........M....B.....Z
12 CB.....B...............B..C...
13 .BBBBBBB.............C.MG.....
14 .M.C........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	13	n	60
b	2	0	5	29	n	60
b	3	0	9	9	n	40
w	4	0	10	21	g	40
w	5	0	10	25	h	60
b	7	1	9	7	n	60
b	8	1	14	3	n	60
b	9	1	9	14	n	60
w	10	1	6	16	b	100
b	15	2	13	21	n	60
b	18	3	12	0	n	60
b	20	3	6	23	n	60
w	22	3	8	12	b	60
w	23	3	4	23	g	100
b	25	2	0	2	n	60
b	27	3	0	14	n	40
w	28	2	0	21	b	100
b	29	3	8	24	n	60
b	30	2	12	26	n	60
b	31	0	5	23	n	60
b	32	2	10	4	n	60
w	33	1	7	24	h	100
b	34	1	5	3	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 221
day 1

score	535	540	180	1220

status	0	0	0	0

commands
20
15	m	r	
7	m	r	
5	m	r	
18	m	d	
8	m	l	
31	m	r	
25	m	l	
30	m	u	
32	m	u	
28	m	l	
4	m	d	
20	m	r	
29	m	d	
9	m	l	
34	m	u	
2	m	u	
22	m	u	
23	m	d	
10	m	l	
33	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .c.....b...b.bCb....W....b....
01 .B....BBBBB...BBBBBBBBBBB....M
02 bB........B...B.............M.
03 .BBBBBBBBBB.F.B...............
04 ...C........MMB.F............C
05 ........BBB...B........WC.....
06 ........B.B...BW........C.....
07 ........B.B.W.B.BBBBBB...W....
08 .......BB.B...B.B....b........
09 ....CB..CCB..C..BBBBBBBBC.....
10 bBBB.BBB..B..C.........B..W...
11 .B.B...B..........M..W.B..C..Z
12 .B.....B...............B......
13 CBBBBBBB..............CMG.....
14 .MC.........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	13	n	60
b	2	0	4	29	n	60
b	3	0	9	9	n	40
w	4	0	11	21	g	40
w	5	0	10	26	h	60
b	7	1	9	8	n	60
b	8	1	14	2	n	60
b	9	1	9	13	n	60
w	10	1	6	15	b	100
b	15	2	13	22	n	60
b	18	3	13	0	n	60
b	20	3	6	24	n	60
w	22	3	7	12	b	60
w	23	3	5	23	g	100
b	25	2	0	1	n	60
b	27	3	0	14	n	40
w	28	2	0	20	b	100
b	29	3	9	24	n	60
b	30	2	11	26	n	60
b	31	0	5	24	n	60
b	32	2	9	4	n	60
w	33	1	7	25	h	100
b	34	1	4	3	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 222
day 1

score	540	545	180	1220

status	0	0	0	0

commands
22
7	m	d	
15	m	r	
5	m	r	
8	m	l	
31	m	u	
4	m	l	
25	m	l	
18	m	d	
9	m	u	
3	m	d	
2	m	u	
34	m	r	
1	m	l	
30	m	d	
10	m	d	
33	m	d	
32	m	u	
28	m	l	
20	m	r	
29	m	d	
22	m	u	
23	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 Cb.....b...b.bCb...W.....b....
01 .B....BBBBB...BBBBBBBBBBB....M
02 bB........B...B.............M.
03 .BBBBBBBBBB.F.B..............C
04 M...C.......MMB.F.......C.....
05 ........BBB...B.........W.....
06 ........B.B.W.B..........C....
07 ........B.B...BWBBBBBB........
08 ....C..BB.B..CB.B....b...W....
09 .....B....B.....BBBBBBBB......
10 bBBB.BBBCCB.C..........BC..W..
11 .B.B...B..........M.W..B.....Z
12 .B.....B...............B..C...
13 .BBBBBBB...............CG.....
14 CC..........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	12	n	60
b	2	0	3	29	n	60
b	3	0	10	9	n	40
w	4	0	11	20	g	40
w	5	0	10	27	h	60
b	7	1	10	8	n	60
b	8	1	14	1	n	60
b	9	1	8	13	n	60
w	10	1	7	15	b	100
b	15	2	13	23	n	60
b	18	3	14	0	n	60
b	20	3	6	25	n	60
w	22	3	6	12	b	60
w	23	3	5	24	g	100
b	25	2	0	0	n	60
b	27	3	0	14	n	40
w	28	2	0	19	b	100
b	29	3	10	24	n	60
b	30	2	12	26	n	60
b	31	0	4	24	n	60
b	32	2	8	4	n	60
w	33	1	8	25	h	100
b	34	1	4	4	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 223
day 1

score	540	550	185	1220

status	0	0	0	0

commands
21
5	m	d	
15	m	l	
18	m	u	
20	m	u	
4	m	l	
29	m	d	
7	m	d	
22	m	u	
8	m	r	
9	m	d	
25	m	d	
34	m	r	
30	m	r	
2	m	u	
32	m	u	
31	m	u	
10	m	d	
33	m	d	
3	m	d	
1	m	u	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb....W....b....
01 CB....BBBBB...BBBBBBBBBBB....M
02 bB........B...B.............MC
03 .BBBBBBBBBB.F.B.........C.....
04 M....C......MMB.F.............
05 ........BBB.W.B.........WC....
06 ........B.B...B...............
07 ....C...B.B...B.BBBBBB........
08 .......BB.B...BWB....b........
09 .....B....B.CC..BBBBBBBB.W....
10 bBBB.BBB..B............B......
11 .B.B...BCC........MW...BC..W.Z
12 .B.....B...............B...C..
13 CBBBBBBB..............C.G.....
14 ..C.........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	12	n	60
b	2	0	2	29	n	60
b	3	0	11	9	n	40
w	4	0	11	19	g	40
w	5	0	11	27	h	60
b	7	1	11	8	n	60
b	8	1	14	2	n	60
b	9	1	9	13	n	60
w	10	1	8	15	b	100
b	15	2	13	22	n	60
b	18	3	13	0	n	60
b	20	3	5	25	n	60
w	22	3	5	12	b	60
w	23	3	5	24	g	100
b	25	2	1	0	n	60
b	27	3	0	14	n	40
w	28	2	0	20	b	100
b	29	3	11	24	n	60
b	30	2	12	27	n	60
b	31	0	3	24	n	60
b	32	2	7	4	n	60
w	33	1	9	25	h	100
b	34	1	4	5	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 224
day 1

score	540	550	185	1220

status	0	0	0	0

commands
22
18	m	u	
5	m	r	
7	m	d	
8	m	r	
4	m	l	
9	m	u	
15	m	u	
2	m	u	
20	m	u	
25	m	d	
31	m	u	
3	m	r	
1	m	u	
29	m	d	
22	m	u	
34	m	r	
10	m	d	
33	m	d	
30	m	r	
23	m	u	
32	m	u	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.....W...b....
01 .B....BBBBB...BBBBBBBBBBB....C
02 cB........B...B.........C...M.
03 .BBBBBBBBBB.F.B...............
04 M.....C.....WMB.F.......WC....
05 ........BBB...B...............
06 ....C...B.B...B...............
07 ........B.B...B.BBBBBB........
08 .......BB.B.CCB.B....b........
09 .....B....B....WBBBBBBBB......
10 bBBB.BBB..B............B.W....
11 .B.B...B..C.......W....B....WZ
12 CB.....BC.............CBC...C.
13 .BBBBBBB................G.....
14 ...C........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	8	12	n	60
b	2	0	1	29	n	60
b	3	0	11	10	n	40
w	4	0	11	18	g	40
w	5	0	11	28	h	60
b	7	1	12	8	n	60
b	8	1	14	3	n	60
b	9	1	8	13	n	60
w	10	1	9	15	b	100
b	15	2	12	22	n	60
b	18	3	12	0	n	60
b	20	3	4	25	n	60
w	22	3	4	12	b	60
w	23	3	4	24	g	100
b	25	2	2	0	n	60
b	27	3	0	14	n	40
w	28	2	0	21	b	100
b	29	3	12	24	n	60
b	30	2	12	28	n	60
b	31	0	2	24	n	60
b	32	2	6	4	n	60
w	33	1	10	25	h	100
b	34	1	4	6	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 225
day 0

score	550	550	185	1225

status	0	0	0	0

commands
22
7	m	r	
8	m	r	
31	m	r	
15	m	d	
9	m	d	
4	m	r	
25	m	d	
5	m	r	
30	m	d	
18	m	u	
34	m	l	
10	m	d	
20	m	u	
2	m	d	
32	m	u	
33	m	d	
29	m	d	
28	m	r	
3	m	l	
1	m	u	
22	m	d	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb......W..b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B..........C..MC
03 CBBBBBBBBBB.F.B..M......WC....
04 M....C.......MB.F.............
05 ....C...BBB.W.B...............
06 ........B.B...B...............
07 ........B.B.C.B.BBBBBB........
08 .......BB.B...B.B....b........
09 .....B....B..C..BBBBBBBB......
10 bBBB.BBB..B....W.......B......
11 CB.B...B.C.........W...B.W...W
12 .B.....B.C..M..........B......
13 .BBBBBBB..............C.C...C.
14 ....C.......F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	7	12	n	60
b	2	0	2	29	n	60
b	3	0	11	9	n	40
w	4	0	11	19	g	40
w	5	0	11	29	b	60
b	7	1	12	9	n	60
b	8	1	14	4	n	60
b	9	1	9	13	n	60
w	10	1	10	15	b	100
b	15	2	13	22	n	60
b	18	3	11	0	n	60
b	20	3	3	25	n	60
w	22	3	5	12	b	60
w	23	3	3	24	g	100
b	25	2	3	0	n	60
b	27	3	0	14	n	40
w	28	2	0	22	b	100
b	29	3	13	24	n	60
b	30	2	13	28	n	60
b	31	0	2	25	n	60
b	32	2	5	4	n	60
w	33	1	11	25	h	100
b	34	1	4	5	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 226
day 0

score	550	550	185	1225

status	0	0	0	0

commands
22
7	m	r	
8	m	r	
31	m	u	
1	m	r	
15	m	d	
9	m	r	
18	m	u	
25	m	d	
20	m	d	
34	m	l	
30	m	l	
10	m	d	
32	m	l	
29	m	d	
33	m	l	
5	m	d	
4	m	r	
22	m	d	
28	m	r	
2	m	l	
23	m	u	
3	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBBC....
02 bB........B...B.........W...C.
03 .BBBBBBBBBB.F.B..M............
04 C...C........MB.F........C....
05 ...C....BBB...B.....M.........
06 ........B.B.W.B...............
07 ........B.B..CB.BBBBBB........
08 ...F...BB.B...B.B....b........
09 .....B....B...C.BBBBBBBB......
10 cBBB.BBB..B............B......
11 .B.B...B..C....W....W..BW.....
12 .B.....B..C.M..........B.....W
13 .BBBBBBB...................C..
14 .M...C......F.........C.C.....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	7	13	n	60
b	2	0	2	28	n	60
b	3	0	11	10	n	40
w	4	0	11	20	g	40
w	5	0	12	29	b	60
b	7	1	12	10	n	60
b	8	1	14	5	n	60
b	9	1	9	14	n	60
w	10	1	11	15	b	100
b	15	2	14	22	n	60
b	18	3	10	0	n	60
b	20	3	4	25	n	60
w	22	3	6	12	b	60
w	23	3	2	24	g	100
b	25	2	4	0	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	14	24	n	60
b	30	2	13	27	n	60
b	31	0	1	25	n	60
b	32	2	5	3	n	60
w	33	1	11	24	h	100
b	34	1	4	4	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 227
day 0

score	555	550	190	1225

status	0	0	0	0

commands
22
7	m	r	
8	m	l	
31	m	u	
1	m	d	
15	m	l	
25	m	d	
9	m	r	
34	m	r	
5	m	d	
18	m	d	
20	m	d	
4	m	r	
10	m	l	
29	m	r	
33	m	d	
3	m	r	
30	m	u	
22	m	d	
32	m	d	
28	m	r	
23	m	r	
2	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wc....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B..........W....
03 .BBBBBBBBBB.F.B..M..........C.
04 .....C.......MB.F.............
05 C.......BBB...B.....M....C....
06 ...C....B.B...B...............
07 ........B.B.W.B.BBBBBB........
08 ...F...BB.B..CB.B....b........
09 .....B....B....CBBBBBBBB......
10 bBBB.BBB..B............B......
11 CB.B...B...C..W......W.B......
12 .B.....B...CM..........BW..C..
13 .BBBBBBB.....................W
14 .M..C.......F........C...C....

citizens
23
type	id	player	row	column	weapon	life
b	1	0	8	13	n	60
b	2	0	3	28	n	60
b	3	0	11	11	n	40
w	4	0	11	21	g	40
w	5	0	13	29	b	60
b	7	1	12	11	n	60
b	8	1	14	4	n	60
b	9	1	9	15	n	60
w	10	1	11	14	b	100
b	15	2	14	21	n	60
b	18	3	11	0	n	60
b	20	3	5	25	n	60
w	22	3	7	12	b	60
w	23	3	2	25	g	100
b	25	2	5	0	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	14	25	n	60
b	30	2	12	27	n	60
b	31	0	0	25	n	60
b	32	2	6	3	n	60
w	33	1	12	24	h	100
b	34	1	4	5	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 228
day 0

score	555	550	190	1225

status	0	0	0	0

commands
22
18	m	d	
20	m	l	
31	m	r	
15	m	l	
1	m	d	
29	m	u	
22	m	d	
7	m	r	
5	m	u	
23	m	d	
25	m	r	
8	m	l	
30	m	u	
4	m	d	
3	m	r	
2	m	d	
9	m	u	
32	m	r	
28	m	l	
34	m	r	
10	m	l	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.bC...
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B...............
03 .BBBBBBBBBB.F.B..M.......W....
04 ......C......MB.F...........C.
05 .C......BBB...B.....M...C.....
06 ....C...B.B...B...............
07 ........BMB...B.BBBBBB........
08 ...F...BB.B.W.BCB....b....F...
09 .....B....B..C..BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.BM..B....CW....M....B...C..
12 CB.....B....C........W.B.....W
13 .BBBBBBB................WC....
14 .M.C........F.......C.........

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	13	n	60
b	2	0	4	28	n	60
b	3	0	11	12	n	40
w	4	0	12	21	g	40
w	5	0	12	29	b	60
b	7	1	12	12	n	60
b	8	1	14	3	n	60
b	9	1	8	15	n	60
w	10	1	11	13	b	100
b	15	2	14	20	n	60
b	18	3	12	0	n	60
b	20	3	5	24	n	60
w	22	3	8	12	b	60
w	23	3	3	25	g	100
b	25	2	5	1	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	13	25	n	60
b	30	2	11	27	n	60
b	31	0	0	26	n	60
b	32	2	6	4	n	60
w	33	1	13	24	h	100
b	34	1	4	6	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 229
day 0

score	555	555	190	1225

status	0	0	0	0

commands
22
18	m	d	
20	m	l	
29	m	u	
3	m	u	
1	m	r	
7	m	r	
15	m	u	
22	m	d	
23	m	u	
5	m	u	
4	m	d	
31	m	d	
2	m	d	
8	m	l	
9	m	u	
25	m	d	
34	m	r	
30	m	u	
10	m	l	
33	m	r	
32	m	d	
28	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB.C...
02 bB........B...B..........W....
03 .BBBBBBBBBB.F.B..M............
04 .......C.....MB.F.............
05 ........BBB...B.....M..C....C.
06 .C......B.B...B...............
07 ....C...BMB...BCBBBBBB........
08 ...F...BB.B...B.B....b....F...
09 .....B....B.W.C.BBBBBBBB......
10 bBBB.BBB..B.C..........B...C..
11 .B.BM..B....W.....M....B.....W
12 .B.....B.....C.........B.C....
13 CBBBBBBB............CW...W....
14 .MC.........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	14	n	60
b	2	0	5	28	n	60
b	3	0	10	12	n	40
w	4	0	13	21	g	40
w	5	0	11	29	b	60
b	7	1	12	13	n	60
b	8	1	14	2	n	60
b	9	1	7	15	n	60
w	10	1	11	12	b	100
b	15	2	13	20	n	60
b	18	3	13	0	n	60
b	20	3	5	23	n	60
w	22	3	9	12	b	60
w	23	3	2	25	g	100
b	25	2	6	1	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	12	25	n	60
b	30	2	10	27	n	60
b	31	0	1	26	n	60
b	32	2	7	4	n	60
w	33	1	13	25	h	100
b	34	1	4	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 230
day 0

score	555	555	190	1225

status	0	0	0	0

commands
22
18	m	d	
15	m	u	
7	m	u	
8	m	l	
31	m	u	
25	m	d	
9	m	u	
3	m	r	
34	m	r	
10	m	u	
33	m	u	
1	m	r	
30	m	u	
32	m	d	
28	m	l	
20	m	l	
29	m	u	
22	m	d	
23	m	u	
5	m	u	
4	m	l	
2	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.bC...
01 .B....BBBBB...BBBBBBBBBBBW....
02 bB........B...B...............
03 .BBBBBBBBBB.F.B..M............
04 ........C....MB.F.............
05 ........BBB...B.....M.C....C..
06 ........B.B...BC..............
07 .C.....MBMB...B.BBBBBB........
08 ...FC..BB.B...B.B....b....F...
09 .....B....B.W..CBBBBBBBB...C..
10 bBBB.BBB..B.WC.........B.....W
11 .B.BM..B.....C....M....B.C....
12 .B.....B............C..B......
13 .BBBBBBB.........M..W....W....
14 CC..........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	15	n	60
b	2	0	5	27	n	60
b	3	0	10	13	n	40
w	4	0	13	20	g	40
w	5	0	10	29	b	60
b	7	1	11	13	n	60
b	8	1	14	1	n	60
b	9	1	6	15	n	60
w	10	1	10	12	b	100
b	15	2	12	20	n	60
b	18	3	14	0	n	60
b	20	3	5	22	n	60
w	22	3	9	12	b	40
w	23	3	1	25	g	100
b	25	2	7	1	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	11	25	n	40
b	30	2	9	27	n	60
b	31	0	0	26	n	60
b	32	2	8	4	n	60
w	33	1	13	25	h	100
b	34	1	4	8	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 231
day 0

score	555	560	190	1225

status	0	0	0	0

commands
22
15	m	u	
25	m	r	
30	m	u	
18	m	u	
7	m	d	
20	m	l	
8	m	r	
31	m	r	
3	m	u	
9	m	u	
32	m	d	
29	m	u	
28	m	r	
22	m	u	
5	m	u	
23	m	r	
4	m	u	
34	m	l	
10	m	r	
2	m	l	
33	m	u	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb.C..
01 .B....BBBBB...BBBBBBBBBBB.W...
02 bB........B...B...............
03 .BBBBBBBBBB.F.B..M............
04 .......C.....MB.F.............
05 ........BBB...BC....MC....C...
06 ........B.B...B...............
07 ..C....MBMB...B.BBBBBB........
08 ...F...BB.B.W.B.B....b....FC..
09 ....CB...ZB..C..BBBBBBBB.....W
10 bBBB.BBB..B..W.C.......B.C....
11 .B.BM..B..........M.C..B......
12 .B.....B.....C......W..B.W....
13 CBBBBBBB.........M............
14 ..C.........F.................

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	15	n	60
b	2	0	5	26	n	60
b	3	0	9	13	n	40
w	4	0	12	20	g	40
w	5	0	9	29	b	60
b	7	1	12	13	n	60
b	8	1	14	2	n	60
b	9	1	5	15	n	60
w	10	1	10	13	b	100
b	15	2	11	20	n	60
b	18	3	13	0	n	60
b	20	3	5	21	n	60
w	22	3	8	12	b	40
w	23	3	1	26	g	100
b	25	2	7	2	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	10	25	n	40
b	30	2	8	27	n	60
b	31	0	0	27	n	60
b	32	2	9	4	n	60
w	33	1	12	25	h	100
b	34	1	4	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 232
day 0

score	555	560	190	1225

status	0	0	0	0

commands
22
7	m	d	
15	m	l	
8	m	r	
25	m	r	
9	m	u	
18	m	u	
30	m	u	
32	m	d	
34	m	d	
10	m	u	
33	m	u	
28	m	l	
31	m	r	
20	m	l	
29	m	u	
22	m	u	
23	m	u	
3	m	u	
1	m	u	
5	m	u	
4	m	u	
2	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.bW.C.
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B...............
03 .BBBBBBBBBB.F.B..M............
04 .............MBCF.............
05 .......CBBB...B.....C....C....
06 ........B.B...B...............
07 ...C...MBMB.W.B.BBBBBB.....C..
08 ...F...BB.B..CB.B....b....F..W
09 .....B...ZB....CBBBBBBBB.C....
10 bBBBCBBB..B..W.........B......
11 .B.BM..B..........MCW..B.W....
12 CB.....B...............B......
13 .BBBBBBB.....C...M............
14 ...C........F...............M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	15	n	60
b	2	0	5	25	n	60
b	3	0	8	13	n	20
w	4	0	11	20	g	40
w	5	0	8	29	b	60
b	7	1	13	13	n	60
b	8	1	14	3	n	60
b	9	1	4	15	n	60
w	10	1	10	13	b	100
b	15	2	11	19	n	60
b	18	3	12	0	n	60
b	20	3	5	20	n	60
w	22	3	7	12	b	40
w	23	3	0	26	g	100
b	25	2	7	3	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	9	25	n	40
b	30	2	7	27	n	60
b	31	0	0	28	n	60
b	32	2	10	4	n	60
w	33	1	11	25	h	100
b	34	1	5	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 233
day 0

score	555	560	190	1230

status	0	0	0	0

commands
22
7	m	r	
31	m	r	
8	m	r	
3	m	d	
5	m	u	
15	m	l	
18	m	u	
20	m	u	
29	m	u	
22	m	u	
25	m	r	
30	m	u	
32	m	d	
23	m	r	
28	m	r	
4	m	l	
2	m	u	
1	m	d	
9	m	u	
34	m	d	
10	m	u	
33	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb.W.C
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B...............
03 .BBBBBBBBBB.F.BC.M............
04 .............MB.F...C....C....
05 ........BBB...B...............
06 .......CB.B.W.B............C..
07 ....C..MBMB...B.BBBBBB.......W
08 ...F...BB.B...B.B....b...CF...
09 .....B...ZB.....BBBBBBBB......
10 bBBB.BBB..B..W.C.......B.W....
11 CB.BC..B..........CW...B......
12 .B.....B...............B......
13 .BBBBBBB......C..M............
14 ....C.......F...............M.

citizens
22
type	id	player	row	column	weapon	life
b	1	0	10	15	n	60
b	2	0	4	25	n	60
w	4	0	11	19	g	40
w	5	0	7	29	b	60
b	7	1	13	14	n	60
b	8	1	14	4	n	60
b	9	1	3	15	n	60
w	10	1	10	13	b	100
b	15	2	11	18	n	60
b	18	3	11	0	n	60
b	20	3	4	20	n	60
w	22	3	6	12	b	40
w	23	3	0	27	g	100
b	25	2	7	4	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	8	25	n	40
b	30	2	6	27	n	60
b	31	0	0	29	n	60
b	32	2	11	4	n	60
w	33	1	10	25	h	100
b	34	1	6	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 234
day 0

score	555	660	200	1230

status	0	0	0	0

commands
21
7	m	r	
15	m	d	
31	m	d	
18	m	u	
25	m	r	
8	m	r	
20	m	u	
9	m	r	
29	m	u	
30	m	u	
34	m	d	
1	m	u	
22	m	u	
5	m	u	
32	m	u	
23	m	r	
28	m	l	
10	m	r	
4	m	l	
2	m	u	
33	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b..W.
01 .B....BBBBB...BBBBBBBBBBB....C
02 bB........B...B...............
03 .BBBBBBBBBB.F.B.CM..C....C....
04 .............MB.F.............
05 ..M.....BBB.W.B............C..
06 ........B.B...B..............W
07 .....C.CBMB...B.BBBBBB...C....
08 ...F...BB.B...B.B....b....F...
09 .....B...ZB....CBBBBBBBB.W....
10 cBBBCBBB..B...W........B......
11 .B.B...B..........W....B......
12 .B.....B..........C....B......
13 .BBBBBBB.G.....C.M............
14 .....C......F...............M.

citizens
22
type	id	player	row	column	weapon	life
b	1	0	9	15	n	60
b	2	0	3	25	n	60
w	4	0	11	18	g	40
w	5	0	6	29	b	60
b	7	1	13	15	n	60
b	8	1	14	5	n	60
b	9	1	3	16	n	60
w	10	1	10	14	b	100
b	15	2	12	18	n	60
b	18	3	10	0	n	60
b	20	3	3	20	n	60
w	22	3	5	12	b	40
w	23	3	0	28	g	100
b	25	2	7	5	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	7	25	n	40
b	30	2	5	27	n	60
b	31	0	1	29	n	60
b	32	2	10	4	n	60
w	33	1	9	25	h	100
b	34	1	7	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 235
day 0

score	555	665	200	1230

status	0	0	0	0

commands
21
15	m	d	
31	m	d	
7	m	r	
25	m	u	
30	m	u	
18	m	u	
20	m	l	
32	m	u	
29	m	u	
1	m	u	
22	m	u	
28	m	r	
8	m	r	
5	m	u	
23	m	d	
9	m	r	
34	m	u	
4	m	d	
2	m	l	
10	m	d	
33	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB...W.
02 bB........B...B..............C
03 .BBBBBBBBBB.F.B..C.C....C.....
04 ............WMB.F..........C..
05 ..M.....BBB...B..............W
06 .....C.CB.B...B..........C....
07 ........BMB...B.BBBBBB........
08 ...F...BB.B...BCB....b...WF...
09 C...CB...ZB.....BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B......W........B......
12 .B.....B..........W....B......
13 .BBBBBBB.G......CMC...........
14 ......C.....F...............M.

citizens
22
type	id	player	row	column	weapon	life
b	1	0	8	15	n	60
b	2	0	3	24	n	60
w	4	0	12	18	g	40
w	5	0	5	29	b	60
b	7	1	13	16	n	60
b	8	1	14	6	n	60
b	9	1	3	17	n	60
w	10	1	11	14	b	100
b	15	2	13	18	n	60
b	18	3	9	0	n	60
b	20	3	3	19	n	60
w	22	3	4	12	b	40
w	23	3	1	28	g	100
b	25	2	6	5	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	6	25	n	40
b	30	2	4	27	n	60
b	31	0	2	29	n	60
b	32	2	9	4	n	60
w	33	1	8	25	h	100
b	34	1	6	7	n	60

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 236
day 0

score	555	670	200	1230

status	0	0	0	0

commands
21
15	m	l	
31	m	d	
25	m	u	
18	m	u	
30	m	u	
20	m	u	
5	m	u	
29	m	u	
22	m	u	
32	m	u	
4	m	d	
7	m	l	
8	m	r	
9	m	d	
2	m	r	
28	m	l	
23	m	d	
34	m	u	
1	m	d	
10	m	d	
33	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 .b.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB........B...B....C........W.
03 .BBBBBBBBBB.W.B..........C.C.C
04 .............MB.FC...........W
05 ..M..C.CBBB...B..........C....
06 ........B.B...B...............
07 ........BMB...B.BBBBBB...W....
08 C..FC..BB.B...B.B....bW...F...
09 .....B...ZB....CBBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B...............B......
12 .B.....B......W........B......
13 .BBBBBBB.G.....C.CW...........
14 .......C....F...........M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	15	n	60
b	2	0	3	25	n	60
w	4	0	13	18	g	40
w	5	0	4	29	b	60
b	7	1	13	15	n	60
b	8	1	14	7	n	60
b	9	1	4	17	n	60
w	10	1	12	14	b	100
b	15	2	13	17	n	60
b	18	3	8	0	n	60
b	20	3	2	19	n	60
w	22	3	3	12	b	60
w	23	3	2	28	g	100
b	25	2	5	5	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	5	25	n	40
b	30	2	3	27	n	60
b	31	0	3	29	n	60
b	32	2	8	4	n	60
w	33	1	7	25	h	100
b	34	1	5	7	n	60
w	35	2	8	22	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 237
day 0

score	555	670	205	1230

status	0	0	0	0

commands
20
7	m	d	
18	m	u	
20	m	d	
8	m	r	
9	m	d	
15	m	u	
29	m	l	
34	m	u	
10	m	r	
4	m	l	
2	m	d	
22	m	d	
23	m	d	
25	m	l	
30	m	l	
32	m	u	
28	m	r	
1	m	l	
33	m	u	
35	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.M......B...B...............
03 .BBBBBBBBBB...B....C......C.WC
04 .......C....WMB.F........C...W
05 ..M.C...BBB...B..C......C.....
06 ........B.B...B..........W....
07 C...C...BMB...B.BBBBBB........
08 ...F...BB.B...B.B....b.W..F...
09 .....B...ZB...C.BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B...............B......
12 .B.....B.......W.C.....B......
13 .BBBBBBB.G.......W............
14 ........C...F..C........M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	14	n	60
b	2	0	4	25	n	60
w	4	0	13	17	g	40
w	5	0	4	29	b	60
b	7	1	14	15	n	60
b	8	1	14	8	n	60
b	9	1	5	17	n	60
w	10	1	12	15	b	100
b	15	2	12	17	n	60
b	18	3	7	0	n	60
b	20	3	3	19	n	60
w	22	3	4	12	b	60
w	23	3	3	28	g	100
b	25	2	5	4	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	5	24	n	40
b	30	2	3	26	n	60
b	31	0	3	29	n	60
b	32	2	7	4	n	60
w	33	1	6	25	h	100
b	34	1	4	7	n	60
w	35	2	8	23	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 238
day 0

score	555	670	205	1230

status	0	0	0	0

commands
22
18	m	d	
20	m	d	
29	m	l	
15	m	r	
7	m	l	
8	m	u	
31	m	u	
22	m	l	
2	m	u	
9	m	l	
5	m	u	
34	m	l	
10	m	d	
4	m	u	
1	m	l	
25	m	l	
23	m	r	
33	m	u	
30	m	u	
32	m	u	
28	m	l	
35	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.....
02 bB.M......B...B...........C..C
03 .BBBBBBBBBB...B..........C..WW
04 ......C....W.MB.F..C..........
05 ..MC....BBB...B.C......C.W....
06 ....C...B.B...B...............
07 ........BMB...B.BBBBBB........
08 C..F...BB.B...B.B....b..W.F...
09 .....B...ZB..C..BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B...............B......
12 .BM....B.........WC....B......
13 .BBBBBBBCG.....W..............
14 ............F.C.........M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	13	n	60
b	2	0	3	25	n	60
w	4	0	12	17	g	40
w	5	0	3	29	b	60
b	7	1	14	14	n	60
b	8	1	13	8	n	60
b	9	1	5	16	n	60
w	10	1	13	15	b	100
b	15	2	12	18	n	60
b	18	3	8	0	n	60
b	20	3	4	19	n	60
w	22	3	4	11	b	60
w	23	3	3	28	g	80
b	25	2	5	3	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	5	23	n	40
b	30	2	2	26	n	60
b	31	0	2	29	n	60
b	32	2	6	4	n	60
w	33	1	5	25	h	100
b	34	1	4	6	n	60
w	35	2	8	24	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 239
day 0

score	555	670	205	1230

status	0	0	0	0

commands
22
31	m	u	
15	m	d	
2	m	u	
5	m	l	
4	m	r	
7	m	r	
18	m	r	
1	m	u	
25	m	l	
30	m	u	
32	m	l	
20	m	d	
29	m	u	
22	m	l	
28	m	r	
8	m	u	
9	m	l	
23	m	u	
35	m	r	
34	m	l	
10	m	u	
33	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB.C..C
02 bB.M......B...B..........C..W.
03 .BBBBBBBBBB...B..............W
04 .....C....W..MB.F......C......
05 ..C.....BBB...BC...C....W.....
06 ...C....B.B...B...............
07 ........BMB...B.BBBBBB........
08 .C.F...BB.B..CB.B....b...WF...
09 .....B...ZB.....BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B...............B...M..
12 .BM....BC......W..W....B......
13 .BBBBBBB.G........C...........
14 ............F..C........M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	8	13	n	60
b	2	0	2	25	n	60
w	4	0	12	18	g	40
w	5	0	3	29	b	60
b	7	1	14	15	n	60
b	8	1	12	8	n	60
b	9	1	5	15	n	60
w	10	1	12	15	b	100
b	15	2	13	18	n	60
b	18	3	8	1	n	60
b	20	3	5	19	n	60
w	22	3	4	10	b	60
w	23	3	2	28	g	60
b	25	2	5	2	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	4	23	n	40
b	30	2	1	26	n	60
b	31	0	1	29	n	60
b	32	2	6	3	n	60
w	33	1	5	24	h	100
b	34	1	4	5	n	60
w	35	2	8	25	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 240
day 0

score	555	670	210	1230

status	0	0	0	0

commands
22
18	m	r	
7	m	r	
15	m	d	
31	m	u	
20	m	d	
25	m	u	
5	m	u	
8	m	u	
4	m	d	
2	m	r	
30	m	u	
32	m	d	
9	m	d	
1	m	u	
28	m	l	
35	m	u	
34	m	l	
29	m	u	
10	m	r	
33	m	u	
22	m	l	
23	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb.......W.bC..C
01 .B....BBBBB...BBBBBBBBBBB...W.
02 bB.M......B...B...........C..W
03 .BBBBBBBBBB...B........C......
04 ..C.C....W...MB.F.......W.....
05 ........BBB...B...............
06 ........B.B...BC...C..........
07 ...C....BMB..CB.BBBBBB...W....
08 ..CF...BB.B...B.B....b....F...
09 .....B...ZB.....BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...BC.............MB...M..
12 .BM....B........W......B......
13 .BBBBBBB.G........W...........
14 ............F...C.C.....M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	7	13	n	60
b	2	0	2	26	n	60
w	4	0	13	18	g	40
w	5	0	2	29	b	60
b	7	1	14	16	n	60
b	8	1	11	8	n	60
b	9	1	6	15	n	60
w	10	1	12	16	b	100
b	15	2	14	18	n	60
b	18	3	8	2	n	60
b	20	3	6	19	n	60
w	22	3	4	9	b	60
w	23	3	1	28	g	60
b	25	2	4	2	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	3	23	n	40
b	30	2	0	26	n	60
b	31	0	0	29	n	60
b	32	2	7	3	n	60
w	33	1	4	24	h	100
b	34	1	4	4	n	60
w	35	2	7	25	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 241
day 0

score	555	670	210	1230

status	0	0	0	0

commands
21
5	m	u	
15	m	r	
18	m	d	
7	m	l	
8	m	u	
20	m	r	
29	m	u	
4	m	d	
22	m	l	
9	m	d	
23	m	u	
34	m	d	
2	m	r	
10	m	d	
1	m	u	
33	m	u	
25	m	l	
30	m	r	
32	m	u	
28	m	r	
35	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb........Wb.CWC
01 .B....BBBBB...BBBBBBBBBBB....W
02 bB.M......B...B........C...C..
03 .BBBBBBBBBB...B.........W.....
04 .C......W....MB.F.............
05 ....C...BBB...B...............
06 ...C....B.B..CB.....C....W....
07 ........BMB...BCBBBBBB........
08 ...F...BB.B...B.B....b....F...
09 ..C..B...ZB.....BBBBBBBB......
10 bBBB.BBBC.B............B......
11 .B.B...B..............MB...M..
12 GBM....B...............B......
13 .BBBBBBB.G......W.............
14 ............F..C..WC....M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	6	13	n	60
b	2	0	2	27	n	60
w	4	0	14	18	g	40
w	5	0	1	29	b	60
b	7	1	14	15	n	60
b	8	1	10	8	n	60
b	9	1	7	15	n	60
w	10	1	13	16	b	100
b	15	2	14	19	n	60
b	18	3	9	2	n	60
b	20	3	6	20	n	60
w	22	3	4	8	b	60
w	23	3	0	28	g	60
b	25	2	4	1	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	2	23	n	40
b	30	2	0	27	n	60
b	31	0	0	29	n	60
b	32	2	6	3	n	60
w	33	1	3	24	h	100
b	34	1	5	4	n	60
w	35	2	6	25	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 242
day 0

score	555	670	210	1230

status	0	0	0	0

commands
20
7	m	u	
15	m	r	
8	m	u	
4	m	r	
1	m	u	
18	m	l	
9	m	d	
2	m	d	
25	m	l	
30	m	l	
34	m	d	
20	m	r	
29	m	l	
32	m	l	
28	m	l	
35	m	u	
22	m	l	
10	m	d	
33	m	l	
23	m	l	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb.......W.bCW.C
01 .B....BBBBB...BBBBBBBBBBB....W
02 bB.M......B...B.......C.......
03 .BBBBBBBBBB...B........W...C..
04 C......W.....MB.F.............
05 ........BBB..CB..........W....
06 ..C.C...B.B...B......C........
07 ........BMB...B.BBBBBB........
08 ...F...BB.B...BCB....b....F...
09 .C...B..CZB.....BBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B..............MB...M..
12 GBM....B...............B......
13 .BBBBBBB.G.....C..............
14 ............F...W..WC...M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	5	13	n	60
b	2	0	3	27	n	60
w	4	0	14	19	g	40
w	5	0	1	29	b	60
b	7	1	13	15	n	60
b	8	1	9	8	n	60
b	9	1	8	15	n	60
w	10	1	14	16	b	100
b	15	2	14	20	n	60
b	18	3	9	1	n	60
b	20	3	6	21	n	60
w	22	3	4	7	b	60
w	23	3	0	27	g	60
b	25	2	4	0	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	2	22	n	40
b	30	2	0	26	n	60
b	31	0	0	29	n	60
b	32	2	6	2	n	60
w	33	1	3	23	h	100
b	34	1	6	4	n	60
w	35	2	5	25	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 243
day 0

score	555	670	210	1230

status	0	0	0	0

commands
20
18	m	l	
7	m	u	
8	m	r	
9	m	d	
20	m	r	
29	m	l	
15	m	r	
25	m	u	
34	m	d	
10	m	r	
33	m	r	
30	m	d	
22	m	d	
4	m	r	
1	m	u	
32	m	l	
2	m	d	
23	m	l	
28	m	r	
35	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb........WbW..C
01 .B....BBBBB...BBBBBBBBBBB.C..W
02 bB.M......B...B......C........
03 CBBBBBBBBBB...B.........W.....
04 .............CB.F..........C..
05 .......WBBB...B...........W...
06 .C......B.B...B.......C.......
07 ....C...BMB...B.BBBBBB........
08 ...F...BB.B...B.B....b....F...
09 C....B...CB....CBBBBBBBB......
10 bBBB.BBB..B............B......
11 .B.B...B..............MB...M..
12 GBM..M.B.......C.......B......
13 .BBBBBBB.G....................
14 ............F....W..WC..M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	4	13	n	60
b	2	0	4	27	n	60
w	4	0	14	20	g	40
w	5	0	1	29	b	60
b	7	1	12	15	n	60
b	8	1	9	9	n	60
b	9	1	9	15	n	60
w	10	1	14	17	b	100
b	15	2	14	21	n	60
b	18	3	9	0	n	60
b	20	3	6	22	n	60
w	22	3	5	7	b	60
w	23	3	0	26	g	60
b	25	2	3	0	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	2	21	n	40
b	30	2	1	26	n	60
b	31	0	0	29	n	60
b	32	2	6	1	n	60
w	33	1	3	24	h	100
b	34	1	7	4	n	60
w	35	2	5	26	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 244
day 0

score	560	670	210	1230

status	0	0	0	0

commands
22
7	m	u	
2	m	u	
8	m	u	
9	m	d	
5	m	l	
18	m	d	
34	m	d	
15	m	r	
20	m	d	
25	m	u	
10	m	r	
33	m	d	
30	m	d	
4	m	r	
29	m	d	
32	m	d	
22	m	d	
23	m	d	
31	m	d	
1	m	d	
28	m	l	
35	m	r	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB.W.WC
02 cB.M......B...B...........C...
03 .BBBBBBBBBB...B......C.....C..
04 ..............B.F.......W.....
05 ........BBB..CB............W..
06 .......WB.B...B...............
07 .C......BMB...B.BBBBBBC.......
08 ...FC..BBCB...B.B....b....F...
09 .....B....B.....BBBBBBBB......
10 cBBB.BBB..B....C.......B......
11 .B.B...B.......C......MB...M..
12 GBM..M.B...............B......
13 .BBBBBBB.GG...................
14 ............F.....W..WC.M...M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	5	13	n	60
b	2	0	3	27	n	60
w	4	0	14	21	g	40
w	5	0	1	28	b	60
b	7	1	11	15	n	60
b	8	1	8	9	n	60
b	9	1	10	15	n	60
w	10	1	14	18	b	100
b	15	2	14	22	n	60
b	18	3	10	0	n	60
b	20	3	7	22	n	60
w	22	3	6	7	b	60
w	23	3	1	26	g	60
b	25	2	2	0	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	3	21	n	40
b	30	2	2	26	n	60
b	31	0	1	29	n	60
b	32	2	7	1	n	60
w	33	1	4	24	h	100
b	34	1	8	4	n	60
w	35	2	5	27	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 245
day 0

score	560	670	210	1230

status	0	0	0	0

commands
22
2	m	u	
7	m	r	
5	m	l	
15	m	r	
25	m	u	
4	m	r	
8	m	d	
18	m	d	
20	m	d	
29	m	d	
22	m	d	
9	m	r	
34	m	d	
31	m	d	
23	m	d	
1	m	d	
10	m	r	
33	m	u	
30	m	d	
32	m	d	
28	m	r	
35	m	u	


   000000000011111111112222222222
   012345678901234567890123456789
00 Mb.....b...b.bCb........Wb....
01 CB....BBBBB...BBBBBBBBBBB.WW..
02 bB.M......B...B............C.C
03 .BBBBBBBBBB...B.........W.C...
04 ..............B.F....C.....W..
05 ........BBB...B...............
06 ........B.B..CB...............
07 .......WBMB...B.BBBBBB........
08 .C.F...BB.B...B.B....bC...F...
09 ....CB...CB.....BBBBBBBB......
10 bBBB.BBB..B.....C......B......
11 CB.B...B........C.....MB...M..
12 GBM..M.B...............B......
13 .BBBBBBB.GG...................
14 ............F......W..WCM.F.M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	6	13	n	60
b	2	0	2	27	n	60
w	4	0	14	22	g	40
w	5	0	1	27	b	60
b	7	1	11	16	n	60
b	8	1	9	9	n	60
b	9	1	10	16	n	60
w	10	1	14	19	b	100
b	15	2	14	23	n	60
b	18	3	11	0	n	60
b	20	3	8	22	n	60
w	22	3	7	7	b	60
w	23	3	1	26	g	60
b	25	2	1	0	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	4	21	n	40
b	30	2	3	26	n	40
b	31	0	2	29	n	60
b	32	2	8	1	n	60
w	33	1	3	24	h	100
b	34	1	9	4	n	60
w	35	2	4	27	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 246
day 0

score	560	670	210	1230

status	0	0	0	0

commands
22
15	m	r	
25	m	u	
18	m	d	
2	m	r	
30	m	d	
32	m	r	
28	m	l	
7	m	r	
8	m	d	
20	m	r	
29	m	d	
5	m	l	
22	m	l	
4	m	r	
31	m	d	
1	m	d	
35	m	u	
9	m	r	
34	m	d	
23	m	d	
10	m	r	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 Cb.....b...b.bCb.......W.b....
01 .B....BBBBB...BBBBBBBBBBB..W..
02 bB.M......B...B...........W.C.
03 .BBBBBBBBBB...B............W.C
04 ..............B.F.......W.C...
05 ........BBB...B......C........
06 ........B.B...B...............
07 ......W.BMB..CB.BBBBBB........
08 ..CF...BB.B...B.B....b.C..F...
09 .....B....B.....BBBBBBBB......
10 bBBBCBBB.CB......C.....B......
11 .B.B...B.........C....MB...M..
12 CBM..M.B...............B......
13 .BBBBBBB.GG...................
14 ............F.......W..WC.F.M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	7	13	n	60
b	2	0	2	28	n	60
w	4	0	14	23	g	40
w	5	0	1	27	b	60
b	7	1	11	17	n	60
b	8	1	10	9	n	60
b	9	1	10	17	n	60
w	10	1	14	20	b	100
b	15	2	14	24	n	60
b	18	3	12	0	n	60
b	20	3	8	23	n	60
w	22	3	7	6	b	60
w	23	3	2	26	g	40
b	25	2	0	0	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	5	21	n	40
b	30	2	4	26	n	40
b	31	0	3	29	n	60
b	32	2	8	2	n	60
w	33	1	4	24	h	100
b	34	1	10	4	n	60
w	35	2	3	27	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 247
day 0

score	560	670	220	1230

status	0	0	0	0

commands
22
18	m	d	
15	m	r	
7	m	r	
20	m	r	
25	m	r	
29	m	d	
22	m	d	
23	m	d	
30	m	d	
8	m	u	
9	m	r	
34	m	d	
10	m	r	
32	m	r	
33	m	d	
31	m	u	
28	m	r	
2	m	u	
35	m	r	
5	m	d	
4	m	r	
1	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .c.....b...b.bCb........Wb....
01 .B....BBBBB...BBBBBBBBBBB...C.
02 bB.M......B...B.....M......W.C
03 .BBBBBBBBBB...B...........W.W.
04 ..............B.F.............
05 ........BBB...B.........W.C...
06 ........B.B...B......C........
07 ........BMB...B.BBBBBB........
08 ...C..WBB.B..CB.B....b..C.F...
09 .....B...CB.....BBBBBBBB......
10 bBBB.BBB..B.......C....B......
11 .B.BC..B..........C...MB...M..
12 .BM..M.B...............B......
13 CBBBBBBB.GG...................
14 ............F........W..WCF.M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	8	13	n	60
b	2	0	1	28	n	60
w	4	0	14	24	g	40
w	5	0	2	27	b	60
b	7	1	11	18	n	60
b	8	1	9	9	n	60
b	9	1	10	18	n	60
w	10	1	14	21	b	100
b	15	2	14	25	n	60
b	18	3	13	0	n	60
b	20	3	8	24	n	60
w	22	3	8	6	b	60
w	23	3	3	26	g	40
b	25	2	0	1	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	6	21	n	40
b	30	2	5	26	n	40
b	31	0	2	29	n	60
b	32	2	8	3	n	60
w	33	1	5	24	h	100
b	34	1	11	4	n	60
w	35	2	3	28	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 248
day 0

score	560	670	220	1230

status	0	0	0	0

commands
22
7	m	r	
8	m	u	
15	m	r	
9	m	r	
18	m	d	
31	m	u	
25	m	r	
30	m	d	
32	m	l	
2	m	u	
28	m	l	
35	m	r	
20	m	d	
5	m	r	
4	m	r	
29	m	u	
34	m	d	
22	m	l	
10	m	r	
33	m	d	
1	m	d	
23	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 .bC....b...b.bCb.......W.b..C.
01 .B....BBBBB...BBBBBBBBBBB....C
02 bB.M......B...B.....M.......W.
03 .BBBBBBBBBB...B..............W
04 ..............B.F.........W...
05 ........BBB...B......C........
06 ........B.B...B.........W.C...
07 ........BMB...B.BBBBBB........
08 ..C..W.BBCB...B.B....b....F...
09 .....B....B..C..BBBBBBBBC.....
10 bBBB.BBB..B........C...B......
11 .B.B...B...........C..MB...M..
12 .BM.CM.B...............B......
13 .BBBBBBB.GG...................
14 C...........F.........W..WC.M.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	9	13	n	60
b	2	0	0	28	n	60
w	4	0	14	25	g	40
w	5	0	2	28	b	60
b	7	1	11	19	n	60
b	8	1	8	9	n	60
b	9	1	10	19	n	60
w	10	1	14	22	b	100
b	15	2	14	26	n	60
b	18	3	14	0	n	60
b	20	3	9	24	n	60
w	22	3	8	5	b	60
w	23	3	4	26	g	40
b	25	2	0	2	n	60
b	27	3	0	14	n	40
w	28	2	0	23	b	100
b	29	3	5	21	n	40
b	30	2	6	26	n	40
b	31	0	1	29	n	60
b	32	2	8	2	n	60
w	33	1	6	24	h	100
b	34	1	12	4	n	60
w	35	2	3	29	h	100

barricades
9
player	row	column	resistance
2	0	1	40
2	0	7	40
0	0	11	40
0	0	13	80
3	0	15	40
0	0	25	40
2	2	0	40
3	8	21	40
3	10	0	40

round 249
day 0

score	560	670	220	1230

status	0	0	0	0

commands
22
7	m	r	
15	m	r	
31	m	u	
18	m	r	
25	m	d	
5	m	r	
8	m	u	
30	m	d	
20	m	d	
4	m	r	
2	m	d	
9	m	r	
32	m	l	
29	m	u	
28	m	r	
35	m	u	
22	m	l	
1	m	d	
34	m	r	
10	m	r	
23	m	d	
33	m	d	


   000000000011111111112222222222
   012345678901234567890123456789
00 ..............C.........W....C
01 .BC...BBBBB...BBBBBBBBBBB...C.
02 .B.M......B...B.....M........W
03 .BBBBBBBBBB...B..............W
04 ..............B.F....C........
05 ........BBB...B...........W...
06 ........B.B...B...............
07 ........BCB...B.BBBBBB..W.C...
08 .C..W..BB.B...B.B.........F...
09 .....B....B.....BBBBBBBB......
10 .BBB.BBB..B..C......C..BC.....
11 .B.B...B............C.MB...M..
12 .BM..C.B...............B......
13 .BBBBBBB.GG...................
14 .C..........F..........W..WCM.

citizens
23
type	id	player	row	column	weapon	life
b	1	0	10	13	n	60
b	2	0	1	28	n	60
w	4	0	14	26	g	40
w	5	0	2	29	b	60
b	7	1	11	20	n	60
b	8	1	7	9	n	60
b	9	1	10	20	n	60
w	10	1	14	23	b	100
b	15	2	14	27	n	60
b	18	3	14	1	n	60
b	20	3	10	24	n	60
w	22	3	8	4	b	60
w	23	3	5	26	g	40
b	25	2	1	2	n	60
b	27	3	0	14	n	40
w	28	2	0	24	b	100
b	29	3	4	21	n	40
b	30	2	7	26	n	40
b	31	0	0	29	n	60
b	32	2	8	1	n	60
w	33	1	7	24	h	100
b	34	1	12	5	n	60
w	35	2	3	29	h	80

barricades
0
player	row	column	resistance

round 250
day 1

score	560	680	220	1230

status	0	0	0	0

