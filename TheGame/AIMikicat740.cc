#include <queue>
#include <set>
#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Mikicat740


struct PLAYER_NAME : public Player {

  /**
   * Factory: returns a new instance of this class.
   * Do not modify this function.
   */

  static Player* factory () {
    return new PLAYER_NAME;
  }

  /**
   * Types and attributes for your player can be defined here.
   */
  const vector<Dir> Movs = {Down, Right, Up, Left}; // Vector de direccions

  // Opposite direction
  inline Dir opposite(Dir d) {
    vector<Dir> Opps = {Up, Left, Down, Right};
    return Opps[int(d)];
  }

  // Find citizens
  Dir attack(int id) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) pendents.push({x, dir_ini+x});
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();

      Cell actual = cell(u.second);
      if (actual.id >= 0 and citizen(actual.id).player != me()) {
        WeaponType myWeapon = thisCitizen.weapon;
        WeaponType targetWeapon = citizen(actual.id).weapon;
        if (targetWeapon == NoWeapon) return u.first;
        if (myWeapon == Hammer and targetWeapon == Hammer) return u.first;
        else if (myWeapon == Gun and (targetWeapon == Hammer or targetWeapon == Gun)) return u.first;
        else if (myWeapon == Bazooka) return u.first;
        else return avoid(id);
      }
      if (actual.weapon != NoWeapon) return u.first;
      if (actual.bonus == Food and thisCitizen.life < warrior_ini_life()) return u.first;
      if (actual.bonus == Money) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if (((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return random_pos(dir_ini);
  }

  // Find weapons
  Dir bfs_weapons(int id) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) pendents.push({x, dir_ini+x});
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();

      Cell actual = cell(u.second);
      if (actual.weapon != NoWeapon) return u.first;
      if (actual.bonus == Money) return u.first;
      if (actual.bonus == Food and thisCitizen.life < warrior_ini_life()) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return random_pos(dir_ini);
  }

  // Find money
  Dir bfs_money(int id) {
    queue<pair<Dir, Pos> > pendents;
    set<Pos> visitats;
    Citizen thisCitizen = citizen(id);
    Pos dir_ini = thisCitizen.pos;
    for (Dir x:Movs) {
      if (pos_ok(dir_ini+x) and cell(dir_ini+x).type == Street) pendents.push({x, dir_ini+x});
    }
    while (not pendents.empty()) {
      pair<Dir, Pos> u = pendents.front();
      pendents.pop();
      Cell actual = cell(u.second);
      if (actual.bonus == Money) return u.first;
      if (actual.bonus == Food and thisCitizen.life < builder_ini_life()) return u.first;
      for (Dir x:Movs) {
        if (visitats.insert(u.second+x).second) {
          if (pos_ok(u.second+x)) {
            Cell _new = cell(u.second+x);
            if (_new.type == Street) {
              if ((_new.id < 0) and ((_new.b_owner < 0) or (_new.b_owner == me()))) {
                pendents.push({u.first, u.second+x});
              }
            }
          }
        }
      }
    }
    return random_pos(dir_ini);
  }

  // If there are other citizens nearby, move in the opposite direction
  inline Dir avoid(int id) {
    Citizen thisCitizen = citizen(id);
    for (Dir x:Movs) {
      if (pos_ok(thisCitizen.pos+x) and cell(thisCitizen.pos+x).id >= 0) return opposite(x);
    }
    if (thisCitizen.type == Builder) return bfs_money(id);
    else return bfs_weapons(id);
  }

  // Checks if citizen is inside a barricade
  inline bool in_barricade(int id) {
    if (cell(citizen(id).pos).b_owner == me()) return true;
    return false;
  }

  // Entra a barricada el jugador o evadeix
  inline Dir try_enter_barricade(int id) {
    Pos citizenPos = citizen(id).pos;
    for (Dir x:Movs) {
      if (pos_ok(citizenPos+x) and cell(citizenPos+x).b_owner == me() and cell(citizenPos+x).id < 0) return x;
    }
    return avoid(id); // Si no hi ha barricada disponible per entrar-hi
  }
  // Return a valid random direction
  inline Dir random_pos(Pos ini) {
    vector<int> perms = random_permutation(4);
    for (int p:perms) {
      Dir _new_d = Movs[p];
      if (pos_ok(ini+_new_d) and (cell(ini+_new_d).type == Street) and (cell(ini+_new_d).id < 0) and ((cell(ini+_new_d).b_owner < 0) or (cell(ini+_new_d).b_owner == me()))) return _new_d;
    }
    return Up; // Mai hauria d'arribar aqui
  }

  /**
   * Play method, invoked once per each round.
   */
  virtual void play () {
    vector<int> b = builders(me());
    for (int i:b) {
      if (is_day()) {
        if (round()%(num_rounds_per_day()/2) == num_rounds_per_day()/2 - 2) {
          build(i, random_pos(citizen(i).pos));
        }
        else if (round()%(num_rounds_per_day()/2) == num_rounds_per_day()/2 - 1) {
          move(i, try_enter_barricade(i));
        }
        else move(i, bfs_money(i));
      }
      else {
        if (not in_barricade(i)) {
          move(i, try_enter_barricade(i));
        }
      }
    }
    vector<int> w = warriors(me());
    for (int i:w) {
      if (is_day()) {
        move(i, bfs_weapons(i));
      }
      else {
        if (citizen(i).life >= warrior_ini_life()/2) move(i, attack(i));
        else move(i, avoid(i));
      }
    }
  }
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
