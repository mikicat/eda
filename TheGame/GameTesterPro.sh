#!/bin/bash

counter=1
wc1=0
wc2=0
wc3=0
wc4=0
l1=0
l2=0
l3=0
l4=0


while [ $counter -le $5 ]
do
ran=$RANDOM
./Game $1 $2 $3 $4 -s $ran -i default.cnf -o game$counter.res &> gameoutput$counter.txt

pl1=$( tail -6 gameoutput$counter.txt | head -1 )
pl2=$( tail -6 gameoutput$counter.txt | head -2 )
pl3=$( tail -6 gameoutput$counter.txt | head -3 )
pl4=$( tail -6 gameoutput$counter.txt | head -4 )

p1=$( echo $pl1 | cut -d " " -f 6 )
p2=$( echo $pl2 | cut -d " " -f 12 )
p3=$( echo $pl3 | cut -d " " -f 18 )
p4=$( echo $pl4 | cut -d " " -f 24 )

winnerline=$( tail -2 gameoutput$counter.txt | head -1 )
winner=$( echo $winnerline | cut -d " " -f 3 )

minval=$p1
if [ $minval -gt $p2 ] 
then 
    minval=$p2
fi
if [ $minval -gt $p3 ] 
then
    minval=$p3
fi
if [ $minval -gt $p4 ] 
then 
    minval=$p4
fi


if [ $minval == $p1 ]
then
    loser=$1
    ((l1++))
fi
if [ $minval == $p2 ]
then
    loser=$2
    ((l2++))
fi
if [ $minval == $p3 ]
then
    loser=$3
    ((l3++))
fi
if [ $minval == $p4 ]
then
    loser=$4
    ((l4++))
fi

if [ $winner == $1 ]
then
((wc1++))
elif [ $winner == $2 ]
then
((wc2++))
elif [ $winner == $3 ]
then
((wc3++))
elif [ $winner == $4 ]
then
((wc4++))
fi

echo $counter:  [seed = $ran]
echo LOSER $loser   TOTAL[$1=$l1, $2=$l2, $3=$l3, $4=$l4]
echo WINNER $winner   TOTAL[$1=$wc1, $2=$wc2, $3=$wc3, $4=$wc4]
echo " "
((counter++))

done
echo losers game results:
echo $1 lost $l1 
echo $2 lost $l2
echo $3 lost $l3
echo $4 lost $l4
echo ------------------------------------------------
echo winners game results:
echo $1 won $wc1 
echo $2 won $wc2
echo $3 won $wc3
echo $4 won $wc4
echo ------------------------------------------------
rm *.txt *.res
