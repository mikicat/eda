#include <iostream>
#include <vector>
#include <queue>
#include <limits>
using namespace std;
typedef pair<int, int> arc;
typedef vector< vector<arc> > graph;

const int INF = numeric_limits<int>::max();

int dijkstra(const graph& G, int x, int y) {
  vector<bool> visitats(G.size(), false);
  vector<int> dist(G.size(), INF);
  priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > Q;

  dist[x] = 0;
  Q.push({dist[x], x});

  while (not Q.empty()) {
    pair<int, int> p = Q.top();
    Q.pop();

    int u = p.second;
    if (not visitats[u]) {
      visitats[u] = true;
      for (arc aux : G[u]) {
        int v = aux.first;
        int w = aux.second;

        if (dist[v] > dist[u] + w) {
          dist[v] = dist[u] + w;
          Q.push({dist[v], v});
        }
      }
    }
  }
  return dist[y];
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    graph G(n); // No definir amb G(n, vector<arc>(m))
    // perque tots els arc no inicialitzats compten com a '0' i llavors altera
    // la min-priority queue
    for (int i = 0; i < m; ++i) {
      int x, y, c;
      cin >> x >> y >> c;
      G[x].push_back(make_pair(y, c));
    }
    int x, y;
    cin >> x >> y;
    int cost = dijkstra(G, x, y);
    if (cost != INF) cout << cost;
    else cout << "no path from " << x << " to " << y;
    cout << endl;
  }

}
