#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <queue>
using namespace std;

int main() {
  int n;
  cin >> n;
  vector< queue<string> > queues(n);
  string s;
  getline(cin, s);
  for (int i = 0; i < n; ++i) {
    getline(cin, s);
    istringstream ss(s);
    string x;
    while (ss >> x) queues[i].push(x);
  }
  string op;
  cout << "DEPARTS" << endl;
  cout << "-------" << endl;
  while (cin >> op) {
    if (op == "LEAVES") {
      int q_num;
      cin >> q_num;
      --q_num;
      if (q_num >= 0 and q_num < n and not queues[q_num].empty())
      {
        cout << queues[q_num].front() << endl;
        queues[q_num].pop();
      }
    }
    else {
      int q_num;
      string person;
      cin >> person >> q_num;
      --q_num;
      if (q_num >= 0 and q_num < n) {
        queues[q_num].push(person);
      }
    }
  }
  cout << endl << "FINAL CONTENTS" << endl;
  cout << "--------------" << endl;
  for (int i = 0; i < n; ++i) {
    cout << "queue " << i+1 << ":";
    while (not queues[i].empty()) {
      cout << ' ' << queues[i].front();
      queues[i].pop();
    }
    cout << endl;
  }
}
