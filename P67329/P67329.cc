#include <iostream>
#include <vector>
using namespace std;
typedef vector<char> VC;
typedef vector<bool> VB;

class DNA {
  int n;
  VC NB = {'A', 'C', 'G', 'T'};
  VC comb;

  void write() {
    for (int i = 0; i < n; ++i) cout << comb[i];
    cout << endl;
  }

  void combinations(int i ) {
    if (i == n) write();
    else {
      for (int j = 0; j < 4; ++j) {
        comb.push_back(NB[j]);
        combinations(i+1);
        comb.pop_back();
      }
    }
  }

public:
  DNA(int n) {
    this->n = n;
    combinations(0);
  }
};

int main() {
  int n;
  cin >> n;
  DNA seq(n);
}
