#include <iostream>
#include <vector>
using namespace std;

vector< vector<int> > floyd_warshall(const vector< vector<int> >& M) {
  vector < vector<int> > dist = M;
  for (int k = 0; k < M.size(); ++k) {
    for (int i = 0; i < M.size(); ++i) {
      for (int j = 0; j < M.size(); ++j) {
        if (dist[i][k] != INF and dist[k][j] != INF) {
          if (dist[i][j] > dist[i][k] + dist[k][j]) {
            dist[i][j] = dist[i][k] + dist[k][j];
          }
        }
      }
    }
  }
  return dist;
}
int main(){}
