#include <iostream>
#include <vector>
using namespace std;

class Number {
  int n;
  int m;
  vector<int> forbidden;

  bool check(int x) {
    for (int div : forbidden) {
      if (x % div == 0) return false;
    }
    return true;
  }

  void solve(int i, int num) {
    if (i == n) cout << num << endl;
    else {
      for (int k = 0; k < 10; ++k) {
        int x = 10 * num + k;
        if (check(x)) {
          solve(i+1, x);
        }
      }
    }
  }

public:
  Number(int n, int m) {
    this->n = n;
    this->m = m;
    forbidden = vector<int>(m);
    for (int i = 0; i < m; ++i) {
      cin >> forbidden[i];
    }
    solve(0, 0);
  }
};

int main() {
  int n, m;
  while (cin >> n >> m) {
    Number num(n, m);
    cout << "----------" << endl;
  }
}
