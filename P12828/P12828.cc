#include <iostream>
#include <vector>
using namespace std;

void combinations(vector<int> v, int n) {
  if (n == v.size()) {
    for (int i = 0; i < n; ++i) {
      if (i > 0) cout << ' ';
      cout << v[i];
    }
    cout << endl;
    return;
  }
  else {
    v[n] = 0;
    combinations(v, n+1);

    v[n] = 1;
    combinations(v, n+1);
  }
}

int main() {
  int x;
  cin >> x;
  vector<int> v(x);
  combinations(v, 0);
}
