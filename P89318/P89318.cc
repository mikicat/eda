#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Letters {
    int n;
    vector<bool> used;
    

    void comb(int k, string p) {
        if (k == n) cout << p << endl;
        else {
            for (int i = 0; i < n; ++i) {
            if (not used[i] and (k == 0 or 'a' + i != p[k-1]+1)) {
                used[i] = true;
                p[k] = 'a' + i;
                comb(k+1, p);
                used[i] = false;
            }
        }
        }
    }
    
public:
    Letters(int n) {
        this->n = n;
        used = vector<bool>(n, false);
        string p(n, 'a');
        comb(0, p);
    }
};

int main() {
    int n;
    cin >> n;
    Letters a(n);
}
