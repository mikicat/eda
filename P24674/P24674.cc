#include <iostream>
#include <vector>
#include <string>
using namespace std;

void permutations(vector<string>& paraules, vector<string>& v, int i, vector<bool>& visitats)
{
  if (i == v.size()) {
    cout << '(';
    for (int k = 0; k < v.size(); ++k) {
      if (k > 0) cout << ',';
      cout << v[k];
    }
    cout << ')' << endl;
  }
  else {
    for (int j = 0; j < v.size(); ++j) {
      if (not visitats[j]) {
        visitats[j] = true;
        v[i] = paraules[j];
        permutations(paraules, v, i+1, visitats);
        visitats[j] = false;
      }
    }
  }
}

int main() {
  int n;
  cin >> n;
  vector<string> paraules(n);
  for (int i = 0; i < n; ++i) cin >> paraules[i];
  vector<string> v(n);
  vector<bool> visitats(n, false);
  permutations(paraules, v, 0, visitats);
}
