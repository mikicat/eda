#include <iostream>
#include <vector>
using namespace std;

int top_i(const vector<int>& a, int l, int r) {
  int m = (l+r)/2;
  if (a[m] >= a[m-1] and a[m+1] <= a[m]) return m;
  else if (a[m] >= a[l]) return top_i(a, m+1, r);
  else return top_i(a, l, m);
}
int top(const vector<int>& a) {
  return top_i(a, 0, a.size()-1);
}
bool dic_search(const vector<int>& a, int x, int l, int r) {
  if (l > r) return false;
  int m = (l+r)/2;
  if (a[m] > x) return dic_search(a, x, l, m-1);
  else if (a[m] < x) return dic_search(a, x, m+1, r);
  return true;
}
bool dic_search_r(const vector<int>& a, int x, int l, int r) {
  if (l > r) return false;
  int m = (l+r)/2;
  if (a[m] < x) return dic_search_r(a, x, l, m-1);
  else if (a[m] > x) return dic_search_r(a, x, m+1, r);
  return true;
}
bool search(const vector<int>& a, int x) {
  int t = top(a);
  if (dic_search(a, x, 0, t)) return true;
  if (dic_search_r(a, x, t, a.size()-1)) return true;
  return false;
}

int main() {
  vector<int> a = {1, 3, 5, 9, 4, 2};
  int x = 6;
  cout << boolalpha << search(a, x) << endl;
}
