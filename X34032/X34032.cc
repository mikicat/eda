#include <iostream>
#include <vector>
#include <queue>
#include <limits>
using namespace std;
typedef vector<char> VC;
typedef vector<VC> CBoard;
typedef vector<int> VI;
typedef vector<VI> IBoard;
typedef pair<int, int> PI;
const int INF = numeric_limits<int>::max();
const vector<PI> dirs = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, 2}, {-1, 2}, {1, -2}, {-1, -2}};

int bfs (const CBoard& taulell, int x, int y) {
  IBoard dist(taulell.size(), VI(taulell[0].size(), INF));
  queue<PI> q;
  q.push({x, y});
  dist[x][y] = 0;
  while (not q.empty()) {
    PI u = q.front();
    q.pop();
    int i = u.first;
    int j = u.second;
    if (taulell[i][j] == 'p') return dist[i][j];
    for (auto& d : dirs) {
      PI v = u;
      v.first += d.first;
      v.second += d.second;
      if (v.first >= 0 and v.first < taulell.size()) {
        if (v.second >= 0 and v.second < taulell[0].size()) {
          if (taulell[v.first][v.second] != 'X') {
            if (dist[v.first][v.second] == INF) {
              dist[v.first][v.second] = 1 + dist[i][j];
              q.push(v);
            }
          }
        }
      }
    }
  }
  return -1;
}

int main() {
  int f, p;
  while (cin >> f >> p) {
    CBoard taulell(f, VC(p));
    int x, y;
    for (int i = 0; i < f; ++i) {
      for (int j = 0; j < p; ++j) {
        cin >> taulell[i][j];
      }
    }
    cin >> x >> y;
    --x, --y;
    int res = bfs(taulell, x, y);
    if (res < 0) cout << "no";
    else cout << res;
    cout << endl;
  }
}
