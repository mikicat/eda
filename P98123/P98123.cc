#include <iostream>
#include <string>
#include <set>
using namespace std;


int main() {
  int n, jewel;
  long taken = 0;
  set<long> bag;
  set<long> cave;
  string op;
  cin >> n;
  while (cin >> op >> jewel) {
    if (op == "leave") {
      if (bag.size() < n) {
        bag.insert(jewel);
        taken += jewel;
      }
      else {
        set<long>::iterator it = bag.begin();
        if (*it < jewel) {
          cave.insert(*it);
          taken -= *it;
          taken += jewel;
          bag.erase(it);
          bag.insert(jewel);
        }
        else cave.insert(jewel);
      }
    }
    else {
      set<long>::iterator it = bag.find(jewel);
      if (it != bag.end()) {
        taken -= *it;
        bag.erase(it);
        if (cave.size() > 0) {
          bag.insert(*cave.rbegin());
          taken += *cave.rbegin();
          cave.erase(*cave.rbegin());
        }
      }
      else {
        it = cave.find(jewel);
        if (it != cave.end()) {
          cave.erase(it);
        }
      }
    }
    cout << taken << endl;
  }
}
