#include <iostream>
#include <vector>
using namespace std;

vector<bool> cols, rows, diag1, diag2;

void n_queens(vector< vector<bool> >& reines, int i) {
  if (i == reines.size()) {
    for (int k = 0; k < reines.size(); ++k) {
      for (int j = 0; j < reines.size(); ++j) {
        if (not reines[k][j]) cout << 'Q';
        else cout << '.';
      }
      cout << endl;
    }
    cout << endl;
  }
  else {
    for (int j = 0; j < reines.size(); ++j) {
      if (reines[i][j] and rows[i] and cols[j] and diag1[i+j] and diag2[i-j+reines.size()]) {
        reines[i][j] = false;
        rows[i] = false;
        cols[j] = false;
        diag1[i+j] = false;
        diag2[i-j+reines.size()] = false;
        n_queens(reines, i+1);
        reines[i][j] = true;
        rows[i] = true;
        cols[j] = true;
        diag1[i+j] = true;
        diag2[i-j+reines.size()] = true;
      }
    }
  }
}

int main() {
  int n;
  cin >> n;
  vector< vector<bool> > reines(n, vector<bool>(n, true));
  cols = vector<bool>(n, true);
  rows = vector<bool>(n, true);
  diag1 = vector<bool>(2*n-1, true);
  diag2 = vector<bool>(2*n-1, true);
  n_queens(reines, 0);
}
