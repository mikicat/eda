#include <iostream>
#include <string>
#include <map>

using namespace std;

int main() {
  map<string, int> dict;
  string op;
  while (cin >> op) {
    if (op == "minimum?") {
      if (dict.size() == 0) cout << "indefinite minimum";
      else cout << "minimum: " << dict.begin()->first << ", " << dict.begin()->second << " time(s)";
      cout << endl;
    }
    else if (op == "maximum?") {
      if (dict.size() == 0) cout << "indefinite maximum";
      else cout << "maximum: " << (--dict.end())->first << ", " << (--dict.end())->second << " time(s)";
      cout << endl;
    }
    else if (op == "store") {
      string w;
      cin >> w;
      dict[w]++;
    }
    else if (op == "delete") {
      string w;
      cin >> w;
      if (dict.find(w) != dict.end()) {
        dict[w]--;
        if (dict[w] == 0) dict.erase(dict.find(w));
      }
    }
  }
}
