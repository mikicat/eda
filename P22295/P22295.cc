#include <iostream>
#include <vector>
using namespace std;
typedef vector<char> VC;
typedef vector<bool> VB;
typedef vector<VC> CBoard;
typedef vector<VB> BBoard;

struct Pos {
  int i;
  int j;
  void read() {
    cin >> i >> j;
  }
};

class TravellingTortoise {
  int n, m;
  Pos init, fi;
  CBoard B;
  BBoard visited;
  VC path;

  void read_board() {
    cin >> n >> m;
    B = CBoard(n, VC(m));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) cin >> B[i][j];
    }
  }

  void write() {
    for (int i = 0; i < path.size(); ++i) cout << path[i];
    cout << endl;
  }

  void rec(int i, int j) {
    if (i == fi.i and j == fi.j) {
      path.push_back(B[i][j]);
      write();
      path.pop_back();
    }
    else {
      if (not visited[i][j]) {
        visited[i][j] = true;
        path.push_back(B[i][j]);
        for (int k = -1; k < 2; ++k) {
          if (k != 0) {
            if (i+k >= 0 and i+k < n) {
              rec(i+k, j);
            }
            if (j+k >= 0 and j+k < m) {
              rec(i, j+k);
            }
          }
        }
        path.pop_back();
        visited[i][j] = false;
      }
    }
  }


public:
  TravellingTortoise() {
    read_board();
    init.read();
    fi.read();
    visited = BBoard(n, VB(m, false));
    rec(init.i, init.j);
  }
};

int main() {
  TravellingTortoise t;
}
