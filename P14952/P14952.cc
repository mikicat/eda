#include <iostream>
#include <vector>
#include <queue>
#include <list>
using namespace std;

list<int> topological_sort(const vector< vector<int> >& adj, vector<int>& ge) {
  priority_queue <int, vector<int>, greater<int> > Q;
  for (int i = adj.size()-1; i >= 0; --i) {
    if (ge[i] == 0) Q.push(i);
  }
  list<int> L;
  while (!Q.empty()) {
    int u = Q.top();
    Q.pop();
    L.push_back(u);
    for (int v : adj[u]) {
      if (--ge[v] == 0) Q.push(v);
    }
  }
  return L;

}

int main() {
  int n, m;
  while (cin >> n >> m){
    vector<vector<int> > adj(n);
    vector<int> ge(n, 0);
    for (int i = 0; i < m; ++i) {
      int x, y;
      cin >> x >> y;
      adj[x].push_back(y);
      ++ge[y];
    }

    list<int> res = topological_sort(adj, ge);
    for (list<int>::iterator it = res.begin(); it != res.end(); ++it) {
      if (it != res.begin()) cout << ' ';
      cout << *it;
    }
    cout << endl;
  }
}
