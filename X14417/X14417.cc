// No acaba de funcionar, ni idea de perq
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <limits>
using namespace std;
typedef vector<string> VS;
typedef vector<int> VI;
typedef vector<VI> matrix;
typedef vector<VS> KeyBoard;
typedef pair<int, int> PI;

vector<PI> dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
const int INF = numeric_limits<int>::max();

int bfs(const KeyBoard& kb, PI& ini, string word) {
  matrix dist(kb.size(), VI(kb[0].size(), INF));
  queue<PI> q;
  dist[ini.first][ini.second] = 0;
  q.push(ini);
  while (not q.empty()) {
    PI x = q.front();
    q.pop();
    if (kb[x.first][x.second] == word) {
      ini = x;
      return dist[x.first][x.second] + 1;
    }
    for (auto& d : dirs) {
      PI y = x;
      y.first += d.first;
      y.second += d.second;
      if ((y.first >= 0) and (y.first < kb.size())) {
        if ((y.second >= 0) and (y.second < kb[0].size())) {
          if (dist[y.first][y.second] == INF) {
            dist[y.first][y.second] = 1 + dist[x.first][x.second];
            if (kb[y.first][y.second] != "*") q.push(y);
          }
        }
      }
    }
  }
  return -1;
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    KeyBoard kb(n, VS(m));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        cin >> kb[i][j];
      }
    }

    int k;
    cin >> k;
    PI ini = {0, 0};
    bool impossible = false;
    int total = 0;
    for (int i = 0; not impossible and i < k; ++i) {
      string word;
      cin >> word;
      int moves = bfs(kb, ini, word);
      if (moves < 0) impossible = true;
      else total += moves;
    }
    if (not impossible) cout << total << endl;
    else cout << "impossible" << endl;
  }
}
