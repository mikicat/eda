#include <iostream>
#include <map>
using namespace std;

void print(const map<int, int>& nums, double& avg, double& t) {
  cout << "minimum: " << nums.begin()->first << ", ";
  cout << "maximum: " << (--nums.end())->first << ", ";
  cout << "average: " << avg / t << endl;
}

int main() {
  cout.setf(ios::fixed);
  cout.precision(4);
  map<int, int> nums;
  map<int, int>::iterator it;
  string op;
  double avg = 0, t = 0;
  while (cin >> op) {
    if (op == "number") {
      int n;
      cin >> n;
      it = nums.find(n);
      if (it != nums.end()) ++(it->second);
      else nums.insert(make_pair(n, 1));
      avg += n;
      ++t;
      print(nums, avg, t);
    }
    else if (op == "delete") {
      if (nums.size() > 0) {
        avg -= nums.begin()->first;
        --(nums.begin()->second);
        --t;
        if (nums.begin()->second == 0) nums.erase(nums.begin());

      }
      if (nums.size() > 0) print(nums, avg, t);
      else cout << "no elements" << endl;
    }
  }
}
