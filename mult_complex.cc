struct complex {
  int real;
  int imag;
};

complex exp(complex z, int n) {
  if (n == 0) {
    complex r;
    r.real = 1, r.imag = 0;
    return r;
  }
  if (n == 1) return z;

  complex r = exp(z, n/2);
  complex act;
  act.real = r.real*r.real - r.imag*r.imag;
  act.imag = r.real*r.imag + r.imag*r.real;

  if (n%2 != 0) {
    r = act;
    act.real = r.real*z.real - r.imag*z.imag;
    act.imag = z.real*r.imag + z.imag*r.real;
  }
  return act;
}
