#include <iostream>
#include <vector>
using namespace std;

enum colors_t {WHITE, BLACK, GRAY};
struct node {
  char type;
  colors_t color;
};
typedef vector<node> graph;

bool dfs_visit(graph& G, int i, int j, int n, int m) {
  int i_0 = i, j_0 = j;
  int pos = i*m+j;
  G[pos].color = GRAY;
  if (G[pos].type == 't') {
    G[pos].color = BLACK;
    return true;
  }
  bool ret = false;
  for (int k = 0; k < 4 and not ret; ++k) {
    if (k == 0) ++j;
    else if (k == 1) --j;
    else if (k == 2) ++i;
    else --i;
    if (i >= 0 and i < n and j >= 0 and j < m and G[i*m+j].color == WHITE) ret = dfs_visit(G, i, j, n, m);
    i = i_0, j = j_0;
  }
  G[pos].color = BLACK;
  return ret;
}

int main() {
  int m, n, x_0, y_0;
  cin >> n >> m;
  graph G(n*m);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> G[i*m+j].type;
      if (G[i*m+j].type == 'X') G[i*m+j].color = BLACK;
      else G[i*m+j].color = WHITE;
    }
  }
  cin >> x_0 >> y_0;
  --x_0;
  --y_0;
  if (dfs_visit(G, x_0, y_0, n, m)) cout << "yes";
  else cout << "no";
  cout << endl;

}
