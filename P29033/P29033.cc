#include <iostream>
#include <vector>
using namespace std;

bool is_twocol(const vector<vector<int> >& adj, vector<int>& colors, int u, int old_u) {
  if (colors[u] == 0) {
    colors[u] = colors[old_u]%2 + 1;
    for (int i : adj[u]) {
      if (not is_twocol(adj, colors, i, u)) return false;
    }
    return true;
  } else return colors[u] != colors[old_u];
}

bool DFS(const vector< vector<int> >& adj) {
  vector<int> colors(adj.size(), 0);
  for (int i = 0; i < adj.size(); ++i) {
    if (colors[i] == 0 and not is_twocol(adj, colors, i, i)) return false;
  }
  return true;
}

int main() {
  int n, m;
  while (cin >> n >> m) {
    vector< vector<int> > adj(n);
    for (int i = 0; i < m; ++i) {
      int x, y;
      cin >> x >> y;
      adj[x].push_back(y);
      adj[y].push_back(x);
    }
    if(DFS(adj)) cout << "yes";
    else cout << "no";
    cout << endl;
  }
}
