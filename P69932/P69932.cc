#include <sstream>
#include <iostream>
#include <string>
#include <map>
using namespace std;

int main() {
  string s;
  while (getline(cin, s)) {
    istringstream ss(s);
    int x;
    map<int, bool> nums;
    while (ss >> x) {
      nums[x] = (x%2 == 0);
    }
    int c = 0;
    bool iseven;
    map<int, bool>::iterator it = nums.begin();
    while (it != nums.end()) {
      if (it == nums.begin()) {
        ++c;
        iseven = it->second;
      }
      else if (iseven and not it->second) {
        ++c;
        iseven = false;
      }
      else if (not iseven and it->second) {
        ++c;
        iseven = true;
      }
      ++it;
    }
    cout << c << endl;
  }
}
