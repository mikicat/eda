#include <iostream>
#include <vector>
#include <queue>
using namespace std;
typedef vector< vector<char> > Graph;

int bfs(const Graph& G, int x, int y) {
  vector< vector<bool> > visitats(G.size(), vector<bool>(G[0].size(), false));
  queue<pair<pair<int, int>, int>> Q;
  Q.push(make_pair(make_pair(x, y), 0));
  visitats[x][y] = true;
  priority_queue<int, vector<int>, less<int> > distances;
  while (not Q.empty()) {
    pair<pair<int,int>, int> u = Q.front();
    Q.pop();
    int i = u.first.first;
    int j = u.first.second;
    int d = u.second;
    if (G[i][j] == 't') distances.push(d);

      if (i > 0 and G[i-1][j] != 'X' and not visitats[i-1][j]) {
        visitats[i-1][j] = true;
        Q.push(make_pair(make_pair(i-1,j), d+1));
      }
      if (j > 0 and G[i][j-1] != 'X' and not visitats[i][j-1]) {
        visitats[i][j-1] = true;
        Q.push(make_pair(make_pair(i,j-1), d+1));
      }
      if (i < G.size()-1 and G[i+1][j] != 'X' and not visitats[i+1][j]) {
        visitats[i+1][j] = true;
        Q.push(make_pair(make_pair(i+1,j), d+1));
      }
      if (j < G[0].size()-1 and G[i][j+1] != 'X' and not visitats[i][j+1]) {
        visitats[i][j+1] = true;
        Q.push(make_pair(make_pair(i,j+1), d+1));
      }

  }
  if (distances.empty()) return -1;
  else return distances.top();
}

int main() {
  int n, m;
  cin >> n >> m;
  Graph G(n, vector<char>(m));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) cin >> G[i][j];
  }
  int x, y;
  cin >> x >> y;
  --x, --y;
  int d = bfs(G, x, y);
  if (d < 0) cout << "no treasure can be reached";
  else cout << "maximum distance: " << d;
  cout << endl;
}
